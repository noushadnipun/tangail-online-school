<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('attgroup_id')->unsigned()->nullable();
            $table->string('field_label');
            $table->string('field_name');
            $table->string('css_class')->nullable();
            $table->string('css_id')->nullable();
            $table->string('minimum')->nullable();
            $table->string('maximum')->nullable();
            $table->string('prepend')->nullable();
            $table->string('append')->nullable();
            $table->string('field_type');
            $table->string('field_capability');
            $table->longText('instructions')->nullable();
            $table->boolean('is_required')->nullable();
            $table->longText('default_value')->nullable();
            $table->string('placeholder')->nullable();
            $table->boolean('position')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('attgroup_id')
                ->references('id')->on('attgroups')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
