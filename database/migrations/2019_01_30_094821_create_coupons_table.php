<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_code')->unique();
            $table->string('coupon_type');
            $table->string('amount_type');
            $table->integer('price');
            $table->integer('upto_amount')->nullable();
            $table->integer('purchase_min')->nullable();
            $table->integer('used_limit')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('apply_for')->nullable();
            $table->integer('apply_id')->nullable();
            $table->longText('comment')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
