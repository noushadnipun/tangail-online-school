<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('sub_title')->nullable();
            $table->string('seo_url')->nullable()->unique();
            $table->string('product_code')->nullable()->unique();
            $table->string('sku')->nullable()->unique();
            $table->integer('qty')->nullable();
            $table->string('local')->nullable();
            $table->float('local_selling_price')->nullable();
            $table->integer('local_discount')->nullable();
            $table->integer('dp_price')->nullable();
            $table->string('intl')->nullable();
            $table->float('intl_selling_price')->nullable();
            $table->integer('intl_discount')->nullable();
            $table->string('stock_status')->nullable();
            $table->string('delivery_area')->nullable();
            $table->string('delivery_charge')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('pro_warranty')->nullable();
            $table->string('barcode')->nullable();
            $table->string('upc')->nullable()->comment = "Universal Product Code";
            $table->string('ean')->nullable()->comment = "International Article Number";
            $table->string('isbn')->nullable()->comment = "International Standard Book Number";
            $table->string('mpn')->nullable()->comment = "Manufacturer Part Number";
            $table->text('short_description')->nullable();
            $table->text('purchase_note')->nullable();
            $table->longText('description');
            $table->longText('tags')->nullable();
            $table->longText('seo_keywords')->nullable();
            $table->string('offer_details')->nullable();
            $table->string('offer_start_date')->nullable();
            $table->string('offer_end_date')->nullable();
            $table->string('enable_comment')->nullable();
            $table->string('enable_review')->nullable();
            $table->string('express_delivery')->nullable();
            $table->string('new_arrival')->nullable();
            $table->string('best_selling')->nullable();
            $table->string('flash_sale')->nullable();
            $table->timestamp('flash_time')->nullable();
            $table->string('recommended')->nullable();
            $table->string('disable_buy')->nullable();
            $table->string('multiple_pricing')->nullable();
            $table->string('emi_available')->nullable();
            $table->integer('position')->nullable();

            $table->text('seo_description')->nullable();
            $table->text('seo_title')->nullable();
            $table->text('seo_h1')->nullable();
            $table->text('seo_h2')->nullable();

            $table->boolean('is_sticky')->nullable();
            $table->string('lang')->nullable()->comment = "English, Bengali or any other language";
            $table->string('parent_id')->nullable();
            $table->boolean('is_active');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
