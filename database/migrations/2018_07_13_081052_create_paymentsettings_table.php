<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('admin_cell_one')->nullable();
            $table->string('admin_cell_two')->nullable();
            $table->string('admin_cell_three')->nullable();
            $table->string('admin_cell_four')->nullable();
            $table->string('admin_cell_five')->nullable();
            $table->string('bkash_active')->nullable();
            $table->string('debitcredit_active')->nullable();
            $table->string('citybank_active')->nullable();
            $table->string('mobilebanking_active')->nullable();
            $table->string('rocket_active')->nullable();
            $table->string('cashondelivery_active')->nullable();
            $table->integer('decidable_amount')->nullable();
            $table->integer('inside_dhaka_fee')->nullable();
            $table->integer('outside_dhaka_fee')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paymentsettings');
    }
}
