<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatedproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relatedproducts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('user_id')->nullable();
            $table->string('main_pid')->nullable();
            $table->string('product_id')->nullable();
            $table->string('title')->nullable();
            $table->string('local_price')->nullable();
            $table->string('local_discount')->nullable();
            $table->string('int_price')->nullable();
            $table->integer('int_discount')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relatedproducts');
    }
}
