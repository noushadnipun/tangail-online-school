<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('order_random');
            $table->string('product_id');
            $table->string('product_name');
            $table->string('product_code');
            $table->string('qty');
            $table->string('img')->nullable();
            $table->string('local_selling_price')->nullable();
            $table->string('local_purchase_price');
            $table->string('delivery_charge')->nullable();
            $table->string('discount')->nullable();
            $table->string('secret_key');
            $table->integer('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_detail');
    }
}
