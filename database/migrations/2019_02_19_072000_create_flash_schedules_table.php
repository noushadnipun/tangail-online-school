<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_schedules', function (Blueprint $table) {
            $table->increments('fs_id');
            $table->string('fs_name')->nullable();
            $table->longText('fs_description')->nullable();
            $table->dateTime('fs_start_date')->nullable();
            $table->dateTime('fs_end_date')->nullable();
            $table->boolean('fs_is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_schedules');
    }
}
