<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('show_on');
            $table->string('label_name'); //for post = 'image', for products = 'size' or 'model' or 'color' etc.
            $table->string('field_name'); //for post = 'image', for products = 'size' or 'model' or 'color' etc.
            $table->longText('field_values')->nullable(); // pipe (|) separated values
            $table->string('field_type'); //for post = 'posts', for products = 'products'.
            $table->string('field_attributes'); // {'class' => 'form-control', 'placeholder' => 'Enter something'}
            $table->boolean('is_active')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variations');
    }
}
