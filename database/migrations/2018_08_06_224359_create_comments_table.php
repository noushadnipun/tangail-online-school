<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('item_id');
            $table->string('comment_on')->nullable()->comment('like products, posts etc');
            $table->string('commenter');
            $table->string('commenter_photo')->nullable();
            $table->string('commenter_email')->nullable();
            $table->string('commenter_phone')->nullable();
            $table->string('comment');
            $table->integer('parent_id')->nullable();
            $table->integer('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
