<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('seo_url')->unique();
            $table->integer('cat_theme')->default(1);
            $table->string('type');
            $table->integer('position')->unique();
            $table->string('cssid')->nullable();
            $table->string('cssclass')->nullable();
            $table->longText('description')->nullable();
            $table->text('term_short_description')->nullable();
            $table->integer('parent')->nullable();
            $table->integer('connected_with')->nullable();
            $table->string('page_image')->nullable();
            $table->string('home_image')->nullable();
            $table->string('term_menu_icon')->nullable();
            $table->string('term_menu_arrow')->nullable();
            $table->string('with_sub_menu')->nullable();
            $table->string('sub_menu_width')->nullable();
            $table->integer('column_count')->nullable();
            $table->boolean('is_active')->nullable();
            $table->string('banner1')->nullable();
            $table->string('banner2')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
