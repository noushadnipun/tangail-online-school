<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('order_id');
            $table->string('customer_name');
            $table->string('contact_number');
            $table->string('alternative_mobile');
            $table->string('email_address');
            $table->string('shiping_address');
            $table->string('order_time');
            $table->string('payment_method_name');
            $table->string('payment_term_status');
            $table->string('payment_parameter');
            $table->string('order_status')->default('order_placed');
            $table->longText('param');
            $table->string('secret_key');
            $table->string('delivery_date');
            $table->string('district', 50);
            $table->string('thana', 50);
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_master');
    }
}
