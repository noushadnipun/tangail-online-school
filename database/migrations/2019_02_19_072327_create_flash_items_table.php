<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlashItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flash_items', function (Blueprint $table) {
            $table->increments('fi_id');
            $table->integer('fi_shedule_id')->nullable();
            $table->integer('fi_product_id')->nullable();
            $table->float('fi_discount')->nullable();
            $table->float('fi_show_tag')->nullable();
            $table->integer('fi_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flash_items');
    }
}
