<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('employee_no')->nullable();
                $table->string('name');
                $table->string('email')->unique();
                $table->string('username')->nullable();
                $table->date('birthday')->nullable();
                $table->string('gender')->nullable();
                $table->string('marital_status')->nullable();
                $table->date('join_date')->nullable();
                $table->string('father')->nullable();
                $table->string('mother')->nullable();
                $table->string('company')->nullable();
                $table->mediumText('address')->nullable();
                $table->mediumText('address_2')->nullable();
                $table->mediumText('postcode')->nullable();
                $table->string('phone')->nullable();
                $table->string('emergency_phone')->nullable();
                $table->string('district')->nullable();
                $table->string('deliveryfee')->nullable();
                $table->string('password');
                $table->boolean('is_active')->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
