<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('com_name');
            $table->string('com_slogan')->nullable();
            $table->string('com_eshtablished')->nullable();
            $table->string('com_licensecode')->nullable();
            $table->string('com_logourl')->nullable();
            $table->string('com_headerurl')->nullable();
            $table->string('com_phone')->nullable();
            $table->string('order_phone')->nullable();
            $table->string('com_email')->nullable();
            $table->string('com_address')->nullable();
            $table->string('com_addressgooglemap')->nullable();
            $table->string('com_website')->nullable();
            $table->string('com_analytics')->nullable();
            $table->string('com_chat_box')->nullable();
            $table->string('com_metatitle')->nullable();
            $table->string('com_metadescription')->nullable();
            $table->string('com_metakeywords')->nullable();
            $table->string('com_workinghours')->nullable();
            $table->string('com_adminname')->nullable();
            $table->string('com_adminphone')->nullable();
            $table->string('com_adminemail')->nullable();
            $table->string('com_adminphotourl')->nullable();
            $table->string('com_facebookpageid')->nullable();
            $table->string('com_favicon')->nullable();
            $table->string('com_timezone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
