<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{

    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'Administrator';
        $role_admin->is_active = 1;
        $role_admin->save();

        $role_manager = new Role();
        $role_manager->name = 'manager';
        $role_manager->description = 'A Manager';
        $role_manager->is_active = 1;
        $role_manager->save();

        $role_editor = new Role();
        $role_editor->name = 'editor';
        $role_editor->description = 'An Editor';
        $role_editor->is_active = 1;
        $role_editor->save();

        $role_product_manager = new Role();
        $role_product_manager->name = 'product_manager';
        $role_product_manager->description = 'A Product Manager';
        $role_product_manager->is_active = 1;
        $role_product_manager->save();

        $role_inventory_manager = new Role();
        $role_inventory_manager->name = 'inventory_manager';
        $role_inventory_manager->description = 'An Inventory Manager';
        $role_inventory_manager->is_active = 1;
        $role_inventory_manager->save();

        $role_accountant = new Role();
        $role_accountant->name = 'accountant';
        $role_accountant->description = 'An Accounts';
        $role_accountant->is_active = 1;
        $role_accountant->save();

        $role_order_viewer = new Role();
        $role_order_viewer->name = 'order_viewer';
        $role_order_viewer->description = 'An Order Viewer';
        $role_order_viewer->is_active = 1;
        $role_order_viewer->save();

        $role_member = new Role();
        $role_member->name = 'member';
        $role_member->description = 'A Member';
        $role_member->is_active = 1;
        $role_member->save();

    }

}
