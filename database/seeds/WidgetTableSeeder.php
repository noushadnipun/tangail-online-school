<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('widgets')->insert([
            'name' => 'Widget 1',
            'type' => 'text',
            'position' => 1,
            'cssclass' => 'widget1',
            'description' => '<div class="col-md-3 col-sm-3 col-xs-12 text-center"><div class="footer-logo"><a href="http://103.218.25.246:8080/tnews/"><img src="http://103.218.25.246:8080/tnews/frontassets/img/logo.png" alt="" class="img-responsive"></a></div></div>
        <div class="col-md-5 col-sm-5 col-xs-12 text-center"><p class="copyright2">দৈনিক সংবাদ বাংলাদেশ সত্য, সুন্দর ও সর্বদা ভালোর পক্ষে। অপ্রয়োজনীয় যৌন সুড়সুড়িমূলক সংবাদের বিপক্ষে। পাঠকের পড়তে পারার আরাম মাথায় রেখে সত্য ও সুন্দর সংবাদ পরিবেশন করাই আমাদের লক্ষ্য। আমাদের লেখা পড়ুন ও সর্বদা ভালো থাকুন। এই সাইটের যাবতীয় মতামত, লেখার বিষয়বস্তু কিংবা মন্তব্য লেখকের একান্তই নিজস্ব। দৈনিক সংবাদ বাংলাদেশের সম্পাদকীয় নীতির সঙ্গে এর মিল আছে, ভাবা অযৌক্তিক। লেখকের মতামত, বক্তব্যের বিষয়বস্তু বা এর যথার্থতা নিয়ে দৈনিক সংবাদ বাংলাদেশ আইনগত বা অন্য কোনো ধরনের কোনো প্রকার দায় বহন করে না।</p></div>
        <div class="col-md-4 col-sm-4 col-xs-12 text-center">
            <p>© সর্বস্বত্ব <b>দৈনিক সংবাদ বাংলাদেশ</b> ২০১৭</p>
            <p>সম্পাদক ও প্রকাশকঃ মোঃ খালেকুজ্জামান খান </p>
            <p>সহ সম্পাদকঃ অরণ্য ইমতিয়াজ </p>
            <p class="copyright">২/১ এ, জি  ব্লক, লালমাটিয়া, মোহাম্মদপুর, ঢাকা ১২০৭ </p>
            <p> ফোনঃ ০১৮২১৬৬০০৬৬</p>
            <p> ইমেইলঃ info@dsbangla.com</p>
        </div>',
            'special' => '1',
            'cssid' => 'widget_1'
        ]);

        DB::table('widgets')->insert([
            'name' => 'Widget 2',
            'type' => 'adsense',
            'position' => 2,
            'cssclass' => 'widget2',
            'description' => '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- bartabazar01 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-4686000664570594"
     data-ad-slot="1370923409"data-language="en"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>',
            'special' => '1',
            'cssid' => 'widget_2'
        ]);

        DB::table('widgets')->insert([
            'name' => 'Widget 3',
            'type' => 'adsense',
            'position' => 3,
            'cssclass' => 'widget3',
            'description' => '<?php echo "Samrat Khan"; ?>',
            'special' => '1',
            'cssid' => 'widget_3'
        ]);
    }
}