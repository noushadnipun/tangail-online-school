<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new Post();
        $post->title = 'This is a post';
        $post->user_id = 1;
        $post->sub_title = 'this is a post sub title';
        $post->seo_url = 'this-is-a-post-sub-title';
        $post->description = 'none for now';
        $post->is_sticky = 1;
        $post->lang = 'en';
        $post->is_active = 1;
        $post->save();

        $post = new Post();
        $post->title = 'This is another post';
        $post->user_id = 2;
        $post->sub_title = 'this is another post sub title';
        $post->seo_url = 'this-is-another-post-sub-title';
        $post->description = 'again none for now';
        $post->is_sticky = 0;
        $post->lang = 'en';
        $post->is_active = 1;
        $post->save();
    }
}
