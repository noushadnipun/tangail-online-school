<?php

use Illuminate\Database\Seeder;
use App\Role_user;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role_user();
        $role_admin->role_id = 1;
        $role_admin->user_id = 1;
        $role_admin->save();

        $role_manager = new Role_user();
        $role_manager->role_id = 2;
        $role_manager->user_id = 2;
        $role_manager->save();


        $role_editor = new Role_user();
        $role_editor->role_id = 3;
        $role_editor->user_id = 3;
        $role_editor->save();

        $role_product_manager = new Role_user();
        $role_product_manager->role_id = 4;
        $role_product_manager->user_id = 4;
        $role_product_manager->save();

        $role_inventory_manager = new Role_user();
        $role_inventory_manager->role_id = 5;
        $role_inventory_manager->user_id = 5;
        $role_inventory_manager->save();

        $role_accountant = new Role_user();
        $role_accountant->role_id = 6;
        $role_accountant->user_id = 6;
        $role_accountant->save();

        $role_order_viewer = new Role_user();
        $role_order_viewer->role_id = 7;
        $role_order_viewer->user_id = 7;
        $role_order_viewer->save();

        $role_member = new Role_user();
        $role_member->role_id = 8;
        $role_member->user_id = 8;
        $role_member->save();
    }
}
