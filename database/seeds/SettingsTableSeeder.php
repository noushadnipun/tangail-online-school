<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->com_name = 'Tritiyo Limited';
        $setting->com_slogan = 'valid reason, dynamic solution';
        $setting->com_eshtablished = '2011';
        $setting->com_licensecode = 'accountant';
        $setting->com_logourl = NULL;
        $setting->com_headerurl = 'info@tritiyo.com';
        $setting->com_phone = '01821660066';
        $setting->com_email = 'info@tritiyo.com';
        $setting->com_address = '119/Shershah Suri Road, Townhall, Mohammadpur, Dhaka - 1207';
        $setting->com_addressgooglemap = NULL;
        $setting->com_website = 'http://www.tritiyo.com';
        $setting->com_analytics = NULL;
        $setting->com_metatitle = 'Tritiyo Limited | valid reason, dynamic solution';
        $setting->com_metadescription = 'Tritiyo Limited | valid reason, dynamic solution';
        $setting->com_metakeywords = 'tritiyo limited, tritiyo, limited, valid reason, dynamic solution';
        $setting->com_workinghours = '9-5';
        $setting->com_adminname = 'Md. Khalakuzzaman Khan';
        $setting->com_adminphone = '01680139540';
        $setting->com_adminemail = 'info@tritiyo.com';
        $setting->com_adminphotourl = NULL;
        $setting->com_facebookpageid = NULL;
        $setting->com_favicon = NULL;
        $setting->com_timezone = NULL;
        $setting->save();
    }
}