<?php

use App\Term;
use Illuminate\Database\Seeder;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $terms = array(
            array(
                "id" => 1,
                "name" => "Opening System",
                "type" => "others",
                "position" => 1,
                "cssid" => "opening_system",
                "cssclass" => "opening_system",
                "description" => "<p>Opening System</p>",
                "parent" => NULL,
                "is_active" => 1,
                "created_at" => "2018-02-01 21:12:38",
                "updated_at" => "2018-02-01 21:12:38",
            ),
            array(
                "id" => 2,
                "name" => "Left Handed",
                "type" => "others",
                "position" => 2,
                "cssid" => "left_handed",
                "cssclass" => "left_handed",
                "description" => "<p>Left Handed</p>",
                "parent" => 1,
                "is_active" => 1,
                "created_at" => "2018-02-01 21:13:10",
                "updated_at" => "2018-02-01 21:13:10",
            ),
            array(
                "id" => 3,
                "name" => "Right Handed",
                "type" => "others",
                "position" => 3,
                "cssid" => "right_handed",
                "cssclass" => "right_handed",
                "description" => "<p>Right Handed</p>",
                "parent" => 1,
                "is_active" => 1,
                "created_at" => "2018-02-01 21:13:33",
                "updated_at" => "2018-02-01 21:13:33",
            ),
            array(
                "id" => 4,
                "name" => "Slide",
                "type" => "others",
                "position" => 4,
                "cssid" => "slide",
                "cssclass" => "slide",
                "description" => "Slide",
                "parent" => 1,
                "is_active" => 1,
                "created_at" => "2018-02-01 21:16:55",
                "updated_at" => "2018-02-01 21:16:55",
            ),
            array(
                "id" => 5,
                "name" => "Inner Open",
                "type" => "others",
                "position" => 5,
                "cssid" => "inner_open",
                "cssclass" => "inner_open",
                "description" => "Inner Open",
                "parent" => 1,
                "is_active" => 1,
                "created_at" => "2018-02-01 21:17:22",
                "updated_at" => "2018-02-01 21:17:22",
            ),
            array(
                "id" => 6,
                "name" => "Outer Open",
                "type" => "others",
                "position" => 6,
                "cssid" => "outer_open",
                "cssclass" => "outer_open",
                "description" => "Outer Open",
                "parent" => 1,
                "is_active" => 1,
                "created_at" => "2018-02-01 21:17:49",
                "updated_at" => "2018-02-01 21:17:49",
            ),
        );

        DB::table('terms')->insert($terms);

//
//        $term = new Term();
//        $term->name = 'Gender';
//        $term->type = 'others';
//        $term->position = 1;
//        $term->cssid = 'gender';
//        $term->cssclass = 'gender';
//        $term->description = 'gender';
//        $term->parent = NULL;
//        $term->is_active = 1;
//        $term->save();
//
//
//        $term = new Term();
//        $term->name = 'Male';
//        $term->type = 'others';
//        $term->position = 2;
//        $term->cssid = 'male';
//        $term->cssclass = 'male';
//        $term->description = 'male';
//        $term->parent = 1;
//        $term->is_active = 1;
//        $term->save();
//
//        $term = new Term();
//        $term->name = 'Female';
//        $term->type = 'others';
//        $term->position = 3;
//        $term->cssid = 'female';
//        $term->cssclass = 'female';
//        $term->description = 'female';
//        $term->parent = 1;
//        $term->is_active = 1;
//        $term->save();
//
//
//        $term = new Term();
//        $term->name = 'Opening System';
//        $term->type = 'others';
//        $term->position = 3;
//        $term->cssid = 'female';
//        $term->cssclass = 'female';
//        $term->description = 'female';
//        $term->parent = 1;
//        $term->is_active = 1;
//        $term->save();
    }
}
