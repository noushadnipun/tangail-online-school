<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        $role_admin = Role::where('name', 'admin')->first();
        $role_manager = Role::where('name', 'manager')->first();
        $role_editor = Role::where('name', 'editor')->first();
        $role_product_manager = Role::where('name', 'product_manager')->first();
        $role_inventory_manager = Role::where('name', 'inventory_manager')->first();
        $role_accountant = Role::where('name', 'accountant')->first();
        $role_order_viewer = Role::where('name', 'order_viewer')->first();
        $role_member = Role::where('name', 'member')->first();

        $admin = new User();
        $admin->name = 'Samrat';
        $admin->email = 'info@tritiyo.com';
        $admin->password = bcrypt('samraj77');
        $admin->is_active = 1;
        $admin->save();
        $admin->roles()->attach($role_admin);


        $manager = new User();
        $manager->name = 'Musa';
        $manager->email = 'musa@tritiyo.com';
        $manager->password = bcrypt('samraj77');
        $manager->is_active = 1;
        $manager->save();
        $manager->roles()->attach($role_manager);

        $editor = new User();
        $editor->name = 'Rajoni';
        $editor->email = 'sam77raj@gmail.com';
        $editor->password = bcrypt('samraj77');
        $editor->is_active = 1;
        $editor->save();
        $editor->roles()->attach($role_editor);


        $product_manager = new User();
        $product_manager->name = 'Rayat';
        $product_manager->email = 'rayat@tritiyo.com';
        $product_manager->password = bcrypt('samraj77');
        $product_manager->is_active = 1;
        $product_manager->save();
        $product_manager->roles()->attach($role_product_manager);


        $inventory_manager = new User();
        $inventory_manager->name = 'Shuvo';
        $inventory_manager->email = 'shuvo@tritiyo.com';
        $inventory_manager->password = bcrypt('samraj77');
        $inventory_manager->is_active = 1;
        $inventory_manager->save();
        $inventory_manager->roles()->attach($role_inventory_manager);


        $accountant = new User();
        $accountant->name = 'Shuvo';
        $accountant->email = 'accountant@tritiyo.com';
        $accountant->password = bcrypt('samraj77');
        $accountant->is_active = 1;
        $accountant->save();
        $accountant->roles()->attach($role_accountant);


        $order_viewer = new User();
        $order_viewer->name = 'Shuvo';
        $order_viewer->email = 'ov@tritiyo.com';
        $order_viewer->password = bcrypt('samraj77');
        $order_viewer->is_active = 1;
        $order_viewer->save();
        $order_viewer->roles()->attach($role_order_viewer);

        $member = new User();
        $member->name = 'Ripon';
        $member->email = 'ripon@tritiyo.com';
        $member->password = bcrypt('samraj77');
        $member->is_active = 1;
        $member->save();
        $member->roles()->attach($role_member);


    }

}
