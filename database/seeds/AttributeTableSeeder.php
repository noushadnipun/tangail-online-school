<?php

use App\Attribute;
use Illuminate\Database\Seeder;

class AttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attr = new Attribute();
        $attr->user_id = 1;
        $attr->post_id = 1;
        $attr->attribute = 'image';
        $attr->values = 5;
        $attr->module_type = 'posts';
        $attr->save();
    }
}
