-- phpMyAdmin SQL Dump
-- version 4.9.5deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 06, 2020 at 06:17 PM
-- Server version: 8.0.20-0ubuntu0.19.10.1
-- PHP Version: 7.3.11-0ubuntu0.19.10.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tritiyo_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int UNSIGNED NOT NULL,
  `serial` int NOT NULL,
  `media_id` int NOT NULL,
  `category_id` int DEFAULT NULL,
  `caption` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `active` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `homesettings`
--

CREATE TABLE `homesettings` (
  `id` int UNSIGNED NOT NULL,
  `cat_first` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_second` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_third` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_fourth` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_fifth` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_sixth` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_seventh` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_eighth` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_category` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homesettings`
--

INSERT INTO `homesettings` (`id`, `cat_first`, `cat_second`, `cat_third`, `cat_fourth`, `cat_fifth`, `cat_sixth`, `cat_seventh`, `cat_eighth`, `home_category`, `created_at`, `updated_at`) VALUES
(1, '8|left|8', '24left|8', '25|right|8', '20|right|8', '599|left|8', NULL, NULL, NULL, '404|203|26|24|489|8|16|397', '2019-01-01 18:00:56', '2020-01-19 01:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int UNSIGNED NOT NULL,
  `original_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `filename` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_extension` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_size_directory` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_size_directory` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Main Menu', NULL, '2018-11-16 14:33:39', '2018-11-16 14:33:39'),
(3, 'Information', NULL, '2018-12-16 04:41:18', '2020-01-10 21:22:54'),
(5, 'Policy', NULL, '2018-12-16 05:16:21', '2020-01-10 22:45:38'),
(7, 'How to buy', NULL, '2018-12-16 05:28:44', '2018-12-16 05:28:44'),
(8, 'Announcement', NULL, '2020-01-01 00:40:12', '2020-01-01 00:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int UNSIGNED NOT NULL,
  `label` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int UNSIGNED NOT NULL DEFAULT '0',
  `sort` int NOT NULL DEFAULT '0',
  `class` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` int UNSIGNED NOT NULL,
  `depth` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `label`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`) VALUES
(4, 'Home', '/', 0, 0, NULL, 1, 0, '2018-12-15 12:40:08', '2020-01-01 00:06:13'),
(5, 'About Us', '/page/1/about_us', 0, 1, NULL, 1, 0, '2018-12-15 12:40:50', '2020-04-06 05:31:03'),
(7, 'Contact Us', 'https://rflbestbuy.com/pages/contact', 0, 2, NULL, 1, 0, '2018-12-15 12:41:42', '2020-01-01 00:08:09'),
(10, 'About us', '/page/1/about_us', 0, 0, NULL, 3, 0, '2018-12-16 04:43:58', '2019-12-01 00:41:28'),
(33, 'How to Buy', '/page/10/how-to-buy', 0, 0, NULL, 7, 0, '2018-12-16 05:28:59', '2019-10-27 03:16:28'),
(36, 'Contact Us', 'https://rflbestbuy.com/pages/contact', 0, 2, NULL, 3, 0, '2019-01-30 09:06:57', '2020-01-01 00:13:45'),
(37, 'Careers', '/career', 0, 3, NULL, 3, 0, '2019-01-30 09:07:36', '2020-01-01 00:13:45'),
(45, 'Privacy Policy', '/page/7/privacy_policy', 0, 0, NULL, 5, 0, '2019-01-30 09:31:54', '2020-01-01 00:14:26'),
(46, 'Sitemap', '/sitemap', 0, 1, NULL, 5, 0, '2019-01-30 09:32:22', '2020-01-01 00:14:26'),
(48, 'Track Order', '/track-order', 0, 1, NULL, 7, 0, '2019-03-10 16:51:36', '2020-01-01 00:19:16'),
(51, 'New Arrival', '/pages/newarrivals', 0, 4, NULL, 3, 0, '2019-03-26 20:56:32', '2020-01-01 00:13:45'),
(56, 'Affiliates', 'https://rflbestbuy.com/pages/affiliates', 0, 1, NULL, 3, 0, '2019-10-28 05:14:23', '2020-01-01 00:13:45'),
(58, 'News & Event', 'https://rflbestbuy.com/news', 0, 0, NULL, 8, 0, '2020-01-01 00:41:02', '2020-01-14 06:09:04'),
(62, 'OFFER', 'https://rflbestbuy.com/offer', 0, 3, NULL, 1, 0, '2020-01-12 06:44:42', '2020-01-12 07:02:20'),
(63, 'Album', 'https://rflbestbuy.com/gallery', 0, 1, NULL, 8, 0, '2020-01-14 06:09:04', '2020-01-16 07:16:35'),
(64, 'Advertisement', 'https://rflbestbuy.com/ads/press', 0, 2, NULL, 8, 0, '2020-01-22 06:45:06', '2020-01-22 06:45:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(10, '2017_11_17_072545_create_roles_table', 1),
(11, '2017_11_17_072610_create_role_user_table', 1),
(12, '2017_12_02_084150_create_todos_table', 1),
(13, '2017_12_03_200119_create_images_table', 1),
(14, '2017_12_26_144334_create_widgets_table', 1),
(16, '2018_01_13_081052_create_settings_table', 1),
(20, '2018_01_30_003055_create_variations_table', 1),
(22, '2018_03_11_085005_create_pages_table', 1),
(25, '2018_04_01_093828_create_dealers_table', 1),
(26, '2018_04_04_202005_create_orders_master_table', 1),
(34, '2017_08_11_073824_create_menus_table', 1),
(35, '2017_08_11_074006_create_menu_items_table', 1),
(36, '2018_01_13_155638_create_posts_table', 1),
(37, '2018_07_13_081052_create_paymentsettings_table', 1),
(38, '2018_07_14_205014_temporary_orders_table', 1),
(39, '2018_07_15_171525_create_orders_master_table', 1),
(40, '2018_07_15_172259_create_orders_detail_table', 1),
(42, '2018_07_07_062938_create_districts_table', 1),
(44, '2018_10_25_095020_create_warishs_table', 1),
(45, '2018_08_06_224359_create_comments_table', 1),
(46, '2018_10_24_051646_create_newsletters_table', 1),
(47, '2018_12_05_130058_create_attgroups_table', 1),
(56, '2018_01_25_190214_create_attributes_table', 1),
(57, '2018_01_10_051005_create_terms_table', 1),
(62, '2018_12_19_211559_create_relatedproducts_table', 1),
(65, '2018_12_20_210353_create_productcategories_table', 1),
(66, '2018_12_21_215051_create_productimages_table', 1),
(68, '2018_12_22_222345_create_productattributesdata_table', 1),
(69, '2018_01_25_185753_create_products_table', 1),
(71, '2019_01_02_061932_create_homesettings_table', 1),
(73, '2019_01_04_173553_create_returns_table', 1),
(76, '2019_01_20_211432_create_productpricecombinations_table', 1),
(77, '2019_01_22_184932_create_productcombinationsdata_table', 1),
(78, '2019_01_28_052658_create_reviews_table', 1),
(79, '2019_01_30_094821_create_coupons_table', 1),
(80, '2019_02_15_130711_create_banks_table', 1),
(81, '2019_02_15_154530_create_productsemidata_table', 1),
(82, '2019_02_19_072000_create_flash_schedules_table', 1),
(83, '2019_02_19_072327_create_flash_items_table', 1),
(84, '2019_03_12_054253_create_wishlists_table', 2),
(85, '2019_07_28_094303_create_careers_table', 2),
(86, '2019_08_03_123456_create_jobs_table', 3),
(87, '2019_09_18_102018_create_session_taxonomies_table', 4),
(88, '2019_10_02_072448_create_order_shipments_table', 4),
(89, '2019_10_07_041954_create_showroom_stocks_table', 5),
(90, '2019_10_15_081228_create_depo_lists_table', 6),
(91, '2019_10_23_060238_create_question_answers_table', 7),
(92, '2019_10_24_054542_create_review_images_table', 8),
(93, '2019_10_31_073828_create_whole_sales_table', 8),
(94, '2019_11_04_065705_create_mix_taxonomies_table', 9),
(95, '2019_11_04_105309_create_tell_uses_table', 10),
(96, '2019_11_05_060403_create_rewards_point_histories_table', 10),
(97, '2019_12_04_065636_create_brands_table', 10),
(98, '2019_12_12_085405_create_contacts_table', 11),
(99, '2019_12_30_092401_create_notifications_table', 12),
(100, '2019_12_30_092401_create_galleries_table', 13),
(101, '2020_01_29_050519_create_corporates_table', 14),
(102, '2020_02_04_111628_create_order_notes_table', 14),
(103, '2020_02_13_085134_create_affiliate_payments_table', 15),
(106, '2020_01_14_063651_create_galleries_table', 16),
(108, '2020_06_02_051629_create_routes_table', 17);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `client_id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `client_id` int NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int UNSIGNED NOT NULL,
  `client_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sticky` tinyint(1) DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'English, Bengali or any other language',
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `user_id`, `title`, `sub_title`, `seo_url`, `description`, `images`, `is_sticky`, `lang`, `is_active`, `created_at`, `updated_at`) VALUES
(6, 1, 'Payments', 'payments', 'payments', 'Payments', NULL, 1, 'en', 1, '2018-12-17 09:17:11', '2019-10-27 04:06:34'),
(11, 1, 'Refunds', 'Refunds', 'refunds', 'Refunds', NULL, 1, 'en', 1, '2019-01-30 11:46:45', '2019-10-27 04:03:41'),
(16, 1, 'Affiliates', 'Affiliates', 'affiliates', 'Affiliates', NULL, 0, 'en', 1, '2019-03-03 20:28:25', '2019-10-27 04:01:00'),
(19, 1, 'Advertisement', 'advertisement', 'advertisement', 'Advertisement', NULL, 1, 'en', 1, '2019-08-03 05:17:04', '2019-10-27 04:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', NULL, '2019-12-22 03:13:52', '2019-12-22 22:34:25'),
(2, 'arifabm@gmail.com', 'ZlNbKociAZitRZjVYVM7yxtV5A5RdJdeNmi5WIUvgAUvJ9xjoCSrouV2l7R5', '2020-03-28 10:06:27', '2020-03-28 21:09:20'),
(3, 'sifatnumber@gmail.com', 'xDwiHNknmOM50ISlToL1n1zHUto4lIpEvBDDRf6tMWYt066vnaDuYgROZOn2', '2020-03-29 11:31:47', '2020-03-29 11:31:51'),
(4, 'shobhra.dreams@gmail.com', '2c5o3hf6jI0VCYTNc2aLG2IRjJnKOuu84sbRfyR0xkzfGC1cpmJJjWpWkeHf', '2020-04-02 09:47:30', '2020-04-02 09:47:57'),
(5, 'amsaikat.cmt@gmail.com', 'jkE5MfU0YL6vFtqfUxUioMmCjhoeoMC6UUH23zfm0gp6OK2tt5iFFzoMySf0', '2020-04-19 02:44:51', '2020-04-19 02:56:31'),
(6, 'Probeerkshaha@gmail.com', 'TyUATEJf9cgerxtC7M0ydDEmOSvgulknpk2EoJWB1vq2HAC6K4o8kgNHiKbQ', '2020-04-19 22:21:59', '2020-04-19 22:22:24'),
(7, 'rocky090977@gmail.com', '9iYsLeP6HUZeDjsYiyB1MXCEPNv4ivj7GCAjf3GYTcxwrdLXzfaiPnG9Wl7w', '2020-04-22 06:53:24', '2020-04-22 06:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `youtube` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `brand` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_hours` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_numbers` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `tags` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `division` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thana` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'English, Bengali or any other language',
  `is_auto_post` tinyint(1) DEFAULT NULL,
  `is_upcoming` tinyint(1) DEFAULT NULL,
  `is_sticky` tinyint(1) DEFAULT NULL,
  `h1tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h2tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `views` int NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `offer_expire_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `sub_title`, `seo_url`, `author`, `categories`, `images`, `description`, `short_description`, `youtube`, `brand`, `phone`, `opening_hours`, `latitude`, `longitude`, `phone_numbers`, `address`, `tags`, `division`, `district`, `thana`, `shop_type`, `lang`, `is_auto_post`, `is_upcoming`, `is_sticky`, `h1tag`, `h2tag`, `seo_title`, `seo_description`, `seo_keywords`, `views`, `is_active`, `offer_expire_date`, `created_at`, `updated_at`) VALUES
(7, 1, 'Regal-Emporium-Shyamoli', '24/2, block B khilji road (41 Ring road), Shymoli', 'regal-emporium-shyamoli', 'Manager', '623', NULL, '<ul style=\"padding-top: 0px; padding-bottom: 0px; list-style: none;\" source=\"\" sans=\"\" pro\",=\"\" sans-serif;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\"><li style=\"\"><font color=\"#454545\">Khilji Road, Shamoli</font><br></li></ul>', 'Khilji Road', NULL, 'Regal', '01844658172', '9:30 AM', NULL, NULL, NULL, '24/2, block B khilji road (41 Ring road), Shymoli', NULL, 'Dhaka', 'Dhaka', 'Mohammadpur', 'RegalEmporium', '0', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, '2019-05-24 16:48:19', '2019-05-24 16:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', 1, '2018-03-31 00:06:27', '2018-03-31 00:06:27'),
(2, 'manager', 'A Website Manager', 1, '2018-03-31 00:06:27', '2018-03-31 00:06:27'),
(3, 'member', 'A Customer', 1, '2020-05-02 08:53:31', '2020-05-02 08:53:31'),
(4, 'employee', 'An Employee/Membership Offer', 1, '2020-05-01 08:55:37', '2020-05-01 08:55:37'),
(5, 'vendor', 'A Vendor', 1, '2020-05-01 08:59:25', '2020-05-02 08:44:24'),
(6, 'applicant', 'An Applicant', 1, '2018-03-31 00:06:27', '2018-03-31 00:06:27'),
(7, 'showroom', 'A Showroom Manager', 1, '2020-05-02 09:06:56', '2020-05-02 09:06:56'),
(8, 'affiliator', 'An Affiliator', 1, '2020-05-02 09:06:56', '2020-04-30 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int UNSIGNED NOT NULL,
  `role_id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-04-01 21:07:03', '2019-04-10 23:33:38');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id` int UNSIGNED NOT NULL,
  `route_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_hash` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_group` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `route_uri`, `route_name`, `route_hash`, `in_group`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin/brand', 'admin.brand.index', 'b88b421450d4ee9214270351966566ba', 'admin', NULL, 1, '2020-06-02 02:01:58', '2020-06-02 12:33:36'),
(6, 'admin/brand/create', 'admin.brand.create', 'cc9d515add249e63b28171d7c96270ca', 'admin', NULL, 1, '2020-06-02 12:33:27', '2020-06-02 12:34:52'),
(7, 'admin/brand', 'admin.brand.store', '7865bbbd3928a70fefaaa7020208aec3', 'admin', NULL, 1, '2020-06-02 12:35:02', '2020-06-02 14:15:59'),
(8, 'admin/brand/{brand}', 'admin.brand.show', '7c78160786e16a84400a5c119a59eacb', 'admin', NULL, 1, '2020-06-02 14:16:05', '2020-06-02 14:16:05'),
(9, 'admin/brand/{brand}/edit', 'admin.brand.edit', '77f4fbc3ad4a3cd9da73690e0441afbf', 'admin', NULL, 1, '2020-06-02 14:16:11', '2020-06-02 14:16:11'),
(10, 'admin/brand/{brand}', 'admin.brand.update', '91ae3293d7ee5862295c7ad7346dbd3a', 'admin', NULL, 1, '2020-06-02 14:16:16', '2020-06-02 14:16:16'),
(11, 'admin/brand/{brand}', 'admin.brand.destroy', 'b70e19f90cd360c3f111e11c44b1c7fb', 'admin', NULL, 1, '2020-06-02 14:16:47', '2020-06-02 14:16:47'),
(12, 'admin/jobs', 'admin.jobs.index', '3010f8be6cab6555f275480ceced6a11', 'admin', NULL, 1, '2020-06-02 14:16:52', '2020-06-02 14:16:52'),
(13, 'admin/jobs/create', 'admin.jobs.create', '126720d6f67ffc3f84e37d5b07dba7a3', 'admin', NULL, 1, '2020-06-02 14:16:59', '2020-06-02 14:16:59'),
(14, 'admin/jobs', 'admin.jobs.store', 'd19a41beb0a9087ed00f708cec8c7e5d', 'admin', NULL, 1, '2020-06-02 14:17:28', '2020-06-02 14:17:28'),
(15, 'admin/jobs/{job}', 'admin.jobs.show', '4bae692bbe6dacc1e6ef16212fdae346', 'admin', NULL, 1, '2020-06-02 14:17:31', '2020-06-02 14:17:31'),
(16, 'admin/jobs/{job}/edit', 'admin.jobs.edit', 'bf2e70b339ab18c904c7da3deaaeb0e4', 'admin', NULL, 1, '2020-06-02 14:17:35', '2020-06-02 14:17:35'),
(17, 'admin/jobs/{job}', 'admin.jobs.update', 'daa5d9c7cd89a8c48f70274b69982d8a', 'admin', NULL, 1, '2020-06-02 14:17:38', '2020-06-02 14:17:38'),
(18, 'admin/jobs/{job}', 'admin.jobs.destroy', 'e56ca572e4df3d124c3a3001117d1b90', 'admin', NULL, 1, '2020-06-02 14:17:41', '2020-06-02 14:17:41'),
(19, 'admin/shipment', 'admin.shipment.index', '95c79448516eb69d447642f61e3bebbd', 'admin', NULL, 1, '2020-06-02 14:17:45', '2020-06-02 14:17:45'),
(20, 'admin/shipment/create', 'admin.shipment.create', '962b213802c814951623f179fa2cbff8', 'admin', NULL, 1, '2020-06-02 14:17:49', '2020-06-02 14:17:49'),
(21, 'admin/shipment', 'admin.shipment.store', 'f15d9669b4a1789487784e231a0bcd67', 'admin', NULL, 1, '2020-06-02 14:17:57', '2020-06-02 14:17:57'),
(22, 'admin/shipment/{shipment}', 'admin.shipment.show', 'd3a091a435d029119c438477227eec7d', 'admin', NULL, 1, '2020-06-02 14:18:02', '2020-06-02 14:18:02'),
(23, 'admin/shipment/{shipment}/edit', 'admin.shipment.edit', 'b0029b4c6b419fb679c61ea21188c433', 'admin', NULL, 1, '2020-06-02 14:18:06', '2020-06-02 14:18:06'),
(24, 'admin/shipment/{shipment}', 'admin.shipment.update', 'c36105f8295e5b75d572c1c2938d317c', 'admin', NULL, 1, '2020-06-02 14:18:10', '2020-06-02 14:18:10'),
(25, 'admin/shipment/{shipment}', 'admin.shipment.destroy', '618b3466aeac47e904228f47d255fd2c', 'admin', NULL, 1, '2020-06-02 14:18:13', '2020-06-02 14:18:13'),
(26, 'admin/pages', 'admin.pages.index', 'e6abe7b90b19587554af1b46458e293b', 'admin', NULL, 1, '2020-06-02 14:18:16', '2020-06-02 14:18:16'),
(27, 'admin/pages/create', 'admin.pages.create', '2d4239affaff8874f5b422729a50d10d', 'admin', NULL, 1, '2020-06-02 14:18:19', '2020-06-02 14:18:19'),
(28, 'admin/pages', 'admin.pages.store', '0bcf15a137cc5da909914f7bbd7b9ad9', 'admin', NULL, 1, '2020-06-02 14:18:22', '2020-06-02 14:18:22'),
(29, 'admin/pages/{page}', 'admin.pages.show', '1c6bda72ddd347422a012145daa7bf68', 'admin', NULL, 1, '2020-06-02 14:18:25', '2020-06-02 14:18:25'),
(30, 'admin/pages/{page}/edit', 'admin.pages.edit', '30aacc9ab5866eb63cf3a819f3a351ea', 'admin', NULL, 1, '2020-06-02 14:19:06', '2020-06-02 14:19:06'),
(31, 'admin/pages/{page}', 'admin.pages.update', 'e7293e8e21b73992d9bb097d502ad614', 'admin', NULL, 1, '2020-06-02 14:19:09', '2020-06-02 14:19:09'),
(32, 'admin/pages/{page}', 'admin.pages.destroy', '7ec7f0d3ab6737c2cdd617f7ec3df010', 'admin', NULL, 1, '2020-06-02 14:19:19', '2020-06-02 14:19:19'),
(33, 'admin/posts', 'admin.posts.index', '4396fd56f6d3235d837b84da6f7ddc72', 'admin', NULL, 1, '2020-06-02 14:19:22', '2020-06-02 14:19:22'),
(34, 'admin/posts/create', 'admin.posts.create', '7a5843105eba173db5830861e5a47ea2', 'admin', NULL, 1, '2020-06-02 14:19:25', '2020-06-02 14:19:25'),
(35, 'admin/posts', 'admin.posts.store', '15a2dc7e0bd28648af5b5aaf72082273', 'admin', NULL, 1, '2020-06-02 14:19:34', '2020-06-02 14:19:34'),
(36, 'admin/posts/{post}', 'admin.posts.show', 'b6b64a44e7b6c4bffe680487cf71b046', 'admin', NULL, 1, '2020-06-02 15:11:18', '2020-06-02 15:11:18'),
(37, 'admin/posts/{post}/edit', 'admin.posts.edit', 'b912a5f3b47e029da9c1e10e6ffd487d', 'admin', NULL, 1, '2020-06-02 15:11:33', '2020-06-02 15:11:33'),
(38, 'admin/posts/{post}', 'admin.posts.update', '13593d06637ac3245e1eb43840c8e88a', 'admin', NULL, 1, '2020-06-02 15:11:39', '2020-06-02 15:11:39'),
(39, 'admin/posts/{post}', 'admin.posts.destroy', '6d8d5a8c19b36a0cef80cd824d69c464', 'admin', NULL, 1, '2020-06-02 15:11:55', '2020-06-02 15:11:55'),
(40, 'admin/corporate', 'admin.corporate.index', '1b5233eab7f5dc7c6d0b76ecbd3c4d08', 'admin', NULL, 1, '2020-06-02 15:12:06', '2020-06-02 15:12:06'),
(41, 'admin/corporate/create', 'admin.corporate.create', 'd7d12dee19fa0db29799285408793e26', 'admin', NULL, 1, '2020-06-02 15:12:12', '2020-06-02 15:12:12'),
(42, 'admin/corporate', 'admin.corporate.store', 'bb61eb122c5b55f574950e2839bc7df5', 'admin', NULL, 1, '2020-06-02 15:12:18', '2020-06-02 15:12:18'),
(43, 'admin/corporate/{corporate}', 'admin.corporate.show', '81fb0b3a02fd5604aae13adc61491772', 'admin', NULL, 1, '2020-06-02 15:12:23', '2020-06-02 15:12:23'),
(44, 'admin/corporate/{corporate}/edit', 'admin.corporate.edit', '41dc44701439f7c3ea5d22f11963242b', 'admin', NULL, 1, '2020-06-02 15:12:27', '2020-06-02 15:12:27'),
(45, 'admin/corporate/{corporate}', 'admin.corporate.update', 'b3f4fec42a9a2c15274fd0bb15c3189d', 'admin', NULL, 1, '2020-06-02 15:13:04', '2020-06-02 15:13:04'),
(46, 'admin/corporate/{corporate}', 'admin.corporate.destroy', '5b835e3a0b23cbb34e77b68f2ac85e8f', 'admin', NULL, 1, '2020-06-02 15:13:11', '2020-06-02 15:13:11'),
(47, 'admin/showroom', 'admin.showroom.index', '5c753adf68172a9da8828b6f2bebb317', 'admin', NULL, 1, '2020-06-02 15:13:15', '2020-06-02 15:13:15'),
(48, 'admin/showroom/create', 'admin.showroom.create', 'd9d5ba2ef41267cdadf88e15c44bf3c8', 'admin', NULL, 1, '2020-06-02 15:13:19', '2020-06-02 15:13:19'),
(49, 'admin/showroom', 'admin.showroom.store', 'bc1d42e589e4589528eff638620e0356', 'admin', NULL, 1, '2020-06-02 15:13:24', '2020-06-02 15:13:24'),
(50, 'set_vendor_access_permissions', 'set_vendor_access_permissions', 'a9436a7d29df038821884b2db1a9bccb', 'admin', NULL, 1, '2020-06-02 15:13:34', '2020-06-02 15:13:34'),
(51, 'admin/showroom/{showroom}', 'admin.showroom.show', 'af1caeb6a7e2b29aab96b84de52acd8f', 'admin', NULL, 1, '2020-06-02 15:13:40', '2020-06-02 15:13:40'),
(52, 'admin/showroom/{showroom}/edit', 'admin.showroom.edit', '245f053d628b81992324022aa5d599cb', 'admin', NULL, 1, '2020-06-02 15:13:44', '2020-06-02 15:13:44'),
(53, 'admin/showroom/{showroom}', 'admin.showroom.update', 'da37263822e679a54c60acc0df896760', 'admin', NULL, 1, '2020-06-02 15:13:48', '2020-06-02 15:13:48'),
(54, 'admin/showroom/{showroom}', 'admin.showroom.destroy', '1ed5b9d0836bff788a9a57bf7fdc3123', 'admin', NULL, 1, '2020-06-02 15:13:53', '2020-06-02 15:13:53'),
(55, 'admin/medias', 'admin.medias.index', 'a0a6996be88537b69c59f2fe038321d5', 'admin', NULL, 1, '2020-06-02 15:13:57', '2020-06-02 15:13:57'),
(56, 'admin/medias/create', 'admin.medias.create', '2dddd499efae5db416c5c5267ba7c6c0', 'admin', NULL, 1, '2020-06-02 15:14:08', '2020-06-02 15:14:08'),
(57, 'admin/medias', 'admin.medias.store', '7280858d998e585002c24b1a5155a77c', 'admin', NULL, 1, '2020-06-02 15:14:13', '2020-06-02 15:14:13'),
(58, 'admin/medias/{media}', 'admin.medias.show', 'cba89a90e046ec3dc6994391cc14a99a', 'admin', NULL, 1, '2020-06-02 15:14:17', '2020-06-02 15:14:17'),
(59, 'admin/medias/{media}/edit', 'admin.medias.edit', 'ad7e82b913b843290a68d5ad96f7271f', 'admin', NULL, 1, '2020-06-02 15:14:24', '2020-06-02 15:14:24'),
(60, 'admin/medias/{media}', 'admin.medias.update', '693e655ad5a892a78094eff40b08045c', 'admin', NULL, 1, '2020-06-02 15:14:31', '2020-06-02 15:14:31'),
(61, 'admin/medias/{media}', 'admin.medias.destroy', 'b0b4a18346418ca9aed58c6c7347e4ad', 'admin', NULL, 1, '2020-06-02 15:14:38', '2020-06-02 15:14:38'),
(62, 'admin/upload', 'admin.upload.index', 'd92fa6da3bb9f510a2281519766ab814', 'admin', NULL, 1, '2020-06-02 15:14:43', '2020-06-02 15:14:43'),
(63, 'admin/upload/create', 'admin.upload.create', '12998503f132e7ef58562f6e203db986', 'admin', NULL, 1, '2020-06-02 15:14:51', '2020-06-02 15:14:51'),
(64, 'admin/upload/{upload}', 'admin.upload.show', '97cabf1c445d68541eea86dd3dff9a53', 'admin', NULL, 1, '2020-06-02 15:14:56', '2020-06-02 15:14:56'),
(65, 'admin/upload', 'admin.upload.store', '3f33abc4a528034341e82f6251491f53', 'admin', NULL, 1, '2020-06-02 15:15:01', '2020-06-02 15:15:01'),
(66, 'admin/upload/{upload}/edit', 'admin.upload.edit', '636f4d958bafb36fe0bd2c8c737871cc', 'admin', NULL, 1, '2020-06-02 15:15:13', '2020-06-02 15:15:13'),
(67, 'admin/upload/{upload}', 'admin.upload.update', '5acb8f97fb60dcfa44c7ba68b68ac005', 'admin', NULL, 1, '2020-06-02 15:15:18', '2020-06-02 15:15:18'),
(68, 'admin/upload/{upload}', 'admin.upload.destroy', '23b4831ebb47cd2d807e6b369235cea9', 'admin', NULL, 1, '2020-06-02 15:15:23', '2020-06-02 15:15:23'),
(69, 'admin/newsletters', 'admin.newsletters.index', '266acc2921b38be823cfb78348b0b5a0', 'admin', NULL, 1, '2020-06-02 15:15:28', '2020-06-02 15:15:28'),
(70, 'admin/newsletters/create', 'admin.newsletters.create', 'ef2ad5ba60974d6e77b2f66c074f1926', 'admin', NULL, 1, '2020-06-02 15:15:33', '2020-06-02 15:15:33'),
(71, 'admin/newsletters', 'admin.newsletters.store', 'd4adc245fa46b9b0373171f25626ebb8', 'admin', NULL, 1, '2020-06-02 15:15:38', '2020-06-02 15:15:38'),
(72, 'admin/newsletters/{newsletter}', 'admin.newsletters.show', '9de278bb3a31cd61522c8960bfad7e7d', 'admin', NULL, 1, '2020-06-02 15:16:08', '2020-06-02 15:16:08'),
(73, 'admin/newsletters/{newsletter}/edit', 'admin.newsletters.edit', 'e0d6318022ec72e687aace6e59cacd1d', 'admin', NULL, 1, '2020-06-02 15:16:14', '2020-06-02 15:16:14'),
(74, 'admin/newsletters/{newsletter}', 'admin.newsletters.update', '85de625531dbc5167c29f015648d0b38', 'admin', NULL, 1, '2020-06-02 15:16:19', '2020-06-02 15:16:19'),
(75, 'admin/newsletters/{newsletter}', 'admin.newsletters.destroy', '658158c68b94f67c7fb448cc3bae34b1', 'admin', NULL, 1, '2020-06-02 15:16:24', '2020-06-02 15:16:24'),
(76, 'admin/distribution_setting', 'admin.distribution_setting.index', '21a79589304e5f46466fd1783a8cd4fb', 'admin', NULL, 1, '2020-06-02 15:16:28', '2020-06-02 15:16:28'),
(77, 'admin/distribution_setting/create', 'admin.distribution_setting.create', 'eb0d3bfa8fb72395256654c7a74f0fdd', 'admin', NULL, 1, '2020-06-02 15:16:33', '2020-06-02 15:16:33'),
(78, 'admin/distribution_setting', 'admin.distribution_setting.store', '49d9bb586fb3a768b9e776ccd82a6e9e', 'admin', NULL, 1, '2020-06-02 15:16:39', '2020-06-02 15:16:39'),
(79, 'admin/distribution_setting/{distribution_setting}', 'admin.distribution_setting.show', 'fedc31d197176d15c2d37da0ed253ca4', 'admin', NULL, 1, '2020-06-02 15:16:44', '2020-06-02 15:16:44'),
(80, 'admin/distribution_setting/{distribution_setting}/edit', 'admin.distribution_setting.edit', '6c25b4787bdee1ae2c1caf26f52622a1', 'admin', NULL, 1, '2020-06-02 15:16:50', '2020-06-02 15:16:50'),
(81, 'admin/distribution_setting/{distribution_setting}', 'admin.distribution_setting.update', '40b5e814212a72892522ad244501cae6', 'admin', NULL, 1, '2020-06-02 15:16:59', '2020-06-02 15:16:59'),
(82, 'admin/distribution_setting/{distribution_setting}', 'admin.distribution_setting.destroy', 'dc50cd507b2c389de234d5c46cd6dc1e', 'admin', NULL, 1, '2020-06-02 15:17:04', '2020-06-02 15:17:04'),
(83, 'admin/users', 'admin.users.index', '8f9cc1294a67fee854c8c841beb2850a', 'admin', NULL, 1, '2020-06-02 15:17:09', '2020-06-02 15:17:09'),
(84, 'admin/users/create', 'admin.users.create', 'd73e6ee4fe77189853d3246410097bd6', 'admin', NULL, 1, '2020-06-02 15:17:14', '2020-06-02 15:17:14'),
(85, 'admin/users', 'admin.users.store', '9db96df0e8be09a2fbf5c1eab8b0222a', 'admin', NULL, 1, '2020-06-02 15:17:18', '2020-06-02 15:17:18'),
(86, 'admin/users/{user}', 'admin.users.show', '581d14cb22bd5c07886cae72d54c6735', 'admin', NULL, 1, '2020-06-02 15:17:23', '2020-06-02 15:17:23'),
(87, 'admin/users/{user}/edit', 'admin.users.edit', '7e0af892083052b6912a38848871f2b7', 'admin', NULL, 1, '2020-06-02 15:17:29', '2020-06-02 15:17:29'),
(88, 'admin/users/{user}', 'admin.users.update', '9529cea05dbff7f487cd7fda8f9e8515', 'admin', NULL, 1, '2020-06-02 15:17:34', '2020-06-02 15:17:34'),
(89, 'admin/users/{user}', 'admin.users.destroy', '4041bddd6bf4e77b73bce6cf089b2951', 'admin', NULL, 1, '2020-06-02 15:17:39', '2020-06-02 15:17:39'),
(90, 'admin/area_managements', 'admin.area_managements.index', '5d73f2122a5e44922e59785223370915', 'admin', NULL, 1, '2020-06-02 15:17:44', '2020-06-02 15:17:44'),
(91, 'admin/area_managements/create', 'admin.area_managements.create', 'c7666e0ef6dafa4c5377ee8b9008b144', 'admin', NULL, 1, '2020-06-02 15:17:55', '2020-06-02 15:17:55'),
(92, 'admin/area_managements', 'admin.area_managements.store', 'f929728e1f8ebfb71b071900aadef578', 'admin', NULL, 1, '2020-06-02 15:18:01', '2020-06-02 15:18:01'),
(93, 'admin/area_managements/{area_management}', 'admin.area_managements.show', '89eea7736ff49f4558d9355d35ab0159', 'admin', NULL, 1, '2020-06-02 15:18:12', '2020-06-02 15:18:12'),
(94, 'admin/area_managements/{area_management}/edit', 'admin.area_managements.edit', '7fbd9a5caefc04f1925fbe145588a230', 'admin', NULL, 1, '2020-06-02 15:18:29', '2020-06-02 15:18:29'),
(95, 'admin/area_managements/{area_management}', 'admin.area_managements.update', '05f865402561166064bb69e4f419e800', 'admin', NULL, 1, '2020-06-02 15:18:35', '2020-06-02 15:18:35'),
(96, 'admin/area_managements/{area_management}', 'admin.area_managements.destroy', 'ff0326c28fffa99b6167b5cf7f8bf703', 'admin', NULL, 1, '2020-06-02 15:18:40', '2020-06-02 15:18:40'),
(97, 'admin/coupons', 'admin.coupons.index', 'f659f30f41ffe8c9dac7302141063c32', 'admin', NULL, 1, '2020-06-02 15:18:46', '2020-06-02 15:18:46'),
(98, 'admin/coupons/create', 'admin.coupons.create', '8b5c3615589a58b2cd1e93790d620c55', 'admin', NULL, 1, '2020-06-02 15:18:55', '2020-06-02 15:18:55'),
(99, 'admin/coupons', 'admin.coupons.store', '44a31d2bfb6444b880ccc8f982f357d9', 'admin', NULL, 1, '2020-06-02 15:19:00', '2020-06-02 15:19:00'),
(100, 'admin/coupons/{coupon}', 'admin.coupons.show', '9da7beb320e561336b202a7db6c806fb', 'admin', NULL, 1, '2020-06-02 15:19:05', '2020-06-02 15:19:05'),
(101, 'admin/coupons/{coupon}/edit', 'admin.coupons.edit', 'eae8ed8f24eebcc679dd7d2e154b0b0c', 'admin', NULL, 1, '2020-06-02 15:19:10', '2020-06-02 15:19:10'),
(102, 'admin/coupons/{coupon}', 'admin.coupons.update', '6f0d6242b24a72e1f8ef66d51bedb51f', 'admin', NULL, 1, '2020-06-02 15:19:15', '2020-06-02 15:19:15'),
(103, 'admin/coupons/{coupon}', 'admin.coupons.destroy', 'bf00cd298dc56c9ce3ada10de5ace5a1', 'admin', NULL, 1, '2020-06-02 15:19:20', '2020-06-02 15:19:20'),
(104, 'admin/comments', 'admin.comments.index', '81d93573f4aecfceda08e0b5516030b5', 'admin', NULL, 1, '2020-06-02 15:19:37', '2020-06-02 15:19:37'),
(105, 'admin/comments/create', 'admin.comments.create', 'fbcdd67724d5964984cfa19875f08003', 'admin', NULL, 1, '2020-06-02 15:19:44', '2020-06-02 15:19:44'),
(106, 'admin/comments', 'admin.comments.store', 'ffee6528a90d14646b73d4af2eebc22c', 'admin', NULL, 1, '2020-06-02 15:19:48', '2020-06-02 15:19:48'),
(107, 'admin/comments/{comment}', 'admin.comments.show', 'd82bfccb379e86c93d42be911a6528d4', 'admin', NULL, 1, '2020-06-02 15:19:53', '2020-06-02 15:19:53'),
(108, 'admin/comments/{comment}/edit', 'admin.comments.edit', '7730ab48dba466297596c27adab5a67b', 'admin', NULL, 1, '2020-06-02 15:19:58', '2020-06-02 15:19:58'),
(109, 'admin/comments/{comment}', 'admin.comments.update', '1aabfe815d3d834406062f93fe655bb4', 'admin', NULL, 1, '2020-06-02 15:20:04', '2020-06-02 15:20:04'),
(110, 'admin/comments/{comment}', 'admin.comments.destroy', '98117ae5d347f3e7227a7f88f0db86fc', 'admin', NULL, 1, '2020-06-02 15:20:08', '2020-06-02 15:20:08'),
(111, 'admin/routes', 'admin.routes.index', '38b305ac0bb5f951d83b8197c165e9e1', 'admin', NULL, 1, '2020-06-02 15:20:13', '2020-06-02 15:20:13'),
(112, 'admin/routes/create', 'admin.routes.create', '2ee19784573ead8dc0985ab88ad915ac', 'admin', NULL, 1, '2020-06-02 15:20:19', '2020-06-02 15:20:19'),
(113, 'admin/routes', 'admin.routes.store', '87ba9195d87d4e08c80a686bfbfa0b0d', 'admin', NULL, 1, '2020-06-02 15:20:28', '2020-06-02 15:20:28'),
(114, 'admin/routes/{route}', 'admin.routes.show', '6d871c6848663e5ccfc5397e2e092d5f', 'admin', NULL, 1, '2020-06-02 15:20:39', '2020-06-02 15:20:39'),
(115, 'admin/routes/{route}/edit', 'admin.routes.edit', '5d3000dc023bc138aefb64bfc12ef05d', 'admin', NULL, 1, '2020-06-02 15:20:45', '2020-06-02 15:20:45'),
(116, 'admin/routes/{route}', 'admin.routes.update', 'a420ad0c2b8c714fab43568a394cbf40', 'admin', NULL, 1, '2020-06-02 15:20:50', '2020-06-02 15:20:50'),
(117, 'admin/routes/{route}', 'admin.routes.destroy', '3b15a51264fac47b1c67f77f5c91ce19', 'admin', NULL, 1, '2020-06-02 15:20:54', '2020-06-02 15:20:54'),
(118, 'admin/routes/index', 'admin.route.role_modify', '865b326d7817a46cce4cfc8edfc9b26f', 'admin', NULL, 1, '2020-06-02 15:21:00', '2020-06-02 15:21:00'),
(119, 'admin/products/import_stocks', 'admin.products.import_stocks_view', 'acbc05928fd65c2b461f8887b7c945e9', 'admin', NULL, 1, '2020-06-02 15:21:05', '2020-06-02 15:21:05'),
(120, 'admin/products/importProductsStore', 'admin.products.import_stocks_store', '52942d983fa3c848d8190aafd4d4bb94', 'admin', NULL, 1, '2020-06-02 15:21:09', '2020-06-02 15:21:09'),
(121, 'admin/brand/{id}/sales_report', 'admin.brand.sales_report', '871706481edba285f2faa04d818c2628', 'admin', NULL, 1, '2020-06-02 15:21:17', '2020-06-02 15:21:17'),
(122, 'admin/jobs/application/delete/{id}', 'admin.jobs.application.delete', 'a47cd4633fddd9c4a605b545a0ce8fb3', 'admin', NULL, 1, '2020-06-02 15:21:22', '2020-06-02 15:21:22'),
(123, 'admin/question/index', 'admin.question.index', '87193944dee660530cd63003746c0fb3', 'admin', NULL, 1, '2020-06-02 15:21:27', '2020-06-02 15:21:27'),
(124, 'admin/question/index', 'admin.question.answer', '2e5a9a251f70ebdc96c598be718920fe', 'admin', NULL, 1, '2020-06-02 15:21:32', '2020-06-02 15:21:32'),
(125, 'admin/question/delete/{id}', 'admin.question.delete', '70f843bbc48d7efac8ca1807703fbc18', 'admin', NULL, 1, '2020-06-02 15:21:37', '2020-06-02 15:21:37'),
(126, 'admin/import_showrooms_view', 'admin.import_showrooms_view', '42b14faef0daa231bdb4f9992db4b656', 'admin', NULL, 1, '2020-06-02 15:21:43', '2020-06-02 15:21:43'),
(127, 'admin/import_showrooms_save', 'admin.import_showrooms', '8d439b00d9c42173e5a990370471cf7a', 'admin', NULL, 1, '2020-06-02 15:21:47', '2020-06-02 15:21:47'),
(128, 'admin/export_showrooms', 'admin.export_showrooms', '5f9613f93abbb36ccd63b108c7b00195', 'admin', NULL, 1, '2020-06-02 15:21:52', '2020-06-02 15:21:52'),
(129, 'admin/newsletters/{id}/{action}', 'admin.newsletter_status', '8fef07a0b4e684d106614cdc8f3f7053', 'admin', NULL, 1, '2020-06-02 15:21:56', '2020-06-02 15:21:56'),
(130, 'admin/distribution/{order_id}', 'admin.distribution.index', '40b30cc960fb00ae6e991f6ac1a4888b', 'admin', NULL, 1, '2020-06-02 15:22:07', '2020-06-02 15:22:07'),
(131, 'admin/shipmentDistributor/{shipment_id}', 'admin.distribution.showroomList', 'b60b31dcf0cc8c0e8f9a41ceb6ffe696', 'admin', NULL, 1, '2020-06-02 15:22:13', '2020-06-02 15:22:13'),
(132, 'admin/shipmentDistribute', 'admin.distribution.shipmentDistribute.submit', '178e55de8552935f2c7cf3518b58e4c2', 'admin', NULL, 1, '2020-06-02 15:22:19', '2020-06-02 15:22:19'),
(133, 'admin/getDemandOrderInfo', 'admin.distribution.getDoInfo', '26cb5cc4afd56d95e798e2681fb505ff', 'admin', NULL, 1, '2020-06-02 15:22:26', '2020-06-02 15:22:26'),
(134, 'admin/submitDemandOrder', 'admin.distribution.submitDemandOrder', 'e7bd8b790b92024c6a21f94aa783543d', 'admin', NULL, 1, '2020-06-02 15:22:34', '2020-06-02 15:22:34'),
(135, 'admin/order/notes/{id}', 'admin.order.notes', 'e84c796f66795eafb4198eba47dcbf04', 'admin', NULL, 1, '2020-06-02 15:22:40', '2020-06-02 15:22:40'),
(136, 'admin/order/notes/add/{id}', 'admin.order.notes.add', 'c8949d5e290bf3cf88b1848c60405452', 'admin', NULL, 1, '2020-06-02 15:22:45', '2020-06-02 15:22:45'),
(137, 'admin/search_users', 'admin.search_users', 'eccab6bf66ff28f121aed141a8a2c2a4', 'admin', NULL, 1, '2020-06-02 15:22:50', '2020-06-02 15:22:50'),
(138, 'admin/modify_password_view/{id}', 'admin.modify_password_view', 'd9a3e27592d7567b91ad890fc7b3c9b2', 'admin', NULL, 1, '2020-06-02 15:22:56', '2020-06-02 15:22:56'),
(139, 'admin/modify_password/{id}', 'admin.modify_password', '462b0c66d483ec5251fe733f0118e41b', 'admin', NULL, 1, '2020-06-02 15:23:52', '2020-06-02 15:23:52'),
(140, 'admin/vendor_url_existing', 'admin.vendor_url', 'a1052e22191baf7b89e9dbb3374e6ab1', 'admin', NULL, 1, '2020-06-02 15:23:58', '2020-06-02 15:23:58'),
(141, 'admin/modify_role_view/{id}', 'admin.modify_role_view', '69515b93b61ff8471540323025cfc2b1', 'admin', NULL, 1, '2020-06-02 15:24:04', '2020-06-02 15:24:04'),
(142, 'admin/modify_role', 'admin.modify_role', '64164b42c7c41b124bb649057487e397', 'admin', NULL, 1, '2020-06-02 15:24:10', '2020-06-02 15:24:10'),
(143, 'admin/set_default_role/{id}', 'admin.set_default_role', '205e8202b58bea246ca3c4cf18a594d7', 'admin', NULL, 1, '2020-06-02 15:24:16', '2020-06-02 15:24:16'),
(144, 'admin/area_managements/status/{id}/{status}', 'admin.area_managements.status', '71bbe386ccc52e686c5e51698a2e52cf', 'admin', NULL, 1, '2020-06-02 15:24:23', '2020-06-02 15:24:23'),
(145, 'admin/wholesale', 'admin.wholesale.index', '52b525b3c4bc4fd8a1da07ca2f694d73', 'admin', NULL, 1, '2020-06-02 15:24:58', '2020-06-02 15:24:58'),
(146, 'admin/wholesale/status/{id}', 'admin.wholesale.status', 'c163da471836ba16c13f7dacf69ae018', 'admin', NULL, 1, '2020-06-02 15:25:11', '2020-06-02 15:25:11'),
(147, 'admin/wholesale/delete/{id}', 'admin.wholesale.delete', 'e6df09291517650947491f1aaaf67fb3', 'admin', NULL, 1, '2020-06-02 15:25:16', '2020-06-02 15:25:16'),
(148, 'admin/tellUs', 'admin.tellUs.index', 'b302a05f9ffb821bcf79acfb5713d39f', 'admin', NULL, 1, '2020-06-02 15:25:22', '2020-06-02 15:25:22'),
(149, 'admin/tellUs/options', 'admin.tellUs.options', 'e94da0838f9dd92d02e9194471832263', 'admin', NULL, 1, '2020-06-02 15:25:27', '2020-06-02 15:25:27'),
(150, 'admin/tellUs/options/status/{id}', 'admin.tellUs.options.status', '57e005e06d0588e980b8b1cc4735481b', 'admin', NULL, 1, '2020-06-02 15:25:31', '2020-06-02 15:25:31'),
(151, 'admin/tellUs/options/delete/{id}', 'admin.tellUs.options.delete', '5131d6827a811814c804748ac191cf4b', 'admin', NULL, 1, '2020-06-02 15:25:36', '2020-06-02 15:25:36'),
(152, 'admin/tellUs/options/AddEditSave', 'admin.tellUs.options.AddEditSave', '8045afcce699c07a5eab9389c9f198db', 'admin', NULL, 1, '2020-06-02 15:25:41', '2020-06-02 15:25:41'),
(153, 'admin/tellUs/optionView/{id}', 'admin.tellUs.option.view', '02ac80ac0d9ecdccac521b21915b41b2', 'admin', NULL, 1, '2020-06-02 15:25:46', '2020-06-02 15:25:46'),
(154, 'admin/tellUs/optionView/AddEditSave/{id}', 'admin.tellUs.option.view.AddEditSave', '506741df2c5bce810b2c328aa5df889c', 'admin', NULL, 1, '2020-06-02 15:25:51', '2020-06-02 15:25:51'),
(155, 'admin/vouchers', 'admin.vouchers', '0aa45b10fc087c45bb153062830f54c0', 'admin', NULL, 1, '2020-06-02 15:25:56', '2020-06-02 15:25:56'),
(156, 'admin/coupon_status/{id}/{action}', 'admin.coupons.status', 'b0d1d3a02a1e747fdd4ce6e79da361b4', 'admin', NULL, 1, '2020-06-02 15:26:00', '2020-06-02 15:26:00'),
(157, 'admin/coupon/suggestion/categories', 'admin.coupon.suggestion.categories', '29373abcacaa1d1499c03cea795fd6da', 'admin', NULL, 1, '2020-06-02 15:26:06', '2020-06-02 15:26:06'),
(158, 'admin/coupon/suggestion/products', 'admin.coupon.suggestion.products', '8813a2bf3a3de38a030c1f89a16c2927', 'admin', NULL, 1, '2020-06-02 15:26:11', '2020-06-02 15:26:11'),
(159, 'admin/coupon/usage/history/{id}', 'admin.coupon.usage.history', 'f7a2b04e92fe09ef47f5530824fe1b36', 'admin', NULL, 1, '2020-06-02 15:26:16', '2020-06-02 15:26:16'),
(160, 'admin/comments/quick_comment_approve/{id}', 'admin.comments.quick_approve', '790ca0d246be3be12a9933aa49572cac', 'admin', NULL, 1, '2020-06-02 15:26:23', '2020-06-02 15:26:23'),
(161, 'admin/comments/add_reply/{id}', 'admin.comments.add_reply', 'b2569b19018855f5cb0ca6eb4360b76f', 'admin', NULL, 1, '2020-06-02 15:26:29', '2020-06-02 15:26:29'),
(162, 'admin/htmlgen', 'admin.htmlgen', '144e1aec03bcdada3efec3190dada7ad', 'admin', NULL, 1, '2020-06-02 15:27:07', '2020-06-02 15:27:07'),
(163, 'admin/comments/reply_save', 'admin.comments.reply_save', '1e16a60416e86216919557e305f70d67', 'admin', NULL, 1, '2020-06-02 15:27:33', '2020-06-02 15:27:33'),
(164, 'admin/comments/edit_reply/{id}', 'admin.comments.edit_reply', '8ea6856e3f961a5b08c2b48eae445518', 'admin', NULL, 1, '2020-06-02 15:27:39', '2020-06-02 15:27:39'),
(165, 'admin/comments/reply/{id}/update', 'admin.comments.reply_update_save', '0d4aa3d138dacbb01fcfc673792461f9', 'admin', NULL, 1, '2020-06-02 15:27:47', '2020-06-02 15:27:47'),
(166, 'admin/reply_delete/{id}', 'admin.', 'e1aa80a61b0b4fb2cbe5a0b3f6ce834f', 'admin', NULL, 1, '2020-06-02 15:27:53', '2020-06-02 15:27:53'),
(167, 'admin/all_returns', 'admin.all_returns', 'b645363c38d8587e898ec495aea91cce', 'admin', NULL, 1, '2020-06-02 15:27:58', '2020-06-02 15:27:58'),
(168, 'admin/all_returns/approve/{id}', 'admin.all_returns.approve', 'b8f5b07f523d18f203dc2bb8bd459211', 'admin', NULL, 1, '2020-06-02 15:28:04', '2020-06-02 15:28:04'),
(169, 'admin/review', 'admin.review', '49c536ac3b276979e74dea80bf779758', 'admin', NULL, 1, '2020-06-02 15:28:09', '2020-06-02 15:28:09'),
(170, 'admin/review/approve/{id}/{action}', 'admin.review.approve', '0926364e26eff99af7b7a33d96a2b0cb', 'admin', NULL, 1, '2020-06-02 15:28:16', '2020-06-02 15:28:16'),
(171, 'admin/review/delete/{id}', 'admin.review.delete', '823bb55c9a626aec1a181bd75ed1da45', 'admin', NULL, 1, '2020-06-02 15:28:22', '2020-06-02 15:28:22'),
(172, 'admin/flash_management/flash_schedule_status/{id}/{action}', 'admin.flash_management.flash_schedule_status', 'cf53d19a8da9cb986a5cac73d1edde2f', 'admin', NULL, 1, '2020-06-02 15:28:27', '2020-06-02 15:28:27'),
(173, 'admin/flash_management/add_schedule_products', 'admin.flash_management.add_schedule_products', 'd9fd38ffb0fe0b9c527203bf66634940', 'admin', NULL, 1, '2020-06-02 15:28:33', '2020-06-02 15:28:33'),
(174, 'admin/flash_management/flash_item_save', 'admin.flash_management.flash_item_save', 'c6258cd921989ebbb0fb2dfc53ce4a83', 'admin', NULL, 1, '2020-06-02 15:28:39', '2020-06-02 15:28:39'),
(175, 'admin/flash_management/store_flash_items', 'admin.flash_management.store_flash_items', 'd18c2d317ca4850751c97c6cd78c886f', 'admin', NULL, 1, '2020-06-02 15:28:44', '2020-06-02 15:28:44'),
(176, 'admin/flash_management/delete_flash_item/{id}', 'admin.flash_management.delete_flash_item', '7a4ccf7e175472c739968241abb2efd6', 'admin', NULL, 1, '2020-06-02 15:28:49', '2020-06-02 15:28:49'),
(177, 'admin/flash_management', 'admin.flash_management.index', '3c2071bab3abe3d20d5b2e30888c3f2f', 'admin', NULL, 1, '2020-06-02 15:28:55', '2020-06-02 15:28:55'),
(178, 'admin/flash_management', 'admin.flash_management.store', '50ffb6a34a50e37ce5bca28078825199', 'admin', NULL, 1, '2020-06-02 15:29:00', '2020-06-02 15:29:00'),
(179, 'admin/flash_management/{flash_management}', 'admin.flash_management.show', 'd1e96fa4b2440fdf5797057e3a0e3d8d', 'admin', NULL, 1, '2020-06-02 15:29:06', '2020-06-02 15:29:06'),
(180, 'admin/flash_management/{flash_management}/edit', 'admin.flash_management.edit', '8f844d07b8137b509cd5323fe1986736', 'admin', NULL, 1, '2020-06-02 15:29:10', '2020-06-02 15:29:10'),
(181, 'admin/flash_management/{flash_management}', 'admin.flash_management.update', 'a88fbe761cb9ad3b3c138de7c9664eec', 'admin', NULL, 1, '2020-06-02 15:29:16', '2020-06-02 15:29:16'),
(182, 'admin/sliders', 'admin.sliders', '883ee8359674c2a6f9fa0c7e93edb8a8', 'admin', NULL, 1, '2020-06-02 15:29:20', '2020-06-02 15:29:20'),
(183, 'admin/add_slider', 'admin.add_slider', '6a11b3d02185f36c559686390fa802ff', 'admin', NULL, 1, '2020-06-02 15:29:25', '2020-06-02 15:29:25'),
(184, 'admin/slider_save', 'admin.slider_save', '0cf23204ada57d64f969dbae6304b8da', 'admin', NULL, 1, '2020-06-02 15:29:35', '2020-06-02 15:29:35'),
(185, 'admin/edit_slider/{id}', 'admin.edit_slider', 'e1769818efd7ab9989a3ccc30ec7580d', 'admin', NULL, 1, '2020-06-02 15:29:41', '2020-06-02 15:29:41'),
(186, 'admin/slider/{id}/update', 'admin.slider_update_save', 'd01a139c904151cb24726105389fae06', 'admin', NULL, 1, '2020-06-02 15:29:46', '2020-06-02 15:29:46'),
(187, 'admin/slider_delete/{id}', 'admin.slider_destroy', '1aa7e2bd8a1d3cd1c36226aeef2eeb09', 'admin', NULL, 1, '2020-06-02 15:29:51', '2020-06-02 15:29:51'),
(188, 'admin/sponsers', 'admin.sponsers', 'e79f7cedf49e1629df7ace947bcd5d77', 'admin', NULL, 1, '2020-06-02 15:29:56', '2020-06-02 15:29:56'),
(189, 'admin/add_sponser', 'admin.add_sponser', '788e00202f00bf423af09b24d4678231', 'admin', NULL, 1, '2020-06-02 15:30:01', '2020-06-02 15:30:01'),
(190, 'admin/sponser_save', 'admin.sponser_save', 'bdc145522f62eea62946a4e7591ec602', 'admin', NULL, 1, '2020-06-02 15:30:05', '2020-06-02 15:30:05'),
(191, 'admin/edit_sponser/{id}', 'admin.edit_sponser', '86704fdfbfd16dfae704a9aa8b953bd2', 'admin', NULL, 1, '2020-06-02 15:30:12', '2020-06-02 15:30:12'),
(192, 'admin/sponser/{id}/update', 'admin.sponser_update_save', '7fb0c44c360107368ffc67d4315f1aa1', 'admin', NULL, 1, '2020-06-02 15:30:18', '2020-06-02 15:30:18'),
(193, 'admin/sponser_delete/{id}', 'admin.sponser_destroy', 'e21b5623e1857a00b1f58327d60f5a21', 'admin', NULL, 1, '2020-06-02 15:30:24', '2020-06-02 15:30:24'),
(194, 'admin/contact/index', 'admin.contact.index', '8b00caf4cf81f852f69635a532cfed21', 'admin', NULL, 1, '2020-06-02 15:30:30', '2020-06-02 15:30:30'),
(195, 'admin/contact/read/{id}', 'admin.contact.read', '9189a464cb19fc2869424fcf3b97fe71', 'admin', NULL, 1, '2020-06-02 15:30:36', '2020-06-02 15:30:36'),
(196, 'admin/common/team', 'admin.common.team.index', '2271a70a51926435cc68e6d1e53a7c8f', 'admin', NULL, 1, '2020-06-02 15:30:41', '2020-06-02 15:30:41'),
(197, 'admin/common/team/store', 'admin.common.team.store', 'c1e99c25294ac5dcc834bc8914458024', 'admin', NULL, 1, '2020-06-02 15:30:46', '2020-06-02 15:30:46'),
(198, 'admin/common/team/update/{id}', 'admin.common.team.update', 'ff6a382f93e2f6b52006d4c915424c85', 'admin', NULL, 1, '2020-06-02 15:30:52', '2020-06-02 15:30:52'),
(199, 'admin/common/team/delete/{id}', 'admin.common.team.delete', '4815cc345024f21d3f6be86792e6c29e', 'admin', NULL, 1, '2020-06-02 15:31:00', '2020-06-02 15:31:00'),
(200, 'admin/common/gallery', 'admin.common.gallery.index', '647fd13d5967ce8f7c11a0025cbe6dc8', 'admin', NULL, 1, '2020-06-02 15:31:06', '2020-06-02 15:31:06'),
(201, 'admin/common/gallery/store', 'admin.common.gallery.store', 'b54e97d6efd95fe3dfb6aecc824689e1', 'admin', NULL, 1, '2020-06-02 15:31:13', '2020-06-02 15:31:13'),
(202, 'admin/common/gallery/update/{id}', 'admin.common.gallery.update', '0d83274342545433c450ce18ae19f546', 'admin', NULL, 1, '2020-06-02 15:31:24', '2020-06-02 15:31:24'),
(203, 'admin/common/gallery/delete/{id}', 'admin.common.gallery.delete', '39f566f119be52a8a72b02536dd9fef8', 'admin', NULL, 1, '2020-06-02 15:31:30', '2020-06-02 15:31:30'),
(204, 'admin/common/gallery/serialUpdate', 'admin.common.gallery.serialupdate', 'ecda5b1c2c3bc5a26175a71d75d555e9', 'admin', NULL, 1, '2020-06-02 15:31:40', '2020-06-02 15:31:40'),
(205, 'admin/affiliate/payouts', 'admin.affiliate.payouts', '833b58825535f4a2ab923556bdfb2552', 'admin', NULL, 1, '2020-06-02 15:31:46', '2020-06-02 15:31:46'),
(206, 'admin/order/onbehalf', 'admin.order.onbehalf', '0333eed29e9e5a2438388058f445294b', 'admin', NULL, 1, '2020-06-02 15:31:52', '2020-06-02 15:31:52'),
(207, 'admin/order/createProduct', 'admin.order.create.product', '9f3b4d7f8533cb89d17058151fe923af', 'admin', NULL, 1, '2020-06-02 15:31:57', '2020-06-02 15:31:57'),
(208, 'admin/order/updateCartProductPrice', 'admin.order.updateProductPrice', '7df605dd6fed54c7f3e630fd55a1b4d6', 'admin', NULL, 1, '2020-06-02 15:32:03', '2020-06-02 15:32:03'),
(209, 'admin/order/updateDeliveryCharge', 'admin.order.updateDeliveryCharge', 'f7e91251f932ce171b1cc005ded0f4ac', 'admin', NULL, 1, '2020-06-02 15:32:10', '2020-06-02 15:32:10'),
(210, 'dashboard', 'dashboarddashboard', '1c7f7ab511f70c343b91edc6632d858b', 'admin', NULL, 1, '2020-06-02 15:32:53', '2020-06-02 15:32:53'),
(211, 'menus', 'menusmenus', '10e1ac18f37c9710f2576a6abee8aadc', 'admin', NULL, 1, '2020-06-02 15:32:57', '2020-06-02 15:32:57'),
(212, 'widgets', 'widgets', '3aa652f41d8b4a23e17937149c784868', 'admin', NULL, 1, '2020-06-02 15:33:01', '2020-06-02 15:33:01'),
(213, 'widget_save', 'widget_savewidget_save', '5d90701f3e4d98da772512ac538c4686', 'admin', NULL, 1, '2020-06-02 15:33:05', '2020-06-02 15:33:05'),
(214, 'edit_widget/{id}', 'edit_widgetedit_widget', '4a75be365728d493e64d5697159e6102', 'admin', NULL, 1, '2020-06-02 15:33:08', '2020-06-02 15:33:08'),
(215, 'widget/{id}/update', 'widget/{id}/updatewidget_update_save', 'bd6f00e4382347ab217212d1dc44da76', 'admin', NULL, 1, '2020-06-02 15:33:14', '2020-06-02 15:33:14'),
(216, 'widget_delete/{id}', 'delete_widget', 'fe59966f9b74bb4ee66c145f87cc64ac', 'admin', NULL, 1, '2020-06-02 15:33:17', '2020-06-02 15:33:17'),
(217, 'variations', 'variationsvariations', 'da1e635a457204c4bc7358995beaf4fe', 'admin', NULL, 1, '2020-06-02 15:33:21', '2020-06-02 15:33:21'),
(218, 'add_variation', 'add_variationadd_variation', '1d6698817a4623c6e1a7ab407d5f9818', 'admin', NULL, 1, '2020-06-02 15:33:34', '2020-06-02 15:33:34'),
(219, 'variation_save', 'variation_savevariation_save', '445c3d300d78659281ab494df636246a', 'admin', NULL, 1, '2020-06-02 15:33:38', '2020-06-02 15:33:38'),
(220, 'edit_variation/{id}', 'edit_variationedit_variation', '9fb01dc798e3acbf66310d76da389df8', 'admin', NULL, 1, '2020-06-02 15:33:41', '2020-06-02 15:33:41'),
(221, 'variation/{id}/update', 'variation/{id}/updatevariation_update_save', '1be6df9f364df9eb155cf77267e14397', 'admin', NULL, 1, '2020-06-02 15:33:49', '2020-06-02 15:33:49'),
(222, 'variation_delete/{id}', 'delete_variation', '9bca882332d339556c50ede10f5c60ca', 'admin', NULL, 1, '2020-06-02 15:33:52', '2020-06-02 15:33:52'),
(223, 'terms', 'terms', '6f493bf6f3beb9d46a9c649d2f7ddd29', 'admin', NULL, 1, '2020-06-02 15:34:03', '2020-06-02 15:34:03'),
(224, 'term_save', 'term_saveterm_save', '3f34e3371e4fe5d20261b3264ebe7679', 'admin', NULL, 1, '2020-06-02 15:34:05', '2020-06-02 15:34:05'),
(225, 'edit_term/{id}', 'edit_termedit_term', 'c50fcb51ec509fa6319e537cbdb6e875', 'admin', NULL, 1, '2020-06-02 15:34:08', '2020-06-02 15:34:08'),
(226, 'term/{id}/update', 'term/{id}/updateterm_update_save', 'f38812a730dc3ec8ee692419a0fa1404', 'admin', NULL, 1, '2020-06-02 15:34:11', '2020-06-02 15:34:11'),
(227, 'delete_term/{id}', 'delete_termdelete_term', '3309103bf18433e8d1322a2c950be200', 'admin', NULL, 1, '2020-06-02 15:34:16', '2020-06-02 15:34:16'),
(228, 'get_categories_on_search', 'get_categories_on_searchget_categories_on_search', '815c3b5aa1126370b492b9920af443ad', 'admin', NULL, 1, '2020-06-02 15:34:22', '2020-06-02 15:34:22'),
(229, 'banks', 'banksbanks', '5466e18592fa66f517ca33fc8842ae72', 'admin', NULL, 1, '2020-06-02 15:34:27', '2020-06-02 15:34:27'),
(230, 'bank_save', 'bank_savebank_save', 'a2f6e4614e76ebf05184c5169fdb5536', 'admin', NULL, 1, '2020-06-02 15:34:31', '2020-06-02 15:34:31'),
(231, 'edit_bank/{id}', 'edit_bankedit_bank', '8221dc8c320ae4223304eec1e95dff69', 'admin', NULL, 1, '2020-06-02 15:34:41', '2020-06-02 15:34:41'),
(232, 'bank/{id}/update', 'bank/{id}/updatebank_update_save', 'acf506723078f6970caa3fd137a1764d', 'admin', NULL, 1, '2020-06-02 15:34:45', '2020-06-02 15:34:45'),
(233, 'delete_bank/{id}', 'delete_bankdelete_bank', '737fad8cf09a80d44c728e23463727d8', 'admin', NULL, 1, '2020-06-02 15:34:49', '2020-06-02 15:34:49'),
(234, 'multiplepricingphoto', 'multiplepricingphoto', '3532333a1d84fbbee1932525b536304e', 'admin', NULL, 1, '2020-06-02 15:34:56', '2020-06-02 15:34:56'),
(235, 'products', 'productsproducts', 'e8cf90617299b1bbc66aca0953af35db', 'admin', NULL, 1, '2020-06-02 15:35:10', '2020-06-02 15:35:10'),
(236, 'draft_products', 'draft_productsdraft_products', '130140fc883609f0a76cac4418fc6ca2', 'admin', NULL, 1, '2020-06-02 15:35:14', '2020-06-02 15:35:14'),
(237, 'products/{id}/sales_report', 'products.sales_report', '859d8d9ae4da587d17ff833f6092d62b', 'admin', NULL, 1, '2020-06-02 15:35:23', '2020-06-02 15:35:23'),
(238, 'never_sold', 'never_sold', 'ef019addccb365379de369be436c5fcf', 'admin', NULL, 1, '2020-06-02 15:35:29', '2020-06-02 15:35:29'),
(239, 'products_express_delivery', 'products_express_deliveryproducts_express_delivery', 'ccd3b60a27756c461aec5ab0665cd4dc', 'admin', NULL, 1, '2020-06-02 15:35:35', '2020-06-02 15:35:35'),
(240, 'products_enable_comment', 'products_enable_commentproducts_enable_comment', '700b117ece72198bf9a2f59875fb43f3', 'admin', NULL, 1, '2020-06-02 15:35:48', '2020-06-02 15:35:48'),
(241, 'products_enable_review', 'products_enable_reviewproducts_enable_review', '1650182ed08fd61ac533b4e71eceee88', 'admin', NULL, 1, '2020-06-02 15:35:51', '2020-06-02 15:35:51'),
(242, 'products_new_arrival', 'products_new_arrivalproducts_new_arrival', '55078e63f33ba40ad0c280cae496cf8a', 'admin', NULL, 1, '2020-06-02 15:35:55', '2020-06-02 15:35:55'),
(243, 'products_best_selling', 'products_best_sellingproducts_best_selling', '813e7e24fc6ee39bd1d263ed340be2d1', 'admin', NULL, 1, '2020-06-02 15:36:01', '2020-06-02 15:36:01'),
(244, 'products_multi_priceing', 'products_multi_priceingproducts_multi_priceing', 'c5b42abe3337c65c239a751e53534dc0', 'admin', NULL, 1, '2020-06-02 15:36:05', '2020-06-02 15:36:05'),
(245, 'products_recommended', 'products_recommendedproducts_recommended', '9675c2450b8e43aeeb2662566654bde1', 'admin', NULL, 1, '2020-06-02 15:36:09', '2020-06-02 15:36:09'),
(246, 'products_disable_buy', 'products_disable_buyproducts_disable_buy', '46b098c303dfbd98a0b8e79bfce92040', 'admin', NULL, 1, '2020-06-02 15:36:14', '2020-06-02 15:36:14'),
(247, 'add_product', 'add_productadd_product', '1912f7f6477d1410d8dd044a3f6450ae', 'admin', NULL, 1, '2020-06-02 15:36:37', '2020-06-02 15:36:37'),
(248, 'product_save', 'product_saveproduct_save', '6a7db6dcb8858bdd8eb2666b8d02893c', 'admin', NULL, 1, '2020-06-02 15:36:43', '2020-06-02 15:36:43'),
(249, 'product_seo_save', 'product_seo_saveproduct_seo_save', '4c6b2d9211b839abc15877f3d81c6829', 'admin', NULL, 1, '2020-06-02 15:36:51', '2020-06-02 15:36:51'),
(250, 'dashboard', 'dashboard', 'dc7161be3dbf2250c8954e560cc35060', 'admin', NULL, 1, '2020-06-02 15:41:41', '2020-06-02 15:41:41'),
(251, 'menus', 'menus', '81ca0b7c951be89184c130d2860a5b00', 'admin', NULL, 1, '2020-06-02 15:41:44', '2020-06-02 15:41:44'),
(252, 'widget_save', 'widget_save', 'cc39c4f7691f93df5093adf40b061860', 'admin', NULL, 1, '2020-06-02 15:41:47', '2020-06-02 15:41:47'),
(253, 'edit_widget/{id}', 'edit_widget', 'd2a93a0c2bc759186125e705459df842', 'admin', NULL, 1, '2020-06-02 15:41:49', '2020-06-02 15:41:49'),
(254, 'widget/{id}/update', 'widget_update_save', '71c26178794c089905314b0b21f8dc1a', 'admin', NULL, 1, '2020-06-02 15:41:52', '2020-06-02 15:41:52'),
(255, 'widget_delete/{id}', 'widget_delete', '463bc942084cd7e8470bed7032883859', 'admin', NULL, 1, '2020-06-02 15:42:34', '2020-06-02 15:42:34'),
(256, 'variations', 'variations', '2598fe3ec117781a941e0e8ff655eaec', 'admin', NULL, 1, '2020-06-02 15:44:52', '2020-06-02 15:44:52'),
(257, 'add_variation', 'add_variation', '5ca308df3b5e2b4cdd6e33ecc9e22691', 'admin', NULL, 1, '2020-06-02 15:44:54', '2020-06-02 15:44:54'),
(258, 'variation_save', 'variation_save', '2fabdae1a43f0331a0a8583f61c51fdd', 'admin', NULL, 1, '2020-06-02 15:44:57', '2020-06-02 15:44:57'),
(259, 'edit_variation/{id}', 'edit_variation', 'b0db2f1fe6c2ea81758884ad1dab797e', 'admin', NULL, 1, '2020-06-02 15:44:59', '2020-06-02 15:44:59'),
(260, 'variation/{id}/update', 'variation_update_save', '8978a93541a5492b1d8cec397228559c', 'admin', NULL, 1, '2020-06-02 15:45:02', '2020-06-02 15:45:02'),
(261, 'variation_delete/{id}', 'variation_delete', '24d539b0967c23ab2c5482d0d28018b0', 'admin', NULL, 1, '2020-06-02 15:45:05', '2020-06-02 15:45:05'),
(262, 'term_save', 'term_save', 'b3292edf090ecefbd985faa24ab55cb6', 'admin', NULL, 1, '2020-06-02 15:45:09', '2020-06-02 15:45:09'),
(263, 'edit_term/{id}', 'edit_term', '02fee6feb7f4d9d8b0499b5dbc418f65', 'admin', NULL, 1, '2020-06-02 15:45:16', '2020-06-02 15:45:16'),
(264, 'term/{id}/update', 'term_update_save', '313fdc0ebf4bd8c3da8aa84436f83929', 'admin', NULL, 1, '2020-06-02 15:45:19', '2020-06-02 15:45:19'),
(265, 'delete_term/{id}', 'delete_term', 'f980e80900e079013a24ef8918c1e55f', 'admin', NULL, 1, '2020-06-02 15:45:22', '2020-06-02 15:45:22'),
(266, 'get_categories_on_search', 'get_categories_on_search', '892d1c170d06abaf82ac5aad13a308ee', 'admin', NULL, 1, '2020-06-02 15:45:24', '2020-06-02 15:45:24'),
(267, 'banks', 'banks', 'f3301f2aa6c52eb78ee4b79832496212', 'admin', NULL, 1, '2020-06-02 15:45:27', '2020-06-02 15:45:27'),
(268, 'bank_save', 'bank_save', '7c9a3debe466cd7d578beea78191296c', 'admin', NULL, 1, '2020-06-02 15:45:29', '2020-06-02 15:45:29'),
(269, 'edit_bank/{id}', 'edit_bank', 'a2bccff4e20101279795e5ecf188d4b6', 'admin', NULL, 1, '2020-06-02 15:45:32', '2020-06-02 15:45:32'),
(270, 'bank/{id}/update', 'bank_update_save', 'f022d61787676ed2620680982899ab88', 'admin', NULL, 1, '2020-06-02 15:45:36', '2020-06-02 15:45:36'),
(271, 'delete_bank/{id}', 'delete_bank', 'dfc29ddaf2013d4b46bad2ad46e0d433', 'admin', NULL, 1, '2020-06-02 15:46:12', '2020-06-02 15:46:12'),
(272, 'products', 'products', '86024cad1e83101d97359d7351051156', 'admin', NULL, 1, '2020-06-03 23:34:30', '2020-06-03 23:34:30'),
(273, 'draft_products', 'draft_products', '154492cdebd3b2184dc9efa860026aa5', 'admin', NULL, 1, '2020-06-03 23:34:35', '2020-06-03 23:34:35'),
(274, 'products_express_delivery', 'products_express_delivery', '6ef09429b2d00fbccc92c7e40965091f', 'admin', NULL, 1, '2020-06-03 23:34:50', '2020-06-03 23:34:50'),
(275, 'products_enable_comment', 'products_enable_comment', 'ec6e144d04a63bd06e8714df4fb1fe4f', 'admin', NULL, 1, '2020-06-03 23:34:53', '2020-06-03 23:34:53'),
(276, 'products_enable_review', 'products_enable_review', '26338193010bca8e5f70635391f3f0f6', 'admin', NULL, 1, '2020-06-03 23:34:57', '2020-06-03 23:34:57'),
(277, 'products_new_arrival', 'products_new_arrival', '8be990a29d71daf5999c90a657f16628', 'admin', NULL, 1, '2020-06-03 23:35:00', '2020-06-03 23:35:00'),
(278, 'products_best_selling', 'products_best_selling', '8cb5a66e71c1c66c15c9e6162eb90e99', 'admin', NULL, 1, '2020-06-03 23:35:04', '2020-06-03 23:35:04'),
(279, 'products_multi_priceing', 'products_multi_priceing', '95e79fb8e1b4cf369eba330f882406e5', 'admin', NULL, 1, '2020-06-03 23:35:08', '2020-06-03 23:35:08'),
(280, 'products_recommended', 'products_recommended', 'ad97aaaa02d88756affd159d3b303564', 'admin', NULL, 1, '2020-06-03 23:35:11', '2020-06-03 23:35:11'),
(281, 'products_disable_buy', 'products_disable_buy', '00ba734d0dd1a9cfa0a9cc66c62cb1f2', 'admin', NULL, 1, '2020-06-03 23:35:16', '2020-06-03 23:35:16'),
(282, 'add_product', 'add_product', '8a125d242898a401b024e7c50d3de8d0', 'admin', NULL, 1, '2020-06-03 23:35:22', '2020-06-03 23:35:22'),
(283, 'product_save', 'product_save', 'a0194386ef07a0d4ca06f6fe2d5c1d75', 'admin', NULL, 1, '2020-06-03 23:35:27', '2020-06-03 23:35:27'),
(284, 'product_seo_save', 'product_seo_save', 'e190bce008917e2fcf577cdf125446ef', 'admin', NULL, 1, '2020-06-03 23:35:32', '2020-06-03 23:35:32'),
(285, 'edit_product/{id}', 'edit_product', '3297120fd0995d3ee3e4f47e17f68eb8', 'admin', NULL, 1, '2020-06-03 23:35:37', '2020-06-03 23:35:37'),
(286, 'product/{id}/update', 'product_update_save', '01a1d2ffba7aafad59eb7a37fda1f7d5', 'admin', NULL, 1, '2020-06-03 23:35:41', '2020-06-03 23:35:41'),
(287, 'product_delivery_location', 'product_update_delivery_location', 'f298a2b1346f6ba1d789d03358ebbd09', 'admin', NULL, 1, '2020-06-03 23:41:16', '2020-06-03 23:41:16'),
(288, 'attribute_based_information/{id}/update', 'attribute_based_information', 'ac7018380c62544f967653f9d25b51d4', 'admin', NULL, 1, '2020-06-03 23:41:27', '2020-06-03 23:41:27'),
(289, 'get_products_on_search', 'get_products_on_search', '2332bb18b3552881d36dae70173d8dad', 'admin', NULL, 1, '2020-06-03 23:41:31', '2020-06-03 23:41:31'),
(290, 'delete_product/{id}', 'delete_product', '79f3c68f6116c114104b0310bbc85853', 'admin', NULL, 1, '2020-06-03 23:41:35', '2020-06-03 23:41:35'),
(291, 'related_products_getter', 'related_products_getter', '8013383f5ccdf8bdec2b6167580eaa73', 'admin', NULL, 1, '2020-06-03 23:41:38', '2020-06-03 23:41:38'),
(292, 'add_related_products', 'add_related_products', 'ac4d50976379c2e5f6094305e55c06cd', 'admin', NULL, 1, '2020-06-03 23:41:43', '2020-06-03 23:41:43'),
(293, 'affiliator/payout/store', 'affiliator.payout.store', 'cb518121cff9ab84ce3992d2ce61182e', 'affiliator', NULL, 1, '2020-06-05 23:03:35', '2020-06-05 23:03:35'),
(294, 'affiliator/payout', 'affiliator.payout.index', 'ff4e19acf3f5bb9be46015e5ad4911d9', 'affiliator', NULL, 1, '2020-06-05 23:03:41', '2020-06-05 23:03:41'),
(295, 'affiliator/saleslist', 'affiliator.saleslist.index', '9fb8cdf8c941713a7467df4d79188b2e', 'affiliator', NULL, 1, '2020-06-05 23:03:47', '2020-06-05 23:03:47'),
(296, 'affiliator/dashboard', 'affiliator.dashboard.index', '02fa20cb498bae46ce67e5e464befa0e', 'affiliator', NULL, 1, '2020-06-05 23:03:54', '2020-06-05 23:03:54'),
(297, 'showroom/shipment/status/{id}/{status}', 'showroom.shipment.status', '0c1f712f0ce5330b5f2573ded7ac89ce', 'showroom', NULL, 1, '2020-06-05 23:04:01', '2020-06-05 23:04:01'),
(298, 'showroom/shipments/{id}', 'showroom.shipment.show', 'e7cd9b58c0ce799446fd2c5e2a3b3072', 'showroom', NULL, 1, '2020-06-05 23:04:07', '2020-06-05 23:04:07'),
(299, 'showroom/shipments', 'showroom.shipment.index', '448e03d15fdcfbd048ac49c3dd9a7615', 'showroom', NULL, 1, '2020-06-05 23:04:14', '2020-06-05 23:04:14'),
(300, 'showroom/dashboard', 'showroom.dashboard', '5d44e314b63a9bffde44770d02448958', 'showroom', NULL, 1, '2020-06-05 23:04:42', '2020-06-05 23:04:42'),
(301, 'vendor/product/export_products_multipricing', 'vendor.product.export_products_multipricing', '622334ea158dae5500828053cee2e314', 'vendor', NULL, 1, '2020-06-05 23:04:51', '2020-06-05 23:04:51'),
(302, 'vendor/product/import_products_multipricing_save', 'vendor.product.import_products_multipricing', '4b64797833fce61e8d6584f1c6983a43', 'vendor', NULL, 1, '2020-06-05 23:04:58', '2020-06-05 23:04:58'),
(303, 'vendor/product/import_products_save', 'vendor.product.import_products', '6a68ec1958f25b9a94bb426717d661e5', 'vendor', NULL, 1, '2020-06-05 23:05:05', '2020-06-05 23:05:05'),
(304, 'add_product_categories', 'add_product_categories', '6e93adc9a8a76a8817cc24f7dbe95817', 'admin', NULL, 1, '2020-06-05 23:05:29', '2020-06-05 23:05:29'),
(305, 'add_product_images', 'add_product_images', '944a894982491b9725e90b85e80c4705', 'admin', NULL, 1, '2020-06-05 23:05:36', '2020-06-05 23:05:36'),
(306, 'add_product_price_combination', 'add_product_price_combination', '1efdfbe68b3dc530a417bf0b9beb6801', 'admin', NULL, 1, '2020-06-05 23:05:42', '2020-06-05 23:05:42'),
(307, 'get_colors_sizes', 'get_colors_sizes', 'f2ae1bfc675973c8e0c0de05953a212b', 'admin', NULL, 1, '2020-06-05 23:05:49', '2020-06-05 23:05:49'),
(308, 'add_more_bank', 'add_more_bank', 'c531832fc6853bc156bc3a98aab78006', 'admin', NULL, 1, '2020-06-05 23:05:53', '2020-06-05 23:05:53'),
(309, 'save_variation', 'save_variation', '861d650184a939795dabba414cab6a31', 'admin', NULL, 1, '2020-06-05 23:06:01', '2020-06-05 23:06:01'),
(310, 'save_emi_data', 'save_emi_data', 'b362bb60b64d3fa1e81335aaec3be77e', 'admin', NULL, 1, '2020-06-05 23:06:06', '2020-06-05 23:06:06'),
(311, 'delete_relatedproduct/{id}', 'delete_relatedproduct', '67f1589c379916ec41bd2d1b67b5e752', 'admin', NULL, 1, '2020-06-05 23:06:11', '2020-06-05 23:06:11'),
(312, 'delete_productcategory/{id}', 'delete_productcategory', 'a299eaff0835e56451873917df640c0c', 'admin', NULL, 1, '2020-06-05 23:06:14', '2020-06-05 23:06:14'),
(313, 'delete_productimage/{id}', 'delete_productimage', '76268eb6a0e612d7ccdf2c649ec2b3f1', 'admin', NULL, 1, '2020-06-05 23:07:07', '2020-06-05 23:07:07'),
(314, 'delete_productpricecombination/{id}', 'delete_productpricecombination', 'ee4c57403a53d7af134061fcc6fa4d42', 'admin', NULL, 1, '2020-06-05 23:07:12', '2020-06-05 23:07:12'),
(315, 'delete_pcomdata/{id}', 'delete_pcomdata', '3081e059eb9557b7f4fa85f5b9405238', 'admin', NULL, 1, '2020-06-05 23:07:17', '2020-06-05 23:07:17'),
(316, 'delete_emi/{id}', 'delete_emi', 'e18166eeff42eb2e9884a276b7c2b6cd', 'admin', NULL, 1, '2020-06-05 23:07:23', '2020-06-05 23:07:23'),
(317, 'edit_multiprice_data', 'edit_multiprice_data', '05d7b15bc1d9a5460739edad0b87e548', 'admin', NULL, 1, '2020-06-05 23:07:27', '2020-06-05 23:07:27'),
(318, 'update_multiprice_data', 'update_multiprice_data', 'ba3be585ae4566a6df238d6affba62e6', 'admin', NULL, 1, '2020-06-05 23:07:31', '2020-06-05 23:07:31'),
(319, 'att_check_add', 'att_check_add', '05d578d2c2d588777368f4a26dc3ba0f', 'admin', NULL, 1, '2020-06-05 23:07:38', '2020-06-05 23:07:38'),
(320, 'att_check_del', 'att_check_del', '787a8d4e5dc31428a48a057d74badf69', 'admin', NULL, 1, '2020-06-05 23:07:42', '2020-06-05 23:07:42'),
(321, 'is_attgroup_active', 'is_attgroup_active', '74f567bc644bb1bd21b30a1d449fcb36', 'admin', NULL, 1, '2020-06-05 23:07:46', '2020-06-05 23:07:46'),
(322, 'is_main_image', 'is_main_image', '365ae3d2645d4741b5a334a733035c33', 'admin', NULL, 1, '2020-06-05 23:07:53', '2020-06-05 23:07:53');
INSERT INTO `routes` (`id`, `route_uri`, `route_name`, `route_hash`, `in_group`, `description`, `status`, `created_at`, `updated_at`) VALUES
(323, 'import_products_view', 'import_products_view', '47a1384f6ef2d447a1f10d1a4181b38a', 'admin', NULL, 1, '2020-06-05 23:08:01', '2020-06-05 23:08:01'),
(324, 'import_products_save', 'import_products', 'ccd451457ef7f440e039f10935465070', 'admin', NULL, 1, '2020-06-05 23:08:06', '2020-06-05 23:08:06'),
(325, 'import_products_multipricing_save', 'import_products_multipricing', '5bb117891e594f4e3b665d54a6b240a1', 'admin', NULL, 1, '2020-06-05 23:08:11', '2020-06-05 23:08:11'),
(326, 'export_products', 'export_products', '63e5cca23a974ff88d8f2548069adcdc', 'admin', NULL, 1, '2020-06-05 23:08:17', '2020-06-05 23:08:17'),
(327, 'export_products_multipricing', 'export_products_multipricing', '307405466a6b5fc75059b197a5370cda', 'admin', NULL, 1, '2020-06-05 23:08:21', '2020-06-05 23:08:21'),
(328, 'check_if_url_exists', 'check_if_url_exists', '7f02ba4271e78ea61f3d008b9bb11bbb', 'admin', NULL, 1, '2020-06-05 23:08:56', '2020-06-05 23:08:56'),
(329, 'check_if_cat_url_exists', 'check_if_cat_url_exists', '299f383948dfdef015e48578ccbccc50', 'admin', NULL, 1, '2020-06-05 23:09:01', '2020-06-05 23:09:01'),
(330, 'get_product_json_data', 'get_product_json_data', 'af9cd61ede76262b5ed4761f460f0217', 'admin', NULL, 1, '2020-06-05 23:09:06', '2020-06-05 23:09:06'),
(331, 'search_orders', 'search_orders', 'e3521411b6f23bee9b5b3094d9897322', 'admin', NULL, 1, '2020-06-05 23:09:11', '2020-06-05 23:09:11'),
(332, 'save_or_update_delivery_date', 'save_or_update_delivery_date', '289a8908a4256a33efe6ea941634e7c8', 'admin', NULL, 1, '2020-06-05 23:09:15', '2020-06-05 23:09:15'),
(333, 'orders', 'orders', '12c500ed0b7879105fb46af0f246be87', 'admin', NULL, 1, '2020-06-05 23:09:21', '2020-06-05 23:09:21'),
(334, 'orders/date_between', 'orders_date_between', '4217ba437fabb94e970485363d2d94a4', 'admin', NULL, 1, '2020-06-05 23:09:27', '2020-06-05 23:09:27'),
(335, 'orders/placed_cod', 'placed_cash_on_delivery', 'f8aa94a9c013a754d609c582496634be', 'admin', NULL, 1, '2020-06-05 23:09:31', '2020-06-05 23:09:31'),
(336, 'orders/placed_opo', 'placed_online_payment', '7e08e8d2fe533979be7c8b526dfe67b1', 'admin', NULL, 1, '2020-06-05 23:09:35', '2020-06-05 23:09:35'),
(337, 'orders/placed', 'orders_placed', '7483e0b6c1ab6482f974bb64e6035542', 'admin', NULL, 1, '2020-06-05 23:09:39', '2020-06-05 23:09:39'),
(338, 'orders/production', 'orders_production', '366eddb677d9b3ab936d05512ba56f4b', 'admin', NULL, 1, '2020-06-05 23:09:43', '2020-06-05 23:09:43'),
(339, 'orders/processing', 'orders_processing', 'e7fa2a08b7c562a0c1480b4d2a0c0502', 'admin', NULL, 1, '2020-06-05 23:09:47', '2020-06-05 23:09:47'),
(340, 'orders/distribution', 'orders_distribution', 'ca6843c3698e82779016ae8c30eeb240', 'admin', NULL, 1, '2020-06-05 23:09:53', '2020-06-05 23:09:53'),
(341, 'orders/done', 'orders_done', 'de845a0b6584678a1d904e2e4361fc61', 'admin', NULL, 1, '2020-06-05 23:09:57', '2020-06-05 23:09:57'),
(342, 'orders/refund', 'orders_refund', '41e1f6de520ac9856e9f3c9831a7e4f6', 'admin', NULL, 1, '2020-06-05 23:10:02', '2020-06-05 23:10:02'),
(343, 'orders/cod', 'orders_cod', 'd5d5f3607c07173f84341e47ae86a2f6', 'admin', NULL, 1, '2020-06-05 23:10:06', '2020-06-05 23:10:06'),
(344, 'orders/deleted', 'orders_deleted', '36156d33eeddfdba8dd99351e6c8d279', 'admin', NULL, 1, '2020-06-05 23:10:11', '2020-06-05 23:10:11'),
(345, 'orders/temporary', 'orders_temporary', 'e4681c41da55e01af8854b8a49cc0024', 'admin', NULL, 1, '2020-06-05 23:10:16', '2020-06-05 23:10:16'),
(346, 'orders/move', 'orders_move', 'b829cc324d8e41ce7604d0e40d06dde8', 'admin', NULL, 1, '2020-06-05 23:10:23', '2020-06-05 23:10:23'),
(347, 'orders/bulk_move', 'orders_bulk_move', 'dfeeaafe7403e4345661677c3931fc38', 'admin', NULL, 1, '2020-06-05 23:11:34', '2020-06-05 23:11:34'),
(348, 'most_sold', 'most_sold', '3e85cdb20501827585230e1fc05c1f0b', 'admin', NULL, 1, '2020-06-05 23:11:38', '2020-06-05 23:11:38'),
(349, 'export_orders', 'export_orders', '4cbc37a8e779d4544d2f8b718ce4d2f4', 'admin', NULL, 1, '2020-06-05 23:11:43', '2020-06-05 23:11:43'),
(350, 'prebooking', 'prebooking', '3be64eda35e05f25a371e98e2830d1d0', 'admin', NULL, 1, '2020-06-05 23:11:48', '2020-06-05 23:11:48'),
(351, 'export_newsletters', 'export_newsletters', '15700cc56f6eb84305301a76a57f9af8', 'admin', NULL, 1, '2020-06-05 23:11:52', '2020-06-05 23:11:52'),
(352, 'attributes', 'attributes', '736b91750e516139acc13c5eb6564f92', 'admin', NULL, 1, '2020-06-05 23:11:56', '2020-06-05 23:11:56'),
(353, 'add_attributes/{id}', 'add_attributes', 'a81147f1f29ac19268f7748f22ce4ed3', 'admin', NULL, 1, '2020-06-05 23:12:00', '2020-06-05 23:12:00'),
(354, 'attribute_save', 'attribute_save', '0596977e0b21710fd1bed5f5f7940acd', 'admin', NULL, 1, '2020-06-05 23:12:05', '2020-06-05 23:12:05'),
(355, 'edit_attribute/{id}', 'edit_attribute', 'e9375194ef3b761148d86360e8d9d232', 'admin', NULL, 1, '2020-06-05 23:12:08', '2020-06-05 23:12:08'),
(356, 'attribute/{id}/update', 'attribute_update_save', 'db4036dd09fb55554fe016a914c0d92c', 'admin', NULL, 1, '2020-06-05 23:12:12', '2020-06-05 23:12:12'),
(357, 'delete_attribute', 'delete_attribute', '377563ab32a3b63624ecc79a04e83b96', 'admin', NULL, 1, '2020-06-05 23:12:16', '2020-06-05 23:12:16'),
(358, 'sortable_update', 'sortable_update', 'b33da28ecdd7f451478f7ee552e7df50', 'admin', NULL, 1, '2020-06-05 23:12:20', '2020-06-05 23:12:20'),
(359, 'attgroups', 'attgroups', 'bce1cfdc8e0c87f6aef98fe23bf47a90', 'admin', NULL, 1, '2020-06-05 23:12:26', '2020-06-05 23:12:26'),
(360, 'add_attgroup', 'add_attgroup', 'c2d54260ea1df6fc868cfdd84d90e71a', 'admin', NULL, 1, '2020-06-05 23:12:31', '2020-06-05 23:12:31'),
(361, 'add_attgroup_save', 'add_attgroup_save', 'b57823b0b29ec774e601d85797415ca0', 'admin', NULL, 1, '2020-06-05 23:13:12', '2020-06-05 23:13:12'),
(362, 'edit_att_group/{id}', 'edit_att_group', 'fe68eda3485f5f6ce08fcf054b32ece5', 'admin', NULL, 1, '2020-06-05 23:13:17', '2020-06-05 23:13:17'),
(363, 'attgroup/{id}/update', 'attgroup_update_save', '122b76a9a884dc319b1ee77bccd2b36a', 'admin', NULL, 1, '2020-06-05 23:13:22', '2020-06-05 23:13:22'),
(364, 'attribute_delete/{id}', 'attribute_delete', '5ebfb5176771317aed24cee58667bc36', 'admin', NULL, 1, '2020-06-05 23:13:27', '2020-06-05 23:13:27'),
(365, 'delete_attgroup/{id}', 'delete_attgroup', '47b84c0604863dd06032969e8f327dab', 'admin', NULL, 1, '2020-06-05 23:13:33', '2020-06-05 23:13:33'),
(366, 'settings', 'settings', '2e5d8aa3dfa8ef34ca5131d20f9dad51', 'admin', NULL, 1, '2020-06-05 23:14:12', '2020-06-05 23:14:12'),
(367, 'edit_setting/{id}', 'edit_setting', '61d3fb6174b67aca9aa64b878f1135fb', 'admin', NULL, 1, '2020-06-05 23:14:22', '2020-06-05 23:14:22'),
(368, 'homesetting_save', 'setting_save', '9a70cfe60e27a99a9e46a3d1658b97da', 'admin', NULL, 1, '2020-06-05 23:14:26', '2020-06-05 23:14:26'),
(369, 'setting/{id}/update', 'setting_update_save', '5c98a9555848a859ae84dc91111c0fb9', 'admin', NULL, 1, '2020-06-05 23:14:33', '2020-06-05 23:14:33'),
(370, 'homesetting_delete/{id}', 'homesetting_delete', 'cb737c562d28646655810b2e27c2891d', 'admin', NULL, 1, '2020-06-05 23:14:41', '2020-06-05 23:14:41'),
(371, 'payment_settings', 'payment_settings', '484d3e112fb89732a762e7b3b5b02694', 'admin', NULL, 1, '2020-06-05 23:14:47', '2020-06-05 23:14:47'),
(372, 'payment_setting_save', 'payment_setting_save', '9bc5f40a3371aa7d0f4b616586031b67', 'admin', NULL, 1, '2020-06-05 23:14:53', '2020-06-05 23:14:53'),
(373, 'payment_edit_setting/{id}', 'payment_edit_setting', 'a63d8811e33c47ebff8c3be9236b8f7c', 'admin', NULL, 1, '2020-06-05 23:14:59', '2020-06-05 23:14:59'),
(374, 'payment_setting/{id}/update', 'payment_setting', '6302dbaba2384a7052656525a8900e30', 'admin', NULL, 1, '2020-06-05 23:16:09', '2020-06-05 23:16:09'),
(375, 'payment_setting_delete/{id}', 'payment_setting_delete', '78f77157b5b05948a660f140dabee723', 'admin', NULL, 1, '2020-06-05 23:16:14', '2020-06-05 23:16:14'),
(376, 'clear-cache', 'clear-cache', '3e3c649b1a26c50ecbfccc4c01f0f0df', 'admin', NULL, 1, '2020-06-05 23:16:20', '2020-06-05 23:16:20'),
(377, 'optimize', 'optimize', 'c14f7a753888287112058264fa40b72d', 'admin', NULL, 1, '2020-06-05 23:16:24', '2020-06-05 23:16:24'),
(378, 'route-cache', 'route-cache', '4c5987522334764b4f535446ef6d07a8', 'admin', NULL, 1, '2020-06-05 23:16:28', '2020-06-05 23:16:28'),
(379, 'route-clear', 'route-clear', 'a772619de7275abc687130fc96c6ac05', 'admin', NULL, 1, '2020-06-05 23:16:33', '2020-06-05 23:16:33'),
(380, 'view-clear', 'view-clear', '065334eec543990a4117a0e7624cbe95', 'admin', NULL, 1, '2020-06-05 23:16:37', '2020-06-05 23:16:37'),
(381, 'config-clear', 'config-clear', '6faf09010cc7a88dfd855c64484345ad', 'admin', NULL, 1, '2020-06-05 23:16:41', '2020-06-05 23:16:41'),
(382, 'config-cache', 'config-cache', '0671390c52a04c9c4e8ffea88ebc9e24', 'admin', NULL, 1, '2020-06-05 23:16:46', '2020-06-05 23:16:46'),
(383, 'vendor/dashboard', 'vendor.dashboard.index', '0fec2b6a5cf3bc73c1c99e58782a2e09', 'vendor', NULL, 1, '2020-06-05 23:16:58', '2020-06-05 23:16:58'),
(384, 'vendor/settings', 'vendor.setting.index', '7a4a5b3a189122d1983689c190ed088c', 'vendor', NULL, 1, '2020-06-05 23:17:11', '2020-06-05 23:17:11'),
(385, 'vendor/settingVendor', 'vendor.setting.vendor', 'f80c9ed288235cd066ab3beed3a53443', 'vendor', NULL, 1, '2020-06-05 23:17:18', '2020-06-05 23:17:18'),
(386, 'vendor/site/content', 'vendor.site.content', '6ff1173aabc20ef4e1b4d6f495d571d8', 'vendor', NULL, 1, '2020-06-05 23:17:26', '2020-06-05 23:17:26'),
(387, 'vendor/site/saveSlider', 'vendor.site.saveSlider', '93809c33af7f3f829288004208841e7d', 'vendor', NULL, 1, '2020-06-05 23:17:56', '2020-06-05 23:17:56'),
(388, 'vendor/site/deleteSlider', 'vendor.site.deleteSlider', '6fba9ace2123e90fec4169b94d613a82', 'vendor', NULL, 1, '2020-06-05 23:18:02', '2020-06-05 23:18:02'),
(389, 'vendor/site/saveChoicesProducts', 'vendor.site.saveChoicesProducts', '4733362629c9378ced1375726fe44e0e', 'vendor', NULL, 1, '2020-06-05 23:18:09', '2020-06-05 23:18:09'),
(390, 'vendor/media', 'vendor.media.index', 'cbab40a986f5a9371e6f0f7169c3e765', 'vendor', NULL, 1, '2020-06-05 23:18:15', '2020-06-05 23:18:15'),
(391, 'vendor/order/index', 'vendor.order.index', '9b3b5a115de7b5be48b9385617258740', 'vendor', NULL, 1, '2020-06-05 23:18:22', '2020-06-05 23:18:22'),
(392, 'vendor/product/index', 'vendor.product.index', '5ecce48347a056b17dae28dcb9cbabef', 'vendor', NULL, 1, '2020-06-05 23:18:27', '2020-06-05 23:18:27'),
(393, 'vendor/product/create', 'vendor.product.create', 'c408d9288c1ff7968e9b77cd1c63b084', 'vendor', NULL, 1, '2020-06-05 23:18:34', '2020-06-05 23:18:34'),
(394, 'vendor/product/edit/{product_id}', 'vendor.product.edit', '97f0e63411a6becc868ac90c0ce27cb7', 'vendor', NULL, 1, '2020-06-05 23:18:41', '2020-06-05 23:18:41'),
(395, 'vendor/product/store', 'vendor.product.store', 'f1d3a617efc231aa068df0816bc73bb1', 'vendor', NULL, 1, '2020-06-05 23:18:47', '2020-06-05 23:18:47'),
(396, 'vendor/product/updateBasic/{product_id}', 'vendor.product.update.basic', '77faa3c885c8b0485716f745d20ecef6', 'vendor', NULL, 1, '2020-06-05 23:18:53', '2020-06-05 23:18:53'),
(397, 'vendor/product/export_products', 'vendor.product.export_products', 'ecc8fe2464271817cd4e61655207cb09', 'vendor', NULL, 1, '2020-06-05 23:19:00', '2020-06-05 23:19:00'),
(398, 'vendor/product/import_products_view', 'vendor.product.import_products_view', '8e58b6600b07938c894a211cb5634825', 'vendor', NULL, 1, '2020-06-05 23:19:06', '2020-06-05 23:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int UNSIGNED NOT NULL,
  `com_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `com_slogan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_eshtablished` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_licensecode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_logourl` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_headerurl` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_addressgooglemap` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_website` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_analytics` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `com_chat_box` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `com_metatitle` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_metadescription` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_metakeywords` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_workinghours` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminphone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminemail` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminphotourl` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_facebookpageid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_favicon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_timezone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_rate` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `com_name`, `com_slogan`, `com_eshtablished`, `com_licensecode`, `com_logourl`, `com_headerurl`, `com_phone`, `order_phone`, `com_email`, `com_address`, `com_addressgooglemap`, `com_website`, `com_analytics`, `com_chat_box`, `com_metatitle`, `com_metadescription`, `com_metakeywords`, `com_workinghours`, `com_adminname`, `com_adminphone`, `com_adminemail`, `com_adminphotourl`, `com_facebookpageid`, `com_favicon`, `com_timezone`, `tax_rate`, `created_at`, `updated_at`) VALUES
(1, 'Roydomi', 'Quick & Smart', '2019', 'License', 'https://rflbestbuy.com/secure/storage/uploads/fullsize/2020-01/best-buy-logo-newde9fe5a4dcebd65f12365b75204f23c5.png', 'https://rfldoor.com/storage/uploads/fullsize/2018-10/web-header-backgraound.png', '+88 09613 14 11 11 (9.00AM- 5.00PM) Any Business Day', '+88 09613 14 11 11', 'rfl908@rflgroupbd.com', 'PRAN-RFL Center, 105 Middle Badda, Dhaka -1212, Bangladesh<br/> Phone: +88-02-9881792- Ext : 880', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.045894644146!2d90.42315831483883!3d23.78137998457409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c796c76c1a', '/', '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-136584014-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-136584014-1\');\r\n</script>', '<!-- Start of REVE Chat Script-->\r\n <script type=\'text/javascript\'>\r\n window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account=\'8167451\';w.__revechat_version=2;\r\n   r._= []; var rc = d.createElement(\'script\'); rc.type = \'text/javascript\'; rc.async = true; rc.setAttribute(\'charset\', \'utf-8\');\r\n   rc.src = (\'https:\' == document.location.protocol ? \'https://\' : \'https://\') + \'static.revechat.com/widget/scripts/new-livechat.js?\'+new Date().getTime();\r\n   var s = d.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(rc, s);\r\n })(document, window);\r\n</script>\r\n<!-- End of REVE Chat Script -->', 'Welcome to RFLBestBuy.com', 'RFLBestBuy.com strives to be the best online shopping destination in Bangladesh featuring a wide range of category products. Experience fast, reliable and effortless online shopping with home', 'RFL Bestbuy, rflbestbuy.com, PVC & Plastic, Electronics, Mobile Phone, Furniture, household, Experience fast, reliable, effortless, online shopping,', '09-05', 'Kamruzzaman Khan', '01844602020', 'rfl908@rflgroupbd.com', 'https://rflbestbuy.com/secure/storage/uploads/fullsize/2019-02/fav.png', '324093091058711', '/secure/storage/uploads/fullsize/2019-03/logo.png', 'Asia/Dhaka', NULL, '2018-03-09 10:03:27', '2020-07-17 08:00:30');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int NOT NULL,
  `title` tinytext,
  `code` varchar(191) DEFAULT NULL,
  `content` text,
  `images` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `code`, `content`, `images`, `created_at`, `updated_at`) VALUES
(12, 'Homepage Slider', 'qsGpz', 'some description', '5457, 5520, 5748, 6232', '2019-09-29 06:13:12', '2020-02-18 02:40:25'),
(13, 'Mobile Sliders', 'zqFPl', 'Mobile Sliders', '5465, 5466, , 5807, 6154', '2019-11-02 08:20:21', '2020-01-27 05:49:29');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_theme` int NOT NULL DEFAULT '1',
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `cssid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cssclass` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `term_short_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `parent` int DEFAULT NULL,
  `connected_with` int DEFAULT NULL,
  `page_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_menu_icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_menu_arrow` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `with_sub_menu` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_menu_width` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `column_count` int DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `banner1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `term_seo_h1` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `term_seo_h2` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `term_seo_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `term_seo_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `term_seo_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `onpage_banner` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `name`, `seo_url`, `cat_theme`, `type`, `position`, `cssid`, `cssclass`, `description`, `term_short_description`, `parent`, `connected_with`, `page_image`, `thumb_image`, `home_image`, `term_menu_icon`, `term_menu_arrow`, `with_sub_menu`, `sub_menu_width`, `column_count`, `is_active`, `banner1`, `banner2`, `created_at`, `updated_at`, `term_seo_h1`, `term_seo_h2`, `term_seo_title`, `term_seo_description`, `term_seo_keywords`, `onpage_banner`) VALUES
(1, 'Products', 'products', 1, 'category', 1, 'products', 'products', 'Products', 'Products', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-15 23:33:56', '2020-07-15 23:33:56', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Posts', 'posts', 1, 'category', 2, 'posts', 'posts', 'Posts', 'Posts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-15 23:34:21', '2020-07-15 23:34:21', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Mens', 'mens', 1, 'category', 3, 'mens', 'mens', '<div class=\"sub-menu-list\"> <div class=\"sub-menu-content\" style=\"\"> <ul class=\"sub-menu-content-list menu-generator-space \"> <li style=\"\"><h6><a href=\"/mens-clothing\"> Mens Clothing</a></h6></li><li style=\"\"><a href=\"/top-wear\"> Top Wear</a></li><li style=\"\"><a href=\"/bottom-wear\"> Bottom Wear</a></li><li style=\"\"><a href=\"/sports-wear\"> Sports Wear</a></li><li style=\"\"><a href=\"/seasonal-wear\"> Seasonal Wear</a></li><li style=\"\"><a href=\"/tie\"> Tie</a></li></ul> <ul class=\"sub-menu-content-list menu-generator-space \"> </ul></div> <div class=\"sub-menu-feature\"> </div> </div> <div class=\"sub-menu-thumb\"> <ul> </ul> </div>', 'Mens', 1, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-15 23:41:25', '2020-07-17 08:53:38', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Mens Clothing', 'mens-clothing', 1, 'category', 4, 'mens-clothing', 'mens-clothing', 'Mens Clothing', 'Mens Clothing', 3, 1, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-15 23:49:07', '2020-07-15 23:49:07', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Top Wear', 'top-wear', 1, 'category', 5, 'top-wear', 'top-wear', 'Top Wear', 'Top Wear', 4, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-17 08:30:15', '2020-07-17 08:30:15', 'Top Wear', 'Top Wear', 'Top Wear', 'Top Wear', 'Top Wear', 'Top Wear'),
(6, 'Bottom Wear', 'bottom-wear', 1, 'category', 6, 'bottom-wear', 'bottom-wear', '<b>Bottom Wear</b>', 'Bottom Wear', 4, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-17 08:32:08', '2020-07-20 01:15:06', 'Bottom Wear', 'Bottom Wear', 'Bottom Wear', 'Bottom Wear', 'Bottom Wear', NULL),
(7, 'Seasonal Wear', 'seasonal-wear', 1, 'category', 7, 'seasonal-wear', 'seasonal-wear', 'Seasonal Wear', 'Seasonal Wear', 4, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-17 08:32:47', '2020-07-17 08:32:47', 'Seasonal Wear', 'Seasonal Wear', 'Seasonal Wear', 'Seasonal Wear', 'Seasonal Wear', NULL),
(8, 'Sports Wear', 'sports-wear', 1, 'category', 8, 'sports-wear', 'sports-wear', 'Sports Wear', 'Sports Wear', 4, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-17 08:34:47', '2020-07-17 08:34:47', 'Sports Wear', 'Sports Wear', 'Sports Wear', 'Sports Wear', 'Sports Wear', NULL),
(10, 'Sweatshirts', 'sweatshirts', 1, 'category', 9, 'sweatshirts', 'sweatshirts', 'Sweatshirts', 'Sweatshirts', 7, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-19 23:38:46', '2020-07-19 23:38:46', 'Sweatshirts', 'Sweatshirts', 'Sweatshirts', 'Sweatshirts', 'Sweatshirts', NULL),
(11, 'Jackets', 'jackets', 1, 'category', 11, 'jackets', 'jackets', 'Jackets', 'Jackets', 7, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-19 23:39:30', '2020-07-19 23:39:30', 'Jackets', 'Jackets', 'Jackets', 'Jackets', 'Jackets', NULL),
(12, 'Sweater', 'sweater', 1, 'category', 12, 'sweater', 'sweater', 'Sweater', 'Sweater', 7, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-19 23:39:56', '2020-07-19 23:39:56', 'Sweater', 'Sweater', 'Sweater', 'Sweater', 'Sweater', NULL),
(13, 'Tracksuits', 'tracksuits', 1, 'category', 13, 'tracksuits', 'tracksuits', 'Tracksuits', 'Tracksuits', 7, 1, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2020-07-19 23:40:23', '2020-07-19 23:40:23', 'Tracksuits', 'Tracksuits', 'Tracksuits', 'Tracksuits', 'Tracksuits', NULL),
(14, 'Footwear', 'footwear', 1, 'category', 14, 'footwear', 'footwear', 'Footwear', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:04:26', '2020-07-20 00:05:35', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Sports Shoes', 'sports-shoes', 1, 'category', 15, 'sports-shoes', 'sports-shoes', '<!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves></w:TrackMoves>\r\n  <w:TrackFormatting></w:TrackFormatting>\r\n  <w:PunctuationKerning></w:PunctuationKerning>\r\n  <w:ValidateAgainstSchemas></w:ValidateAgainstSchemas>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF></w:DoNotPromoteQF>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables></w:BreakWrappedTables>\r\n   <w:SnapToGridInCell></w:SnapToGridInCell>\r\n   <w:WrapTextWithPunct></w:WrapTextWithPunct>\r\n   <w:UseAsianBreakRules></w:UseAsianBreakRules>\r\n   <w:DontGrowAutofit></w:DontGrowAutofit>\r\n   <w:SplitPgBreakAndParaMark></w:SplitPgBreakAndParaMark>\r\n   <w:DontVertAlignCellWithSp></w:DontVertAlignCellWithSp>\r\n   <w:DontBreakConstrainedForcedTables></w:DontBreakConstrainedForcedTables>\r\n   <w:DontVertAlignInTxbx></w:DontVertAlignInTxbx>\r\n   <w:Word11KerningPairs></w:Word11KerningPairs>\r\n   <w:CachedColBalance></w:CachedColBalance>\r\n  </w:Compatibility>\r\n  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"></m:mathFont>\r\n   <m:brkBin m:val=\"before\"></m:brkBin>\r\n   <m:brkBinSub m:val=\"--\"></m:brkBinSub>\r\n   <m:smallFrac m:val=\"off\"></m:smallFrac>\r\n   <m:dispDef></m:dispDef>\r\n   <m:lMargin m:val=\"0\"></m:lMargin>\r\n   <m:rMargin m:val=\"0\"></m:rMargin>\r\n   <m:defJc m:val=\"centerGroup\"></m:defJc>\r\n   <m:wrapIndent m:val=\"1440\"></m:wrapIndent>\r\n   <m:intLim m:val=\"subSup\"></m:intLim>\r\n   <m:naryLim m:val=\"undOvr\"></m:naryLim>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"\r\n  DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"267\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Table Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"></w:LsdException>\r\n </w:LatentStyles>\r\n</xml><![endif]--><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;\r\n       mso-fareast-font-family:&quot;Times New Roman&quot;\"><a href=\"https://www.flipkart.com/mens-footwear/sports-shoes/pr?sid=osp,cil,1cu&amp;otracker=nmenu_sub_Men_0_Sports%20Shoes\" title=\"Sports Shoes\"><span style=\"color:blue\">Sports Shoes</span></a></span>', 'Sports Shoes', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:07:01', '2020-07-20 00:07:01', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'casual shoes', 'casual-shoes', 1, 'category', 16, 'casual-shoes', 'casual-shoes', 'casual shoes<br>', 'casual shoes', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:08:26', '2020-07-20 00:08:26', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Formal Shoes', 'formal-shoes', 1, 'category', 17, 'formal-shoes', 'formal-shoes', 'Formal Shoes', NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:12:26', '2020-07-20 00:12:26', NULL, NULL, 'Formal Shoes', 'Formal Shoes', 'Formal Shoes', 'Formal Shoes'),
(18, 'Sandals & Floaters', 'sandals-and-floaters', 1, 'category', 18, 'sandals-and-floaters', 'sandals-and-floaters', '<!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:DontVertAlignCellWithSp/>\r\n   <w:DontBreakConstrainedForcedTables/>\r\n   <w:DontVertAlignInTxbx/>\r\n   <w:Word11KerningPairs/>\r\n   <w:CachedColBalance/>\r\n  </w:Compatibility>\r\n  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"/>\r\n   <m:brkBin m:val=\"before\"/>\r\n   <m:brkBinSub m:val=\"--\"/>\r\n   <m:smallFrac m:val=\"off\"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val=\"0\"/>\r\n   <m:rMargin m:val=\"0\"/>\r\n   <m:defJc m:val=\"centerGroup\"/>\r\n   <m:wrapIndent m:val=\"1440\"/>\r\n   <m:intLim m:val=\"subSup\"/>\r\n   <m:naryLim m:val=\"undOvr\"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"\r\n  DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"267\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:\"Table Normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0in 5.4pt 0in 5.4pt;\r\n	mso-para-margin-top:0in;\r\n	mso-para-margin-right:0in;\r\n	mso-para-margin-bottom:10.0pt;\r\n	mso-para-margin-left:0in;\r\n	line-height:115%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:\"Times New Roman\";\r\n	mso-bidi-theme-font:minor-bidi;}\r\n</style>\r\n<![endif]--><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;\r\n       mso-fareast-font-family:&quot;Times New Roman&quot;\"><a href=\"https://www.flipkart.com/mens-footwear/sandals-floaters/pr?sid=osp,cil,e83&amp;otracker=nmenu_sub_Men_0_Sandals%20%26%20Floaters\" title=\"Sandals &amp; Floaters\"><span style=\"color:blue\">Sandals &amp;\r\n       Floaters</span></a></span>', 'Sandals & Floaters', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:14:22', '2020-07-20 00:14:22', 'Sandals & Floaters', 'Sandals & Floaters', 'Sandals & Floaters', 'Sandals & Floaters', 'Sandals & Floaters', 'Sandals & Floaters'),
(21, 'Slippers & Flip Flops', 'slippers-and-flip-flops', 1, 'category', 20, 'slippers-and-flip-flops', 'slippers-and-flip-flops', 'Slippers &amp; Flip Flops', 'Slippers & Flip Flops', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:26:09', '2020-07-20 00:26:09', 'Slippers & Flip Flops', 'Slippers & Flip Flops', 'Slippers & Flip Flops', 'Slippers & Flip Flops', 'Slippers & Flip Flops', 'Slippers & Flip Flops'),
(22, 'Loafers', 'loafers', 1, 'category', 22, 'loafers', 'loafers', 'Loafers', 'Loafers', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:27:54', '2020-07-20 00:27:54', 'Loafers', 'Loafers', 'Loafers', 'Loafers', 'Loafers', 'Loafers');
INSERT INTO `terms` (`id`, `name`, `seo_url`, `cat_theme`, `type`, `position`, `cssid`, `cssclass`, `description`, `term_short_description`, `parent`, `connected_with`, `page_image`, `thumb_image`, `home_image`, `term_menu_icon`, `term_menu_arrow`, `with_sub_menu`, `sub_menu_width`, `column_count`, `is_active`, `banner1`, `banner2`, `created_at`, `updated_at`, `term_seo_h1`, `term_seo_h2`, `term_seo_title`, `term_seo_description`, `term_seo_keywords`, `onpage_banner`) VALUES
(23, 'Boots', 'boots', 1, 'category', 23, 'boots', 'Boots', 'Boots', 'Boots', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:30:39', '2020-07-20 00:30:39', 'Boots', 'Boots', 'Boots', 'Boots', 'Boots', 'Boots'),
(24, 'Running Shoes', 'running-shoes', 1, 'category', 24, 'running-shoes', 'running-shoes', 'Running Shoes', 'Running Shoes', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:32:37', '2020-07-20 00:32:37', 'Running Shoes', 'Running Shoes', 'Running Shoes', 'Running Shoes', 'Running Shoes', 'Running Shoes'),
(25, 'Sneakers', 'sneakers', 1, 'category', 25, 'sneakers', 'sneakers', '<!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:DontVertAlignCellWithSp/>\r\n   <w:DontBreakConstrainedForcedTables/>\r\n   <w:DontVertAlignInTxbx/>\r\n   <w:Word11KerningPairs/>\r\n   <w:CachedColBalance/>\r\n  </w:Compatibility>\r\n  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"/>\r\n   <m:brkBin m:val=\"before\"/>\r\n   <m:brkBinSub m:val=\"--\"/>\r\n   <m:smallFrac m:val=\"off\"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val=\"0\"/>\r\n   <m:rMargin m:val=\"0\"/>\r\n   <m:defJc m:val=\"centerGroup\"/>\r\n   <m:wrapIndent m:val=\"1440\"/>\r\n   <m:intLim m:val=\"subSup\"/>\r\n   <m:naryLim m:val=\"undOvr\"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"true\"\r\n  DefSemiHidden=\"true\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"267\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Normal\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"heading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"toc 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" QFormat=\"true\" Name=\"caption\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" Name=\"Default Paragraph Font\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Strong\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"59\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Table Grid\"/>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Placeholder Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" UnhideWhenUsed=\"false\" Name=\"Revision\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"List Paragraph\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Light Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Shading 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium List 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Medium Grid 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Dark List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" Name=\"Colorful Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Subtle Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Intense Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" SemiHidden=\"false\"\r\n   UnhideWhenUsed=\"false\" QFormat=\"true\" Name=\"Book Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" Name=\"Bibliography\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><span style=\"font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;\r\n       mso-fareast-font-family:&quot;Times New Roman&quot;\"><a href=\"https://www.flipkart.com/mens-footwear/casual-shoes/sneakers~type/pr?sid=osp%2Ccil%2Ce1f&amp;otracker=nmenu_sub_Men_0_Sneakers\" title=\"Sneakers\"><span style=\"color:blue\">Sneakers</span></a></span>', 'Sneakers', 14, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:34:48', '2020-07-20 00:34:48', 'Sneakers', 'Sneakers', 'Sneakers', 'Sneakers', 'Sneakers', 'Sneakers'),
(26, 'Men\'s Groming', 'mens-groming', 1, 'category', 26, 'mens-groming', 'mens-groming', 'Men\'s Groming', 'Men\'s Groming', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:39:21', '2020-07-20 00:39:21', 'Men\'s Groming', 'Men\'s Groming', 'Men\'s Groming', 'Men\'s Groming', 'Men\'s Groming', 'Men\'s Groming'),
(27, 'Deodorants', 'deodorants', 1, 'category', 27, 'deodorants', 'deodorants', 'Deodorants', 'Deodorants', 26, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:41:27', '2020-07-20 00:41:27', 'Deodorants', 'Deodorants', 'Deodorants', 'Deodorants', 'Deodorants', 'Deodorants'),
(28, 'Perfumes', 'perfumes', 1, 'category', 28, 'perfumes', 'perfumes', 'Perfumes', 'Perfumes', 26, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:42:31', '2020-07-20 00:42:31', 'Perfumes', 'Perfumes', 'Perfumes', 'Perfumes', 'Perfumes', 'Perfumes'),
(29, 'Beard Care & Grooming', 'beard-care-and-grooming', 1, 'category', 29, 'beard-care-and-grooming', 'beard-care-and-grooming', 'Beard Care &amp; Grooming', 'Beard Care & Grooming', 26, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:43:48', '2020-07-20 00:43:48', 'Beard Care & Grooming', 'Beard Care & Grooming', 'Beard Care & Grooming', 'Beard Care & Grooming', 'Beard Care & Grooming', 'Beard Care & Grooming'),
(30, 'Shaving & After Shave', 'shaving-and-after-shave', 1, 'category', 30, 'shaving-and-after-shave', 'shaving-and-after-shave', 'Shaving &amp; After Shave', 'Shaving & After Shave', 26, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:44:58', '2020-07-20 00:44:58', 'Shaving & After Shave', 'Shaving & After Shave', 'Shaving & After Shave', 'Shaving & After Shave', 'Shaving & After Shave', 'Shaving & After Shave'),
(31, 'Sexual Wellness', 'sexual-wellness', 1, 'category', 31, 'sexual-wellness', 'sexual-wellness', 'Sexual Wellness', 'Sexual Wellness', 26, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:45:57', '2020-07-20 00:45:57', 'Sexual Wellness', 'Sexual Wellness', 'Sexual Wellness', 'Sexual Wellness', 'Sexual Wellness', 'Sexual Wellness'),
(32, 'T-Shirt', 't-shirt', 1, 'category', 32, 't-shirt', 't-shirt', 'T-Shirt', 'T-Shirt', 5, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 00:58:55', '2020-07-20 00:58:55', 'T-Shirt', 'T-Shirt', 'T-Shirt', 'T-Shirt', 'T-Shirt', 'T-Shirt'),
(33, 'Formal Shirt', 'formal-shirt', 1, 'category', 33, 'formal-shirt', 'formal-shirt', 'Formal Shirt', 'Formal Shirt', 5, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:05:56', '2020-07-20 01:05:56', 'Formal Shirt', 'Formal Shirt', 'Formal Shirt', 'Formal Shirt', 'Formal Shirt', 'Formal Shirt'),
(34, 'Casual Shirt', 'casual-shirt', 1, 'category', 34, 'casual-shirt', 'casual-shirt', 'Casual Shirt', 'Casual Shirt', 5, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:06:55', '2020-07-20 01:06:55', 'Casual Shirt', 'Casual Shirt', 'Casual Shirt', 'Casual Shirt', 'Casual Shirt', 'Casual Shirt'),
(35, 'Jeans', 'jeans', 1, 'category', 35, 'jeans', 'jeans', 'Jeans', 'Jeans', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:13:32', '2020-07-20 01:13:32', 'Jeans', 'Jeans', 'Jeans', 'Jeans', 'Jeans', 'Jeans'),
(36, 'Casual Trousers', 'casual-trousers', 1, 'category', 36, 'casual-trousers', 'casual-trousers', 'Casual Trousers', 'Casual Trousers', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:14:37', '2020-07-20 01:14:37', 'Casual Trousers', 'Casual Trousers', 'Casual Trousers', 'Casual Trousers', 'Casual Trousers', 'Casual Trousers'),
(37, 'Formal Trousers', 'formal-trousers', 1, 'category', 37, 'formal-trousers', 'formal-trousers', 'Formal Trousers', 'Formal Trousers', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:16:28', '2020-07-20 01:16:28', 'Formal Trousers', 'Formal Trousers', 'Formal Trousers', 'Formal Trousers', 'Formal Trousers', 'Formal Trousers'),
(38, 'Track Pants', 'track-pants', 1, 'category', 38, 'track-pants', 'Track Pants', 'Track Pants', 'Track Pants', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:17:35', '2020-07-20 01:17:35', 'Track Pants', 'Track Pants', 'Track Pants', 'Track Pants', 'Track Pants', 'Track Pants'),
(39, 'Shorts', 'shorts', 1, 'category', 39, 'shorts', 'shorts', 'Shorts', 'Shorts', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:18:34', '2020-07-20 01:18:34', 'Shorts', 'Shorts', 'Shorts', 'Shorts', 'Shorts', 'Shorts'),
(40, 'Cargos', 'cargos', 1, 'category', 40, 'cargos', 'cargos', 'Cargos', 'Cargos', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:19:49', '2020-07-20 01:19:49', 'Cargos', 'Cargos', 'Cargos', 'Cargo\'s', 'Cargo\'s', 'Cargo\'s'),
(41, 'Three-Fourth', 'three-fourth', 1, 'category', 41, 'three-fourth', 'Three-Fourth', 'Three-Fourth', 'Three-Fourth', 6, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:20:56', '2020-07-20 01:20:56', 'Three-Fourth', 'Three-Fourth', 'Three-Fourth', 'Three-Fourth', 'Three-Fourth', NULL),
(42, 'Suits Blazers & Waistcoats', 'suits-blazers-and-waistcoats', 1, 'category', 42, 'suits-blazers-and-waistcoats', 'suits-blazers-and-waistcoats', 'Suits Blazers &amp; Waistcoats', 'Suits Blazers & Waistcoats', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:25:05', '2020-07-20 01:25:05', 'Suits Blazers & Waistcoats', 'Suits Blazers & Waistcoats', 'Suits Blazers & Waistcoats', 'Suits Blazers & Waistcoats', 'Suits Blazers & Waistcoats', 'Suits Blazers & Waistcoats'),
(43, 'Ties, Socks, Caps & More', 'ties-socks-caps-and-more', 1, 'category', 43, 'ties-socks-caps-and-more', 'ties-socks-caps-and-more', 'Ties, Socks, Caps &amp; More', 'Ties, Socks, Caps & More', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:26:56', '2020-07-20 01:26:56', 'Ties, Socks, Caps & More', 'Ties, Socks, Caps & More', 'Ties, Socks, Caps & More', 'Ties, Socks, Caps & More', 'Ties, Socks, Caps & More', 'Ties, Socks, Caps & More'),
(44, 'Fabrics', 'fabrics', 1, 'category', 44, 'fabrics', 'fabrics', 'Fabrics', 'Fabrics', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:27:47', '2020-07-20 01:27:47', 'Fabrics', 'Fabrics', 'Fabrics', 'Fabrics', 'Fabrics', 'Fabrics'),
(45, 'Winter  Wear', 'winter--wear', 1, 'category', 45, 'winter--wear', 'winter--wear', 'Winter&nbsp; Wear', 'Winter  Wear', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:28:58', '2020-07-20 01:28:58', 'Winter  Wear', 'Winter  Wear', 'Winter  Wear', 'Winter  Wear', 'Winter  Wear', 'Winter  Wear'),
(46, 'Sweater Shirt', 'sweater-shirt', 1, 'category', 46, 'sweater-shirt', 'sweater-shirt', 'Sweater Shirt', 'Sweater Shirt', 45, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:30:18', '2020-07-20 01:30:18', 'Sweater Shirt', 'Sweater Shirt', 'Sweater Shirt', 'Sweater Shirt', 'Sweater Shirt', 'Sweater Shirt'),
(47, 'Jackets', 'jackets-0733', 1, 'category', 47, 'Jackets', 'Jackets', 'Jackets', 'Jackets', 45, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:31:04', '2020-07-20 01:31:04', 'Jackets', 'Jackets', 'Jackets', 'Jackets', 'Jackets', 'Jackets'),
(48, 'Tracksuits', 'tracksuits-0731', 1, 'category', 48, 'tracksuits', 'tracksuits', 'Tracksuits', 'Tracksuits', 45, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:31:56', '2020-07-20 01:31:56', 'Tracksuits', 'Tracksuits', 'Tracksuits', 'Tracksuits', 'Tracksuits', 'Tracksuits'),
(49, 'Ethnic Wear', 'ethnic-wear', 1, 'category', 49, 'ethnic-wear', 'ethnic-wear', 'Ethnic Wear', 'Ethnic Wear', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:52:48', '2020-07-20 01:52:48', 'Ethnic Wear', 'Ethnic Wear', 'Ethnic Wear', 'Ethnic Wear', 'Ethnic Wear', 'Ethnic Wear'),
(50, 'Kurta', 'kurta', 1, 'category', 50, 'kurta', 'kurta', 'Kurta', 'Kurta', 49, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:53:55', '2020-07-20 01:53:55', 'Kurta', 'Kurta', 'Kurta', 'Kurta', 'Kurta', 'Kurta'),
(51, 'Ethnic Sets', 'ethnic-sets', 1, 'category', 51, 'ethnic-sets', 'ethnic-sets', 'Ethnic Sets', 'Ethnic Sets', 49, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:54:55', '2020-07-20 01:54:55', 'Ethnic Sets', 'Ethnic Sets', 'Ethnic Sets', 'Ethnic Sets', 'Ethnic Sets', 'Ethnic Sets'),
(52, 'Sherwanis', 'sherwanis', 1, 'category', 52, 'sherwanis', 'sherwanis', 'Sherwanis', 'Sherwanis', 49, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:56:06', '2020-07-20 01:56:06', 'Sherwanis', 'Sherwanis', 'Sherwanis', 'Sherwanis', 'Sherwanis', 'Sherwanis'),
(53, 'Ethnic Payjamas', 'ethnic-payjamas', 1, 'category', 53, 'ethnic-payjamas', 'ethnic-payjamas', 'Ethnic Payjamas', 'Ethnic Payjamas', 49, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:56:51', '2020-07-20 01:56:51', 'Ethnic Payjamas', 'Ethnic Payjamas', 'Ethnic Payjamas', 'Ethnic Payjamas', 'Ethnic Payjamas', 'Ethnic Payjamas'),
(54, 'Dhoti', 'dhoti', 1, 'category', 54, 'dhoti', 'dhoti', 'Dhoti', 'Dhoti', 49, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:57:41', '2020-07-20 01:57:41', 'Dhoti', 'Dhoti', 'Dhoti', 'Dhoti', 'Dhoti', 'Dhoti'),
(55, 'Lungi', 'lungi', 1, 'category', 55, 'lungi', 'lungi', 'Lungi', 'Lungi', 49, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 01:58:37', '2020-07-20 01:58:37', 'Lungi', 'Lungi', 'Lungi', 'Lungi', 'Lungi', 'Lungi'),
(56, 'Inner Wear & Lounge Wear', 'inner-wear-and-lounge-wear', 1, 'category', 56, 'inner-wear-and-lounge-wear', 'inner-wear-and-lounge-wear', 'Inner Wear &amp; Lounge Wear', 'Inner Wear & Lounge Wear', 3, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-20 02:07:20', '2020-07-20 02:07:20', 'Inner Wear & Lounge Wear', 'Inner Wear & Lounge Wear', 'Inner Wear & Lounge Wear', 'Inner Wear & Lounge Wear', 'Inner Wear & Lounge Wear', 'Inner Wear & Lounge Wear'),
(57, 'Blog', 'blog', 1, 'category', 57, 'blog', 'blog', 'Blog', 'Blog', 2, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-06 11:49:19', '2020-08-06 11:49:19', 'blog', 'blog', 'blog', 'blog', 'blog', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `employee_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ukey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('Male','Female','Others') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` enum('Married','Single','Widow','Others') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `father` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `address_2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emergency_phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryfee` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark_verifed` int NOT NULL DEFAULT '0',
  `postcode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reward_points` int NOT NULL DEFAULT '0',
  `vendor_status` int DEFAULT NULL,
  `vendor_banner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_logo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_dis_img` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_dis_img_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_web` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_mark_as_authorized` tinyint DEFAULT NULL,
  `vendor_trade_license_no` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_vat_certificate` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_etin_certificate` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_bank_solvency_certificate` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_incorporation_certificate` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_article_of_memorandum` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_bank_account_details` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_nid` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_md_photo` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_md_signature` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `vendor_shop_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `login_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_no`, `name`, `email`, `username`, `ukey`, `seo_url`, `birthday`, `gender`, `marital_status`, `join_date`, `father`, `mother`, `password`, `company`, `address`, `address_2`, `phone`, `emergency_phone`, `district`, `deliveryfee`, `mark_verifed`, `postcode`, `reward_points`, `vendor_status`, `vendor_banner`, `vendor_logo`, `vendor_title`, `vendor_description`, `vendor_dis_img`, `vendor_dis_img_title`, `vendor_web`, `vendor_mark_as_authorized`, `vendor_trade_license_no`, `vendor_vat_certificate`, `vendor_etin_certificate`, `vendor_bank_solvency_certificate`, `vendor_incorporation_certificate`, `vendor_article_of_memorandum`, `vendor_bank_account_details`, `vendor_nid`, `vendor_md_photo`, `vendor_md_signature`, `vendor_shop_type`, `provider`, `provider_id`, `is_active`, `login_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1', 'Palash Roy', 'info@roydomi.com', 'PalashRoy', NULL, NULL, '1970-01-01', 'Male', 'Married', '2020-02-02', NULL, NULL, '$2y$10$3uIb4PQ3GNJqRSBmvLPGjO2tRlf7iDGdphHyd2pv7FIqXyVMj7J4u', 'RFL Bestbuy', 'Demo', '2/1/A, Block G', '01915513898', '01521112134', 'Dhaka', NULL, 0, '1207', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'ZE7N3iU1KCX139HAkHVhelEZxxLtfKPP1uy878H3nIaOcJSs5XxU6dzwo1q5', '2018-01-30 21:01:03', '2020-02-02 23:32:19');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `cssid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cssclass` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `special` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homesettings`
--
ALTER TABLE `homesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_user_id_foreign` (`user_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_foreign` (`menu`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seo_url` (`seo_url`),
  ADD KEY `pages_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_user_user_id_unique` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `terms_seo_url_unique` (`seo_url`),
  ADD UNIQUE KEY `terms_position_unique` (`position`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `seo_url` (`seo_url`),
  ADD UNIQUE KEY `ukey` (`ukey`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `widgets_position_unique` (`position`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homesettings`
--
ALTER TABLE `homesettings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1430;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=399;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1769;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
