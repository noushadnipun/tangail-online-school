-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 21, 2020 at 02:54 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `cssid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cssclass` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `special` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name`, `position`, `cssid`, `cssclass`, `description`, `special`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Album 1', 1, 'a', 'a', 'Album 1', '', 1, '2020-08-12 01:31:16', '2020-08-12 01:31:16');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'ষষ্ঠ শ্রেণি', 'ষষ্ঠ', 1, '2020-08-11 13:32:55', '2020-11-19 12:52:37'),
(2, 'সপ্তম', 'সপ্তম', 1, '2020-08-11 13:31:14', '2020-08-11 13:31:14'),
(3, 'অষ্টম', 'অষ্টম', 1, '2020-08-11 13:30:41', '2020-08-11 13:30:41'),
(4, 'নবম', 'নবম', 1, '2020-08-11 13:30:17', '2020-08-11 13:30:17'),
(5, 'দশম', 'দশম', 1, '2020-08-11 13:29:55', '2020-08-11 13:29:55'),
(6, 'একাদশ', 'একাদশ', 1, '2020-08-11 13:29:29', '2020-08-11 13:29:29'),
(7, 'দ্বাদশ', 'দ্বাদশ', 1, '2020-08-11 13:28:31', '2020-08-11 13:28:31');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `serial` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `serial`, `media_id`, `caption`, `category_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 14, 'Online Teacher', 1, '1', '2020-08-12 01:45:03', '2020-08-12 01:45:03'),
(2, 2, 16, 'Live Class Time', 1, '1', '2020-08-12 01:58:24', '2020-08-12 01:58:24');

-- --------------------------------------------------------

--
-- Table structure for table `homesettings`
--

CREATE TABLE `homesettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_first` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_second` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_third` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_fourth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_fifth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_sixth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_seventh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_eighth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_category` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homesettings`
--

INSERT INTO `homesettings` (`id`, `cat_first`, `cat_second`, `cat_third`, `cat_fourth`, `cat_fifth`, `cat_sixth`, `cat_seventh`, `cat_eighth`, `home_category`, `created_at`, `updated_at`) VALUES
(1, '8|left|8', '24left|8', '25|right|8', '20|right|8', '599|left|8', NULL, NULL, NULL, '404|203|26|24|489|8|16|397', '2019-01-01 18:00:56', '2020-01-19 01:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `ictlab`
--

CREATE TABLE `ictlab` (
  `id` int(11) NOT NULL,
  `upozila` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lab_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `institute_name` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `incharge_teacher` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `mobile` tinytext,
  `email` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `laptop` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `desktop` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `projector` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `smart_televison` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `internet` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ictlab`
--

INSERT INTO `ictlab` (`id`, `upozila`, `lab_type`, `institute_name`, `incharge_teacher`, `mobile`, `email`, `laptop`, `desktop`, `projector`, `smart_televison`, `internet`, `created_at`, `updated_at`) VALUES
(4, 'বাসাইল', 'আই এল সি', 'বাসাইল সরকারী স্কুল', '10', '0198', 'basail.school@gmail.com', '1', '2', '3', '4', 'yes', '2020-09-24 13:37:39', '2020-09-24 15:23:05'),
(5, 'বাসাইল', 'বিসিসি', 'ভূয়াপুর সরকারী স্কুল', '11', '127', 'vuapur.school@gmail.com', '4', '3', '2', '1', 'No', '2020-09-24 15:20:55', '2020-09-25 16:23:02');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `original_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  `filename` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_extension` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_size_directory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_size_directory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `original_name`, `link`, `filename`, `file_type`, `file_size`, `file_extension`, `full_size_directory`, `icon_size_directory`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(14, 'online-teacher.jpg', NULL, 'online-teacher_1597217624.jpg', 'image/jpeg', '47597', 'jpg', 'storage/uploads/fullsize/2020-08/online-teacher_1597217624.jpg', 'storage/uploads/iconsize/2020-08/online-teacher_1597217624.jpg', 1, 1, '2020-08-12 01:33:44', '2020-08-12 01:33:44'),
(16, 't-live-class.jpg', NULL, 't-live-class_1597219068.jpg', 'image/jpeg', '98682', 'jpg', 'storage/uploads/fullsize/2020-08/t-live-class_1597219068.jpg', 'storage/uploads/iconsize/2020-08/t-live-class_1597219068.jpg', 1, 1, '2020-08-12 01:57:48', '2020-08-12 01:57:48'),
(17, 'National-Helpline.jpg', NULL, 'national-helpline_1597232645.jpg', 'image/jpeg', '34334', 'jpg', 'storage/uploads/fullsize/2020-08/national-helpline_1597232645.jpg', 'storage/uploads/iconsize/2020-08/national-helpline_1597232645.jpg', 1, 1, '2020-08-12 05:44:05', '2020-08-12 05:44:05'),
(18, 'news-banner.jpg', NULL, 'news-banner_1597249891.jpg', 'image/jpeg', '102290', 'jpg', 'storage/uploads/fullsize/2020-08/news-banner_1597249891.jpg', 'storage/uploads/iconsize/2020-08/news-banner_1597249891.jpg', 1, 1, '2020-08-12 10:31:31', '2020-08-12 10:31:31'),
(19, 'Picture1.jpg', NULL, 'picture1_1597251347.jpg', 'image/jpeg', '15820', 'jpg', 'storage/uploads/fullsize/2020-08/picture1_1597251347.jpg', 'storage/uploads/iconsize/2020-08/picture1_1597251347.jpg', 1, 1, '2020-08-12 10:55:47', '2020-08-12 10:55:47'),
(20, 'class-video.jpg', NULL, 'class-video_1597251673.jpg', 'image/jpeg', '108285', 'jpg', 'storage/uploads/fullsize/2020-08/class-video_1597251673.jpg', 'storage/uploads/iconsize/2020-08/class-video_1597251673.jpg', 1, 1, '2020-08-12 11:01:13', '2020-08-12 11:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `institutions`
--

CREATE TABLE `institutions` (
  `id` int(11) NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upozila` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institutions_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `institutions_head` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `establishment_date` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_teacher` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_student` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institutions`
--

INSERT INTO `institutions` (`id`, `status`, `type`, `level`, `upozila`, `institutions_name`, `institutions_head`, `position`, `establishment_date`, `mobile`, `email`, `total_teacher`, `total_student`, `created_at`, `updated_at`) VALUES
(1, 'এমপিও', 'বিদ্যালয়', 'মাধ্যমিক', 'ঘাটাইল', 'ঘাটাইল গণ পাইলট', 'মোঃষাশেম', 'অধ্যক্ষ', '১৯৬২', '০১৯২', 'ght.gbg@h\\mail.com', '100', '8000', '2020-09-25 16:04:38', '2020-09-25 16:18:54'),
(2, 'সরকারী', 'কলেজ', 'স্নাতোত্তর', 'টাঙ্গাইল সদর', 'সাদাত কলেজ', 'মোঃ রশিদ', 'অধ্যক্ষ', '১৯৪৩', '০১৭৮৯', 'sadat.clg@mail.com', '২০০', '১২০০০', '2020-09-25 16:07:26', '2020-09-25 16:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Primary Menu', NULL, '2020-08-06 14:55:07', '2020-08-06 14:55:07'),
(2, 'টাংগাইল অনলাইন স্কুল সম্পর্কিত', NULL, '2020-08-12 05:27:39', '2020-08-12 05:27:39');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu` int(10) UNSIGNED NOT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `label`, `link`, `parent`, `sort`, `class`, `menu`, `depth`, `created_at`, `updated_at`) VALUES
(65, 'প্রতিযোগিতা', '#', 0, 5, NULL, 1, 0, '2020-08-06 14:55:37', '2020-08-08 15:45:40'),
(66, 'কুইজ', '#', 65, 7, NULL, 1, 1, '2020-08-06 15:02:45', '2020-08-08 15:54:28'),
(67, 'বিতর্ক', '#', 65, 6, NULL, 1, 1, '2020-08-06 15:03:37', '2020-08-08 15:54:28'),
(68, 'অনলাইন টেস্ট', '#', 65, 8, NULL, 1, 1, '2020-08-06 15:04:29', '2020-08-08 15:54:28'),
(69, 'ফলাফল', '/page/result', 65, 9, NULL, 1, 1, '2020-08-06 15:05:15', '2020-08-08 15:54:28'),
(70, 'শিক্ষা সংক্রান্ত', '#', 0, 10, NULL, 1, 0, '2020-08-06 15:05:47', '2020-08-08 15:48:18'),
(71, 'প্রতিষ্ঠান সমূহ', '/page/institutions', 70, 11, NULL, 1, 1, '2020-08-06 15:06:05', '2020-09-25 15:54:02'),
(72, 'প্রচ্ছদ', '/', 0, 0, NULL, 1, 0, '2020-08-06 15:06:25', '2020-08-08 15:41:36'),
(73, 'বাণী', '/page/quote', 0, 1, NULL, 1, 0, '2020-08-06 15:07:08', '2020-08-08 15:42:08'),
(74, 'শিক্ষক মন্ডলী', 'page/teacher', 70, 13, NULL, 1, 1, '2020-08-06 15:07:22', '2020-08-13 10:21:26'),
(75, 'আইসিটি ল্যাব', '/page/ictlab', 70, 15, NULL, 1, 1, '2020-08-06 15:08:02', '2020-09-24 13:58:30'),
(76, 'বই বিতরণ', '#', 70, 14, NULL, 1, 1, '2020-08-06 15:08:53', '2020-08-13 04:40:53'),
(77, 'ফলাফল', '/page/result', 70, 16, NULL, 1, 1, '2020-08-06 15:09:39', '2020-08-13 04:40:53'),
(78, 'সংবাদ', '/page/news', 0, 17, NULL, 1, 0, '2020-08-06 15:10:33', '2020-08-13 04:40:53'),
(80, 'শ্রেণি ক্লাশ', '/page/video', 0, 18, NULL, 1, 0, '2020-08-06 15:11:49', '2020-11-19 16:54:26'),
(81, 'যোগাযোগ', '#', 0, 19, NULL, 1, 0, '2020-08-06 15:12:36', '2020-11-19 16:54:26'),
(82, 'প্রশাসন', '/page/administration', 0, 2, NULL, 1, 0, '2020-08-08 15:42:47', '2020-08-08 15:54:46'),
(83, 'এডমিন প্যানেল', '/page/34/admin-panel', 0, 3, NULL, 1, 0, '2020-08-08 15:43:13', '2020-08-08 16:02:48'),
(84, 'ICT4E অ্যাম্বাসেডর', 'page/tlist-ict4e-ambassador', 0, 4, NULL, 1, 0, '2020-08-08 15:44:59', '2020-08-13 05:38:52'),
(89, 'বার্ষিক প্রতিবেদন', '/page/40/Annual-report', 0, 3, NULL, 2, 0, '2020-08-12 05:32:53', '2020-08-12 05:34:48'),
(90, 'আইন ও বিধি', '/page/37/Laws-and-regulations', 0, 2, NULL, 2, 0, '2020-08-12 05:33:44', '2020-08-12 05:34:48'),
(91, 'সাংগাঠনিক কাঠামো', '/page/36/Organizational-structure', 0, 1, NULL, 2, 0, '2020-08-12 05:34:19', '2020-08-12 05:34:48'),
(92, 'তথ্য প্রদানকারী কর্মকর্তা', '/page/35/Information-officer', 0, 0, NULL, 2, 0, '2020-08-12 05:34:46', '2020-08-12 05:34:48'),
(93, 'প্রতিষ্ঠান প্রধান', 'page/tlist-headmaster', 70, 12, NULL, 1, 1, '2020-08-13 04:40:45', '2020-08-13 05:07:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(10, '2017_11_17_072545_create_roles_table', 1),
(11, '2017_11_17_072610_create_role_user_table', 1),
(12, '2017_12_02_084150_create_todos_table', 1),
(13, '2017_12_03_200119_create_images_table', 1),
(14, '2017_12_26_144334_create_widgets_table', 1),
(16, '2018_01_13_081052_create_settings_table', 1),
(20, '2018_01_30_003055_create_variations_table', 1),
(22, '2018_03_11_085005_create_pages_table', 1),
(25, '2018_04_01_093828_create_dealers_table', 1),
(26, '2018_04_04_202005_create_orders_master_table', 1),
(34, '2017_08_11_073824_create_menus_table', 1),
(35, '2017_08_11_074006_create_menu_items_table', 1),
(36, '2018_01_13_155638_create_posts_table', 1),
(37, '2018_07_13_081052_create_paymentsettings_table', 1),
(38, '2018_07_14_205014_temporary_orders_table', 1),
(39, '2018_07_15_171525_create_orders_master_table', 1),
(40, '2018_07_15_172259_create_orders_detail_table', 1),
(42, '2018_07_07_062938_create_districts_table', 1),
(44, '2018_10_25_095020_create_warishs_table', 1),
(45, '2018_08_06_224359_create_comments_table', 1),
(46, '2018_10_24_051646_create_newsletters_table', 1),
(47, '2018_12_05_130058_create_attgroups_table', 1),
(56, '2018_01_25_190214_create_attributes_table', 1),
(57, '2018_01_10_051005_create_terms_table', 1),
(62, '2018_12_19_211559_create_relatedproducts_table', 1),
(65, '2018_12_20_210353_create_productcategories_table', 1),
(66, '2018_12_21_215051_create_productimages_table', 1),
(68, '2018_12_22_222345_create_productattributesdata_table', 1),
(69, '2018_01_25_185753_create_products_table', 1),
(71, '2019_01_02_061932_create_homesettings_table', 1),
(73, '2019_01_04_173553_create_returns_table', 1),
(76, '2019_01_20_211432_create_productpricecombinations_table', 1),
(77, '2019_01_22_184932_create_productcombinationsdata_table', 1),
(78, '2019_01_28_052658_create_reviews_table', 1),
(79, '2019_01_30_094821_create_coupons_table', 1),
(80, '2019_02_15_130711_create_banks_table', 1),
(81, '2019_02_15_154530_create_productsemidata_table', 1),
(82, '2019_02_19_072000_create_flash_schedules_table', 1),
(83, '2019_02_19_072327_create_flash_items_table', 1),
(84, '2019_03_12_054253_create_wishlists_table', 2),
(85, '2019_07_28_094303_create_careers_table', 2),
(86, '2019_08_03_123456_create_jobs_table', 3),
(87, '2019_09_18_102018_create_session_taxonomies_table', 4),
(88, '2019_10_02_072448_create_order_shipments_table', 4),
(89, '2019_10_07_041954_create_showroom_stocks_table', 5),
(90, '2019_10_15_081228_create_depo_lists_table', 6),
(91, '2019_10_23_060238_create_question_answers_table', 7),
(92, '2019_10_24_054542_create_review_images_table', 8),
(93, '2019_10_31_073828_create_whole_sales_table', 8),
(94, '2019_11_04_065705_create_mix_taxonomies_table', 9),
(95, '2019_11_04_105309_create_tell_uses_table', 10),
(96, '2019_11_05_060403_create_rewards_point_histories_table', 10),
(97, '2019_12_04_065636_create_brands_table', 10),
(98, '2019_12_12_085405_create_contacts_table', 11),
(99, '2019_12_30_092401_create_notifications_table', 12),
(101, '2020_01_29_050519_create_corporates_table', 14),
(102, '2020_02_04_111628_create_order_notes_table', 14),
(103, '2020_02_13_085134_create_affiliate_payments_table', 15),
(111, '2020_06_02_051629_create_routes_table', 16),
(112, '2020_08_06_223718_create_albums_table', 17),
(113, '2020_08_10_183410_create_classes_table', 17),
(114, '2020_08_10_210758_create_subjects_table', 18),
(122, '2020_08_10_222119_create_videos_table', 19),
(123, '2020_01_14_063651_create_galleries_table', 20);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sticky` tinyint(1) DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'English, Bengali or any other language',
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `user_id`, `title`, `sub_title`, `seo_url`, `description`, `images`, `is_sticky`, `lang`, `is_active`, `created_at`, `updated_at`) VALUES
(34, 1, 'এডমিন প্যানেল', 'এডমিন প্যানেল', 'admin-panel', '<pre style=\"background-color:#ffffff;color:#000000;font-family:\'Nirmala UI Semilight\';font-size:11.3pt;\"><span style=\"color:#808080;background-color:#f7faff;font-style:italic;\">এডমিন প্যানেল</span></pre>', NULL, 1, 'en', 1, '2020-08-08 15:57:45', '2020-08-08 16:02:22'),
(35, 1, 'তথ্য প্রদানকারী কর্মকর্তা', 'তথ্য প্রদানকারী কর্মকর্তা', 'Information-officer', '<a href=\"http://localhost/toschool_html/#\" style=\"color: rgb(34, 34, 34); background-color: rgb(245, 245, 245); font-family: Lato, SolaimanLipi, sans-serif;\">তথ্য প্রদানকারী কর্মকর্তা</a>', NULL, 0, 'en', 1, '2020-08-12 04:39:07', '2020-08-12 04:40:13'),
(36, 1, 'সাংগাঠনিক কাঠামো', 'সাংগাঠনিক কাঠামো', 'Organizational-structure', 'সাংগাঠনিক কাঠামো', NULL, 0, 'en', 1, '2020-08-12 04:41:30', '2020-08-12 04:41:30'),
(37, 1, 'আইন ও বিধি', 'আইন ও বিধি', 'Laws-and-regulations', 'আইন ও বিধি', NULL, 0, 'en', 1, '2020-08-12 04:42:07', '2020-08-12 04:42:07'),
(40, 1, 'বার্ষিক প্রতিবেদন', 'বার্ষিক প্রতিবেদন', 'Annual-report', 'বার্ষিক প্রতিবেদন', NULL, 0, 'en', 1, '2020-08-12 04:43:19', '2020-08-12 04:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.com', NULL, '2019-12-22 03:13:52', '2019-12-22 22:34:25'),
(2, 'arifabm@gmail.com', 'ZlNbKociAZitRZjVYVM7yxtV5A5RdJdeNmi5WIUvgAUvJ9xjoCSrouV2l7R5', '2020-03-28 10:06:27', '2020-03-28 21:09:20'),
(3, 'sifatnumber@gmail.com', 'xDwiHNknmOM50ISlToL1n1zHUto4lIpEvBDDRf6tMWYt066vnaDuYgROZOn2', '2020-03-29 11:31:47', '2020-03-29 11:31:51'),
(4, 'shobhra.dreams@gmail.com', '2c5o3hf6jI0VCYTNc2aLG2IRjJnKOuu84sbRfyR0xkzfGC1cpmJJjWpWkeHf', '2020-04-02 09:47:30', '2020-04-02 09:47:57'),
(5, 'amsaikat.cmt@gmail.com', 'jkE5MfU0YL6vFtqfUxUioMmCjhoeoMC6UUH23zfm0gp6OK2tt5iFFzoMySf0', '2020-04-19 02:44:51', '2020-04-19 02:56:31'),
(6, 'Probeerkshaha@gmail.com', 'TyUATEJf9cgerxtC7M0ydDEmOSvgulknpk2EoJWB1vq2HAC6K4o8kgNHiKbQ', '2020-04-19 22:21:59', '2020-04-19 22:22:24'),
(7, 'rocky090977@gmail.com', '9iYsLeP6HUZeDjsYiyB1MXCEPNv4ivj7GCAjf3GYTcxwrdLXzfaiPnG9Wl7w', '2020-04-22 06:53:24', '2020-04-22 06:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` mediumtext COLLATE utf8mb4_unicode_ci,
  `youtube` mediumtext COLLATE utf8mb4_unicode_ci,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opening_hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_numbers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8mb4_unicode_ci,
  `tags` longtext COLLATE utf8mb4_unicode_ci,
  `division` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thana` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'English, Bengali or any other language',
  `is_auto_post` tinyint(1) DEFAULT NULL,
  `is_upcoming` tinyint(1) DEFAULT NULL,
  `is_sticky` tinyint(1) DEFAULT NULL,
  `h1tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h2tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci,
  `seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `views` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `offer_expire_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `sub_title`, `seo_url`, `author`, `categories`, `images`, `description`, `short_description`, `youtube`, `brand`, `phone`, `opening_hours`, `latitude`, `longitude`, `phone_numbers`, `address`, `tags`, `division`, `district`, `thana`, `shop_type`, `lang`, `is_auto_post`, `is_upcoming`, `is_sticky`, `h1tag`, `h2tag`, `seo_title`, `seo_description`, `seo_keywords`, `views`, `is_active`, `offer_expire_date`, `created_at`, `updated_at`) VALUES
(10, 1, 'জিডিপির এত প্রবৃদ্ধি হলো কীভাবে', NULL, 'জিডিপির-এত-প্রবৃদ্ধি-হলো-কীভাবে', NULL, '65,69,', '18', '<p style=\"outline: 0px; padding: 0px; margin-bottom: 16px; overflow: hidden; font-size: 18px; line-height: 30px; overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: Kiron, SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; background-color: rgb(240, 240, 237);\">করোনা বদলে দিয়েছে জীবন। গত মার্চ থেকে বাংলাদেশে করোনার প্রকোপ শুরু হয়। এরপর এপ্রিল ও মে মাসে বাস, ট্রাক, ট্রেন, ব্যক্তিগত গাড়ি—সব ধরনের যান চলাচল প্রায় বন্ধ ছিল। একইভাবে ট্রলার-লঞ্চ চলাচলও বন্ধ ছিল। আকাশে ডানা মেলেনি বিমান। মানুষও প্রয়োজন ছাড়া ঘরের বাইরে খুব একটা বের হননি। জুন মাসে সীমিত পরিসরে গাড়ি চলাচল শুরু হয়।</p><p class=\"TEXT\" style=\"outline: 0px; padding: 0px; margin-bottom: 16px; overflow: hidden; font-size: 18px; line-height: 30px; overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: Kiron, SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; background-color: rgb(240, 240, 237);\">সব মিলিয়ে প্রায় তিন মাস অর্থাৎ অর্থবছরের চার ভাগের এক ভাগ সময় পরিবহন চলাচল বন্ধ থাকলেও দেশের অর্থনীতিতে এ খাতের অবদান কমেনি। বরং বেড়েছে। বাংলাদেশ পরিসংখ্যান ব্যুরো (বিবিএস) বলছে, সদ্য বিদায়ী অর্থবছরের (২০১৯-২০) মোট দেশজ উৎপাদনে (জিডিপি) বাস, ট্রাক, ট্রেন—এসবের মূল্য সংযোজন আগের বছরের চেয়ে ৪ হাজার ৭৯১ কোটি টাকা বেড়েছে। নৌযান চলাচলে বেড়েছে ২৪৬ কোটি টাকা। আর বিমান চলাচলে বেড়েছে ৫১ কোটি টাকা।</p><p class=\"TEXT\" style=\"outline: 0px; padding: 0px; margin-bottom: 16px; overflow: hidden; font-size: 18px; line-height: 30px; overflow-wrap: break-word; color: rgb(0, 0, 0); font-family: Kiron, SolaimanLipi, Arial, Vrinda, FallbackBengaliFont, Helvetica, sans-serif; background-color: rgb(240, 240, 237);\">এবার আসি পর্যটনশিল্পে। সেখানেও একই দুর্দশা। সেই ২৬ মার্চ থেকে পর্যটন এলাকায় হোটেল-মোটেল বন্ধ রাখা হয়েছে। করোনার শুরুর পর কক্সবাজারসহ দেশের পর্যটন স্পটগুলো প্রায় পর্যটকশূন্য ছিল। এখনো পরিস্থিতির উন্নতি হয়নি। করোনার কারণে প্রথম তিন মাস তো রেস্তোরাঁর মালিকেরা মাথায় হাত দিয়ে বসেছিলেন। প্রায় সব রেস্তোরাঁ বন্ধ হয়ে যায়। জুন মাস থেকে ধীরে কিছু রেস্তোরাঁ সীমিত পরিসরে খোলা হয়েছে। তবে বেচাকেনায় আগের মতো জমজমাট ভাব নেই। বিবিএসের হিসাবে, বছর শেষে হোটেল-রেস্তোরাঁর অবদান জিডিপিতে বেড়েছে ৫০৯ কোটি টাকা।</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 1, 1, 0, '', '', '', '', '', 0, 1, NULL, '2020-08-06 03:22:37', '2020-08-12 10:31:37'),
(11, 1, 'জীবনের নতুন অধ্যায় নিয়ে উচ্ছ্বসিত প্রিয়াঙ্কা', NULL, 'জীবনের-নতুন-অধ্যায়-নিয়ে-উচ্ছ্বসিত-প্রিয়াঙ্কা', NULL, '65,71,72,', '16', 'নাম আনফিনিশড, কিন্তু সেটাই ফিনিশড করে ছাড়লেন বলিউড অভিনেত্রী প্রিয়াঙ্কা চোপড়া। তাঁর নিজের স্মৃতিকথাগুলো বইয়ের মোড়কে আসছে অবশেষে। আর তা তিনি জানান দিলেন ইনস্টাগ্রামে একটি ছবি পোস্ট করে। সেখানে দেখা গেল প্রিয়াঙ্কার হাসির ছবি। তাতে লেখা ‘আনফিনিশড বাই প্রিয়াঙ্কা চোপড়াঽ।&nbsp;<p>&nbsp;এটাই বইয়ের প্রচ্ছদ কি না, তা অবশ্য নিশ্চিত নয়। ছবিটির সঙ্গে একটি দীর্ঘ স্ট্যাটাসও দিয়েছেন প্রিয়াঙ্কা। তিনি লিখেছেন, ‘অসম্পূর্ণ এবার সম্পূর্ণ হলো। মাত্র চূড়ান্ত কপি পাঠালাম। ওয়াও! আপনাদের সঙ্গে এটি শেয়ার না করে পারলাম না। স্মৃতিকথার প্রতিটি শব্দ আমার আত্মদর্শন এবং আমার জীবনের প্রতিচ্ছবি।ঽ হ্যাশট্যাগ দিয়ে লিখেছেন—‘আসছেঽ, ‘আনফিনিশডঽ।\r\nদুই বছর আগে এই স্মৃতিকথা লেখা নিয়ে শিরোনামে এসেছিলেন তিনি। তখন জানা যায়, নিজের জীবনকে এবার ভক্তদের সঙ্গে শেয়ার করতেই স্মৃতিকথা লেখার তোড়জোড়। তারপর অনেক সময় পেরিয়েছে। লকডাউনের দীর্ঘ বন্ধ কাজে লাগিয়ে এবার শেষ করে ফেললেন কাজ।</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 1, 1, 0, '', '', '', '', '', 0, 1, NULL, '2020-08-12 03:23:48', '2020-11-21 00:45:44'),
(12, 1, 'মহেশখালীর আদালতে প্রদীপসহ ২৯ জনের বিরুদ্ধে মামলা', NULL, '-মহেশখালীর-আদালতে-প্রদীপসহ-২৯-জনের-বিরুদ্ধে-মামলা', NULL, '65,71,72,', '14', 'কক্সবাজারের দ্বীপ উপজেলা মহেশখালীর সিনিয়র জুডিসিয়াল ম্যাজিস্ট্রেট আদালতে মহেশখালী থানার সাবেক ওসি প্রদীপ কুমার দাশসহ ২৯ জনের বিরুদ্ধে মামলা হয়েছে। বুধবার দুপুরে কথিত বন্দুকযুদ্ধে নিহত লবণচাষি আবদুস সাত্তারের স্ত্রী হামিদা বেগম বাদী হয়ে এ মামলাটি করেন।&nbsp;<p>এতে ওই মামলায় প্রদীপ কুমার দাশ ছাড়াও পুলিশের আরও পাঁচ সদস্যকে আসামি করা হয়। তাঁরা হলেন মহেশখালী থানার সাবেক উপপরিদর্শক (এসআই) হারুনুর রশীদ, এসআই ইমাম হোসেন ও সহকারী উপপরিদর্শক (এএসআই) মনিরুল ইসলাম, শাহেদুল ইসলাম, আজিম উদ্দিন।\r\n\r\nআর প্রদীপ কুমার দাশসহ পুলিশের ছয় সদস্যের পাশাপাশি স্থানীয় সন্ত্রাসী বাহিনীর প্রধান ফেরদৌস চৌধুরীসহ তাঁর বাহিনীর ২৩ জনকে আসামি করা হয়। মামলায় ফেরদৌস চৌধুরীকে প্রধান আসামি করা হয়। ২৪ নম্বর আসামি হলেন প্রদীপ কুমার দাশ।</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 1, 1, 1, '', '', '', '', '', 0, 1, NULL, '2020-08-12 09:04:47', '2020-08-12 10:25:37'),
(13, 1, 'প্রধান শিক্ষকের বাণী', NULL, 'প্রধান-শিক্ষকের-বাণী', NULL, '63,', '19', '<p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">আন্তর্জাতিক ক্রিকেটে বাংলাদেশের খেলা নেই অনেকদিন হয়ে গেল। করোনার কারনে থেমে আছে বাংলাদেশের ক্রিকেট ম্যাচ গুলো। ক্রিকেট মাঠে ফিরলেও এখনও সিরিজ পায় নি বাংলাদেশ। প্রথমে কয়েকটি সিরিজ আর আইপিএল হলেও বাংলাদেশের অপেক্ষা করতে হচ্ছে আরও কিছু সময়।</p><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">যদিও গত মাসেই শ্রীলংকার সাথে সিরিজ হওয়ার কথা ছিল তবে শ্রীলংকান বোর্ডের অদ্ভুত সব কড়াকড়ি নিয়মকানুন বেধে দেয়ায় শেষ পর্যন্ত সিরিজই প্রত্যাখ্যান করেছিল বিসিবি। যদিও পরিবর্তীতে তারা কিছু নিয়ম শিথিল করে বাংলাদেশ কে আমন্ত্রণ জানিয়ে ছিল তবে বাংলাদেশ বোর্ড আর তাতে সাড়া দেয়নি।</p><div class=\"a4bcd0deb56e105b3d7723da3d878046\" data-index=\"2\" style=\"padding: 0px; margin: 10px 0px; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; float: none; text-align: center;\"></div><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">বাংলাদেশের ক্রিকেট খেলা বন্ধ থাকলেও বন্ধ নেই ক্রিকেট র‍্যাংকিং। কিছু সিরিজ হওয়ায় কিছু কিছু প্লেয়ারদের পজিশন উপরে উঠছে নীচেও নামছে।</p><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\"><span style=\"padding: 0px; margin: 0px; outline: 0px; font-weight: bolder;\">চলুন দেখে নেই বাংলাদেশের কিছু ক্রিকেটারদের বর্তমান টিটুয়েন্টি র‍্যাংকিং:-</span></p><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">সাকিব আল হাসান :- সাকিব মানেই যেন অলরাউন্ডারের শীর্ষস্থান! অন্তত সেটাই দেখে আসছে ক্রিকেট ভক্তরা। যদিও ক্রিকেটে ১ বছর ব্যান ছিলেন তিনি। তবে নতুন আইসিসির র‍্যাংকিং এ টিটুয়েন্টির অলরাউন্ডারের শীর্ষস্থান ফেরত পান নি তিনি। তালিকায় মোহাম্মদ নবীর পরে ২য় স্থানে আছেন সাকিব। ব্যাটিং এ সাকিবের অবস্থান ৪৯ ও বোলিং এ ২০।</p><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">তামিম ইকবাল :- বাংলাদেশের হয়ে টিটুয়েন্টি তে সর্বোচ্চ রান ও একমাত্র সেঞ্চুরি তামিম ইকবালের।<br style=\"padding: 0px; margin: 0px; outline: 0px;\">তার বর্তমান র‍্যাংকিং ৪৭ ও ক্যারিয়ার সেরা র‍্যাংকিং ২২।</p><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">মুশফিকুর রহিম:- বাংলাদেশের অন্যতম ডিপেন্ডেবল মুশফিকুর রহিম। তিনি প্রায় ১৪ বছর ধরে জাতীয় দলে খেলছেন। দলের অন্যতম সেরা ক্রিকেটারও তিনি। বর্তমানে তার টিটুয়েন্টি র‍্যাংকিং ৫৮ ও ক্যারিয়ার সেরা র‍্যাংকিং ৪৮।</p><p style=\"padding: 0px; margin-bottom: 1rem; outline: 0px; color: rgb(34, 34, 34); font-family: SolaimanLipi, Arial, sans-serif; font-size: 16px; text-align: justify;\">মুস্তাফিজুর রহমান :- বর্তমানে তার টিটুয়েন্টি বোলিং ৩১ নাম্বার ও সর্বোচ্চ ৫ নাম্বারে ছিলেন তিনি। একমাত্র বোলার হিসেবে টিটুয়েন্টি বিশ্বকাপে নিউজিল্যান্ডের সাথে ৫ উইকেট নিয়েছেন তিনি।</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 1, 1, 1, '', '', '', '', '', 0, 1, NULL, '2020-08-12 10:50:03', '2020-11-19 15:40:48'),
(14, 1, 'Bani 2', NULL, 'bani-2', NULL, '63,', '20', '<span style=\"color: rgb(85, 85, 85); font-family: Lato, sans-serif; font-size: 18px; text-align: center; background-color: rgb(255, 255, 255);\">We bilieve we have created the most efficient environment and guides for students. Our environment for studies that will convince you to use it for your future.</span>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 1, 1, 1, '', '', '', '', '', 0, 1, NULL, '2020-08-12 11:01:19', '2020-08-12 16:02:47'),
(15, 1, 'দু\'দিন ব্যাপী অনলাইন ভিত্তিক প্রশিক্ষণ কর্মসূচি রেজিস্ট্রেশন', NULL, 'দুদিন-ব্যাপী-অনলাইন-ভিত্তিক-প্রশিক্ষণ-কর্মসূচি-রেজিস্ট্রেশন', NULL, '62,', NULL, '<h2 style=\"margin-top: 0px; margin-bottom: 0.5rem; line-height: 1.2; font-size: 2rem; color: rgb(33, 37, 41); font-family: Lato, SolaimanLipi, sans-serif; background-color: rgb(255, 255, 255);\">Con</h2>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 1, 1, 1, '', '', '', '', '', 0, 1, NULL, '2020-11-19 17:10:13', '2020-11-19 17:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `upozila` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `exam_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `total_candidates` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `total_pass` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `percentage_pass` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a_plus` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `a_min` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `b` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `c` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `d` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `f` tinytext CHARACTER SET utf32 COLLATE utf32_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `upozila`, `exam_name`, `year`, `total_candidates`, `total_pass`, `percentage_pass`, `a_plus`, `a`, `a_min`, `b`, `c`, `d`, `f`, `created_at`, `updated_at`) VALUES
(4, 'বাসাইল', 'জেএসসি', '২০১৯', '২৩৬০', '২২১৯', '৮২.২%', '১৮', '৫০০', '৪০০', '৩০০', '২০০', '১০০', '২০', '2020-09-25 13:23:31', '2020-09-25 13:23:31'),
(5, 'ভুয়াপুর', 'এস এস সি', '২০১৭', '২২৯০', '১৮৩৮', '৭৩.৮%', '৮৯', '১৩০', '৩৮৯', '৩৭৮', '৪৫৬', '৫৩', '১৯০', '2020-09-25 13:25:46', '2020-09-25 13:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'A consicutive executive to manage data of school', 1, '2020-08-10 18:00:00', '2020-08-10 18:00:00'),
(2, 'editor', 'An editor from a school who are permitted to manage data of school.', 1, '2020-08-10 18:00:00', '2020-08-10 18:00:00'),
(3, 'staff', 'A Staff\r\n', 1, NULL, NULL),
(4, 'teacher', 'A Teacher of school', 1, '2020-08-10 18:00:00', '2020-08-10 18:00:00'),
(5, 'user', 'A normal user', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-08-10 18:00:00', '2020-08-10 18:00:00'),
(5, 4, 9, '2020-08-11 09:32:50', '2020-08-11 09:33:46'),
(6, 4, 10, '2020-08-11 09:34:51', '2020-08-11 09:34:51'),
(7, 4, 11, '2020-08-14 09:24:34', '2020-08-14 09:24:34'),
(14, 3, 18, '2020-11-21 06:09:33', '2020-11-21 06:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `route_uri` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_hash` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `com_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `com_slogan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_eshtablished` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_licensecode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_logourl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_headerurl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_addressgooglemap` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_analytics` longtext COLLATE utf8mb4_unicode_ci,
  `com_chat_box` text COLLATE utf8mb4_unicode_ci,
  `com_metatitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_metadescription` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_metakeywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_workinghours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminphone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminemail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_adminphotourl` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_facebookpageid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `com_timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_rate` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `com_name`, `com_slogan`, `com_eshtablished`, `com_licensecode`, `com_logourl`, `com_headerurl`, `com_phone`, `order_phone`, `com_email`, `com_address`, `com_addressgooglemap`, `com_website`, `com_analytics`, `com_chat_box`, `com_metatitle`, `com_metadescription`, `com_metakeywords`, `com_workinghours`, `com_adminname`, `com_adminphone`, `com_adminemail`, `com_adminphotourl`, `com_facebookpageid`, `com_favicon`, `com_timezone`, `tax_rate`, `created_at`, `updated_at`) VALUES
(1, 'Medico', 'Quick & Smart', '2019', 'License', 'https://rflbestbuy.com/secure/storage/uploads/fullsize/2020-01/best-buy-logo-newde9fe5a4dcebd65f12365b75204f23c5.png', 'https://rfldoor.com/storage/uploads/fullsize/2018-10/web-header-backgraound.png', '+88 09613 14 11 11 (9.00AM- 5.00PM) Any Business Day', '+88 09613 14 11 11', 'rfl908@rflgroupbd.com', 'PRAN-RFL Center, 105 Middle Badda, Dhaka -1212, Bangladesh<br/> Phone: +88-02-9881792- Ext : 880', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.045894644146!2d90.42315831483883!3d23.78137998457409!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c796c76c1a', '/', '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-136584014-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-136584014-1\');\r\n</script>', '<!-- Start of REVE Chat Script-->\r\n <script type=\'text/javascript\'>\r\n window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account=\'8167451\';w.__revechat_version=2;\r\n   r._= []; var rc = d.createElement(\'script\'); rc.type = \'text/javascript\'; rc.async = true; rc.setAttribute(\'charset\', \'utf-8\');\r\n   rc.src = (\'https:\' == document.location.protocol ? \'https://\' : \'https://\') + \'static.revechat.com/widget/scripts/new-livechat.js?\'+new Date().getTime();\r\n   var s = d.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(rc, s);\r\n })(document, window);\r\n</script>\r\n<!-- End of REVE Chat Script -->', 'Welcome to RFLBestBuy.com', 'RFLBestBuy.com strives to be the best online shopping destination in Bangladesh featuring a wide range of category products. Experience fast, reliable and effortless online shopping with home', 'RFL Bestbuy, rflbestbuy.com, PVC & Plastic, Electronics, Mobile Phone, Furniture, household, Experience fast, reliable, effortless, online shopping,', '09-05', 'Kamruzzaman Khan', '01844602020', 'rfl908@rflgroupbd.com', 'https://rflbestbuy.com/secure/storage/uploads/fullsize/2019-02/fav.png', '324093091058711', '/secure/storage/uploads/fullsize/2019-03/logo.png', 'Asia/Dhaka', NULL, '2018-03-09 10:03:27', '2020-08-07 16:25:35');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `title` tinytext,
  `code` varchar(191) DEFAULT NULL,
  `content` text,
  `images` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `code`, `content`, `images`, `created_at`, `updated_at`) VALUES
(12, 'Homepage Slider', 'qsGpz', 'some description', '12, 10', '2019-09-29 06:13:12', '2020-08-07 04:50:27');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'বাংলা ১ম পত্র', 'বাংলা ১ম পত্র', 1, '2020-08-11 01:56:33', '2020-08-12 00:46:55'),
(2, 'বাংলা ২য় পত্র', 'বাংলা ২য় পত্র', 1, '2020-08-12 00:47:15', '2020-08-12 00:47:15'),
(3, 'ইংরেজী ১ম পত্র', 'ইংরেজী ১ম পত্র', 1, '2020-08-10 16:20:28', '2020-08-12 00:47:38'),
(4, 'ইংরেজী ২য়  পত্র', 'ইংরেজী ২য়  পত্র', 1, '2020-08-12 00:48:00', '2020-08-12 00:48:00'),
(5, 'গণিত', 'গণিত', 1, '2020-08-11 13:37:23', '2020-08-11 13:37:23'),
(6, 'বাংলাদেশ ও বিশ্বপরিচয়', 'বাংলাদেশ ও বিশ্বপরিচয়', 1, '2020-08-11 13:37:47', '2020-08-11 13:37:47'),
(7, 'তথ্য ও যোগাযোগ প্রযুক্তি', 'তথ্য ও যোগাযোগ প্রযুক্তি', 1, '2020-08-11 13:38:07', '2020-08-11 13:38:07'),
(8, 'পদার্থ বিজ্ঞান', 'পদার্থ বিজ্ঞান', 1, '2020-08-11 13:38:31', '2020-08-11 13:38:31'),
(9, 'রসায়ন', 'রসায়ন', 1, '2020-08-11 13:39:55', '2020-08-11 13:39:55'),
(10, 'জীববিজ্ঞান', 'জীববিজ্ঞান', 1, '2020-08-11 13:40:21', '2020-08-11 13:40:21'),
(11, 'হিসাব বিজ্ঞান', 'হিসাব বিজ্ঞান', 1, '2020-08-12 00:53:42', '2020-08-12 00:53:42'),
(12, 'অর্থনীতি', 'অর্থনীতি', 1, '2020-08-11 13:39:20', '2020-08-11 13:39:20'),
(13, 'ইসলাম শিক্ষা', 'ইসলাম শিক্ষা', 1, '2020-08-12 00:59:34', '2020-08-12 00:59:34'),
(14, 'সাধারণ বিজ্ঞান', 'সাধারণ বিজ্ঞান', 1, '2020-08-14 09:19:46', '2020-08-14 09:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_theme` int(11) NOT NULL DEFAULT '1',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `cssid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cssclass` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `term_short_description` text COLLATE utf8mb4_unicode_ci,
  `parent` int(11) DEFAULT NULL,
  `connected_with` int(11) DEFAULT NULL,
  `page_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_menu_icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `term_menu_arrow` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `with_sub_menu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_menu_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `column_count` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `banner1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `term_seo_h1` text COLLATE utf8mb4_unicode_ci,
  `term_seo_h2` text COLLATE utf8mb4_unicode_ci,
  `term_seo_title` text COLLATE utf8mb4_unicode_ci,
  `term_seo_description` text COLLATE utf8mb4_unicode_ci,
  `term_seo_keywords` text COLLATE utf8mb4_unicode_ci,
  `onpage_banner` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `name`, `seo_url`, `cat_theme`, `type`, `position`, `cssid`, `cssclass`, `description`, `term_short_description`, `parent`, `connected_with`, `page_image`, `thumb_image`, `home_image`, `term_menu_icon`, `term_menu_arrow`, `with_sub_menu`, `sub_menu_width`, `column_count`, `is_active`, `banner1`, `banner2`, `created_at`, `updated_at`, `term_seo_h1`, `term_seo_h2`, `term_seo_title`, `term_seo_description`, `term_seo_keywords`, `onpage_banner`) VALUES
(2, 'Posts', 'posts', 1, 'category', 2, 'posts', 'posts', 'Posts', 'Posts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-07-15 23:34:21', '2020-07-15 23:34:21', NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Blog', 'blog', 1, 'category', 57, 'blog', 'blog', 'Blog', 'Blog', 2, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-06 11:49:19', '2020-08-06 11:49:19', 'blog', 'blog', 'blog', 'blog', 'blog', NULL),
(62, 'Events', 'events', 1, 'category', 62, 'events', 'events', 'Events', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-06 23:19:07', '2020-08-06 23:19:07', NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'বাণী', 'বাণী', 1, 'category', 63, 'বাণী', 'বাণী', '<span style=\"color: rgb(34, 34, 34); font-family: Consolas, \" lucida=\"\" console\",=\"\" \"courier=\"\" new\",=\"\" monospace;=\"\" font-size:=\"\" 12px;=\"\" white-space:=\"\" pre-wrap;=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);\"=\"\">বাণী</span>', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-11 17:03:00', '2020-08-11 17:06:06', NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'News', 'news', 1, 'category', 65, 'news', 'news', 'News', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-12 01:25:52', '2020-08-12 01:25:52', NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'রাজনীতি', 'রাজনীতি', 1, 'category', 66, 'রাজনীতি', 'রাজনীতি', '<a href=\"http://localhost/toschool_html/\" style=\"color: rgb(34, 34, 34); background-color: rgb(238, 238, 238); font-family: Lato, SolaimanLipi, sans-serif;\">রাজনীতি</a>', NULL, 65, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-12 03:14:33', '2020-08-12 03:14:33', NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'বাংলাদেশ', 'বাংলাদেশ', 1, 'category', 69, 'বাংলাদেশ', 'বাংলাদেশ', '<a href=\"http://localhost/toschool_html/\" style=\"color: rgb(34, 34, 34); background-color: rgb(238, 238, 238); font-family: Lato, SolaimanLipi, sans-serif;\">বাংলাদেশ</a>', NULL, 65, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-12 03:14:53', '2020-08-12 03:14:53', NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'আন্তর্জাতিক', 'আন্তর্জাতিক', 1, 'category', 70, 'আন্তর্জাতিক', 'আন্তর্জাতিক', '<a href=\"http://localhost/toschool_html/\" style=\"color: rgb(34, 34, 34); background-color: rgb(238, 238, 238); font-family: Lato, SolaimanLipi, sans-serif;\">আন্তর্জাতিক</a>', NULL, 65, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-12 03:15:20', '2020-08-12 03:15:20', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'বিনোদন', 'বিনোদন', 1, 'category', 71, 'বিনোদন', 'বিনোদন', 'বিনোদন', NULL, 65, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-12 03:24:11', '2020-08-12 03:24:11', NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'লিড নিউজ', 'লিড নিউজ', 1, 'category', 72, 'লিড নিউজ', 'লিড নিউজ', 'লিড নিউজ', NULL, 65, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '2020-08-12 09:01:32', '2020-11-21 00:39:50', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ukey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('Male','Female','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` enum('Married','Single','Widow','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `father` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `thana` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teacher_type` tinytext COLLATE utf8mb4_unicode_ci,
  `subject_id` tinytext COLLATE utf8mb4_unicode_ci,
  `employee_title` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deliveryfee` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mark_verifed` int(11) NOT NULL DEFAULT '0',
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reward_points` int(11) NOT NULL DEFAULT '0',
  `vendor_status` int(11) DEFAULT NULL,
  `vendor_banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_description` longtext COLLATE utf8mb4_unicode_ci,
  `vendor_dis_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_dis_img_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_mark_as_authorized` tinyint(4) DEFAULT NULL,
  `vendor_trade_license_no` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_vat_certificate` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_etin_certificate` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_bank_solvency_certificate` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_incorporation_certificate` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_article_of_memorandum` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_bank_account_details` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_nid` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_md_photo` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_md_signature` tinytext COLLATE utf8mb4_unicode_ci,
  `vendor_shop_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `login_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_no`, `name`, `email`, `username`, `ukey`, `seo_url`, `birthday`, `gender`, `marital_status`, `join_date`, `father`, `mother`, `password`, `school`, `company`, `address`, `thana`, `phone`, `teacher_type`, `subject_id`, `employee_title`, `district`, `deliveryfee`, `mark_verifed`, `postcode`, `reward_points`, `vendor_status`, `vendor_banner`, `vendor_logo`, `vendor_title`, `vendor_description`, `vendor_dis_img`, `vendor_dis_img_title`, `vendor_web`, `vendor_mark_as_authorized`, `vendor_trade_license_no`, `vendor_vat_certificate`, `vendor_etin_certificate`, `vendor_bank_solvency_certificate`, `vendor_incorporation_certificate`, `vendor_article_of_memorandum`, `vendor_bank_account_details`, `vendor_nid`, `vendor_md_photo`, `vendor_md_signature`, `vendor_shop_type`, `provider`, `provider_id`, `is_active`, `login_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Palash Roy', 'info@roydomi.com', NULL, NULL, NULL, '1970-01-01', 'Male', 'Married', '2020-02-02', NULL, NULL, '$2y$10$3uIb4PQ3GNJqRSBmvLPGjO2tRlf7iDGdphHyd2pv7FIqXyVMj7J4u', NULL, 'RFL Bestbuy', 'Demo', '2/1/A, Block G', '01915513898', '', NULL, '01521112134', 'Dhaka', NULL, 0, '1207', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'BLBm3iuQanJeULktEgMTmDAPoN7HRitBh4VXmwkcYgDPaC8LOjDGYuO0mlxv', '2018-01-30 21:01:03', '2020-09-24 07:50:02'),
(9, NULL, 'Md. Abdul Malek', 'malek@mail.com', NULL, NULL, NULL, '1970-01-01', NULL, NULL, '2020-02-02', NULL, NULL, '$2y$10$ZS5G5xuqtJmBks64OWDwouNtNj2/qJ2OzzAgUYu7Xs8ZijdTlUKaC', NULL, 'জেলা শিক্ষা অফিস', NULL, NULL, NULL, '', '', 'Assistant Teacher', NULL, NULL, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-11 09:32:46', '2020-11-21 03:18:51'),
(10, NULL, 'Mohammad Kamrul Azad', '', NULL, NULL, NULL, '1970-01-01', NULL, NULL, '2002-02-02', NULL, NULL, '$2y$10$hoLPWNh5GTgJvc4gvgCGieZVryfSsYmpcrG1DMS/sEnml72sLTXDS', NULL, 'উপজেলা মাধ্যমিক শিক্ষা অফিস', NULL, 'Kalihati', NULL, 'headmaster,teacher', '1', NULL, 'Tangail', NULL, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-11 09:34:47', '2020-11-21 03:19:09'),
(11, NULL, 'Abdullah Khan', 'abdullah.khan@mail.com', NULL, NULL, NULL, '1970-01-01', NULL, NULL, '2002-02-02', NULL, NULL, '$2y$10$e2oPbzff4Csrs12Bug.LE.QU.krASfFakbHgQbyIY2su9vcdzdi2a', 'Azim Memorial High School', 'জেলা শিক্ষা অফিস', NULL, 'Modhupur', NULL, '', '', 'Assistant Teacher', 'Tangail', NULL, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-08-14 09:24:34', '2020-11-21 03:18:36'),
(18, NULL, 'Revehost', 'revehost@gmail.com', NULL, NULL, NULL, '1970-01-01', NULL, NULL, '2010-02-01', NULL, NULL, '$2y$10$EjO1E1Vn8pbUvjpKCrA6iOUxRuY6bt4.unD3acuDW5J7dKuGaVoKK', NULL, NULL, 'Tejgoan', NULL, '+8801677618199', '', '', NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2020-11-21 06:09:33', '2020-11-21 06:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `video_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_lession` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `video_title`, `video_link`, `video_image`, `class_id`, `subject_id`, `subject_lession`, `teacher_id`, `user_id`, `is_active`, `created_at`, `updated_at`) VALUES
(6, 'Class : Six, English First Paper  Lesson : 13', 'https://www.facebook.com/tangailonlineschool/videos/703903730428429/', NULL, 1, 3, 13, 10, 1, 1, '2020-08-14 09:21:59', '2020-08-14 09:25:01'),
(7, 'demo 2', 'https://www.facebook.com/tangailgirlshighsschool/videos/398819734802281/', NULL, 1, 14, 10, 9, 1, 1, '2020-11-19 12:22:59', '2020-11-19 12:23:07'),
(8, 'উচ্চতর গণিত', 'https://www.facebook.com/plahstliveclass/videos/1279769229022710', NULL, 4, 5, 6, 9, 1, 1, '2020-11-21 05:23:36', '2020-11-21 05:23:36');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `cssid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cssclass` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `special` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `name`, `type`, `position`, `cssid`, `cssclass`, `description`, `special`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Homepage Slider  Right widget', 'text', 1, 'w', 'w', '<img src=\"../storage/uploads/iconsize/2020-08/national-helpline_1597232645.jpg\" alt=\"\" class=\"img-fluid\">', '1', 1, '2020-08-12 05:36:53', '2020-11-19 06:45:08'),
(2, 'Homepage Menu Link Box', 'text', 2, '1', 'a', '<div class=\"row\">\r\n\r\n<!-- 1 -->\r\n<div class=\"col-md-4\">\r\n  <div class=\"card slider-right-box mb-2\">\r\n				<div class=\"card-body box\">\r\n					<h4>শুদ্ধাচার</h4>\r\n					<div class=\"row\">\r\n						<div class=\"col-md-5 col-4\">\r\n							<img class=\"img-fluid\" src=\"http://tangailonlineschool.edu.bd/storage/uploads/iconsize/2020-11/শুদ্ধাচার_1605830373.png\">\r\n						</div>\r\n						<div class=\"col-md-7 col-8\">\r\n							<ul class=\"box-caption\">\r\n																	<li><a href=\"http://bangladesh.gov.bd/site/view/policy/\" title=\"জাতীয় শুদ্ধাচার কৌশল\">জাতীয় শুদ্ধাচার কৌশল</a></li>\r\n<li><a href=\"http://জেলা-কমিটি/\" title=\"জেলা কমিটি\">জেলা কমিটি</a></li>\r\n<li><a href=\"http://www.tangail.gov.bd/site/page/fa3f965c-2ba6-42d8-a2ec-aabcf62e048a/%E0%A6%95%E0%A6%B0%E0%A7%8D%E0%A6%AE%E0%A6%AA%E0%A6%B0%E0%A6%BF%E0%A6%95%E0%A6%B2%E0%A7%8D%E0%A6%AA%E0%A6%A8%E0%A6%BE\" title=\"কর্মপরিকল্পনা\">কর্মপরিকল্পনা</a></li>\r\n<li><a href=\"http://www.tangail.gov.bd/site/page/a32ce043-d68f-40d3-a098-cca419e879ff/%E0%A6%AA%E0%A6%B0%E0%A6%BF%E0%A6%AA%E0%A6%A4%E0%A7%8D%E0%A6%B0/%E0%A6%A8%E0%A7%80%E0%A6%A4%E0%A6%BF%E0%A6%AE%E0%A6%BE%E0%A6%B2%E0%A6%BE\" title=\"পরিপত্র/নীতিমালা\">পরিপত্র/নীতিমালা</a></li>\r\n															</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n</div>\r\n\r\n\r\n\r\n<!-- 2 -->\r\n\r\n<div class=\"col-md-4\">\r\n  <div class=\"card slider-right-box mb-2\">\r\n				<div class=\"card-body box\">\r\n					<h4>বিজয় ফুল</h4>\r\n					<div class=\"row\">\r\n						<div class=\"col-md-5 col-4\">\r\n							<img class=\"img-fluid\" src=\"http://tangailonlineschool.edu.bd/storage/uploads/iconsize/2020-11/বিজয়-ফুল_1605830409.png\">\r\n						</div>\r\n						<div class=\"col-md-7 col-8\">\r\n							<ul class=\"box-caption\">\r\n																	<li><a href=\"https://cabinet.gov.bd/site/db_video_gallray/4e438dd1-df65-4645-8e0e-dfae3601703d/%E0%A6%AC%E0%A6%BF%E0%A6%9C%E0%A7%9F-%E0%A6%AB%E0%A7%81%E0%A6%B2-%E0%A6%A4%E0%A7%88%E0%A6%B0%E0%A6%BF%E0%A6%B0-%E0%A6%95%E0%A7%8C%E0%A6%B6%E0%A6%B2-/\" title=\"বিজয় ফুল তৈরির কৌশল \">বিজয় ফুল তৈরির কৌশল </a></li>\r\n				<li><a href=\"https://cabinet.gov.bd/site/db_video_gallray/32799978-589e-4692-a4e6-ff4518fab1af/%E0%A6%AC%E0%A6%BF%E0%A6%9C%E0%A7%9F-%E0%A6%AB%E0%A7%81%E0%A6%B2-%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%A4%E0%A6%BF%E0%A6%AF%E0%A7%8B%E0%A6%97%E0%A6%BF%E0%A6%A4%E0%A6%BE%E0%A6%B0-%E0%A6%A5%E0%A6%BF%E0%A6%AE-%E0%A6%B8%E0%A6%82/\" title=\"প্রতিযোগিতার থিম সং\">প্রতিযোগিতার থিম সং</a></li>\r\n				<li><a href=\"https://cabinet.gov.bd/site/db_video_gallray/ff3f2861-383b-41e0-b8e6-007ac7c28dae//%E0%A6%AC%E0%A6%BF%E0%A6%9C%E0%A7%9F-%E0%A6%AB%E0%A7%81%E0%A6%B2-%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%A4%E0%A6%BF%E0%A6%AF%E0%A7%8B%E0%A6%97%E0%A6%BF%E0%A6%A4%E0%A6%BE,-%E0%A7%A8%E0%A7%A6%E0%A7%A7%E0%A7%AF-%E0%A6%8F%E0%A6%B0-%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%9A%E0%A6%BE%E0%A6%B0%E0%A6%A3%E0%A6%BE%E0%A6%AE%E0%A7%82%E0%A6%B2%E0%A6%95-%E0%A6%AD%E0%A6%BF%E0%A6%A1%E0%A6%BF%E0%A6%93/\" title=\"প্রতিযোগিতার প্রচারণামূলক ভিডিও\">প্রতিযোগিতার প্রচারণামূলক ভিডিও</a></li>\r\n															</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n</div>\r\n\r\n\r\n<!-- 3 -->\r\n\r\n<div class=\"col-md-4\">\r\n  <div class=\"card slider-right-box mb-2\">\r\n				<div class=\"card-body box\">\r\n					<h4>সেবাসমূহ</h4>\r\n					<div class=\"row\">\r\n						<div class=\"col-md-5 col-4\">\r\n							<img class=\"img-fluid\" src=\"http://tangailonlineschool.edu.bd/storage/uploads/iconsize/2020-11/সেবাসমূহ_1605830428.png\">\r\n						</div>\r\n						<div class=\"col-md-7 col-8\">\r\n							<ul class=\"box-caption\">\r\n																	<li><a href=\"http://www.ais.gov.bd/\" title=\"সেবার তালিকা	\">সেবার তালিকা	</a></li>\r\n				<li><a href=\"https://www.facebook.com/fishadvice/\" title=\"কী সেবা কীভাবে পাবেন	\">কী সেবা কীভাবে পাবেন	</a></li>\r\n				<li><a href=\"http://sms.dls.gov.bd/\" title=\"সিটিজেন চার্টার	\">সিটিজেন চার্টার	</a></li>\r\n				<li><a href=\"https://play.google.com/store/apps/details?id=com.FATB/\" title=\"সেবাকুঞ্জ	\">সেবাকুঞ্জ	</a></li>\r\n															</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n</div>\r\n\r\n<!-- 4 -->\r\n\r\n<div class=\"col-md-4\">\r\n  <div class=\"card slider-right-box mb-2\">\r\n				<div class=\"card-body box\">\r\n					<h4>অভিযোগ প্রতিকার ব্যবস্থাপনা</h4>\r\n					<div class=\"row\">\r\n						<div class=\"col-md-5 col-4\">\r\n							<img class=\"img-fluid\" src=\"http://tangailonlineschool.edu.bd/storage/uploads/iconsize/2020-11/অভিযোগ-প্রতিকার-ব্যবস্থাপনা_1605830443.png\">\r\n						</div>\r\n						<div class=\"col-md-7 col-8\">\r\n							<ul class=\"box-caption\">\r\n																	<li><a href=\"http://bangladesh.gov.bd/site/view/policy/\" title=\"নির্দেশিকাসমূহ	\">নির্দেশিকাসমূহ	</a></li>\r\n				<li><a href=\"http://nothi.gov.bd/dak_nagoriks/NagorikAbedon/\" title=\"অনলাইনে অভিযোগ দাখিল	\">অনলাইনে অভিযোগ দাখিল	</a></li>\r\n				<li><a href=\"http://www.grs.gov.bd/\" title=\"অভিযোগ প্রতিকার ব্যবস্থা\">অভিযোগ প্রতিকার ব্যবস্থা</a></li>\r\n				<li><a href=\"/site/onik/9795a9ea-db54-4f03-a764-8109a928acfc/অনিক-ও-আপিল-কর্মকর্তা\" title=\"অনিক ও আপিল কর্মকর্তা\">অনিক ও আপিল কর্মকর্তা</a></li>\r\n															</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n</div>\r\n\r\n<!-- 5 -->\r\n\r\n<div class=\"col-md-4\">\r\n  <div class=\"card slider-right-box mb-2\">\r\n				<div class=\"card-body box\">\r\n					<h4>তথ্য অধিকার</h4>\r\n					<div class=\"row\">\r\n						<div class=\"col-md-5 col-4\">\r\n							<img class=\"img-fluid\" src=\"http://tangailonlineschool.edu.bd/storage/uploads/iconsize/2020-11/তথ্য-অধিকার_1605830474.png\">\r\n						</div>\r\n						<div class=\"col-md-7 col-8\">\r\n							<ul class=\"box-caption\">\r\n																	<li><a href=\"/site/info_officer/4ca79745-16e6-40ec-8d2c-6736bf00b7a8/তথ্য-প্রদানকারি-কর্মকর্তা-\" title=\"তথ্য প্রদানকারি কর্মকর্তা \">তথ্য প্রদানকারি কর্মকর্তা </a></li>\r\n				<li><a href=\"http://www.infocom.gov.bd/site/view/law/%E0%A6%86%E0%A6%87%E0%A6%A8,-%E0%A6%AC%E0%A6%BF%E0%A6%A7%E0%A6%BF%E0%A6%AE%E0%A6%BE%E0%A6%B2%E0%A6%BE-%E0%A6%93-%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%AC%E0%A6%BF%E0%A6%A7%E0%A6%BE%E0%A6%A8%E0%A6%AE%E0%A6%BE%E0%A6%B2%E0%A6%BE/\" title=\"তথ্য আইন ও বিধিমালা\">তথ্য আইন ও বিধিমালা</a></li>\r\n				<li><a href=\"/#/কর্মকর্তা-ও-আবেদনকারির-নির্দেশিকা-\" title=\"কর্মকর্তা ও আবেদনকারির নির্দেশিকা \">কর্মকর্তা ও আবেদনকারির নির্দেশিকা </a></li>\r\n				<li><a href=\"http://www.forms.gov.bd/site/view/form-office/6621/তথ্য-কমিশন/\" title=\"তথ্যের আবেদন ফরম\">তথ্যের আবেদন ফরম</a></li>\r\n															</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n</div>\r\n\r\n<!-- 6 -->\r\n\r\n<div class=\"col-md-4\">\r\n  <div class=\"card slider-right-box mb-2\">\r\n				<div class=\"card-body box\">\r\n					<h4>আইন ও বিধি</h4>\r\n					<div class=\"row\">\r\n						<div class=\"col-md-5 col-4\">\r\n							<img class=\"img-fluid\" src=\"http://tangailonlineschool.edu.bd/storage/uploads/iconsize/2020-11/আইন-ও-বিধি_1605830492.png\">\r\n						</div>\r\n						<div class=\"col-md-7 col-8\">\r\n							<ul class=\"box-caption\">\r\n																	<li><a href=\"http://bdlaws.minlaw.gov.bd/\" title=\"আইন	\">আইন	</a></li>\r\n				<li><a href=\"http://bangladesh.gov.bd/site/view/policy/\" title=\"বিধিমালা	\">বিধিমালা	</a></li>\r\n				<li><a href=\"/site/law_policy/00e45052-e4e8-4b84-b26f-47682d68b0e1/পরিপত্র/নীতিমালা	\" title=\"পরিপত্র/নীতিমালা	\">পরিপত্র/নীতিমালা	</a></li>\r\n				<li><a href=\"http://bangladesh.gov.bd/site/view/gurd_files_category/\" title=\"গার্ডফাইল	\">গার্ডফাইল	</a></li>\r\n\r\n															</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n</div>\r\n\r\n<!-- -->\r\n\r\n\r\n</div>', '1', 1, '2020-11-19 13:10:56', '2020-11-19 18:02:34'),
(3, 'Footer Widget', 'text', 3, 'f', 'f', '<div class=\"col-6 col-md-6 col-lg-3\">\r\n				<div class=\"short-about\">\r\n					<div class=\"footer-title\">\r\n						<h5>পরিকল্পনা ও বাস্তবায়নে </h5>\r\n					</div>\r\n					<p>\r\n						শিক্ষা মন্ত্রণালয় <br>\r\n						প্রাথমিক ও গণশিক্ষা মন্ত্রণালয় <br>\r\n						মাধ্যমিক ও উচ্চশিক্ষা অধিদপ্তর এবং<br>\r\n						প্রাথমিক শিক্ষা অধিদপ্তর\r\n					</p>\r\n\r\n					<div class=\"footer-title ftb-3 mt-3\">\r\n						<h5> ব্যবহার নির্দেশিকা </h5>\r\n					</div>\r\n\r\n					<p class=\"footer-link\">\r\n						<a href=\"\">\r\n							শিক্ষক বাতায়ন ব্যবহার জানতে ক্লিক করুন\r\n						</a>\r\n					</p>\r\n					\r\n				</div>\r\n			</div>\r\n			<div class=\"col-6 col-md-6 col-lg-3\">\r\n				<div class=\"footer-title\">\r\n					<h5> যোগাযোগ </h5>\r\n				</div>\r\n				<p> </p>\r\n				<p> ইমেইলঃ <br><span>info@tanagilonlineschool.edu.bd</span></p>\r\n\r\n				\r\n\r\n				<nav class=\"nav social-links mt-3\">\r\n					<div class=\"s-links form-inline\">\r\n						<a href=\"\"><i class=\"fa fa-facebook-f\"></i></a>\r\n						<a href=\"\"><i class=\"fa fa-twitter\"></i></a>\r\n						<a href=\"\"><i class=\"fa fa-linkedin\"></i></a>\r\n					</div>\r\n				</nav>\r\n\r\n			</div>\r\n			<div class=\"col-6 col-md-6 col-lg-3\">\r\n				<div class=\"footer-title ftb-4\">\r\n					<h5> গুরুত্বপূর্ণ লিংক </h5>\r\n				</div>\r\n\r\n				<ul class=\"footer-link\">\r\n					<li><a href=\"\">প্রধানমন্ত্রীর কার্যালয়</a> </li>\r\n					<li><a href=\"\">শিক্ষা মন্ত্রণালয়</a> </li>\r\n					<li><a href=\"\">মন্ত্রিপরিষদ বিভাগ</a> </li>\r\n\r\n					<li><a href=\"\">প্রাথমিক ও গণশিক্ষা মন্ত্রণালয়</a> </li>\r\n					<li><a href=\"\">প্রাথমিক শিক্ষা অধিদপ্তর</a> </li>\r\n					<li><a href=\"\">মাধ্যমিক ও উচ্চ শিক্ষা অধিদপ্তর</a> </li>\r\n					<li><a href=\"\">ডাক ও টেলিযোগাযোগ বিভাগ</a> </li>\r\n					<li><a href=\"\"> তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ</a> </li>\r\n					<li><a href=\"\">জাতীয় তথ্য বাতায়ন</a> </li>\r\n\r\n				</ul>\r\n			</div>\r\n			<div class=\"col-6 col-md-6 col-lg-3\">\r\n				<div class=\"footer-title ftb-5\">\r\n					<h5> গুরুত্বপূর্ণ লিংক </h5>\r\n				</div>\r\n\r\n				<ul class=\"footer-link\">\r\n					<li><a href=\"\">জাতীয় শিক্ষাক্রম ও পাঠ্যপুস্তক বোর্ড (এনসিটিবি)</a> </li>\r\n					<li><a href=\"\">জাতীয় শিক্ষা ব্যবস্থাপনা একাডেমি(নায়েম)</a> </li>\r\n					<li><a href=\"\">বাংলাদেশ শিক্ষাতথ্য ও পরিসংখ্যান ব্যুরো(ব্যানবেইস)</a> </li>\r\n					<li><a href=\"\">মুক্তপাঠ</a> </li>\r\n					<li><a href=\"\">কিশোর বাতায়ন</a> </li>\r\n					<li><a href=\"\">মাল্টিমিডিয়া ক্লাসরুম মনিটরিং সিস্টেম</a> </li>\r\n					<li><a href=\"\"> এসডিজি ট্র্যাকার </a> </li>\r\n				</ul>\r\n\r\n				<a href=\"\" class=\"\">\r\n					<img src=\"/public/assets/images/google-play.png\" class=\"mt-3\" alt=\"\" height=\"35\">\r\n				</a>\r\n			</div>', '1', 1, '2020-11-21 07:41:36', '2020-11-21 08:09:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `albums_position_unique` (`position`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homesettings`
--
ALTER TABLE `homesettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ictlab`
--
ALTER TABLE `ictlab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_user_id_foreign` (`user_id`);

--
-- Indexes for table `institutions`
--
ALTER TABLE `institutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_foreign` (`menu`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seo_url` (`seo_url`),
  ADD KEY `pages_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_user_user_id_unique` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `terms_seo_url_unique` (`seo_url`),
  ADD UNIQUE KEY `terms_position_unique` (`position`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `seo_url` (`seo_url`),
  ADD UNIQUE KEY `ukey` (`ukey`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `widgets_position_unique` (`position`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `homesettings`
--
ALTER TABLE `homesettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ictlab`
--
ALTER TABLE `ictlab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `institutions`
--
ALTER TABLE `institutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
