@extends('layouts.app')

@section('title', 'Distribution Settings')
@section('sub_title', 'Distribution settings modification panel')
@section('content')
    <div class="row">
        @if(!empty($distribution_setting))

            @php
                $setting = $distribution_setting[0];
                 if (!empty($setting)) {
                    if ($setting->showrooms_depo == 1) {
                        $showrooms_depo = TRUE;
                    } else {
                        $showrooms_depo = FALSE;
                    }
                     if ($setting->product_individual == 1) {
                        $product_individual = TRUE;
                    } else {
                        $product_individual = FALSE;
                    }
                     if ($setting->showrooms_do == 1) {
                        $showrooms_do = TRUE;
                    } else {
                        $showrooms_do = FALSE;
                    }

                } else {
                    $product_individual = FALSE;
                    $showrooms_depo = FALSE;
                    $showrooms_do = FALSE;
                }
            @endphp
        @endif
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($setting->id))
                        setting_forms
                    @else
                        setting_form
                    @endif
                @endslot
                @slot('title')
                    @if (!empty($setting->id))
                        Edit setting
                    @else
                        Add a new  setting
                    @endif

                @endslot

                @slot('route')
                    @if (!empty($setting->id))
                        {{route('admin.distribution_setting.update',$setting->id )}}
                    @else
                            {{route('admin.distribution_setting.store' )}}
                    @endif
                @endslot

                    @slot('method')
                        @if (!empty($setting->id))
                           {{method_field('PUT')}}

                        @endif
                    @endslot

                @slot('fields')
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                {{ Form::label('circle_distance', 'Circle Distance', array('class' => 'circle_distance')) }}
                                {{ Form::text('circle_distance', (!empty($setting->circle_distance) ? $setting->circle_distance : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Circle Distance']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('showrooms_maximum', 'Maximum Showroom', array('class' => 'Maximum Showroom')) }}
                                {{ Form::text('showrooms_maximum', (!empty($setting->showrooms_maximum) ? $setting->showrooms_maximum : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Maximum Showroom']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_order_check', 'Product Check Order ', array('class' => 'Maximum Showroom')) }}
                                {{ Form::text('product_order_check', (!empty($setting->product_order_check) ? $setting->product_order_check : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Product check order ']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_shipment', 'Product Shipment ', array('class' => 'Maximum Showroom')) }}
                                {{ Form::text('product_shipment', (!empty($setting->product_shipment) ? $setting->product_shipment : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Product Shipment ']) }}
                                <small>Pattern: 1-4|5-10|10-20</small>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                {{ Form::label('showrooms_depo', 'Showrooms not match then go to DEPO', array('class' => 'showrooms_depo')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('showrooms_depo', 1, ($showrooms_depo == TRUE) ? TRUE : FALSE, ['class' => 'radio']) }}
                                        Active
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('showrooms_depo', 0, ($showrooms_depo == FALSE) ? TRUE : FALSE, ['class' => 'radio']) }}
                                        Deactive
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('product_individual', 'Product Individually Check ', array('class' => 'product_individual')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('product_individual', 1, ($product_individual == TRUE) ? TRUE : FALSE, ['class' => 'radio']) }}
                                        Active
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('product_individual', 0, ($product_individual == FALSE) ? TRUE : FALSE, ['class' => 'radio']) }}
                                        Deactive
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('showrooms_do', 'Showrooms to DO ', array('class' => 'pro')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('showrooms_do', 1, ($showrooms_do == TRUE) ? TRUE : FALSE, ['class' => 'radio']) }}
                                        Active
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('showrooms_do', 0, ($showrooms_do == FALSE) ? TRUE : FALSE, ['class' => 'radio']) }}
                                        Deactive
                                    </label>
                                </div>
                            </div>





                        </div>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection