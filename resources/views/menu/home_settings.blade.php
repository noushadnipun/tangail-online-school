@extends('layouts.app')

@section('title', 'Home Settings')
@section('sub_title', 'home settings modification panel')
@section('content')
    <div class="row">
        @if(!empty($settings))
            <?php //owndebugger($settings); ?>
            <?php $setting = $settings[0]; ?>
        @endif
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

    
        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="col-md-12">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($setting->id))
                        setting_forms
                    @else
                        setting_form
                    @endif
                @endslot
                @slot('title')
                    @if (!empty($setting->id))
                        Edit setting
                    @else
                        Add a new setting
                    @endif

                @endslot

                @slot('route')
                    @if (!empty($setting->id))
                        homesetting/{{$setting->id}}/update
                    @else
                        homesetting_save
                    @endif
                @endslot

                @slot('fields')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('cat_first', 'Category First Row', array('class' => 'cat_first')) }}
                                {{ Form::text('cat_first', (!empty($setting->cat_first) ? $setting->cat_first : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'category']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_second', 'Category Second Row', array('class' => 'cat_second')) }}
                                {{ Form::text('cat_second', (!empty($setting->cat_second) ? $setting->cat_second : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'category']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_third', 'Category Third Row', array('class' => 'cat_third')) }}
                                {{ Form::text('cat_third', (!empty($setting->cat_third) ? $setting->cat_third : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'category']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_fourth', 'Category Fourth Row', array('class' => 'cat_fourth')) }}
                                {{ Form::text('cat_fourth', (!empty($setting->cat_fourth) ? $setting->cat_fourth : NULL), ['class' => 'form-control', 'placeholder' => 'category']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_fifth', 'Category Fifth Row', array('class' => 'cat_fifth')) }}
                                {{ Form::text('cat_fifth', (!empty($setting->cat_fifth) ? $setting->cat_fifth : NULL), ['class' => 'form-control', 'placeholder' => 'category']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_sixth', 'Category Sixth Row', array('class' => 'cat_sixth')) }}
                                {{ Form::text('cat_sixth', (!empty($setting->cat_sixth) ? $setting->cat_sixth : NULL), ['class' => 'form-control', 'placeholder' => 'Enter category setting...']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_seventh', 'Category Seventh Row', array('class' => 'cat_seventh')) }}
                                {{ Form::text('cat_seventh', (!empty($setting->cat_seventh) ? $setting->cat_seventh : NULL), ['class' => 'form-control', 'placeholder' => 'Enter category setting...']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>
                            <div class="form-group">
                                {{ Form::label('cat_eighth', 'Category Eighth Row', array('class' => 'cat_eighth')) }}
                                {{ Form::text('cat_eighth', (!empty($setting->cat_eighth) ? $setting->cat_eighth : NULL), ['class' => 'form-control', 'placeholder' => 'Enter category setting...']) }}
                                <small>Pattern: CatID|ImageAlignment|LimitOfProduct</small>
                            </div>

                            <div class="form-group">
                                {{ Form::label('home_category', 'Home Category', array('class' => 'home_category')) }}
                                {{ Form::textarea('home_category', (!empty($setting->home_category) ? $setting->home_category : NULL), ['class' => 'form-control', 'rows' => 3,'placeholder' => 'Enter category ID ...']) }}
                                <small>Pattern: CatID|CatID.....</small>
                            </div>
                        </div>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection