@extends('layouts.app')

@section('title', 'Change User Permissions')
@section('sub_title', 'permission change or modification page')
@section('content')
    <div class="">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="box box-warning">

            <div class="box-header with-border">
                <h3 class="box-title">
                    @if (!empty($user->id))
                        Edit user permissions
                    @endif
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        @if (!empty($user->id))
                            @php
                                $route = route('admin.modify_role', $user->id);
                            @endphp
                        @else
                            @php
                                $route = route('admin.modify_role');
                            @endphp
                        @endif
                        {{ Form::open(array('url' => $route, 'method' => 'post', 'value' => 'PATCH', 'id' => 'modify_role', 'files' => true, 'autocomplete' => 'off')) }}
                        {{ Form::hidden('id', (!empty($user->id) ? $user->id : NULL), ['id' => 'user_id']) }}
                        <div class="form-group">
                            You are changing permissions for: <strong>{{ $user->email }}</strong>.
                            Current user role is <strong>{{ $user->roles->first()->name }}</strong>.
                            <br/>If you are sure, then go on and tick on the page lists to update-
                        </div>
                        <div class="form-group">
                            <div class="box-body no-padding">
                                @php
                                    $all_roles = \App\Route::groupBy('in_group')->get();
                                @endphp
                                @foreach($all_roles as $role)
                                    @if(request()->get('role') == $role->in_group)
                                        @php
                                            $btn = ' btn-success';
                                        @endphp
                                    @else
                                        @php
                                            $btn = ' btn-info';
                                        @endphp
                                    @endif
                                    <a href="{{ route('admin.modify_role_view', (!empty($user->id) ? $user->id : NULL)) }}?role={{ $role->in_group }}"
                                       class="btn btn-xs {{ $btn }}">
                                        {{ $role->in_group }}
                                    </a>
                                @endforeach
                            </div>
                            <br/>
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover" id="permission_table">
                                    <tbody>
                                    <tr>
                                        <th><input type="checkbox" id="ckbCheckAll"/></th>
                                        <th style="text-align: left;">Route ID</th>
                                        <th style="text-align: left;">Page URI</th>
                                        <th style="text-align: left;">Route Name</th>
                                    </tr>
                                    <?php
                                    if (!empty(request()->get('role'))) {
                                        $role = request()->get('role');
                                        $data = \App\Route::where(['in_group' => $role])->get();
                                    }
                                    $routes = [];
                                    $i = 0;
                                    foreach ($data as $route) {
                                        //dd($route);
                                        echo '<tr>';

                                        $matched = \App\MixTaxonomy::select('mtvalue')
                                            ->where('mtkey', 'useraccesspermission_' . $user->id . '_' . $route->route_hash)
                                            ->get()->first();
                                        if ($matched['mtvalue'] == 'on') {
                                            $checked = "checked=checked";
                                            $value = 'on';
                                        } else {
                                            $checked = "";
                                            $value = 'off';
                                        }
                                        echo '<td>';
                                        echo '<input class="checked_checking"
                                                   name="page_list[' . $i . '][]"
                                                   value="' . $route->route_hash . '"
                                                   type="hidden" />';
                                        echo '<input class="checked_checking"
                                                   name="page_list[' . $i . '][]"
                                                   type="checkbox"
                                                    ' . $checked . ' />';
                                        echo '</td>';
                                        echo '<td>' . $route->id . '</td>';
                                        echo '<td>' . $route->route_uri . '</td>';
                                        echo '<td>' . $route->route_name . '</td>';
                                        echo '</tr>';
                                        $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{ Form::submit('Save Changes', ['name' => 'submit_role', 'class' => 'btn btn-success']) }}
                    </div>
                </div>
            </div>

            <div class="box-footer">
            </div>
        </div>
        {{ Form::close() }}

    </div>
@endsection

@push('scripts')

    <script type="text/javascript">

        jQuery(document).ready(function ($) {
            $.noConflict();

            $("#ckbCheckAll").click(function () {
                $(".checked_checking").prop('checked', $(this).prop('checked'));
            });

            $(".checked_checking").change(function () {
                if (!$(this).prop("checked")) {
                    $("#ckbCheckAll").prop("checked", false);
                }
            });

            // $('.checked_checking').on('change', function () {
            //     if (this.checked == true || this.checked == false) {
            //
            //         var prefix = 'useraccesspermission_';
            //         var userid = $('#user_id').val();
            //         var key = $(this).data('key');
            //         var fullpattern = prefix + userid + '_' + key;
            //
            //         if (this.checked == true) {
            //             var mtvalue = 'on';
            //         } else {
            //             var mtvalue = 'off';
            //         }
            //         //console.log(fullpattern, mtvalue);
            //
            //         // var id = jQuery(self).data('id');
            //         // var load_class = jQuery(self).data('load_class');
            //         // load_class = '.'+load_class;
            //         var data = {'pattern': fullpattern, 'mtvalue': mtvalue};
            //
            //
            //         $.ajax({
            //             url: baseurl + '/admin/modify_role',
            //             method: 'get',
            //             data: data,
            //             success: function (data) {
            //                 location.reload();
            //             },
            //             error: function () {
            //             }
            //         });
            //     }
            // });


            $('#table_search').keyup(function () {
                search_table($(this).val());
            });

            function search_table(value) {
                $('#permission_table tr').each(function () {
                    var found = 'false';
                    $(this).each(function () {
                        if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {
                            found = 'true';
                        }
                    });
                    if (found == 'true') {
                        $(this).show();
                    } else {
                        $(this).children('td').children('.checked_checking').removeAttr('name');
                        $(this).hide();
                    }
                });
            }
        });


    </script>

@endpush