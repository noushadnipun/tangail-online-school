@extends('layouts.app')

@section('title', 'Profile')
@section('sub_title', 'profile add or modification form')
@section('content')
    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
         @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
    
    <div class="row">

        @component('component.form')
            @slot('method')
                {{ method_field(isset($user->id) ? 'PUT' : 'POST') }}
            @endslot

            @slot('form_id')
                @if (!empty($user->id))
                    user_forms
                @else
                    user_form
                @endif
            @endslot
            @slot('title')
                @if (!empty($user->id))
                    Edit user
                @else
                    Add a new user
                @endif
            @endslot

            @slot('route')
                @if (!empty($user->id))
                    {{ route('admin.users.update', $user->id) }}
                @else
                    {{ route('admin.users.store') }}
                @endif
            @endslot
            @slot('method')
                @if (!empty($user->id))
                    {{ method_field('PUT') }}

                @endif
            @endslot

            @slot('fields')
                <div class="col-md-4">

                    {{-- <div class="form-group">
                        {{ Form::label('employee_no', 'Employee No', array('class' => 'employee_no')) }}
                        {{ Form::text('employee_no', (!empty($user->employee_no) ? $user->employee_no : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter employee no...']) }}
                    </div> --}}

                    <div class="form-group">
                        {{ Form::label('name', 'Name', array('class' => 'name')) }}
                        {{ Form::text('name', (!empty($user->name) ? $user->name : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter name...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Email', array('class' => 'email')) }}
                        {{ Form::text('email', (!empty($user->email) ? $user->email : NULL), ['',  'type' => 'email', '', 'class' => 'form-control', 'placeholder' => 'Enter email...']) }}
                    </div>

                   {{--  <div class="form-group">
                        {{ Form::label('username', 'Username', array('class' => 'username')) }}
                        {{ Form::text('username', (!empty($user->username) ? $user->username : NULL), ['', 'class' => 'form-control', 'placeholder' => 'Enter username...']) }}
                    </div> --}}

                    {{-- <div class="form-group">
                        {{ Form::label('seo_url', 'Seo url', array('class' => 'seo_url')) }}
                        {{ Form::text('seo_url', (!empty($user->seo_url) ? $user->seo_url : NULL), ['id' => 'seo_url','class' => 'form-control', 'placeholder' => 'Enter seo url...']) }}
                        <div id="error_seo_url"></div>
                    </div> --}}

                    <div class="form-group">
                        {{ Form::label('birthday', 'Birthday', array('class' => 'birthday')) }}
                        {{ Form::text('birthday', (!empty($user->birthday) ? $user->birthday : NULL), ['id' => 'date', '', 'class' => 'form-control', 'placeholder' => 'Enter birthday...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('gender', 'Gender', array('class' => 'gender')) }}
                        {{ Form::text('gender', (!empty($user->gender) ? $user->gender : NULL), ['class' => 'form-control', 'placeholder' => 'Enter gender...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('marital_status', 'Marital Status', array('class' => 'marital_status')) }}
                        {{ Form::text('marital_status', (!empty($user->marital_status) ? $user->marital_status : NULL), ['class' => 'form-control', 'placeholder' => 'Enter marital status...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('join_date', 'Joining Date', array('class' => 'join_date')) }}
                        {{ Form::text('join_date', (!empty($user->join_date) ? $user->join_date : NULL), ['id' => 'date1', 'class' => 'form-control', 'placeholder' => 'Enter joining date...']) }}
                    </div>

                   {{--  <div class="form-group">
                        {{ Form::label('father', 'Father', array('class' => 'father')) }}
                        {{ Form::text('father', (!empty($user->father) ? $user->father : NULL), ['class' => 'form-control', 'placeholder' => 'Enter father...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('mother', 'Mother', array('class' => 'mother')) }}
                        {{ Form::text('mother', (!empty($user->mother) ? $user->mother : NULL), ['class' => 'form-control', 'placeholder' => 'Enter mother...']) }}
                    </div> --}}

                    <div class="form-group">
                        {{ Form::label('phone', 'Phone', array('class' => 'phone')) }}
                        {{ Form::text('phone', (!empty($user->phone) ? $user->phone : NULL), ['class' => 'form-control', 'placeholder' => 'Enter phone...']) }}
                    </div>

                    {{-- <div class="form-group">
                        {{ Form::label('emergency_phone', 'Emergency Phone', array('class' => 'emergency_phone')) }}
                        {{ Form::text('emergency_phone', (!empty($user->emergency_phone) ? $user->emergency_phone : NULL), ['', 'class' => 'form-control', 'placeholder' => 'Enter emergency phone...']) }}
                    </div> --}}

                    <div class="form-group">
                        {{ Form::label('address', 'Address', array('class' => 'address')) }}
                        {{ Form::text('address', (!empty($user->address) ? $user->address : NULL), ['class' => 'form-control', 'placeholder' => 'Enter address...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('thana', 'Thana', array('class' => 'thana')) }}
                        {{-- {{ Form::text('thana', (!empty($user->thana) ? $user->thana : NULL), ['class' => 'form-control', 'placeholder' => 'Enter Thana...']) }} --}}
                        <select name="thana" id="" class="form-control">
                            <option value="টাঙ্গাইল সদর" {{!empty($user->thana) && $user->thana == 'টাঙ্গাইল সদর' ? 'selected' : NULL}}>টাঙ্গাইল সদর</option>
                            <option value="কালিহাতি" {{!empty($user->thana) && $user->thana == 'কালিহাতি' ? 'selected' : NULL}}>কালিহাতি</option>
                            <option value="ঘাটাইল"  {{!empty($user->thana) && $user->thana == 'ঘাটাইল' ? 'selected' : NULL}}>ঘাটাইল</option>
                            <option value="বাসাইল"  {{!empty($user->thana) && $user->thana == 'বাসাইল' ? 'selected' : NULL}}>বাসাইল</option>
                            <option value="গোপালপুর" {{!empty($user->thana) && $user->thana == 'গোপালপুর' ? 'selected' : NULL}}>গোপালপুর</option>
                            <option value="মির্জাপুর" {{!empty($user->thana) && $user->thana == 'মির্জাপুর' ? 'selected' : NULL}}>মির্জাপুর</option>
                            <option value="ভূঞাপুর" {{!empty($user->thana) && $user->thana == 'ভূঞাপুর' ? 'selected' : NULL}}>ভূঞাপুর</option>
                            <option value="নাগরপুর" {{!empty($user->thana) && $user->thana == 'নাগরপুর' ? 'selected' : NULL}}>নাগরপুর</option>
                            <option value="মধুপুর" {{!empty($user->thana) && $user->thana == 'মধুপুর' ? 'selected' : NULL}}>মধুপুর</option>
                            <option value="সখিপুর" {{!empty($user->thana) && $user->thana == 'সখিপুর' ? 'selected' : NULL}}>সখিপুর</option>
                            <option value="দেলদুয়ার" {{!empty($user->thana) && $user->thana == 'দেলদুয়ার' ? 'selected' : NULL}}>দেলদুয়ার</option>
                            <option value="ধনবাড়ী" {{!empty($user->thana) && $user->thana == 'ধনবাড়ী' ? 'selected' : NULL}}>ধনবাড়ী</option>
                        </select>
                    </div>

                    <div class="form-group">
                        {{ Form::label('district', 'District', array('class' => 'district')) }}
                        {{ Form::text('district', (!empty($user->district) ? $user->district : 'টাংগাইল'), ['class' => 'form-control', 'placeholder' => 'Enter Dsitrict...', 'readonly']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('company', 'Office', array('class' => 'company')) }}
                        {{ Form::text('company', (!empty($user->company) ? $user->company : NULL), ['class' => 'form-control', 'placeholder' => 'Enter Office...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('school', 'School / College Name', array('class' => 'school')) }}
                        {{ Form::text('school', (!empty($user->school) ? $user->school : NULL), ['class' => 'form-control', 'placeholder' => 'Enter School / College Name...']) }}
                    </div>

                   
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                       
                    {{ Form::label('teacher_type', 'Tecaher Type', array('class' => 'teacher_type')) }}
                         <?php 
                         $teacher_type_array = ['headmaster', 'teacher', 'ict-ambassador', 'administrator'];
                         $checked_arr = explode(",", !empty($user->teacher_type) ? $user->teacher_type : NULL);
                         foreach($teacher_type_array as $att_data){

                            $checked = "";
                            if(in_array($att_data, $checked_arr)){
                                $checked = "checked";
                            } ?>
                            <div class="checkbox">
                                <label>
                                    <input class="checkbox" name="teacher_type[]" type="checkbox" value="{{$att_data}}" {{$checked}}>
                                    <?php 
                                        switch($att_data){
                                            case 'headmaster':
                                                echo ' প্রতিষ্ঠান প্রধান';
                                            break;

                                            case 'teacher':
                                                echo 'Is Teacher';
                                            break;

                                            case 'ict-ambassador':
                                                echo 'ICT4E অ্যাম্বাসেডর';
                                            break;

                                            case 'administrator':
                                                echo 'Is Administrator';
                                            break;

                                            default:

                                            break;
                                        }
                                    ?>
                                </label>
                            </div>
                            <?php } ?>
                    </div>

                    <div class="form-group">
                        {{ Form::label('subject_id', 'Select Subject Of Teacher', array('class' => 'subject_id')) }}
                        <div style="height: 200px; overflow-y: scroll; border: 1px solid #ddd; padding: 0px 5px;">
                            <?php 
                                $getSubjectList = \App\Subjects::get();
                                $checked_subject = explode(",", !empty($user->subject_id) ? $user->subject_id : NULL);
                                //dump($getSubjectList); 
                                foreach($getSubjectList as $data){ 
                                 $checked = "";
                                if(in_array($data['id'],$checked_subject)){
                                    $checked = "checked";
                                }     
                                ?>
                                <div class="checkbox">
                                    <label>
                                        <input class="checkbox" name="subject_id[]" type="checkbox" value="{{$data->id}}" {{$checked}}>
                                        {{$data->name}}
                                    </label>
                                </div>
                                <?php }
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        @foreach($roles as $key => $val)
                            <?php $roless[$val->id] = $val->description ?>
                        @endforeach
                        <?php
                        if (!empty($user)) {
                            $role_id = get_userrole_by_user_id($user->id);
                        }
                        ?>
                        {{ Form::label('user_role', 'User Role', array('class' => 'user_role')) }}
                        {{ Form::select('user_role', $roless, (!empty($role_id->role_id) ? $role_id->role_id : NULL), ['class' => 'form-control']) }}
                        {{ Form::hidden('id_of_role_user', (!empty($role_id->id) ? $role_id->id : NULL), []) }}
                    </div>
                        {{-- <div class="form-group">
                            {{ Form::label('mark_verifed', 'Mark as Verified', array('class' => 'mark_verifed')) }}
                            <div class="radio">
                                <label>
                                    {{ Form::radio('mark_verifed', 1, ($user->mark_verifed??0) == 1 ? true : false, ['class' => 'radio']) }}
                                    Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    {{ Form::radio('mark_verifed', 0, ($user->mark_verifed??1) == 0 ? true : false, ['class' => 'radio']) }}
                                    No
                                </label>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            {{ Form::label('employee_title', 'Teacher / Emplyee Title', array('class' => 'employee_title')) }}
                            {{ Form::text('employee_title', (!empty($user->employee_title) ? $user->employee_title : NULL), ['class' => 'form-control', 'placeholder' => 'Enter Teacher / Emplyee Title...']) }}
                        </div>
                    <div class="form-group">
                        {{Form::label('is_active','Status',['class' => 'is_active'])}}
                        {{Form::select('is_active',['1' => 'Active','0' => 'Inactive'],$user->is_active??1, ['class' => 'form-control'])}}
                    </div>

                    @if (empty($user->id))
                        <div class="form-group">
                            {{ Form::label('password', 'Password', array('class' => 'password')) }}
                            {{ Form::text('password', NULL, ['type' => 'password', 'class' => 'form-control', 'placeholder' => 'Enter new password...']) }}
                        </div>
                    @endif
                    {{-- <fieldset>
                        <legend>Vendor Informations</legend>
                            <div class="form-group">
                                {{ Form::label('vendor_banner', 'Vendor banner url', array('class' => 'vendor_banner')) }}
                                {{ Form::text('vendor_banner', (!empty($user->vendor_banner) ? $user->vendor_banner : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter banner url...']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('vendor_logo', 'Vendor logo url', array('class' => 'vendor_logo')) }}
                                {{ Form::text('vendor_logo', (!empty($user->vendor_logo) ? $user->vendor_logo : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter logo url...']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor_web', 'Vendor Website', array('class' => 'vendor_web')) }}
                                {{ Form::text('vendor_web', (!empty($user->vendor_web) ? $user->vendor_web : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter website url...']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor_title', 'Vendor About titel', array('class' => 'vendor_title')) }}
                                {{ Form::text('vendor_title', (!empty($user->vendor_title) ? $user->vendor_title : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter Vendor About Title...']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('vendor_description', 'Vendor description', array('class' => 'vendor_description')) }}
                                {{ Form::textarea('vendor_description', (!empty($user->vendor_description) ? $user->vendor_description : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter Vendor discription...']) }}
                            </div>

                            <div class="form-group">
                                {{Form::label('is_active','Status',['class' => 'is_active'])}}
                                {{Form::select('is_active',['1' => 'Active','0' => 'Inactive'],$user->is_active??1, ['class' => 'form-control'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('vendor_shop_type','Vendor Shop Type',['class' => 'vendor_shop_type'])}}
                                {{Form::select('vendor_shop_type',[null => 'None','RFLBestbuy' => 'RFL BestBuy','VisionEmporium' => 'Vision Emporium','RegalEmporium' => 'Regal Emporium','DurantaBikeGallery' => 'Duranta Bike Gallery','EasyBuild' => 'EasyBuild',],$user->vendor_shop_type??1, ['class' => 'form-control'])}}
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('vendor_dis_img', 'Vendor discription image url', array('class' => 'vendor_dis_img')) }}
                                        {{ Form::text('vendor_dis_img', (!empty($user->vendor_dis_img) ? $user->vendor_dis_img : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter Vendor discription image url...']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('vendor_dis_img_title', 'Vendor discription image title', array('class' => 'vendor_dis_img_title')) }}
                                        {{ Form::text('vendor_dis_img_title', (!empty($user->vendor_dis_img_title) ? $user->vendor_dis_img_title : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter Vendor discription image title...','rows' => 5]) }}
                                    </div>
                                </div>
                            </div>
                    </fieldset>

                    <fieldset>
                        <legend>Authorized Vendors Information</legend>

                        <div class="form-group">
                            {{ Form::label('vendor_status', 'Vendor Status', array('class' => 'vendor_mark_as_authorized')) }}
                            <div class="radio">
                                <label>
                                    {{ Form::radio('vendor_status', 1, ($user->vendor_status??0) === 1 ? true : false, ['class' => 'radio']) }}
                                    Approved
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    {{ Form::radio('vendor_status', 0, ($user->vendor_status??0) != 1 ? true : false, ['class' => 'radio']) }}
                                    Not Approved
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('vendor_mark_as_authorized', 'Mark as authorized', array('class' => 'vendor_mark_as_authorized')) }}
                            <div class="radio">
                                <label>
                                    {{ Form::radio('vendor_mark_as_authorized', 1, (!empty($user) ? ((!empty($user->vendor_mark_as_authorized) ? $user->vendor_mark_as_authorized == 1 : 1) ? TRUE : FALSE) : TRUE), ['class' => 'radio']) }}
                                    Yes
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    {{ Form::radio('vendor_mark_as_authorized', 0, (!empty($user) ? ((!empty($user->vendor_mark_as_authorized) ? $user->vendor_mark_as_authorized == 0 : 0) ? FALSE : TRUE) : null), ['class' => 'radio']) }}
                                    No
                                </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_trade_license_no', 'Trade License', array('class' => 'vendor_trade_license_no')) }}
                                    {{ Form::text('vendor_trade_license_no', (!empty($user->vendor_trade_license_no) ? $user->vendor_trade_license_no : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter trade license no...']) }}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_vat_certificate', 'VAT certificate', array('class' => 'vendor_vat_certificate')) }}
                                    {{ Form::text('vendor_vat_certificate', (!empty($user->vendor_vat_certificate) ? $user->vendor_vat_certificate : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter VAT certificate...']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_etin_certificate', 'eTIN certificate', array('class' => 'vendor_etin_certificate')) }}
                                    {{ Form::text('vendor_etin_certificate', (!empty($user->vendor_etin_certificate) ? $user->vendor_etin_certificate : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter eTIN certificate...']) }}
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_bank_solvency_certificate', 'Bank Solvency certificate', array('class' => 'vendor_bank_solvency_certificate')) }}
                                    {{ Form::text('vendor_bank_solvency_certificate', (!empty($user->vendor_bank_solvency_certificate) ? $user->vendor_bank_solvency_certificate : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter bank solvency certificate...']) }}
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_incorporation_certificate', 'Incorporation certificate', array('class' => 'vendor_incorporation_certificate')) }}
                                    {{ Form::text('vendor_incorporation_certificate', (!empty($user->vendor_incorporation_certificate) ? $user->vendor_incorporation_certificate : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter incorporation certificate...']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_article_of_memorandum', 'Article of Memorandum', array('class' => 'vendor_article_of_memorandum')) }}
                                    {{ Form::text('vendor_article_of_memorandum', (!empty($user->vendor_article_of_memorandum) ? $user->vendor_article_of_memorandum : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter AOM certificate...']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('vendor_bank_account_details', 'Bank Account Details', array('class' => 'vendor_bank_account_details')) }}
                            {{ Form::textarea('vendor_bank_account_details', (!empty($user->vendor_bank_account_details) ? $user->vendor_bank_account_details : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter bank account details...', 'rows' => 2]) }}
                        </div>
                        <div class="row">


                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_nid', 'NID', array('class' => 'vendor_nid')) }}
                                    {{ Form::text('vendor_nid', (!empty($user->vendor_nid) ? $user->vendor_nid : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter NID...']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_md_photo', 'MD Photo', array('class' => 'vendor_md_photo')) }}
                                    {{ Form::text('vendor_md_photo', (!empty($user->vendor_md_photo) ? $user->vendor_md_photo : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter MD photo...']) }}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ Form::label('vendor_md_signature', 'MD Signature', array('class' => 'vendor_md_signature')) }}
                                    {{ Form::text('vendor_md_signature', (!empty($user->vendor_md_signature) ? $user->vendor_md_signature : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter MD sign...']) }}
                                </div>
                            </div>

                        </div>

                    </fieldset> --}}

                </div>
            @endslot
        @endcomponent

    </div>
@endsection

@push('scripts')

    <script type="text/javascript">

        jQuery(document).ready(function ($) {
            $.noConflict();


            $(document).on('blur', '#seo_url', function (e) {
                var seo_url = $('#seo_url').val();
                var user_id = '<?php echo !empty($user->id) ? $user->id : null; ?>';
                var data = {
                    'id': user_id,
                    'seo_url': seo_url
                };
                $.ajax({
                    url: baseurl + '/vendor_url_existing',
                    method: 'get',
                    data: data,
                    success: function (data) {

                        if (data.has_data == 'Yes') {
                            $('#seo_url').val(null);
                            $('#error_seo_url').html(data.massage);
                        } else if (data.has_data == 'No') {
                            $('#error_seo_url').html(data.massage);
                        }

                    },
                    error: function () {
                    }
                });
            });

        });


    </script>

@endpush
