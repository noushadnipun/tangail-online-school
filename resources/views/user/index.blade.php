@extends('layouts.app')

@section('title', 'Users')
@section('sub_title', 'list of all users')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h5 class="box-title">Advanced Search</h5>
                </div>
                <div class="box-body">
                    {{ Form::open(array('url' => route('admin.search_users'), 'method' => 'post', 'value' => 'PATCH', 'id' => '')) }}
                    <div class="row">
                        <div class="col-xs-2">
                            <select name="column" required class="form-control select2" style="width: 100%;">
                                <option value="Name" {{ (Request::post('column') == 'name') ? 'selected="selected"' : 'selected="selected"' }}>
                                    Name
                                </option>
                                <option value="nidno" {{ (Request::post('column') == 'nidno') ? 'selected="selected"' : '' }}>
                                    NID
                                </option>
                                <option value="passportno" {{ (Request::post('column') == 'passportno') ? 'selected="selected"' : '' }}>
                                    Passport No.
                                </option>
                                <option value="birthcertificateno" {{ (Request::post('column') == 'birthcertificateno') ? 'selected="selected"' : '' }}>
                                    Birth Certificate No.
                                </option>
                                <option value="email" {{ (Request::post('column') == 'email') ? 'selected="selected"' : '' }}>
                                    Email
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-2">
                            @php
                                $get_rol = App\Role::where(['is_active' => 1])->get();

                            @endphp
                            <select name="role" class="form-control select2" style="width: 100%;">
                                <option value="">Select</option>
                                @foreach($get_rol as $rol)
                                    <option value="{{ $rol->id }}" {{ (Request::post('role') == $rol->id) ? 'selected="selected"' : '' }}>
                                        {{ $rol->description??'' }}
                                    </option>
                                @endforeach

                            </select>
                        </div>

                        <div class="col-xs-4">
                            {{ Form::text('search_key', Request::post('search_key'), ['class' => 'form-control', 'placeholder' => 'Search Keys...']) }}
                        </div>
                        <div class="col-xs-1">
                            {{ Form::submit('Search', ['class' => 'btn btn-success']) }}
                        </div>
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Users
                        <a href="{{ route('admin.users.create') }}" class="btn btn-xs btn-success">
                            <i class="fa fa-plus"></i>
                        </a>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="reload_me">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Role</th>
                            <th>Modify Permissions</th>
                            <th>Email</th>
                            @if($_GET['role']??0 == 10)
                                <th>Status</th>
                            @endif
                            <th>Date Created</th>
                            <th>Date Updated</th>
                            <th colspan="1">Action</th>
                        </tr>
                        @foreach($users as $user)

                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ get_user_role($user->id)->description??'' }}</td>
                                <td>
                                    <a href="{{ route('admin.modify_role_view', $user->id) }}?role=showroom"
                                       class="btn btn-xs btn-success">Modify</a>
{{--                                    <a href="{{ route('admin.set_default_role', $user->id) }}"--}}
{{--                                       class="btn btn-xs btn-warning">Set Default</a>--}}
                                </td>
                                <td>{{ $user->email }}</td>
                                @if($_GET['role']??0 == 10)
                                    <td class="{{$user->vendor_status == 1 ?'btn btn-success btn-xs':'btn btn-danger btn-xs'}}">
                                        {{$user->vendor_status == 1 ? 'Approved':'Not Approved'}}
                                    </td>
                                @endif
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->updated_at }}</td>
                                <td>

                                    <a class="btn btn-xs btn-warning"
                                       href="{{ route('admin.modify_password_view',$user->id) }}">
                                        <i class="fa fa-user-circle-o"></i>
                                    </a>
                                    <a class="btn btn-xs btn-success"
                                       href="{{ route("admin.users.edit", $user->id) }}">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    @php
                                        $roleUser = \App\Role_User::where('role_id', '1')->first();
                                    @endphp
                                    @if($roleUser->user_id == auth()->user()->id && $user->id != auth()->user()->id)
                                        {{ Form::open(['method' => 'delete', 'route' => ['admin.users.destroy', $user->id], 'class' => 'delete_form']) }}
                                        {{ Form::button('<i class="fa fa-times"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger')) }}
                                        {{ Form::close() }}
                                    @endif
                                </td>
                                <td>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="box-footer clearfix">
                        {{ $users->links('component.paginator', ['object' => $users]) }}
                    </div>
                    <!-- /.pagination pagination-sm no-margin pull-right -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection
