@extends('layouts.app')

@section('title', 'Change Password')
@section('sub_title', 'password change or modification form')
@section('content')
    <div class="">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        {{--@endif--}}
        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif



        @component('component.form')
            @slot('form_id')
                @if (!empty($user->id))
                    user_forms
                @else
                    user_form
                @endif
            @endslot
            @slot('title')
                @if (!empty($user->id))
                    Edit user
                @else
                    Add a new user
                @endif

            @endslot

            @slot('route')
                @if (!empty($user->id))
                    {{route('admin.modify_password',$user->id)}}
                @else
                    {{route('admin.modify_password_view')}}
                @endif
            @endslot

                @slot('method')

                @endslot

            @slot('fields')
                <div class="row">
                    <div class="col-md-3">

                        <div class="form-group">
                            You are changing password for: <strong>{{ $user->email }}</strong>. If you are sure, then go
                            on and enter the new password in below fields-
                        </div>

                        <div class="form-group">
                            @foreach($roles as $key => $val)
                                @php
                                    $roless[$val->id] = $val->description
                                @endphp
                            @endforeach
                            @php
                                if (!empty($user)) {
                                    $role_id = get_userrole_by_user_id($user->id);
                                }
                            @endphp
                            {{ Form::label('user_role', 'User Role', array('class' => 'user_role')) }}
                            {{ Form::select('user_role', $roless, (!empty($role_id->role_id) ? $role_id->role_id : NULL), ['class' => 'form-control']) }}
                            {{ Form::hidden('id_of_role_user', (!empty($role_id->id) ? $role_id->id : NULL), []) }}

                        </div>

                        <div class="form-group">
                            {{ Form::label('new_password', 'New Password', array('class' => 'new_password')) }}
                            <br/>
                            {{ Form::password('new_password', (!empty($user->new_password) ? $user->new_password : NULL), [ 'class' => 'form-control', 'placeholder' => 'Enter new password...']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('new_password_confirm', 'Confirm New Password', array('class' => 'new_password_confirm')) }}
                            <br/>
                            {{ Form::password('new_password_confirm', (!empty($user->new_password_confirm) ? $user->new_password_confirm : NULL), [ 'class' => 'form-control', 'placeholder' => 'Confirm new password...']) }}
                        </div>
                    </div>
                </div>
            @endslot
        @endcomponent

    </div>
@endsection

@push('scripts')

    <script type="text/javascript">

        jQuery(document).ready(function ($) {
            $.noConflict();

        });


    </script>

@endpush
