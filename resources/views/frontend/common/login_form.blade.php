@if(Session::has('success'))
    <div class="col-md-12">
        <div class="callout callout-success">
            {{ Session::get('success') }}
        </div>
    </div>
@endif
{{--@endif--}}
@if($errors->any())
    <div class="col-md-12">
        <div class="callout callout-danger">
            <h4>Warning!</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

{{ Form::open(array('url' => 'web_login', 'method' => 'post', 'value' => 'PATCH', 'id' => '')) }}
<div class="form-group">
    {{ Form::label('email', 'Email Address', array('class' => 'title')) }}
    {{ Form::text('email', old('email'),
    ['required', 'class' => 'form-control', 'placeholder' => 'Enter Email']) }}
</div>
<div class="form-group">
    {{ Form::label('password', 'Password', array('class' => 'sub_title')) }}
    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter Password']) }}
</div>


<div class="form-group">
    <label class="login-bar">
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}>
        Remember Me
    </label>
</div>

<div class="form-group">
    {{ Form::submit('Sign In', ['class' => 'btn btn-md btn-success btn-block mt-2']) }}

    <a class="btn btn-link" href="{{ url('/password/reset') }}">
        Forgot Your Password?
    </a>
</div>
{{ Form::close() }}
