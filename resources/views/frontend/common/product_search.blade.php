{{ Form::open(array('url' => 'search', 'method' => 'get', 'value' => 'PATCH', 'role' => 'search_product', 'id' => 'search')) }}


<div class="nav-bar arafta">
    <i class="fa fa-search" aria-hidden="true"></i>
    <div class="">
	     <div class="intro search-box1">
	        <div class="input-group add-on">
	          <input class="form-control" placeholder="Search..." type="text" name="q" id="srch-term" type="text" value="{{ !empty($search_key) ? $search_key : NULL  }}">
	          <div class="input-group-btn">
	            <button class="btn btn-default ses-btn" type="submit">Search</button>
	          </div>
	        </div>
	    </div>
    </div>
</div> 



<!--<div class="search-icon">-->
<!--    <input type="text" name="q" class="search-expand" value="{{ !empty($search_key) ? $search_key : NULL  }}"/>-->
<!--    <span class="search_icon" href="javascript:void(0)"><i class="fa fa-search"></i></span>-->
<!--</div>-->

{{ Form::close() }}