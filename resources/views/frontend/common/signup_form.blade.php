@if(Session::has('success'))
    <div class="col-md-12">
        <div class="callout callout-success">
            {{ Session::get('success') }}
        </div>
    </div>
@endif
{{--@endif--}}
@if($errors->any())
    <div class="col-md-12">
        <div class="callout callout-danger alert alert-danger">
            <h4>Warning!</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

{{ Form::open(array('url' => 'web_signup', 'method' => 'post', 'value' => 'PATCH', 'id' => 'web_signup')) }}
{{ Form::hidden('role_id', 3, ['required']) }}
<div class="form-group">
    {{ Form::label('name', 'Full Name', array('class' => 'title')) }}
    {{ Form::text('name', NULL, ['required', 'class' => 'form-control', 'placeholder' => 'Enter Full Name']) }}
</div>
<div class="form-group">
    {{ Form::label('email', 'Email Address', array('class' => 'sub_title')) }}
    {{ Form::text('email', NULL, ['class' => 'form-control', 'placeholder' => 'Enter Email Address']) }}
</div>
<div class="form-group">
    {{ Form::label('phone', 'Phone Number', array('class' => 'sub_title')) }}
    {{ Form::text('phone', NULL, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']) }}
</div>
<div class="form-group">
    {{ Form::label('password', 'Password', array('class' => 'sub_title')) }}
    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter Password']) }}
</div>
<div class="form-group">
    {{ Form::label('confirm_password', 'Confirm password', array('class' => 'sub_title')) }}
    {{ Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => 'Enter Confirm password']) }}
</div>
{{ Form::hidden('user_group', 5, []) }}
<div class="form-group">
    {{ Form::submit('Create Account', ['class' => 'btn btn-md btn-success btn-block mt-2']) }}
</div>
{{ Form::close() }}