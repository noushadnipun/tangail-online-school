<section class="banner-area">
    <div id="banner-slider" class="owl-carousel owl-theme">

        <?php $side_bar_menu = dynamic_widget($widgets, ['id' => 1, 'heading' => NULL]); ?>
        {!!  $side_bar_menu !!}

    </div>
</section>