@extends('frontend.layouts.app')

@section('content')

<section class="contentbox card mb-3">
    <div class="container-fluid">
        <div class="col-md-12">
            

            @if (Auth::check())
            <div class="row">
                <div class="col-md-12 p-3">
                <h3 class="text-dark pb-2">User panel</h3>
                    <div class="user-pnale-data">
                        <div class="user-pln-info">

                            @if (Auth::user()->isAdmin() || Auth::user()->isEditor() || Auth::user()->isStaff() ||  Auth::user()->isTaecher() || Auth::user()->isUser())
                            <ul class="list-unstyled">
                                <li class="pull-right">
                                    <a href="{{ url('/dashboard') }}">
                                        <i class="fa fa-cog"></i> Dashboard
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-user"></i> Hello, {{ Auth::user()->name }}
                                    </a>
                                </li>
                            </ul>
                            @endif

                        </div>

                    </div>
                </div>
            </div>



            @endif

        </div>
    </div>
</section>

@endsection

@section('cusjs')
<style>
    table {
        background: #FFFFFF;
    }

    thead>tr {
        background: #92252c;
        color: #FFF;
        font-weight: 700;
    }

    tbody,
    tr,
    td {
        background: transparent;
    }

    .table-striped>tbody>tr:nth-of-type(odd) {
        background: transparent;
    }
</style>
@endsection