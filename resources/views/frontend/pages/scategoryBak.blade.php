@extends('frontend.layouts.app')

@section('content')
    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb(ucfirst($category->name), 'page');

                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section>
    @if($category->id === 110)
        <div class="contantbox-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="contantbox-banner-warp">
                            <div class="contantbox-banner-title text-center">
                                <p>We are very pleased to announced that, Vision Electronics has a lot of
                                    showrooms all over the Bangladesh</p>
                                <h1>{{ $category->name }}</h1>
                                <div class="col-md-4">
                                    <!-- <div class="input-group">
                                        <input type="text" class="form-control"
                                               placeholder="Showroom Search">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div> -->
                                    {{--{{$divisions}}--}}

                                    <select class="form-control" id="division">
                                        <option>Select Division</option>
                                        @foreach ($divisions as $division)
                                            <option value="{{ $division->division }}">{{ $division->division }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <select class="form-control" id="district">
                                        <option>District</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" id="thana">
                                        <option>Thana</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <section class="contentbox">
        <div class="container">
            <div class="col-md-12">
                @if($category->id !== 112)
                    <h1>{{ $category->name }}</h1>
                @endif
                <div class="news-warp">
                    <?php if ($category->id === 112) { ?>
                    <div class="row col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select class="form-control" id="division">
                                            <option>Select Division</option>
                                            @foreach ($divisions as $division)
                                                <option value="{{ $division->division }}">{{ $division->division }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <select class="form-control" id="district">
                                            <option>District</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" id="thana">
                                            <option>Thana</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="service-center-warp">
                                        <div class="service-center-list">
                                            <ol>
                                                @foreach($sposts as $post)
                                                    <li>
                                                        <p>{{ $post->title }}</p>
                                                        <p>{{ $post->address }}</p>
                                                        <p>{{ $post->phone }}</p>
                                                        <p>{{ $post->brand }}</p>
                                                    </li>
                                                @endforeach
                                            </ol>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="service-senter-map">
                                        <div class="map">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.047205744717!2d90.42337821540663!3d23.781333293509896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c796c76c1a8b%3A0x7ff1d179fba4c47c!2z4Kaq4KeN4Kaw4Ka-4KajLeCmhuCmsOCmj-Cmq-Cmj-CmsiDgppfgp43gprDgp4Hgpqo!5e0!3m2!1sbn!2sbd!4v1525602394927"
                                                    width="100%" height="350" frameborder="0" style="border:0"
                                                    allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else if($category->id === 107) { ?>
                    <div class="row">
                        <div class="col-md-9">
                            @foreach($sposts as $post)

                                @if($post->categories == 107 && $post->is_upcoming == 0)
                                    <div class="row single-news">
                                        <div class="col-md-4">

                                            <div class="news-img">
                                                <a href="{{ '/post/' . $post->id . '/' . $post->seo_url }}">
                                                    <?php $img = App\Image::find($post->images); ?>
                                                    <img src="{{ url($img->full_size_directory) }}"
                                                         style="height: 220px; width: 250px;"
                                                         alt="{{ $post->title }}"
                                                         class="img-responsive"/>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="col-md-8">
                                            <div class="news-content">
                                                <div class="mc-header news-text">
                                                    <h5>
                                                        <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h5>
                                                    <p>{!! text_limit($post->description, 250) !!}</p>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="clearfix"></div>
                            @endforeach
                        </div>
                        <div class="col-md-3">
                            <div class="upcoming_events">
                                <h2>Upcoming News & Events</h2>
                                <div class="upcoming_events-list">
                                    <ul class="list-unstyled">
                                        @foreach($sposts as $post)

                                            @if($post->categories == 107 && $post->is_upcoming == 1)
                                                <li>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        {{ $post->title }}
                                                    </a>
                                                </li>
                                            @endif

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else if ($category->id === 108) { ?>

                    <section class="service-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="search-container">
                                                <form action="#">
                                                    <input type="text" placeholder="Search.." name="search">
                                                    <button type="submit">Find Vision Service Points</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-md-6">
                                                <div class="service-center-warp">
                                                    <div class="service-center-list">

                                                        <ol>
                                                            @foreach($posts as $post)
                                                                @if($post->categories == 108)
                                                                    <li>
                                                                        <h5>{{ $post->title }}</h5>
                                                                        <p>{{ $post->address }}</p>
                                                                        <p>{{ 'Phone: ' . $post->phone }}</p>
                                                                        <p>{{ 'Opening Hours: ' . $post->opening_hours }}</p>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        </ol>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="service-senter-map">
                                                    <div class="map">
                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.047205744717!2d90.42337821540663!3d23.781333293509896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c796c76c1a8b%3A0x7ff1d179fba4c47c!2z4Kaq4KeN4Kaw4Ka-4KajLeCmhuCmsOCmj-Cmq-Cmj-CmsiDgppfgp43gprDgp4Hgpqo!5e0!3m2!1sbn!2sbd!4v1525602394927"
                                                                width="100%" height="350" frameborder="0"
                                                                style="border:0" allowfullscreen=""></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php } else if($category->id === 135) { ?>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row single-news">
                                @foreach($sposts as $post)
                                    @if($post->categories == 135)
                                        <div class="col-md-4">
                                            <div class="news-content">
                                                <div class="mc-header news-text">
                                                    <h5>
                                                        <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h5>
                                                    <p>{!! text_limit($post->description, 250) !!}</p>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <?php } else if($category->id === 136) { ?>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row single-news">
                                @foreach($sposts as $post)
                                    @if($post->categories == 136)
                                        <div class="col-md-4">
                                            <div class="news-content">
                                                <div class="mc-header news-text">
                                                    <h5>
                                                        <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h5>
                                                    <p>{!! text_limit($post->description, 250) !!}</p>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <?php } else if($category->id === 137 || $category->id === 109 || $category->id === 111) { ?>
                    <div class=" row col-md-12">
                        <div class="row news-warp">
                            <div class="promotions-warp">
                                <div class="jagoTab2 TwoTab">
                                    <div class="tab-content">
                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($category->id === 111) ? ' active in ' : null; ?> fade"
                                             id="">
                                            @foreach($sposts as $post)
                                                @if($post->categories == 111)
                                                    <div class="col-md-4">
                                                        <div class="single-news showroom-single">
                                                            <div class="news-img print-media">
                                                                <?php $img = App\Image::find($post->images); ?>
                                                                <a data-lightbox="example-set"
                                                                   href="{{ url($img->full_size_directory) }}"
                                                                   class="example-image-link">
                                                                    <img src="{{ url($img->full_size_directory) }}"
                                                                         alt="{{ $post->title }}">
                                                                </a>
                                                            </div>
                                                            <div class="news-content">
                                                                <div class="mc-header news-text tab-para">
                                                                    <p>{{ text_limit($post->short_description, 50) }}</p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                        </div>

                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($category->id === 109) ? ' active in ' : null; ?>"
                                             id="">
                                            @foreach($sposts as $post)
                                                @if($post->categories == 109)
                                                    <div class="col-md-4">
                                                        <div class="single-news showroom-single">
                                                            <div class="news-img print-media">
                                                                {!! convert_youtube($post->youtube) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                        </div>
                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($category->id === 137) ? ' active in ' : null; ?>"
                                             id="">

                                            @foreach($sposts as $post)
                                                @if($post->categories == 137)
                                                    <div class="col-md-4">
                                                        <div class="single-news showroom-single">
                                                            <div class="news-img print-media">
                                                                <?php $img = App\Image::find($post->images); ?>
                                                                <a data-lightbox="example-set"
                                                                   href="{{ url($img->full_size_directory) }}"
                                                                   class="example-image-link">
                                                                    <img src="{{ url($img->full_size_directory) }}"
                                                                         alt="{{ $post->title }}">
                                                                </a>
                                                            </div>
                                                            <div class="news-content">
                                                                <div class="mc-header news-text tab-para">
                                                                    <p>{{ text_limit($post->short_description, 50) }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else if($category->id === 110) { ?>

                    <section class="contentbox">
                        <div class="col-md-12">
                            <div class="row news-warp" id="showroom">

                                @foreach($posts as $post)
                                    <div class="showroom-warp">

                                        @if($post->categories == 110)
                                            <div class="col-md-6">
                                                <div class="single-news showroom-single">
                                                    <div class="news-img">
                                                        <?php
                                                        //$images = explode(',', $post->images);
                                                        $img = App\Image::find($post->images);
                                                        ?>
                                                        <img src="{{ url($img->full_size_directory) }}"
                                                             style="min-width: 200px;"
                                                             alt="{{ $post->title }}"
                                                             class="img-responsive"/>
                                                    </div>
                                                    <div class="news-content">
                                                        <div class="mc-header news-text">
                                                            <h5>{{ $post->title }}</h5>
                                                            <p>{{ $post->address }}</p>
                                                            <p>{{ $post->phone }}</p>
                                                        </div>
                                                        <div class="get_direction">
                                                            <a href="#" class="btn btn-sm btn-danger">
                                                                Get Direction
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </section>


                    <?php } ?>

                </div>
            </div>
        </div>
    </section>

@endsection
