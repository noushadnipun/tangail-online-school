@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">

    <div class="row">
        <div class="col-md-9 mb-3">
            <div class="bg-white p-3">
                <h3 class="text-dark pb-2">{!! $page->title !!}</h3>
                <div class="pb-4">
                    {!! $page->description !!}
                </div>
            </div>

        </div>
        <div class="col-md-3 mb-3">
            @include('frontend.tschool-pages.sidebar')
        </div>
    </div>


</section>

@endsection
