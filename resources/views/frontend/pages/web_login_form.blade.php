@extends('frontend.layouts.app')

@section('content')

<div class="container-fluid pb-3 mt-2">
    <div class="row card card-body ">
        <div class="col-md-5 mx-auto">
            <div class="wrapper-fluid-padding" >
                <h4 class="text-center mt-2 mb-0">Log in</h4>
                <hr>
                    @include('frontend.common.login_form')
                <hr>

                <p class="mx-auto">Don't you have any account? Please use <a class="text-danger" href="{{ url('/create_an_account') }}">Sign up</a> form to register.</p>
            </div>
        </div>
    </div>
</div>

{{-- <section class="contentbox">
    <div class="container-fluid">
        <div class="col-md-12">            
            <div class="row">
                <div class="col-md-4">
                <h1>Login</h1>
                    <br />
                    @include('frontend.common.login_form')
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <h4>
                        Don't you have any account?<br />
                        Please use <a href="{{ url('/create_an_account') }}">Signup</a>
                        form to register.
                    </h4>
                    Or,<br />
                    <p>
                        <h4>Contact Us</h4>
                        Roydomi Online Shopping<br />
                        74, East Tejturi Bazar<br />
                        Tejgoan 1215<br />
                        Dhaka, Bangladesh<br />
                        Telephone: 01677618199<br />
                        Email: info@roydomi.com
                    </p>
                </div>
            </div>
        </div>
    </div>
</section> --}}
@endsection