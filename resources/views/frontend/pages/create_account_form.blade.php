@extends('frontend.layouts.app')

@section('content')

<div class="container-fluid pb-3 mt-2">
    <div class="row card card-body ">
        <div class="col-md-5 mx-auto">
            <div class="wrapper-fluid-padding" >
                <h4 class="text-center mt-2 mb-0">Create your account</h4>
                <hr>
                    @include('frontend.common.signup_form')
                <hr>

                <p class="mx-auto">Have you signed up? Please use <a class="text-danger" href="{{ url('/login_now') }}">Login</a> form to login.</p>
            </div>
        </div>
    </div>
</div>

@endsection