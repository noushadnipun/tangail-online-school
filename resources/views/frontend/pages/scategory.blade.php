@extends('frontend.layouts.app')

@section('content')


    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb(ucfirst($category->name), 'page');

                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section>
    @if($category->id === 110)
        <div class="contantbox-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="contantbox-banner-warp">
                            <div class="contantbox-banner-title text-center">
                                <p>We are very pleased to announced that, Vision Electronics has a lot of
                                    showrooms all over the Bangladesh</p>
                                <h1>{{ $category->name }}</h1>
                                <div class="col-md-4">
                                    <!-- <div class="input-group">
                                        <input type="text" class="form-control"
                                               placeholder="Showroom Search">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div> -->
                                    {{--{{$divisions}}--}}

                                    <select class="form-control" id="division">
                                        <option>Select Division</option>
                                        @foreach ($divisions as $division)
                                            <option value="{{ $division->division }}">{{ $division->division }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <select class="form-control" id="district">
                                        <option>District</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" id="thana">
                                        <option>Thana</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <section class="contentbox">
        <div class="container">
            <div class="col-md-12">
                @if($category->id !== 112)
                    <h1>{{ $category->name }}</h1>
                @endif
                <div class="news-warp">
                    <?php if ($category->id === 112) { ?>
                    <div class="row col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-4">
                                        <select class="form-control" id="division">
                                            <option>Select Division</option>
                                            @foreach ($divisions as $division)
                                                <option value="{{ $division->division }}">{{ $division->division }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <select class="form-control" id="district">
                                            <option>District</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" id="thana">
                                            <option>Thana</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="service-center-warp">
                                        <div class="service-center-list">
                                            <ol>
                                                @foreach($sposts as $post)
                                                    <li>
                                                        <p>{{ $post->title }}</p>
                                                        <p>{{ $post->address }}</p>
                                                        <p>{{ $post->phone }}</p>
                                                        <p>{{ $post->brand }}</p>
                                                    </li>
                                                @endforeach
                                            </ol>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="service-senter-map">
                                        <div class="map">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.047205744717!2d90.42337821540663!3d23.781333293509896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c796c76c1a8b%3A0x7ff1d179fba4c47c!2z4Kaq4KeN4Kaw4Ka-4KajLeCmhuCmsOCmj-Cmq-Cmj-CmsiDgppfgp43gprDgp4Hgpqo!5e0!3m2!1sbn!2sbd!4v1525602394927"
                                                    width="100%" height="350" frameborder="0" style="border:0"
                                                    allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else if($category->id === 139) { ?>
                    <div class="row">
                        <div class="col-md-9">
                            @foreach($sposts as $post)

                                <?php //dd($post); ?>

                                @if($post->categories == 139)
                                    <div class="row single-news">
                                        <div class="col-md-4">

                                            <div class="news-img">
                                                <a href="{{ '/post/' . $post->id . '/' . $post->seo_url }}">
                                                    <?php $img = App\Image::find($post->images); ?>
                                                    <?php if(!empty($img)) : ?>
                                                    <img src="{{ url($img->full_size_directory) }}"
                                                         alt="{{ $post->title }}"
                                                         class="img-responsive"/>
                                                    <?php else: ?>
                                                    <img src="{{ url('storage/uploads/default.png') }}"
                                                         style="min-height: 189px; min-width: 255px;"
                                                         alt="{{ $post->title }}"
                                                         class="img-responsive"/>
                                                    <?php endif; ?>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="col-md-8">
                                            <div class="news-content">
                                                <div class="mc-header news-text">
                                                    <h5>
                                                        <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h5>
                                                    <p>{!! text_limit($post->description, 250) !!}</p>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="clearfix"></div>
                            @endforeach
                            
                           <div class="clearfix"></div>
                                            <?php
                                     
                                                echo pagination( $totalRows, 9,app('request')->input('pages'),  '?limit=' . (!empty(app('request')->input('limit')) ? app('request')->input('limit') : 9) . '&');
                                            
                                            ?>
                                            ?>
                        </div>
                        <div class="col-md-3">
                            <div class="upcoming_events">
                                <h2>Recent Posts</h2>
                                <div class="upcoming_events-list">

                                    <?php 
                                    $resents = App\Post::where('categories',139)->orderBy('id', 'DESC')->take(5)->get(); 
                             
                                    ?>

                                    <ul class="list-unstyled">
                                        @foreach($resents as $item)
                                            <li>
                                                <a href="{{ url('/post/' . $item->id.'/'.$item->seo_url) }}">{{ $item->title }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                             <div class="upcoming_events">
                                <h2>Popular Posts</h2>
                                <div class="upcoming_events-list">

                                   
                                     <?php 
                                    $populers = App\Post::where('categories',139)->orderBy('post_view', 'DESC')->take(5)->get(); 
                             
                                    ?>
                                    <ul class="list-unstyled">
                                          @foreach($populers as $item)
                                            <li>
                                                <a href="{{ url('/post/' . $item->id.'/'.$item->seo_url) }}">{{ $item->title }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                             <div class="upcoming_events">
                                <h2>Post Archive</h2>
                                <div class="upcoming_events-list">

                                    <?php $years = App\Post::select(DB::raw('YEAR(created_at) year'))->groupBy('year')->orderBy('year', 'desc')->get(); ?>

                                    <ul class="list-unstyled">
                                        @foreach($years as $year)
                                            <li>
                                                <a href="{{ url('scategory/139/blogs?year=' . $year->year) }}">{{ $year->year }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } else if($category->id === 107) { ?>
                    <div class="row">
                        <div class="col-md-9">
                            @foreach($sposts as $post)

                                @if($post->categories == 107 && $post->is_upcoming == 0)
                                    <div class="row single-news">
                                        <div class="col-md-4">

                                            <div class="news-img">
                                                <a href="{{ '/post/' . $post->id . '/' . $post->seo_url }}">
                                                    <?php $img = App\Image::find($post->images); ?>

                                                    <?php if(!empty($img)) : ?>
                                                    <img src="{{ url($img->full_size_directory) }}"
                                                         alt="{{ $post->title }}"
                                                         class="img-responsive"/>
                                                    <?php else: ?>
                                                    <img src="{{ url('storage/uploads/default.png') }}"
                                                         style="min-height: 189px; min-width: 255px;"
                                                         alt="{{ $post->title }}"
                                                         class="img-responsive"/>
                                                    <?php endif; ?>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="col-md-8">
                                            <div class="news-content">
                                                <div class="mc-header news-text">
                                                    <h5>
                                                        <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h5>
                                                    <p>{!! text_limit($post->description, 250) !!}</p>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="clearfix"></div>
                            @endforeach
                             <div class="clearfix"></div>
                                            <?php
                                     
                                                echo pagination( $totalRows, 9,app('request')->input('pages'),  '?limit=' . (!empty(app('request')->input('limit')) ? app('request')->input('limit') : 9) . '&');
                                            
                                            ?>
                        </div>
                        <div class="col-md-3">
                            <div class="upcoming_events">
                                <h2>Upcoming News & Events</h2>
                                <div class="upcoming_events-list">
                                    <ul class="list-unstyled">
                                        @foreach($sposts as $post)

                                            @if($post->categories == 107 && $post->is_upcoming == 1)
                                                <li>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        {{ $post->title }}
                                                    </a>
                                                </li>
                                            @endif

                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="upcoming_events">
                                <h2>News & Events Archive</h2>
                                <div class="upcoming_events-list">
                                    <?php $years = App\Post::select(DB::raw('YEAR(created_at) year'))->groupBy('year')->orderBy('year', 'desc')->get(); ?>

                                    <ul class="list-unstyled">
                                        @foreach($years as $year)
                                            <li>
                                                <a href="{{ url('scategory/107/news-events?year=' . $year->year) }}">{{ $year->year }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else if($category->id === 137 || $category->id === 109 || $category->id === 111) { ?>
                    <div class="col-md-12">
                        <div class="row news-warp">
                            <div class="promotions-warp">
                                <div class="jagoTab2 TwoTab">
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane <?php echo ($category->id === 111) ? ' active in ' : null; ?> fade" id="">
                                            @foreach($sposts as $post)
                                                @if($post->categories == 111)
                                                    <div class="col-md-4">
                                                        <div class="single-news showroom-single" style="display:block">
                                                            <div class="news-img print-media">
                                                                <?php $img = App\Image::find($post->images); ?>

                                                                <?php if(!empty($img)) : ?>
                                                                <a data-lightbox="example-set"
                                                                   href="{{ url($img->full_size_directory??'') }}"
                                                                   class="example-image-link">
                                                                    <img src="{{ url($img->full_size_directory??'') }}"
                                                                         alt="{{ $post->title }}" style="width: 100%">
                                                                </a>
                                                                <?php else: ?>

                                                                <a data-lightbox="example-set"
                                                                   href="javascript:void(0)"
                                                                   class="example-image-link">
                                                                    <img src="{{ url('storage/uploads/default.png') }}"
                                                                         alt="{{ $post->title }}">
                                                                </a>
                                                                <?php endif; ?>
                                                            </div>
                                                            <div class="news-content" style="height:auto; padding: 5px 20px 5px 20px; margin-top: 5px;">
                                                                <div class="mc-header news-text tab-para" style="min-height: inherit;">
                                                                    <p style="margin-bottom:0; text-align:center">{{ text_limit($post->short_description, 50) }}</p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                          <div class="clearfix"></div>
                                            <?php
                                     
                                                echo pagination( $totalRows, 9,app('request')->input('pages'),  '?limit=' . (!empty(app('request')->input('limit')) ? app('request')->input('limit') : 9) . '&');
                                            
                                            ?>

                                        </div>

                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($category->id === 109) ? ' active in ' : null; ?>"
                                             id="">
                                            @foreach($sposts as $post)
                                                @if($post->categories == 109)
                                                    <div class="col-md-4">
                                                        <div class="single-news showroom-single">
                                                            <div class="news-img print-media">
                                                                {!! convert_youtube($post->youtube) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="clearfix"></div>
                                            <?php
                                     
                                                echo pagination( $totalRows, 9,app('request')->input('pages'),  '?limit=' . (!empty(app('request')->input('limit')) ? app('request')->input('limit') : 9) . '&');
                                            
                                            ?>
                                            
                               
                                            

                                        </div>
                                        <div role="tabpanel"
                                             class="tab-pane <?php echo ($category->id === 137) ? ' active in ' : null; ?>"
                                             id="">

                                            @foreach($sposts as $post)
                                                @if($post->categories == 137)
                                                    <div class="col-md-4">
                                                        <a href="{{ route('post_details',$post->id).'/'.$post->seo_url }}">
                                                         <div class="single-news showroom-single" style="display:block">
                                                            <div class="news-img print-media">
                                                                <?php $img = App\Image::find($post->images);?>
                                                            <img src="{{url($img->full_size_directory??'')}}" alt="{{$post->title}}" style="width: 100%">
                                                        
                                                                                                                            </div>
                                                            <div class="news-content" style="height:auto; padding: 5px 20px 5px 20px; margin-top: 5px;">
                                                                <div class="mc-header news-text tab-para" style="min-height: inherit;">
                                                                    <p style="margin-bottom:0; text-align:center">{{$post->title}}</p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        </a>
                                                        </div>
                                                    
                                                @endif
                                            @endforeach
                                          <div class="clearfix"></div>
                                          <div class="col-md-12">
                                            <?php
                                     
                                                echo pagination( $totalRows, 9,app('request')->input('pages'),  '?limit=' . (!empty(app('request')->input('limit')) ? app('request')->input('limit') : 9) . '&');
                                            
                                            ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } else if($category->id === 136) { ?>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row single-news">
                                @foreach($sposts as $post)
                                    @if($post->categories == 136)
                                        <div class="col-md-4">
                                            <div class="news-content">
                                                <div class="mc-header news-text">
                                                    <h5>
                                                        <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </h5>
                                                    <p>{!! text_limit($post->description, 250) !!}</p>
                                                    <a href="{{ url('/post/' . $post->id . '/' . $post->seo_url) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                            <?php
                                     
                                                echo pagination( $totalRows, 9,app('request')->input('pages'),  '?limit=' . (!empty(app('request')->input('limit')) ? app('request')->input('limit') : 9) . '&');
                                            
                                            ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <?php } else if($category->id === 110) { ?>

                    <div class="news-warp">
                        <section class="contentbox">
                            <div class="col-md-12">
                                <div class="row news-warp" id="showroom">
                                    <div class="showroom-warp">
                                        <div class="col-md-4">
                                            <div class="single-news showroom-dels">
                                                <h2>Address</h2>
                                                <div id="contentreloader">
                                                    <div class="news-content">
                                                        @foreach($posts as $post)
                                                            @if($post->categories == 110)
                                                                <div class="mc-header news-text nearest-shop-list">
                                                                    <h5>{{ $post->title }}</h5>
                                                                    <p>{{ $post->address }}</p>
                                                                    <p><i class="fa fa-phone"></i> {{ $post->phone }}
                                                                    </p>
                                                                    <a href="javascript:void(0)"
                                                                       class="btn btn-sm btn-danger"
                                                                       onclick="mapGenerate({{ $post->id }}, {{ $post->latitude }}, {{ $post->longitude }})">
                                                                        Get Direction
                                                                    </a>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="single-news showroom-dels">
                                                <h2>Map Location</h2>
                                                <div class="lock">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.0449902073383!2d90.42313525096105!3d23.781412193431294!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c796c76c1a8b%3A0x7ff1d179fba4c47c!2sPRAN-RFL+GROUP!5e0!3m2!1sen!2sbd!4v1537611653733"
                                                            style="border:0" allowfullscreen="" width="100%"
                                                            height="590"
                                                            frameborder="0"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('cusjs')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $.noConflict();

            $('#division').on('change', function () {
                var division = $(this).val();

                $.ajax({
                    url: baseurl + '/get_district_by_division',
                    method: 'get',
                    data: {division: division},
                    success: function (data) {
                        //console.log(data);
                        $('#district').html(data);
                    },
                    error: function () {
                    }
                });
            });

            $('#district').on('change', function () {
                var district = $(this).val();

                $.ajax({
                    url: baseurl + '/get_thana_by_district',
                    method: 'get',
                    data: {district: district},
                    success: function (data) {
                        $('#thana').html(data);
                    },
                    error: function () {
                    }
                });
            });


            $('#thana').on('change', function () {
                var thana = $(this).val();

                $.ajax({
                    url: baseurl + '/get_showroom_by_thana',
                    method: 'get',
                    data: {thana: thana},
                    success: function (data) {
                        $('#contentreloader').html(data);
                        //$('.news-content').html(data);
                    },
                    error: function () {
                    }
                });
            });

        });

        function mapGenerate(id, lat, long) {
            var newmap = '<iframe' +
                ' width="100%" \n' +
                ' height="590" \n' +
                ' frameborder="0" \n' +
                ' scrolling="no" \n' +
                ' marginheight="0" \n' +
                ' marginwidth="0" ' +
                ' src="https://maps.google.com/maps?q=' + lat + ', ' + long + '&hl=es;z=14&amp;output=embed"' +
                ' style="border:0" ' +
                ' allowfullscreen=""></iframe>';

            jQuery('.lock').html(newmap);
        }
    </script>
@endsection