@extends('frontend.layouts.app')

@section('content')

    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    //$title = get_breadcrumb_title(Request::segment(1));

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb($post->title, 'post');

                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="contentbox">
        <div class="container">
            <div class="row">
            <div class="col-md-9">
                <h1>{!! $post->title !!}</h1>
                @if($post->categories == 137)
                <?php $img = App\Image::find($post->images);?>
                <img src="{{url($img->full_size_directory??'')}}" alt="{{$post->title}}" style="width: 50%" class="text-center">
                <h3>{{$post->sub_title}}</h3>
                @endif
                {!! $post->description !!}
                
                @if($post->categories == 137 && isset($post->attachment))
                <?php
                $files = explode(',',$post->attachment);
                ?>
                <ul>
                @foreach($files as $file)
                
                <?php $fi = App\Media::find($file); //var_dump($fi); ?>
                @if(isset($fi->file_directory))
                <li>
                    
                    <a href="{{url($fi->file_directory)}}" target="_blank">
                        
                        <b>Download {{$fi->original_name}}</b>
                    
                    </a>
                
                </li>
                @endif
                @endforeach
                </ul>
                @endif
                
                
            </div>
            
                 <div class="col-md-3"><br/>
                            <div class="upcoming_events">
                                <h2>Recent Posts</h2>
                                <div class="upcoming_events-list">

                                    <?php 
                                    $resents = App\Post::where('categories',139)->orderBy('id', 'DESC')->take(5)->get(); 
                             
                                    ?>

                                    <ul class="list-unstyled">
                                        @foreach($resents as $item)
                                            <li>
                                                <a href="{{ url('/post/' . $item->id.'/'.$item->seo_url) }}">{{ $item->title }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                             <div class="upcoming_events">
                                <h2>Popular Posts</h2>
                                <div class="upcoming_events-list">

                                   
                                     <?php 
                                    $populers = App\Post::where('categories',139)->orderBy('post_view', 'DESC')->take(5)->get(); 
                             
                                    ?>
                                    <ul class="list-unstyled">
                                          @foreach($populers as $item)
                                            <li>
                                                <a href="{{ url('/post/' . $item->id.'/'.$item->seo_url) }}">{{ $item->title }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                             <div class="upcoming_events">
                                <h2>Post Archive</h2>
                                <div class="upcoming_events-list">

                                    <?php $years = App\Post::select(DB::raw('YEAR(created_at) year'))->groupBy('year')->orderBy('year', 'desc')->get(); ?>

                                    <ul class="list-unstyled">
                                        @foreach($years as $year)
                                            <li>
                                                <a href="{{ url('scategory/139/blogs?year=' . $year->year) }}">{{ $year->year }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
            
            
        </div>
        </div>
    </section>

@endsection