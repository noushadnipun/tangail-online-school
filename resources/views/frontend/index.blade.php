@extends('frontend.layouts.app')

@section('title', 'Tangail Online School')
@section('sub_title', 'Page add or modification form')
@section('content')
   {{--  Content will be here
    Global Settings Data: --}}
    <?php
    //dump($settings->first());
    ?>
    {{-- Pages: --}}
    <?php
    //dump($pages);
    ?>
   {{--  Widgets: --}}
    <?php
    //dump($widgets);
    ?>
   {{--  Home Settings: --}}
    <?php
    //dump($homesettig->first());
    ?>
    @include ('frontend.homepage.slider')
    @include ('frontend.homepage.subject-grid')
    @include ('frontend.homepage.news-block')
    @include ('frontend.homepage.photo-gallery')

@endsection
@section('cusjs')

@endsection

