@extends('frontend.layouts.app')

@section('content')
    <?php
    $categories = explode(',', $product->categories);
    $category_info = App\Term::where('id', $categories[0])->get()->first();
    
    $p_qty = 1;
    if(isset($product->othoba_product_id)){
    $othoba_info =json_decode(CallAPI($product->othoba_product_id));
    $othoba_img = $othoba_info->DefaultPictureModel->ImageUrl;
    
   $cart_id = $othoba_info->Id;
   $max_qty = $othoba_info->OrderMaximumQuantity;
   if( isset(Session::get('cart')->items[$cart_id])){
       $cart_qty = Session::get('cart')->items[$cart_id]['qty'];
       $def_max_qty = $max_qty - $cart_qty; 
       
       if($def_max_qty > 1){
           $p_qty = 1;
       }else{
            $p_qty = 0;
       }
    }
}
    
    ?>
    <section class="cat-banner"
             style="background: url({{ !empty($category_info->term_image) ? $category_info->term_image : null }}) !important;background-size: cover !important; background-repeat: no-repeat !important; background-position: center center !important;">
        <div class="single-inr-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{ get_category_name_by_product_id($product, TRUE) }}</h1>
                        <h4>{{ $product->title }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="define-item">
        <div class="container">
            <div class="row">
                <div class="item-wap">
                    <div class="col-md-6 col-sm-12 col-xs-12 leftside">
                        {!! corousel($product->carousel) !!} 
                        <!-- <div class="test owl-carousel owl-theme">
                            <div class="item" style="background: red; width: 100%;"><h4>1</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>2</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>3</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>4</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>5</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>6</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>7</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>8</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>9</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>10</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>11</h4></div>
                            <div class="item" style="background: red; width: 100%;"><h4>12</h4></div>
                        </div> -->
                    </div>

                    <div class="col-md-6 rightside">
                        <div class="life-inner">
                            <div class="product-specition">
                                <div class="ps-header">
                                    <h1>{{ $product->title }}</h1>
                                </div>
                                <div class="ps-body cat-pro-body">
                                    <h4>
                                        <b>
                                            <?php
                                            $tksign = 'BDT. ';
                                            if(isset($othoba_info->ProductPrice->PriceValue)){
                                                $f_price = $othoba_info->ProductPrice->PriceValue;
                                            }else{
                                                $f_price = $product->local_selling_price;
                                            }
                                            if(isset($othoba_info->DefaultPictureModel->ImageUrl)){
                                                $f_ImageUrl= $othoba_info->DefaultPictureModel->ImageUrl;
                                            }else{
                                                $f_ImageUrl= null;
                                            }

                                            ?>
                                            {{ $tksign . number_format($f_price) }}
                                        </b>
                                    </h4>
                                    <ul class="list-unstyled">
                                        <li>{{ $product->title }}</li>
                                        <li>{!! $product->description !!}</li>
                                    </ul>
                                </div>
                                <div class="ps-footer">
                                    <div class="cat-btm">

                                        @if($product->is_othoba_product !=1 )
                                            @if(!empty($product->go_to_url))

                                                <a href="{{ $product->go_to_url }}" class="">BUY ONLINE</a>
                                                <a class="btn-ner" href="{{ url('scategory/110/nearest-shop') }}"
                                                   class="">Find
                                                    the Nearest Shop</a>
                                            @endif
                                        @else
                                            <button id="buynow" type="button"
                                                    class="btn buy-now single-buy-now"
                                                    onclick="add_to_cart(
                                                            '{{ $product->id }}',
                                                            '{{ $product->othoba_product_id }}',
                                                            '{{ $product->product_code }}',
                                                            null,
                                                            null,
                                                            '{{ $f_price }}',
                                                            100,
                                                            null,
                                                            {{$p_qty}});">
                                                <i class="fa fa-shopping-cart"></i> BUY NOW
                                            </button>
                                        @endif
                                    <p><span>*</span>Othoba.com is the trusted online partner.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="quote-text">
                        <p>
                            {{ $product->short_description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="single-post-wrap">

        @foreach($productDescriptions as $pdesc)

        <div class="product-description-wrapper">
            <div class="container">

                @php 

                $data = json_decode($pdesc->meta_data);

                @endphp

                {!! $data->html??'nothing' !!}
            </div>
        </div>

        @endforeach

    </section>

    {!! section_printer($product->custom_content) !!}


    <section class="specifications">
        <div class="container">
            <div class="row">

                {!!  $product->specifications !!}

            </div>
        </div>
    </section>

@endsection
@section('cusjs')
    <script type="text/javascript">
        //picZoomer
        jQuery(document).ready(function ($) {
            $.noConflict();
//            $('.picZoomer').picZoomer();

            //切换图片
            $('.piclist li').on('click', function (event) {
                var $pic = $(this).find('img');
                var $buynow = $("#buynow").data('imageurl');
                $('.picZoomer-pic').attr('src', $pic.attr('src'));
                $('#buynow').data('imageurl', $buynow.data('imageurl'));
            });

            $('#combinition').on('change', function (event) {
                event.preventDefault();

                var val = $(this).val();
                //var n = val.split('|');

                //var id = n[1];
                //var item = n[0];

                var url = window.location.pathname;
                window.location.replace(url + '?product_code=' + val);

            });

            $('#plus, #minus').on('click', function (e) {

            });

            var ps = $('.product-specition').height();
            console.log(ps);
            var ls = $('.leftside').height();

            $('.leftside').css('margin-top', function () {
                if (ps <= 580) {
                    return "0%";
                } else if (ps > 580 && ps <= 1000) {
                    return "8%";
                } else if (ps > 800 && ps <= 1200) {
                    return "20%";
                } else if (ps > 1200 && ps <= 1600) {
                    return "40%";
                } else {
                    return ($('.leftside').height() - ps) + "px";
                }
            });


        });
        
        function add_to_cart(productid, productcode, productsku, regularprice, saveprice, purchaseprice, deliverycharge, imageurl, qty) {

           // alert(qty);
            if (qty !== null && qty !== undefined) {
                var qty1 = qty;
            } else {
                var qty1 = jQuery('#quantity').val();
            }
            var suv = jQuery('#show_unit_values').val();

            //alert(suv);

            if (suv !== undefined && suv !== null) {
                var npp = suv * purchaseprice;
            } else {
                var npp = 1 * purchaseprice;
            }

            //alert(npp);


            var data = {
                'productid': productid,
                'productcode': productcode,
                'productsku': productsku,
                'regularprice': parseInt(regularprice),
                'saveprice': parseInt(saveprice),
                'purchaseprice': parseInt(npp),
                'deliverycharge': parseInt(deliverycharge),
                'imageurl': imageurl,
                'qty': qty
            };

            jQuery.ajax({
                url: baseurl + '/add_to_cart',
                method: 'get',
                data: data,
                success: function (data) {
                    //console.log(data);
                    update_mini_cart();
                },
                error: function () {
                    // showError('Sorry. Try reload this page and try again.');
                    // processing.hide();
                }
            });
        }

        function update_mini_cart() {
            jQuery.ajax({
                url: baseurl + '/mini_cart',
                method: 'get',
                success: function (data) {
                    console.log(data);
                    //jQuery('#animated_total').animateNumber({number: data.total_price}).html();
                    //jQuery('#sidr').html(data.data);

                    jQuery('#animated_total').animate({someValue: data.total_price}, {
                        duration: 1000,
                        easing: 'swing', // can be anything
                        step: function () { // called on every step
                            // Update the element's text with rounded-up value:
                            jQuery('#animated_total').text(numberFormat(this.someValue));
                        }
                    });
                    swal("");
                    
                    swal({
                        
                        title: "Thanks",
                        text: "This item added to your cart",
                        icon: "success",
                    
                        })
                        .then((value) => {
                          location.reload();  
                        });
             
                      //location.reload();  
                    jQuery('#items_loading_div').html(data.data);
                    // location.reload();
                    //console.log(data.data);
                    //$('footer').after().html(data.data);
                },
                error: function () {
                }
            });
        };
        
    </script>
    <style type="text/css">
        .leftside {
            vertical-align: middle;
            display: inline-block;
        }

        .rightside {
            vertical-align: middle;
            display: inline-block;
        }
      
       .product-description-wrapper img{
            width: 100%;
        }
      
        .single-post-title h3 {
            color: #333;
            font-weight: bold;
            font-size: 27px;
            margin: 0;
            margin-bottom: 15px;
        }
        .single-post-title p {
            font-size: 14px;
            font-weight: 400;
            color: #333;
        }
        .single-inner-post1 .single-post-title,
        .single-post-wrapper5 .single-post-title{
            padding-bottom: 20px;
        }
        .single-post-title.wrap-box{
            position: relative;
        }
        
        .single-post-wrapper5 .single-post-title h3,
        .single-post-wrapper4 .single-post-title h3{
            margin-top: 15px;
            margin-bottom: 8px;
        }
        .related-img{
            margin-top: 20px;
        }
        .single-post-txt.post-box,
        .single-post-txt.post-box2{
            position: absolute;
            top: 50px;
            width: 35%;
            background: #fff;
            padding:50px  30px;
            bottom: 50px;
            overflow: hidden;
        }
        .product-description-wrapper .single-post-txt.post-box{
            left: 50px;
            
        }
        .product-description-wrapper .single-post-txt.post-box2{
            right: 50px;
        }
        .singe-txt-wrap{
            height: 100%;
            overflow: hidden;
        }

        .product-description-wrapper:nth-child(even){
            background: #ddd;
            padding: 40px 0;
        }

        .product-description-wrapper:nth-child(odd){
            background: #fff;
            padding: 40px 0;
        }

        .mb-20{
            margin-bottom: 20px;
        }
    </style>
@endsection