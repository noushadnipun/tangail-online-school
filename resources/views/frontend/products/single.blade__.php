@extends('frontend.layouts.app')

@section('content')
    <?php
    //dump($product);
    if (!empty($product)) {
        $product_information = product_attributes($product, FALSE);

        $infoss[] = json_decode($product_information['values']);
        $infoss[] = $product_information['id'];
    }

    $tksign = '&#2547; ';
    ?>

    <section class="cat-banner-one">
        <div class="cat-banner-one-top">
            <img src="http://103.218.26.178:8081/vision/public/frontend/img/pexels-photo-279719.jpeg" alt="">
            <div class="single-inr-title">
            <div class="container">
             <div class="row">
            <div class="col-md-12">
              <h1>Televisions</h1>
              <h4>Vision 24" LED TV T01</h4>
              </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <section class="define-item">
        <div class="container">
            <div class="row">
                <div class="item-wap">
                <div class="col-md-6">
                    <div class="a owl-carousel owl-theme">
                        <div class="item"><img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision43incht02code-823050big-800x800.png" alt=""></div>
                        <div class="item"><img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision43incht02code-823050big-800x800.png" alt=""></div>
                        <div class="item"><img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision43incht02code-823050big-800x800.png" alt=""></div>
                    </div>
                </div>
                <div class="life-inner">
                  <div class="col-md-6">
                     <div class="life-inner">
                      <div class="product-specition">
                        <div class="ps-header">
                            <h1>{{ $product->title }}</h1>
                            {{--<ul class="list-inline">--}}
                            {{--<li><i class="fa fa-star"></i></li>--}}
                            {{--<li><i class="fa fa-star"></i></li>--}}
                            {{--<li><i class="fa fa-star"></i></li>--}}
                            {{--<li><i class="fa fa-star"></i></li>--}}
                            {{--<li><i class="fa fa-star"></i></li>--}}
                            {{--</ul>--}}
                        </div>
                        <div class="ps-body cat-pro-body">
                            {{--<p class="model">Material: {{ $product->material }}</p>--}}
                            {{--<p class="color">Color: {{ $product->color }}</p>--}}
                            <h4>
                                <b>
                                    <?php $tksign = '&#2547; '; ?>
                                    {{ $tksign . number_format($product->local_selling_price) }}
                                </b>
                            </h4>
                            <ul class="list-unstyled">
                                <li>{{ $product->title }}</li>
                                <li>{!! $product->description !!}</li>
                            </ul>
                        </div>
                        <div class="ps-footer">
                           <div class="cat-btm text-center">
                                <a href="" class="">Buy Now</a>
                            </div>
                        </div>
                    </div>
            {{--@include('frontend.products.features_template')--}}
            {{--@include('frontend.products.you_may_like')--}}


     
                    </div>
                </div>
            </div>
         </div>
        </div>
     </div>
    </section>
    <section class="section-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="quote-text">
                        <p>"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here content here"</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="editorial">
        <div class="editorial-warp">
            <div class="editorial-warp-left">
                <img src="http://103.218.26.178:8081/vision/public/frontend/img/samsung_curved_oled_tv_live_3-820x420.jpg " alt="">
            </div>
            <div class="editorial-warp-right">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="editorial-warp-contant">
                                <h1>Reality as it was meant to be <br> experienced</h1>
                                <h3>4K HDR Processor X1™ Extreme</h3>
                                <p>X1 Extreme shows images and colors as they truly are, the way you want to see them.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
        </div>
    </section>
    <section class="editorial">
        <div class="editorial-warp editorial-warp-2">
            <div class="editorial-warp-right">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="editorial-warp-contant">
                                <h1>Uncover the detail with 4K <br> HDR</h1>
                                <h3>4K HDR Processor X1™ Extreme</h3>
                                <p>X1 Extreme shows images and colors as they truly are, the way you want to see them.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            <div class="editorial-warp-left">
                <img src="http://103.218.26.178:8081/vision/public/frontend/img/image.jpg" alt="">
            </div>
        </div>
    </section>
     <section class="editorial">
        <div class="editorial-warp">
            <div class="editorial-warp-left">
            <img src="http://103.218.26.178:8081/vision/public/frontend/img/app-structure-browse-02.jpg" alt="">
            </div>
            <div class="editorial-warp-right">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="editorial-warp-contant">
                                <h1>Transform your viewing with dramatic <br> imaging</h1>
                                <h3>4K HDR Processor X1™ Extreme</h3>
                                <p>X1 Extreme shows images and colors as they truly are, the way you want to see them.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
        </div>
    </section>
    <section class="editorial">
        <div class="editorial-warp editorial-warp-2">
            <div class="editorial-warp-right">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="editorial-warp-contant">
                                <h1>Life's brilliance, revealed with extra <br>colors</h1>
                                <h3>4K HDR Processor X1™ Extreme</h3>
                                <p>X1 Extreme shows images and colors as they truly are, the way you want to see them.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            <div class="editorial-warp-left">
                <img src="http://103.218.26.178:8081/vision/public/frontend/img/wallpapers-hd-3_6NsJLwq.jpg" alt="">
            </div>
        </div>
    </section>
    <section class="cat-banner-one">
        <div class="cat-banner-one-top">
            <img src="http://103.218.26.178:8081/vision/public/frontend/img/nc50-reallife.png" alt="">
            <div class="single-inr-title">
            <div class="container">
             <div class="row">
            <div class="col-md-6 col-md-offset-6">
                <div class="lest-bar-pg">
              <h1>The greatest sound you've never seen</h1>
              <p>Unlike most TV speakers, sound comes to you from the entire screen, immersing you in exciting new entertainment experiences. It's pictures and sound in perfect harmony.</p>
              </div>
             </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <section class="specifications">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="specifications-content">
                        <h1>Specifications and Features</h1>
                        <p>Discover incredible contrast on an OLED 4K HDR TV with 8 million self-emitting pixels. Pictures are amazingly real with the deepest blacks providing detailed shadow expression and vibrant color. Innovative Acoustic Surface™ technology delivers engaging sound directly from the screen.
                        </p>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-12">
                     <div class="specifications-list">
                        <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li>Available in 55" class (54.6" diag), 65" class (64.5" diag), 77" class (76.7" diag)</li>

                            <li>4K High Dynamic Range</li>

                            <li>4K HDR Processor X1™ Extreme</li>
                            </ul>
                            </div>
                            <div class="col-md-6">
                            <ul class="list-unstyled">
                            <li>OLED</li>

                            <li>Acoustic Surface™ technology</li>
                        </ul>
                     </div>
                    </div>
                     
                 </div>
            </div>
        </div>
    </section>
<!-- 
    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    $title = get_breadcrumb_title(Request::segment(1));

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb(ucfirst($title), 'page');
                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section> -->

@endsection
@section('cusjs')
    <script type="text/javascript">
        //picZoomer
        jQuery(document).ready(function ($) {
            $.noConflict();
//            $('.picZoomer').picZoomer();

            //切换图片
            $('.piclist li').on('click', function (event) {
                var $pic = $(this).find('img');
                var $buynow = $("#buynow").data('imageurl');
                $('.picZoomer-pic').attr('src', $pic.attr('src'));
                $('#buynow').data('imageurl', $buynow.data('imageurl'));
            });

            $('#combinition').on('change', function (event) {
                event.preventDefault();

                var val = $(this).val();
                //var n = val.split('|');

                //var id = n[1];
                //var item = n[0];

                var url = window.location.pathname;
                window.location.replace(url + '?product_code=' + val);

            });

            $('#plus, #minus').on('click', function (e) {

            });

        });
    </script>
@endsection