<div class="row">
    <div class="col-md-12">
        <div class="related-product">
            <h1>related products</h1>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="cat-products">
                        <div class="cat-pro-header">
                            <img src="img/home2.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="cat-pro-body">
                            <h2>36-inch Wide Side-by-Side Counter Depth Refrigerator...</h2>
                            <p class="model">Model #: WRS973CIDM</p>
                            <p class="color">Color: Monochromatic Stainless Steel</p>
                            <h4>MSRP $2,599.00</h4>
                        </div>
                        <div class="cat-pro-footer">
                            <a href="product.php" class="vison-btn vison-btn-fluid">View Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="cat-products">
                        <div class="cat-pro-header">
                            <img src="img/home2.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="cat-pro-body">
                            <h2>36-inch Wide Side-by-Side Counter Depth Refrigerator...</h2>
                            <p class="model">Model #: WRS973CIDM</p>
                            <p class="color">Color: Monochromatic Stainless Steel</p>
                            <h4>MSRP $2,599.00</h4>
                        </div>
                        <div class="cat-pro-footer">
                            <a href="product.php" class="vison-btn vison-btn-fluid">View Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="cat-products">
                        <div class="cat-pro-header">
                            <img src="img/home2.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="cat-pro-body">
                            <h2>36-inch Wide Side-by-Side Counter Depth Refrigerator...</h2>
                            <p class="model">Model #: WRS973CIDM</p>
                            <p class="color">Color: Monochromatic Stainless Steel</p>
                            <h4>MSRP $2,599.00</h4>
                        </div>
                        <div class="cat-pro-footer">
                            <a href="product.php" class="vison-btn vison-btn-fluid">View Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="cat-products">
                        <div class="cat-pro-header">
                            <img src="img/home2.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="cat-pro-body">
                            <h2>36-inch Wide Side-by-Side Counter Depth Refrigerator...</h2>
                            <p class="model">Model #: WRS973CIDM</p>
                            <p class="color">Color: Monochromatic Stainless Steel</p>
                            <h4>MSRP $2,599.00</h4>
                        </div>
                        <div class="cat-pro-footer">
                            <a href="product.php" class="vison-btn vison-btn-fluid">View Details</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>