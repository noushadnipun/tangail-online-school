@extends('frontend.layouts.app')

@section('content')
    <!--breadcrumb-area start  -->

    <!-- <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

    $title = get_breadcrumb_title(Request::segment(1));

    $breadcrumbs->setDivider(' » &nbsp;');
    $breadcrumbs->addCrumb('Home', url('/'))
        ->addCrumb($title, 'category');
    echo $breadcrumbs->render();
    ?>
            </div>
        </div>
    </div>
</section> -->

    <section class="cat-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cat-inner">
                        <h1>{{ $category->description }}</h1>
                        <h4><a href="#">Televisions</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-section">
        <div class="container">
            <div class="cat-topbar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="cat-topbar-left">
                            <p>Showing 12 results</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="cat-topbar-right">
                            <div class="dropdown filter">
                                <button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown">
                                    Featured
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu filter-menu">
                                    <li><a href="#">Price - high to low</a></li>
                                    <li><a href="#">Price - low to high</a></li>
                                    <li><a href="#">New</a></li>
                                    <li><a href="#">Ratings</a></li>
                                </ul>
                            </div>
                            <div class="topbar-right-search-bar">
                                <div class="input-group">
                                    <input type="text" placeholder="Search Products" class="form-control cat-search">
                                    <span class="input-group-btn">
                                            <button type="button" class="btn btn-default cat-search">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision43incht02code-823050big-800x800.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 24" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision32incht02smartcode-801949big-800x800.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 32" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://www.rfleshop.com/uploads/medium/VISION_43inch_T01_smart,_Code-823056_medium-400x400.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 43" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision43incht02code-823050big-800x800.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 24" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision32incht02smartcode-801949big-800x800.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 32" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://www.rfleshop.com/uploads/medium/VISION_43inch_T01_smart,_Code-823056_medium-400x400.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 43" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision43incht02code-823050big-800x800.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 24" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://103.218.26.178:8081/vision/storage/uploads/fullsize/2018-06/vision32incht02smartcode-801949big-800x800.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 32" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-cat">
                        <a href="">
                            <div class="cat-top">
                                <img src="http://www.rfleshop.com/uploads/medium/VISION_43inch_T01_smart,_Code-823056_medium-400x400.png"
                                     alt="">
                            </div>
                        </a>
                        <div class="cat-mid">
                            <div class="cat-mid-title">
                                <h1>Vision 43" LED TV T01</h1>
                            </div>
                            <div class="review-str">
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-o"></i></a></li>
                                </ul>
                            </div>
                            <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>Code: 94833</li>
	    								<li>24" DC HD Ready TV T-01</li>
	    								<li>Advance viewing experience</li>
	    								<li>Contrast ratio 3000: 1</li>
	    							</ul>
	    						</span>
                            <span class="price">
	    							Price: 50000 Tk
	    						</span>
                        </div>
                        <div class="cat-btm text-center">
                            <a href="product.php" class="">Buy Now</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
@include('frontend.products.search_js')