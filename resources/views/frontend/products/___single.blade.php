@extends('frontend.layouts.app')

@section('content')
    <?php
    $categories = explode(',', $product->categories);
    $category_info = App\Term::where('id', $categories[0])->get()->first();
    //dump($category_info);
    ?>
    <section class="cat-banner"
             style="background: url({{ !empty($category_info->term_image) ? $category_info->term_image : null }}) !important;">
        <div class="single-inr-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{ get_category_name_by_product_id($product, TRUE) }}</h1>
                        <h4>{{ $product->title }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="define-item">
        <div class="container">
            <div class="row">
                <div class="item-wap">
                    <div class="col-md-6 leftside">
                        {!! corousel($product->carousel) !!}
                    </div>
                    <div class="col-md-6 rightside">
                        <div class="life-inner">
                            <div class="product-specition">
                                <div class="ps-header">
                                    <h1>{{ $product->title }}</h1>
                                </div>
                                <div class="ps-body cat-pro-body">
                                    <h4>
                                        <b>
                                            <?php $tksign = 'BDT. '; ?>
                                            {{ $tksign . number_format($product->local_selling_price) }}
                                        </b>
                                    </h4>
                                    <ul class="list-unstyled">
                                        <li>{{ $product->title }}</li>
                                        <li>{!! $product->description !!}</li>
                                    </ul>
                                </div>
                                <div class="ps-footer">
                                    <div class="cat-btm">
                                        <?php if(!empty($product->go_to_url)) : ?>
                                        <a href="{{ $product->go_to_url }}" class="">BUY ONLINE</a>
                                        <a class="btn-ner" href="{{ url('scategory/110/nearest-shop') }}" class="">Find
                                            the Nearest Shop</a>
                                        <?php endif; ?>
                                    </div>
                                    <p><span>*</span>Othoba.com is the trusted online partner.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="quote-text">
                        <p>
                            {{ $product->short_description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {!! section_printer($product->custom_content) !!}


    <section class="specifications">
        <div class="container">
            <div class="row">

                {!!  $product->specifications !!}

            </div>
        </div>
    </section>

@endsection
@section('cusjs')
    <script type="text/javascript">
        //picZoomer
        jQuery(document).ready(function ($) {
            $.noConflict();
//            $('.picZoomer').picZoomer();

            //切换图片
            $('.piclist li').on('click', function (event) {
                var $pic = $(this).find('img');
                var $buynow = $("#buynow").data('imageurl');
                $('.picZoomer-pic').attr('src', $pic.attr('src'));
                $('#buynow').data('imageurl', $buynow.data('imageurl'));
            });

            $('#combinition').on('change', function (event) {
                event.preventDefault();

                var val = $(this).val();
                //var n = val.split('|');

                //var id = n[1];
                //var item = n[0];

                var url = window.location.pathname;
                window.location.replace(url + '?product_code=' + val);

            });

            $('#plus, #minus').on('click', function (e) {

            });

            var ps = $('.product-specition').height();
            console.log(ps);
            var ls = $('.leftside').height();

            $('.leftside').css('margin-top', function () {
                if (ps <= 580) {
                    return "0%";
                } else if (ps > 580 && ps <= 1000) {
                    return "8%";
                } else if (ps > 800 && ps <= 1200) {
                    return "20%";
                } else if (ps > 1200 && ps <= 1600) {
                    return "40%";
                } else {
                    return ($('.leftside').height() - ps) + "px";
                }
            });


        });
    </script>
    <style type="text/css">
        .leftside {
            vertical-align: middle;
            display: inline-block;
        }

        .rightside {
            vertical-align: middle;
            display: inline-block;
        }
    </style>
@endsection