<?php $i = 0; ?>
@foreach($t_exclusive as $exclusive_product)
    {{--@if($exclusive_product->is_sticky == 1)--}}
    {{--@if($i <= 3)--}}
    @if (!empty($exclusive_product))
        <?php
        $tojson = product_information_attributes($exclusive_product->product_attributes);
        $infoss[] = json_decode($tojson['values']);
        $infoss[] = $tojson['id'];
        $tksign = '&#2547; ';
        ?>

        <div class="col-md-3 col-sm-12 col-xs-12 col100">
            <div class="cat-products">
                <div class="cat-pro-header">
                    <img src="{{ main_image($exclusive_product->product_attributes) }}" class="img-responsive"
                         alt="exc-prdt">
                </div>
                <div class="car-wrapper">
                    <div class="cat-pro-body">
                        <h2>
                            {{ $exclusive_product->title }}
                        </h2>
                        <p class="model">
                            Product Code: {{ value_by_key($exclusive_product, 'product_code') }}
                        </p>
                        <p class="color">
                            Color: {{ value_by_key($exclusive_product, 'color') }}
                        </p>
                        <h4>MSRP {{ $tksign }} {!!  product_price($exclusive_product->id) !!}</h4>
                    </div>
                    <div class="cat-pro-footer">
                        <a href="{!! product_seo_url($exclusive_product->seo_url, $exclusive_product->id) !!}" class="vison-btn vison-btn-fluid">View Details</a>
                    </div>
                </div>
            </div>
        </div>

    @endif
    <?php $i++; ?>
@endforeach
