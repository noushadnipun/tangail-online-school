@extends('frontend.layouts.app')

@section('content')
    <!--breadcrumb-area start  -->

    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    $title = get_breadcrumb_title(Request::segment(1));

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb($title, 'category');
                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="cat-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cat-inner">
                        <h1>{{ $category->description }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contentbox cat-product">
        <div class="container category">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="cat-menu">
                        {{--<h1>Refine By:</h1>--}}
                       @include('frontend.products.category_sidebar')
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="cat-pro-list">

                        <div class="left_cat_title pull-left">
                            <h6>{{ $title }}</h6>
                        </div>

                        {{--<div class="searh-reasult">--}}
                        {{--<p>3 result</p>--}}
                        {{--</div>--}}
                        <div class="sort-reasult pull-right">
                            <button class="list-reslut">
                                <span class="glyphicon glyphicon-th-list list-switch "></span>
                            </button>
                            <button class="column-reslut">
                                <span class="glyphicon glyphicon-th grid-switch on"></span>
                            </button>
                        </div>
                        {{--<div class="sortname">--}}
                        {{--<p>Sort By:</p>--}}
                        {{--</div>--}}
                        {{--<div class="sortby-reasult">--}}
                        {{--<form action="">--}}
                        {{--<div class="form-group">--}}
                        {{--<select class="form-control" id="sel1">--}}
                        {{--<option>Relevance</option>--}}
                        {{--<option>Relevance</option>--}}
                        {{--<option>Relevance</option>--}}
                        {{--<option>Relevance</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">

                        <div class="exc-item-warp exc-prdt-warp  category-item">
                            @foreach($products as $product)
                                <div class="col-md-4 col-sm-12 col-xs-12 col100">
                                    <div class="cat-products">
                                        <div class="cat-pro-header">

                                            <?php
                                            $images = explode(',', $product->images);
                                            $imgs = App\Image::find($images);
                                            ?>

                                            <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                                 class="img-responsive"
                                                 alt="{{ $imgs[0]['original_name'] }}"/>

                                        </div>
                                        <div class="car-wrapper">
                                            <div class="cat-pro-body">
                                                <h2>{{ $product->title }}</h2>
                                                {{--<p class="model">Material: {{ $product->material }}</p>--}}
                                                {{--<p class="color">Color: {{ $product->color }}</p>--}}
                                                <h4>
                                                    <?php $tksign = '&#2547; '; ?>
                                                    {{ $tksign . number_format($product->local_selling_price) }}
                                                </h4>
                                            </div>
                                            <div class="cat-pro-footer">
                                                <a href="{{ url('product/' . $product->id . '/' . $product->seo_url) }}"
                                                   class="vison-btn vison-btn-1  vison-btn-fluid pull-right">
                                                    Buy
                                                </a>
                                                <a href="{{ url('product/' . $product->id . '/' . $product->seo_url) }}"
                                                   class="vison-btn vison-btn-fluid">
                                                    Details
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection
@include('frontend.products.search_js')