<div class="row">
    <div class="col-md-12">

        <div class="Details_product">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#homee">FEATURES</a></li>
                <li><a data-toggle="tab" href="#menu11">SPECIFICATIONS & MANUALS</a></li>
                {{--<li><a data-toggle="tab" href="#menu22">RATINGS & REVIEWS</a></li>--}}
                {{--<li><a data-toggle="tab" href="#menu33">RECOMMENDATIONS</a></li>--}}
            </ul>

            <div class="tab-content">
                <div id="homee" class="tab-pane fade in active">
                    <p>
                        {!! $product->description !!}
                    </p>

                </div>
                <div id="menu11" class="tab-pane fade">
                    <h3>SPECIFICATIONS & MANUALS</h3>
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>Capacity</td>
                            <td>6.40 cu. ft.</td>
                        </tr>
                        <tr>
                            <td>Cutout Depth</td>
                            <td>24.0 in</td>
                        </tr>
                        <tr>
                            <td>Cutout Height</td>
                            <td>41-5/16 in</td>
                        </tr>
                        <tr>
                            <td>Capacity</td>
                            <td>6.40 cu. ft.</td>
                        </tr>
                        <tr>
                            <td>Cutout Depth</td>
                            <td>24.0 in</td>
                        </tr>
                        <tr>
                            <td>Cutout Height</td>
                            <td>41-5/16 in</td>
                        </tr>
                        <tr>
                            <td>Capacity</td>
                            <td>6.40 cu. ft.</td>
                        </tr>
                        <tr>
                            <td>Cutout Depth</td>
                            <td>24.0 in</td>
                        </tr>
                        <tr>
                            <td>Cutout Height</td>
                            <td>41-5/16 in</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                {{--<div id="menu22" class="tab-pane fade">--}}
                {{--<h3>RATINGS & REVIEW</h3>--}}
                {{--<table class="table table-striped">--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                {{--<td>Capacity</td>--}}
                {{--<td>6.40 cu. ft.</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Depth</td>--}}
                {{--<td>24.0 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Height</td>--}}
                {{--<td>41-5/16 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Capacity</td>--}}
                {{--<td>6.40 cu. ft.</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Depth</td>--}}
                {{--<td>24.0 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Height</td>--}}
                {{--<td>41-5/16 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Capacity</td>--}}
                {{--<td>6.40 cu. ft.</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Depth</td>--}}
                {{--<td>24.0 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Height</td>--}}
                {{--<td>41-5/16 in</td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
                {{--</table>--}}
                {{--</div>--}}
                {{--<div id="menu33" class="tab-pane fade">--}}
                {{--<h3>RECOMMENDATIONS</h3>--}}
                {{--<table class="table table-striped">--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                {{--<td>Capacity</td>--}}
                {{--<td>6.40 cu. ft.</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Depth</td>--}}
                {{--<td>24.0 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Height</td>--}}
                {{--<td>41-5/16 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Capacity</td>--}}
                {{--<td>6.40 cu. ft.</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Depth</td>--}}
                {{--<td>24.0 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Height</td>--}}
                {{--<td>41-5/16 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Capacity</td>--}}
                {{--<td>6.40 cu. ft.</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Depth</td>--}}
                {{--<td>24.0 in</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td>Cutout Height</td>--}}
                {{--<td>41-5/16 in</td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
                {{--</table>--}}
                {{--</div>--}}
            </div>
        </div>

    </div>
</div>