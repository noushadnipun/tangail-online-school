@extends('frontend.layouts.app')

@section('content')
    <?php
    //dump($product);
    if (!empty($product)) {
        $product_information = product_attributes($product, FALSE);

        $infoss[] = json_decode($product_information['values']);
        $infoss[] = $product_information['id'];
    }

    $tksign = '&#2547; ';
    ?>


    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    $title = get_breadcrumb_title(Request::segment(1));

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb(ucfirst($title), 'page');
                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="contentboxs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{--<h1>{{ $title }}</h1>--}}

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="sProduct-img">
                                <div class="spimg-img">
                                    <img src="{!! product_main_image($product->id) !!}"
                                         alt="{!! product_title($product->id) !!}"/>
                                </div>
                                <div class="spimg-list">
                                    <?php $i = 0; ?>
                                    @foreach($infoss[0] as $prd_info)
                                        @if(isset($_GET['product_code']))
                                            @if($prd_info->product_code === $_GET['product_code'])
                                                <?php $images = explode(',', $prd_info->variation_images); ?>
                                                @foreach($images as $img)
                                                    {!! images_by_ids($img, ['link' => true,'size' => 'full']) !!}
                                                @endforeach
                                            @endif
                                        @else
                                            @if($i < 1)
                                                <?php $images = explode(',', $prd_info->variation_images); ?>
                                                @foreach($images as $img)
                                                    {!! images_by_ids($img, ['link' => true,'size' => 'full']) !!}
                                                @endforeach
                                            @endif
                                        @endif
                                        <?php $i++; ?>
                                    @endforeach
                                </div>
                            </div>

                            <div class="bzoom_wrap">
                                <ul id="bzoom">

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php $i = 0; ?>
                            @foreach($infoss[0] as $prd_info)
                                @if(isset($_GET['product_code']) && !empty($_GET['product_code']))
                                    {{--@if($i < 1)--}}
                                    @if($prd_info->product_code === $_GET['product_code'])
                                        <div class="product-specition">
                                            <div class="ps-header">
                                                <h1>{!! $product->title !!}</h1>
                                            </div>
                                            <div class="ps-body cat-pro-body">
                                                <h4>
                                                    <b>{{ $tksign }} {{ value_by_key($prd_info, 'selling_price') }} </b>
                                                </h4>

                                                <p class="model">
                                                    Product Code: {{ value_by_key($prd_info, 'product_code') }}
                                                </p>
                                                <p class="color">
                                                    Color: {{ value_by_key($prd_info, 'color') }}
                                                </p>
                                                <p class="color">
                                                    Material: {{ value_by_key($prd_info, 'material') }}
                                                </p>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="ps-footer">
                                                <a href="#" class="vison-btn vison-btn-fluid">Buy Now</a>
                                            </div>
                                        </div>

                                        <div class="ps-bottom">
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-heart"></i>
                                                    <span>sign to save</span>
                                                </a>
                                            </div>
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-circle-thin"></i>
                                                    <span>Compare</span>
                                                </a>
                                            </div>
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-share"></i>
                                                    <span>share</span>
                                                </a>
                                            </div>
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-print"></i>
                                                    <span>print</span>
                                                </a>
                                            </div>
                                        </div>
                                    @endif
                                    <?php $i++; ?>
                                @else
                                    @if($i < 1)

                                        <div class="product-specition">
                                            <div class="ps-header">
                                                <h1 style="border-bottom: 1px dashed #FFF; padding-bottom: 5px;">{!! $product->title !!}</h1>
                                            </div>
                                            <div class="ps-body cat-pro-body">

                                                <h4>
                                                    <b>{{ $tksign }} {{ value_by_key($prd_info, 'selling_price') }} </b>
                                                </h4>

                                                <p class="model">
                                                    Product Code: {{ value_by_key($prd_info, 'product_code') }}
                                                </p>
                                                <p class="color">
                                                    Color: {{ value_by_key($prd_info, 'color') }}
                                                </p>
                                                <p class="color">
                                                    Material: {{ value_by_key($prd_info, 'material') }}
                                                </p>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="ps-footer">
                                                <a href="#" class="vison-btn vison-btn-fluid">Buy Now</a>
                                            </div>
                                        </div>

                                        <div class="ps-bottom">
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-heart"></i>
                                                    <span>sign to save</span>
                                                </a>
                                            </div>
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-circle-thin"></i>
                                                    <span>Compare</span>
                                                </a>
                                            </div>
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-share"></i>
                                                    <span>share</span>
                                                </a>
                                            </div>
                                            <div class="ps-itemss">
                                                <a href="">
                                                    <i class="fa fa-print"></i>
                                                    <span>print</span>
                                                </a>
                                            </div>
                                        </div>

                                    @endif
                                @endif
                                <?php $i++; ?>
                            @endforeach
                        </div>
                    </div>

                    @include('frontend.products.features_template')
                    @include('frontend.products.you_may_like')

                </div>
            </div>
        </div>
    </section>
@endsection
@section('cusjs')
    <script type="text/javascript">
        //picZoomer
        jQuery(document).ready(function ($) {
            $.noConflict();
//            $('.picZoomer').picZoomer();

            //切换图片
            $('.piclist li').on('click', function (event) {
                var $pic = $(this).find('img');
                var $buynow = $("#buynow").data('imageurl');
                $('.picZoomer-pic').attr('src', $pic.attr('src'));
                $('#buynow').data('imageurl', $buynow.data('imageurl'));
            });

            $('#combinition').on('change', function (event) {
                event.preventDefault();

                var val = $(this).val();
                //var n = val.split('|');

                //var id = n[1];
                //var item = n[0];

                var url = window.location.pathname;
                window.location.replace(url + '?product_code=' + val);

            });

            $('#plus, #minus').on('click', function (e) {

            });

        });
    </script>
@endsection


<?php
$img = explode(',', $product->images);
$im = App\Image::find($img);
?>
<div class="bzoom_wrap">
    <ul id="bzoom" class="bzoom clearfix" style="display: block;">
        <li class="bzoom_thumb" style="display: none; opacity: 0;">
            <img class="bzoom_thumb_image" src="img/led3.jpg" title="first img"
                 style="width: 300px; height: 400px; display: inline;">
            <img class="bzoom_big_image" src="img/led3.jpg">
        </li>
    </ul>
</div>
<div class="sProduct-img">

    <div class="spimg-list">

        @foreach($images as $img)
            <img src="{{ url($im->full_size_directory) }}" alt="small">
        @endforeach

    </div>
</div>
