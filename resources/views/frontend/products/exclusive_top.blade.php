<div class="col-sm-12 col-md-9">
    <div class="exc-prdt-warp">
        <div class="section-title">
            <h1>Exclusive Products</h1>
        </div>
        <div class="exc-item-warp">
            <?php $i = 0; ?>
            @foreach($t_exclusive as $exclusive_product)
                {{--@if($exclusive_product->is_sticky == 1)--}}
                {{--@if($i <= 3)--}}
                @if (!empty($exclusive_product))
                    <?php
                    $tojson = product_information_attributes($exclusive_product->product_attributes);
                    $infoss[] = json_decode($tojson['values']);
                    $infoss[] = $tojson['id'];
                    ?>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="singele-exc-prdt">
                            <div class="exc-prdt-img">
                                <a href="{!! product_seo_url($exclusive_product->seo_url, $exclusive_product->id) !!}">
                                    <img src="{{ main_image($exclusive_product->product_attributes) }}"
                                         alt="exc-prdt">
                                </a>
                            </div>
                            <div class="exc-prdt-text">
                                <h3>
                                    <a href="{!! product_seo_url($exclusive_product->seo_url, $exclusive_product->id) !!}">
                                        {{ $exclusive_product->title }}
                                    </a>
                                </h3>
                                <h2>
                                    <?php $tksign = '&#2547; '; ?>
                                    <a href="{!! product_seo_url($exclusive_product->seo_url, $exclusive_product->id) !!}">
                                        {{ $tksign }} {!!  product_price($exclusive_product->id) !!}
                                    </a>
                                </h2>
                                <div class="product-btn">
                                    <?php
                                    $regularprice = product_normal_price_without_sign($exclusive_product->id);
                                    $save = product_save_1st($exclusive_product->id);
                                    $sp = product_normal_price_without_sign($exclusive_product->id);
                                    ?>

                                    <div class="buy-btn">
                                        <a type="button"
                                           id="buynow"
                                           href="javascript:void(0)"
                                           class="buy-now"
                                           onclick="add_to_cart(
                                                   '{{ $exclusive_product->id }}',
                                                   '{{ product_code_1st($exclusive_product->id) }}',
                                                   '{{ product_code_1st($exclusive_product->id) }}',
                                                   '{{ $regularprice }}',
                                                   '{{ $save }}',
                                                   '{{ $sp }}',
                                                   null,
                                                   null,
                                                   1);">
                                            <i class="fa fa-shopping-cart"></i> Buy
                                        </a>

                                    </div>
                                    <div class="detalis-btn">
                                        <a href="{!! product_seo_url($exclusive_product->seo_url, $exclusive_product->id) !!}">detalis</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- singele-exc-prdt -->
                    </div>
                @endif
                {{--@endif--}}
                <?php $i++; ?>
                {{--@endif--}}
            @endforeach
        </div>
    </div>
</div>

