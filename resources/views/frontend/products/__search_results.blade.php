@extends('frontend.layouts.app')

@section('content')
    @if(Request::segment(1) == 'c')
        @php $img = $category_info->term_image; @endphp
    @else
        @php $img = 'https://vision.com.bd/public/frontend/img/slide3.jpg'; @endphp
    @endif
    

    <section class="cat-banner" style="background: url({{ $img }}) !important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cat-inner">
                        <h1>{{ (!empty($category_info->description) ? $category_info->description : 'Search Results') }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-section">
        <div class="container">

            <div class="row cat-max-hadmat">

                <?php $tksign = 'BDT. '; ?>

                @foreach($products as $product)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-cat">
                            <a href="{{ url('p/' . $product->seo_url) }}">
                                <div class="cat-top">
                                    <?php
                                    $images = explode(',', $product->images);
                                    $imgs = App\Image::find($images);
                                    ?>
                                    <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                         alt="{{ $imgs[0]['original_name'] }}">
                                </div>
                            </a>
                            <div class="cat-mid">
                                <div class="cat-mid-title">
                                    <h1>
                                        <a href="{{ url('p/' . $product->seo_url) }}">
                                            {{ $product->title }}
                                        </a>
                                    </h1>
                                </div>
                                {{--<div class="review-str">--}}
                                {{--<ul class="list-unstyled">--}}
                                {{--<li><a href="#"><i class="fa fa-star"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-star"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-star"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-star"></i></a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-star-o"></i></a></li>--}}
                                {{--</ul>--}}
                                {{--</div>--}}
                                <span class="detl-list">
	    							<ul class="list-unstyled">
	    								<li>{{ !empty($product->sub_title) ?  $product->sub_title : '&nbsp;' }}</li>
	    								<li>{{ !empty($product->stock_status == 1) ? ' Stock Available ' : ' Out of stock ' }}</li>
	    							</ul>
	    						</span>
                                <span class="price">
	    							{{ $tksign . number_format($product->local_selling_price) }}
	    						</span>
                                <span class="detl-list">
                                    <ul class="list-unstyled">
                                        <li class="deft"><span class="star_pc">*</span> <small>The price is for online purchase only</small></li>
                                    </ul>
                                </span>


                            </div>
                            <div class="cat-btm text-center">
                                <a href="{{ url('p/' . $product->seo_url) }}" class="">
                                    <i class="fa fa-shopping-cart"></i> BUY
                                </a>

                                <a type="button"
                                   id="buynow"
                                   href="javascript:void(0)"
                                   class="compare buy-now"
                                   onclick="add_to_compare(
                                           '{{ $product->id }}',
                                           '{{ $product->product_code }}');">
                                    <i class="fa fa-balance-scale"></i> Compare
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="box-footer clearfix">
                    {{--{{ $products->links('component.paginator', ['object' => $products]) }}--}}
                </div>

            </div>
        </div>
    </section>

@endsection
@include('frontend.products.search_js')