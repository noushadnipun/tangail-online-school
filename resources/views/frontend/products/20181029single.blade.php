@extends('frontend.layouts.app')

@section('content')
    <section class="cat-banner-one">
        <div class="cat-banner-one-top">
            <img src="https://vision.com.bd/public/frontend/img/pexels-photo-279719.jpeg" alt="">
            <div class="single-inr-title">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>{{ get_category_name_by_product_id($product, TRUE) }}</h1>
                            <h4>{{ $product->title }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="define-item">
        <div class="container">
            <div class="row">
                <div class="item-wap">
                    <div class="col-md-6">
                        {!! corousel($product->carousel) !!}
                    </div>
                    <div class="life-inner">
                        <div class="col-md-6">
                            <div class="life-inner">
                                <div class="product-specition">
                                    <div class="ps-header">
                                        <h1>{{ $product->title }}</h1>
                                    </div>
                                    <div class="ps-body cat-pro-body">
                                        <h4>
                                            <b>
                                                <?php $tksign = 'BDT. '; ?>
                                                {{ $tksign . number_format($product->local_selling_price) }}
                                            </b>
                                        </h4>
                                        <ul class="list-unstyled">
                                            <li>{{ $product->title }}</li>
                                            <li>{!! $product->description !!}</li>
                                        </ul>
                                    </div>
                                    <div class="ps-footer">
                                        <div class="cat-btm text-center">

                                            <?php if(!empty($product->go_to_url)) : ?>
                                            <a href="{{ $product->go_to_url }}" class="">BUY ONLINE</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="quote-text">
                        <p>
                            {{ $product->short_description }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {!! section_printer($product->custom_content) !!}


    <section class="specifications">
        <div class="container">
            <div class="row">

                {!!  $product->specifications !!}

            </div>
        </div>
    </section>

@endsection
@section('cusjs')
    <script type="text/javascript">
        //picZoomer
        jQuery(document).ready(function ($) {
            $.noConflict();
//            $('.picZoomer').picZoomer();

            //切换图片
            $('.piclist li').on('click', function (event) {
                var $pic = $(this).find('img');
                var $buynow = $("#buynow").data('imageurl');
                $('.picZoomer-pic').attr('src', $pic.attr('src'));
                $('#buynow').data('imageurl', $buynow.data('imageurl'));
            });

            $('#combinition').on('change', function (event) {
                event.preventDefault();

                var val = $(this).val();
                //var n = val.split('|');

                //var id = n[1];
                //var item = n[0];

                var url = window.location.pathname;
                window.location.replace(url + '?product_code=' + val);

            });

            $('#plus, #minus').on('click', function (e) {

            });

        });
    </script>
@endsection