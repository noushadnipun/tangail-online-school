@extends('frontend.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="breadcrumb-warp section-margin-two">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <?php
                        $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                        $breadcrumbs->setDivider(' » &nbsp;');
                        $breadcrumbs->addCrumb('Home', url('/'))
                            ->addCrumb('Order List', 'product');
                        echo $breadcrumbs->render();
                        ?>
                    </div>
                    <!-- breadcrumb  end-->
                </div>
            </div>
        </div>
    </div>
    <?php $tksign = '&#2547; ';// dump($cartproducts); ?>
    <!--breadcrumb-area end  -->
    <!--prosuct-view-section  -->
    <section class="prosuct-view-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-text">
                        <h2 class="carddlist-title"><i class="fa fa-shopping-cart"></i> Order List </h2>
                        @if(Session::has('cart'))
                            <table class=" table table-one table-striped" style="width:100%">
                                <thead>
                                <tr class="CartProduct cartTableHeader pd-table-header table-striped">
                                    <td style="width:15%; text-align: left;"> Product</td>
                                    <td style="width:40%; text-align: left;">Details</td>
                                    <td style="width:10%" class="delete">&nbsp;</td>
                                    <td style="width:10%; text-align:center">QTY</td>

                                    <td style="width:15%; text-align: right;">Total</td>
                                </tr>
                                </thead>
                                <tbody>


                                @foreach($cartproducts as $product)
                                <?php
                                    $othoba_info =json_decode(CallAPI($product['item']['productcode']));
                                    $othoba_img = $othoba_info->DefaultPictureModel->ImageUrl;
                                    ?>
                                    <tr class="CartProduct">
                                        <td class="CartProductThumb cart-pd-thumb">
                                            <div>
                                                <a href="{{ seo_url_by_id($product['item']['productid']) }}">
                                                    <img src="{{ $othoba_img }}"
                                                         alt="img" style="opacity: 1;"/>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="CartDescription">
                                                <h4>
                                                    <a href="{{ seo_url_by_id($product['item']['productid']) }}">
                                                        {{ product_title($product['item']['productid']) }}
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                    <small>
                                                        <span>
                                                            PP: {{ $tksign }} {{ $product['item']['purchaseprice'] }},
                                                        </span>
                                                        <span>
                                                            PC: {{ $product['item']['productcode'] }}
                                                        </span>
                                                    </small>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="delete">
                                          
                                            <a href="javascript:void(0)"
                                                       class="remove_from_cart"
                                                       data-toggle="tooltip"
                                                       title="Remove"
                                                       onclick="remove_cart_item({{ $product['item']['productid'] . ', ' . $product['item']['productcode'] }})">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                        </td>
                                        <td class="cart check" style="text-align: center;">
                                            {{ $product['qty'] }}
                                        </td>

                                        <td class="price price-one">
                                            <span> {{ $tksign }} {{ $product['purchaseprice'] * $product['qty']  }}</span>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div class="cart-note"><b></b>Note:</b> We will redirect to othoba.com. Than you can buy this items and you can update you cart. </div>
                            <div class="table-box">
                                <a class="btn pull-left btn-one"><i class="fa fa-arrow-left"></i> More Buying</a>

                                <a class="btn pull-right btn-two" href="{{ url('product_buy') }}">
                                    Confirm Order <i class="fa fa-arrow-right"></i>
                                </a>
                                <button class="btn pull-right btn-three"
                                            id="update_cart">
                                        <i class="fa fa-undo"></i>
                                        Update cart
                                    </button>
                            </div>
                        @else
                            <h3>Opps... You have not added any product on your cart yet.</h3>
                        @endif
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>
    </section>
@endsection
@section('cusjs')
    <script type="text/javascript">

        function remove_cart_item(id, code) {
        
            var data = {
                'productid': id,
                'productcode': code,
            };
        
            jQuery.ajax({
                url: baseurl + '/remove_cart_item',
                method: 'get',
                data: data,
                success: function (data) {
                    jQuery("span#items_total").html(data.total_amount);
                    jQuery("span#items_count").html(data.total_qty);
                    jQuery("span#items_total").html(data.total_amount);
                    jQuery("span#items_count").html(data.total_qty);
        
                    location.reload();
                    //jQuery("div.reloader").load(baseurl + "/view_cart div.reloader");
                },
                error: function () {
                    // showError('Sorry. Try reload this page and try again.');
                    // processing.hide();
                }
            });
        }
   


    </script>
@endsection