<!-- sidebar-area section start -->
<div class="sidebar-pg-mu">
    <div class="panel panel-default">
        <div class="panel-body">
            {!! category_sidebar_menu_on_category_page($categories, $parent = 100, $seperator = ' ', $cid = NULL) !!}
        </div>
    </div>
</div>

<!--sidebar panel  -->
<div class="panel-group" id="accordionNo">
    <!--Price-section-->
    {{ Form::open(array('url' => url()->current(), 'method' => 'get', 'value' => 'PATCH', 'id' => 'searcher')) }}
    <div class="panel panel-default side-bar-menu">
        <div class="panel-heading pl-heading">
            <h4 class="panel-title">
                <a class="collapseWill" data-toggle="collapse" href="#collapsePrice">
                    <span class="pull-left"><i class="fa fa-caret-right"></i></span>
                    &nbsp;Price
                </a>
            </h4>
        </div>

        <div id="collapsePrice" class="panel-collapse collapse in " aria-expanded="true">
            <div class="panel-body priceFilterBody">
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 2000000) ? 'checked' : null) ?>
                           data-minprice="0" data-maxprice="2000000">
                    <label for="ritem16">Any Price</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 3000) ? 'checked' : null) ?>
                           data-minprice="0" data-maxprice="3000">
                    <label for="ritem15">Under 3000TK</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 5000) ? 'checked' : null) ?>
                           data-minprice="3000" data-maxprice="5000">
                    <label for="ritem14">3000 TK - 5000TK</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 10000) ? 'checked' : null) ?>
                           data-minprice="5000" data-maxprice="10000">
                    <label for="ritem13">5,000TK - 10,000TK</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 20000) ? 'checked' : null) ?>
                           data-minprice="10000" data-maxprice="20000">
                    <label for="ritem12">10,000TK - 20,000TK</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 30000) ? 'checked' : null) ?>
                           data-minprice="20000" data-maxprice="30000">
                    <label for="ritem11">20,000TK - 30,000TK</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="range" name="range" value=""
                           <?php echo((app('request')->input('maxprice') == 30000) ? 'checked' : null) ?>
                           data-minprice="30000" data-maxprice="">
                    <label for="ritem10">Abobe 30,000TK</label>
                </div>
            </div>
        </div>
    </div>

    <!--Sort-By-Price section-->
    <div class="panel panel-default side-bar-menu">
        <div class="panel-heading pl-heading">
            <h4 class="panel-title">
                <a class="collapseWill" data-toggle="collapse" href="#sortbyprice">
                    <span class="pull-left"><i class="fa fa-caret-right"></i></span> &nbsp;
                    Sort By Price
                </a>
            </h4>
        </div>
        <div id="sortbyprice" class="panel-collapse collapse in ">
            <div class="panel-body priceFilterBody pl-object">
                <div class="radio-item">
                    <input type="radio" id="price_sort" name="price_sort" value="all" data-field="" data-type="">
                    <label for="ritem4">Any</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="price_sort" name="price_sort" value="priceasc" data-field="price"
                           data-type="asc">
                    <label for="ritem3">Low to High</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="price_sort" name="price_sort" value="priceasc" data-field="price"
                           data-type="desc">
                    <label for="ritem2">High to Low</label>
                </div>
                <div class="radio-item">
                    <input type="radio" id="price_sort" name="price_sort"
                           data-field="id" data-type="desc">
                    <label for="ritem1">Newest</label>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
</div>