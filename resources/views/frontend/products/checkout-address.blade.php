@extends('frontend.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="breadcrumb-warp section-margin-two">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <?php
                        $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                        $breadcrumbs->setDivider(' » &nbsp;');
                        $breadcrumbs->addCrumb('Home', url('/'))
                            ->addCrumb('Delivery Address', 'product');
                        echo $breadcrumbs->render();
                        ?>
                    </div>
                    <!-- breadcrumb  end-->
                </div>
            </div>
        </div>
    </div>
    <?php $tksign = '&#2547; '; ?>
    <!--breadcrumb-area end  -->
    <!--prosuct-view-section  -->
    <section class="prosuct-view-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('cart'))
                        <section>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="about-text">
                                            <h2 class="carddlist-title">
                                                <i class="fa fa-map-marker"></i> Delivery Address
                                            </h2>
                                            <p class="delivery-text">
                                                To add a new delivery address, please fill out the form below.
                                            </p>
                                            <form action="">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            {{ Form::label('title', 'Full Name', array('class' => 'title')) }}
                                                            {{ Form::text('title', (!empty($page->title) ? $page->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Full Name']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::label('title', 'Mobile Number*', array('class' => 'title')) }}
                                                            {{ Form::text('title', (!empty($page->title) ? $page->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Mobile Number']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::label('title', 'Emergency Mobile Number', array('class' => 'title')) }}
                                                            {{ Form::text('title', (!empty($page->title) ? $page->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Emergency Mobile Number']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            {{ Form::label('title', 'Email', array('class' => 'title')) }}
                                                            {{ Form::text('title', (!empty($page->title) ? $page->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Email ']) }}
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            {{ Form::label('title', 'Enter Full Name', array('class' => 'title')) }}
                                                            {{ Form::textarea('title', (!empty($page->title) ? $page->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Full Name']) }}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-box">
                                                            <a class="btn pull-left btn-one">
                                                                <i class="fa fa-arrow-left"></i>
                                                                Back
                                                            </a>

                                                            <a class="btn pull-right btn-two" href="{{ url('checkout/payment_method') }}">
                                                                Next <i class="fa fa-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                        </section>
                    @else
                        <h3>Opps... You have not added any product on your cart yet.</h3>
                    @endif
                </div>
            </div>
            <br>
            <br>
        </div>
    </section>
@endsection
@section('cusjs')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $.noConflict();


        });

        function product_qty(id) {
            //$
        }
    </script>
@endsection