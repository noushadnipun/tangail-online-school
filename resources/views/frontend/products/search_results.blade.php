@extends('frontend.layouts.app')

@section('content')
    @if(Request::segment(1) == 'c')
        @php $img = $category_info->term_image; @endphp
    @else
        @php $img = 'https://vision.com.bd/public/frontend/img/slide3.jpg'; @endphp
        
    @endif


    <section class="cat-banner"
             style="background: url({{ $img }}) !important;background-size: cover !important; background-repeat: no-repeat !important; background-position: center center !important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cat-inner">
                        @php
                            $url_segment = request()->segment(1);
                        @endphp
                        <h1>{!! (!empty($category_info->title) && $url_segment == 'c' ? $category_info->title : 'Search Results') !!}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-section">
        <div class="container">
            
            
            <?php if($url_segment == 'search') { ?>
                <div class="row cat-max-hadmat">
                    <?php $tksign = 'BDT. '; ?>
                    
                    @foreach($products as $product)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="single-cat">
                                <a href="{{ url('p/' . $product->seo_url) }}">
                                    <div class="cat-top">
                                        <?php
                                        $images = explode(',', $product->images);
                                        $imgs = App\Image::find($images);
                                        ?>
                                        <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                             alt="{{ $imgs[0]['original_name'] }}">
                                    </div>
                                </a>
                                <div class="cat-mid">
                                    <div class="cat-mid-title">
                                        <h1>
                                            <a href="{{ url('p/' . $product->seo_url) }}">
                                                {{ $product->title }}
                                            </a>
                                        </h1>
                                    </div>
                                    <span class="detl-list">
    	    							<ul class="list-unstyled">
    	    								<li>{{ !empty($product->sub_title) ?  $product->sub_title : '&nbsp;' }}</li>
    	    								<li>{{ !empty($product->stock_status == 1) ? ' Stock Available ' : ' Out of stock ' }}</li>
    	    							</ul>
    	    						</span>
                                    <span class="price">
    	    							{{ $tksign . number_format($product->local_selling_price) }}
    	    						</span>
                                    <span class="detl-list">
                                        <ul class="list-unstyled">
                                            <li class="deft"><span class="star_pc">*</span> <small>The price is for online purchase only</small></li>
                                        </ul>
                                    </span>
    
    
                                </div>
                                <div class="cat-btm text-center">
                                    <a href="{{ url('p/' . $product->seo_url) }}" class="">
                                        <i class="fa fa-shopping-cart"></i>
                                        @if($product->is_othoba_product != null)
                                             BUY
                                        @else
                                            BUY ONLINE
                                        @endif
                                    </a>
    
                                    <a type="button"
                                       id="buynow"
                                       href="javascript:void(0)"
                                       class="compare buy-now"
                                       onclick="add_to_compare(
                                               '{{ $product->id }}',
                                               '{{ $product->product_code }}');">
                                        <i class="fa fa-balance-scale"></i> Compare
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            <?php } else { ?>
                <div class="row cat-max-hadmat">
    
                    <?php $tksign = 'BDT. '; ?>
                    
                    <div class="products_slide_r">
                        <div class="mc-header arrival-header">
                                <h2 class="text-center">Featured products</h2>
                            </div>
                        <div class="product_slide_owl owl-carousel owl-theme">
    
                            @foreach($products as $product)
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="single-cat">
                                        <a href="{{ url('p/' . $product->seo_url) }}">
                                            <div class="cat-top">
                                                <?php
                                                $images = explode(',', $product->images);
                                                $imgs = App\Image::find($images);
                                                ?>
                                                <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                                     alt="{{ $imgs[0]['original_name'] }}">
                                            </div>
                                        </a>
                                        <div class="cat-mid">
                                            <div class="cat-mid-title">
                                                <h1>
                                                    <a href="{{ url('p/' . $product->seo_url) }}">
                                                        {{ $product->title }}
                                                    </a>
                                                </h1>
                                            </div>
                                            <span class="detl-list"><ul class="list-unstyled"><li>{{ !empty($product->sub_title) ?  $product->sub_title : '&nbsp;' }}</li><li>{{ !empty($product->stock_status == 1) ? ' Stock Available ' : ' Out of stock ' }}</li></ul></span>
                                            <span class="price">{{ $tksign . number_format($product->local_selling_price) }}</span>
                                            <span class="detl-list"><ul class="list-unstyled"><li class="deft"><span
                                                                class="star_pc">*</span> <small>The price is for online purchase only</small></li></ul></span>
                                        </div>
                                        <div class="cat-btm text-center">
                                            <a href="{{ url('p/' . $product->seo_url) }}" class="">
                                                <i class="fa fa-shopping-cart"></i> 
                                                @if($product->is_othoba_product != null)
                                                         BUY
                                                    @else
                                                        BUY ONLINE
                                                    @endif
                                            </a>
                                            <a type="button"
                                               id="buynow"
                                               href="javascript:void(0)"
                                               class="compare buy-now"
                                               onclick="add_to_compare(
                                                       '{{ $product->id }}',
                                                       '{{ $product->product_code }}');">
                                                <i class="fa fa-balance-scale"></i> Compare
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
    
                    @if(!empty($category_info))
                    <?php $sub_cats = App\Term::where('parent', $category_info->id)->get(); $subcatscount = $sub_cats->count(); ?>
                    
                    
                    
                    <!--These two are same,later will convert to one -->
                    <div class="col-md-12">
                        <!-- We can convert these two into one, but for now its double -->
                        @if($subcatscount)
                        @foreach($sub_cats as $cat)
                   
                            <div class="mc-header arrival-header">
                                <h2 class="text-center">{{ $cat->name }}</h2>
                            </div>
    
                            <?php //dd($cat->id); ?>
    
                            <?php
                            $productss = App\Product::whereRaw('parent_id IS NULL')
                                ->whereRaw('FIND_IN_SET(' . $cat->id . ', categories)')
                                //->whereRaw($price_btw)
                                ->orderByRaw('created_at desc')
                                ->get();
                            ?>
                            <div class="row">
                                @foreach($productss as $product)
                                
                                
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="single-cat">
                                            <a href="{{ url('p/' . $product->seo_url) }}">
                                                <div class="cat-top">
                                                    <?php
                                                    $images = explode(',', $product->images);
                                                    $imgs = App\Image::find($images);
                                                    ?>
                                                    <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                                         alt="{{ $imgs[0]['original_name'] }}">
                                                </div>
                                            </a>
                                            <div class="cat-mid">
                                                <div class="cat-mid-title">
                                                    <h1>
                                                        <a href="{{ url('p/' . $product->seo_url) }}">
                                                           {{ text_limit( $product->title, 30) }}
                                                        </a>
                                                    </h1>
                                                </div>
                                                <span class="detl-list"><ul class="list-unstyled"><li>{{ !empty($product->sub_title) ?  $product->sub_title : '&nbsp;' }}</li><li>{{ !empty($product->stock_status == 1) ? ' Stock Available ' : ' Out of stock ' }}</li></ul></span>
                                                <span class="price">{{ $tksign . number_format($product->local_selling_price) }}</span>
                                                <span class="detl-list"><ul class="list-unstyled"><li class="deft"><span
                                                                    class="star_pc">*</span> <small>The price is for online purchase only</small></li></ul></span>
                                            </div>
                                            <div class="cat-btm text-center">
                                                <a href="{{ url('p/' . $product->seo_url) }}" class="">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    
                                                    @if($product->is_othoba_product != null)
                                                         BUY
                                                    @else
                                                        BUY ONLINE
                                                    @endif
                                                    
                                                    
                                                
                                                    
                                                </a>
                                                <a type="button"
                                                   id="buynow"
                                                   href="javascript:void(0)"
                                                   class="compare buy-now"
                                                   onclick="add_to_compare(
                                                           '{{ $product->id }}',
                                                           '{{ $product->product_code }}');"><i
                                                            class="fa fa-balance-scale"></i> Compare
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            
                        @endforeach
                    @else
                      <?php $cat = $category_info; ?>
                    
                     <div class="mc-header arrival-header">
                                <h2 class="text-center">{{ $cat->name }}</h2>
                            </div>
    
                            <?php //dd($cat->id); ?>
    
                            <?php
                            $productss = App\Product::whereRaw('parent_id IS NULL')
                                ->whereRaw('FIND_IN_SET(' . $cat->id . ', categories)')
                                //->whereRaw($price_btw)
                                ->orderByRaw('created_at desc')
                                ->get();
                            ?>
                            <div class="row">
                                @foreach($productss as $product)
                                
                                
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="single-cat">
                                            <a href="{{ url('p/' . $product->seo_url) }}">
                                                <div class="cat-top">
                                                    <?php
                                                    $images = explode(',', $product->images);
                                                    $imgs = App\Image::find($images);
                                                    ?>
                                                    <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                                         alt="{{ $imgs[0]['original_name'] }}">
                                                </div>
                                            </a>
                                            <div class="cat-mid">
                                                <div class="cat-mid-title">
                                                    <h1>
                                                        <a href="{{ url('p/' . $product->seo_url) }}">
                                                           {{ text_limit( $product->title, 30) }}
                                                        </a>
                                                    </h1>
                                                </div>
                                                <span class="detl-list"><ul class="list-unstyled"><li>{{ !empty($product->sub_title) ?  $product->sub_title : '&nbsp;' }}</li><li>{{ !empty($product->stock_status == 1) ? ' Stock Available ' : ' Out of stock ' }}</li></ul></span>
                                                <span class="price">{{ $tksign . number_format($product->local_selling_price) }}</span>
                                                <span class="detl-list"><ul class="list-unstyled"><li class="deft"><span
                                                                    class="star_pc">*</span> <small>The price is for online purchase only</small></li></ul></span>
                                            </div>
                                            <div class="cat-btm text-center">
                                                <a href="{{ url('p/' . $product->seo_url) }}" class="">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    
                                                    @if($product->is_othoba_product != null)
                                                         BUY
                                                    @else
                                                        BUY ONLINE
                                                    @endif
                                                    
                                                    
                                                
                                                    
                                                </a>
                                                <a type="button"
                                                   id="buynow"
                                                   href="javascript:void(0)"
                                                   class="compare buy-now"
                                                   onclick="add_to_compare(
                                                           '{{ $product->id }}',
                                                           '{{ $product->product_code }}');"><i
                                                            class="fa fa-balance-scale"></i> Compare
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            
                    
                    
                    </div>
                    @endif
                    
                    @endif
    
                    <div class="box-footer clearfix">
                        {{--{{ $products->links('component.paginator', ['object' => $products]) }}--}}
                    </div>
    
                </div>
            <?php } ?>
        </div>
    </section>

@endsection
@include('frontend.products.search_js')
