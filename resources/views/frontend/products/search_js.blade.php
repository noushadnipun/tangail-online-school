@section('cusjs')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $.noConflict();
            $(window).ready(function () {
                $('#range, #price_sort, #material').on('click', function () {
                    var search_key = $('input[name="search_key"]').val();

                    var minprice = $('input[name="range"]:checked').data('minprice');
                    var maxprice = $('input[name="range"]:checked').data('maxprice');

                    var field = $('input[name="price_sort"]:checked').data('field');
                    var type = $('input[name="price_sort"]:checked').data('type');

                    var material = $('input[name="material"]:checked').val();

                    var limit = 10;
                    var offset = 0;

                    var filters = {
                        'search_key': search_key,
                        'minprice': minprice,
                        'maxprice': maxprice,
                        'field': field,
                        'type': type,
                        'material': material,
                        'limit': limit,
                        'offset': offset
                    };

                    $.ajax({
                        //url: baseurl + '/search_product?search_key=' + data.search_key + '&minprice=' + data.minprice + '&maxprice=' + data.maxprice + '&field=' + data.field + '&type=' + data.type + '&material=' + data.material + '&limit=' + data.limit + '&offset=' + data.offset,
                        url: baseurl + '/search_product',
                        method: 'get',
                        data: filters,
                        success: function (data) {
                            window.location.replace(baseurl + '/search_product?search_key=' + filters.search_key + '&minprice=' + filters.minprice + '&maxprice=' + filters.maxprice + '&field=' + filters.field + '&type=' + filters.type + '&material=' + filters.material + '&limit=' + filters.limit + '&offset=' + filters.offset);
                        },
                        error: function () {
                            showError('Sorry. Try reload this page and try again.');
                            processing.hide();
                        }
                    });
                });
            });
        });
    </script>
@endsection
