@extends('frontend.layouts.app')

@section('content')

    <section class="breadcum-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                    $breadcrumbs->setDivider(' » &nbsp;');
                    $breadcrumbs->addCrumb('Home', url('/'))
                        ->addCrumb('Comparison', 'page');

                    echo $breadcrumbs->render();
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="contentbox">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('comparison'))
                        <?php
                        $limit = 3;
                        $oldcomparison = Session::get('comparison');
                        
                        if (!empty($oldcomparison->items)) {
                        ?>
                        <div class="compare-content">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="150">Product name</th>
                                    @php
                                        $i = 0
                                    @endphp
                                    @foreach($oldcomparison->items as $title)
                                        @if($i <= 2)
                                            <th>{{ product_title($title['item']['productid']) }}</th>
                                        @endif
                                        @php
                                            $i++
                                        @endphp
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th>Images</th>
                                    @php
                                        $i = 0;
                                    @endphp
                                    
                                    @foreach($oldcomparison->items as $title)
                                        @php
                                            $image_ids = get_product_info_by_key($title['item']['productid'], 'images');
                                            //dump($product_info);
                                            $images = explode(',', $image_ids);
                                            //dump($images);
                                            $imgs = App\Image::find($images);
                                        @endphp
                                        @if($i <= 2)
                                            <th>
                                                <img src="{{ url($imgs[0]['full_size_directory']) }}"
                                                     alt="{{ $imgs[0]['original_name'] }}"
                                                     width="180"/>
                                            </th>
                                        @endif
                                        @php
                                            $i++
                                        @endphp
                                    @endforeach
                                    
                                </tr>
                                <tr>
                                    <th>Price</th>
                                    @php
                                        $i = 0
                                    @endphp
                                    @foreach($oldcomparison->items as $title)
                                        @if($i <= 2)
                                            <th>BDT. {{ get_product_selling_price($title['item']['productid']) }}/=
                                            </th>
                                        @endif
                                        @php
                                            $i++
                                        @endphp
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Product code</th>
                                    @php
                                        $i = 0
                                    @endphp
                                    @foreach($oldcomparison->items as $title)
                                        @if($i <= 2)
                                            <td>{{ get_product_code($title['item']['productid']) }}</td>
                                        @endif
                                        @php
                                            $i++
                                        @endphp
                                    @endforeach
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    @php
                                        $i = 0
                                    @endphp
                                    @foreach($oldcomparison->items as $title)
                                        @if($i <= 2)
                                            <td>{!! get_product_info_by_key($title['item']['productid'], 'description') !!}</td>
                                        @endif
                                        @php
                                            $i++
                                        @endphp
                                    @endforeach
                                </tr>

                                <tr>
                                    <th>&nbsp;</th>
                                    @php
                                        $i = 0
                                    @endphp
                                    @foreach($oldcomparison->items as $title)
                                            @if($i <= 2)
                                                <td>
                                                    <div class="sl-pdt-det-btn cp-btn">
                                                        <!--<button class="btn btn-danger" type="button">Buy Online</button>-->
                                                        <a class="btn btn-info compare" type="button" onclick="remove_cart_item('<?php echo $title['item']['productid']; ?>', '<?php echo $title['item']['productcode']; ?>');" href="javascript:void(0)" >Remove</a>
                                                    </div>
                                                </td>
                                            @endif
                                        @php
                                            $i++
                                        @endphp
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection