@extends('frontend.layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="breadcrumb-warp section-margin-two">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <?php
                        $breadcrumbs = new Creitive\Breadcrumbs\Breadcrumbs;

                        $breadcrumbs->setDivider(' » &nbsp;');
                        $breadcrumbs->addCrumb('Home', url('/'))
                            ->addCrumb('Payment Method', 'product');
                        echo $breadcrumbs->render();
                        ?>
                    </div>
                    <!-- breadcrumb  end-->
                </div>
            </div>
        </div>
    </div>
    <?php $tksign = '&#2547; '; ?>
    <!--breadcrumb-area end  -->
    <!--prosuct-view-section  -->
    <section class="prosuct-view-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('cart'))
                        <section>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="about-text">
                                            <h2 class="carddlist-title">
                                                {{ $tksign }} Payment Method
                                            </h2>
                                            <p>Please select the preferred Payment method to use on this order.</p>
                                            <p>
                                                All transactions are secure and encrypted, and we neverstore.
                                                To learn more, please view our privacy policy.
                                            </p>
                                            <form action="">
                                                <div class="payment-method">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">Payment Method</div>
                                                        <div class="panel-body">
                                                            <!-- single-button -->
                                                            <div class="radio-item">
                                                                <input type="radio" id="ritem133" name="ritem11"
                                                                       value="ropt133">
                                                                <label for="ritem133">Debit or Credit Card </label>
                                                            </div>
                                                            <br>
                                                            <!-- single-button -->
                                                            <div class="radio-item">
                                                                <input type="radio" id="ritem122" name="ritem11"
                                                                       value="ropt122">
                                                                <label for="ritem122">Mobile Banking </label>
                                                            </div>
                                                            <br>
                                                            <!-- single-button -->
                                                            <div class="radio-item">
                                                                <input type="radio" id="ritem122" name="ritem11"
                                                                       value="ropt122">
                                                                <label for="ritem122">bKash</label>
                                                            </div>
                                                        </div>
                                                        <div class="panel-footer method-footer">
                                                            <label class="login-bar">
                                                                <input type="checkbox"
                                                                       name="remember" {{ old('remember') ? 'checked' : ''}}>
                                                                I have read and agree to the
                                                                <span>
                                                                    <a style="color: #0A70B9" href="#">Terms & Conditions</a>
                                                                </span>
                                                            </label>
                                                            <a class="btn pull-right btn-two method-btn">
                                                                Next <i class="fa fa-arrow-right"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                        </section>
                    @else
                        <h3>Opps... You have not added any product on your cart yet.</h3>
                    @endif
                </div>
            </div>
            <br>
            <br>
        </div>
    </section>
@endsection
@section('cusjs')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $.noConflict();


        });

        function product_qty(id) {
            //$
        }
    </script>
@endsection