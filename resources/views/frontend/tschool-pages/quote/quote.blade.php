@extends('frontend.layouts.app')

@section('content')

    <section class="mt-2">
        <div class="row">
            <div class="col-md-9 mb-3">
                <div class="row pb-4">
                    @foreach($getQuote as $data)
                     @php
                         $getQuoteImage = \App\Image::where('id', $data['images'])->get()->first();
                     @endphp
                    <div class="col-md-12">
                        <div class="card mb-2 p-1">
                            <div class="xrow">
                                <div class="xcol-3 ccol-md-3 float-left">
                                    <img class="img-card-top img-fluid quote-images mr-3 mt-2" src="{{url('/')}}/{{$getQuoteImage['full_size_directory']}}" alt="">
                                </div>
                                <div class="xcard-body m-2 xcol-9 xcol-md-9">
                                    <h5 class="card-title mb-2 d-inline"><a href="{{ route('page_single_quote', $data['id']) }}" class="text-dark">{{ $data['title'] }}</a></h5><br/>
                                    <?php echo implode(' ', array_slice(explode(' ', $data['description']), 0, 50)) ?> <a href="{{ route('page_single_quote', $data['id']) }}">..বিস্তারিত  </a>
                                </div>
                            </div>
                        </div><!-- Card -->
                    </div><!-- cOl 4 -->
                    @endforeach
                </div><!-- /Row  -->
                <nav aria-label="Page navigation" class="custom-paginition pb-4">
                    <div class="pagination mb-0 justify-content-center pagination-sm">
                        {{$getQuote->links()}}
                    </div>
                </nav>
            </div><!--/ col-9 -->
            <div class="col-md-3 mb-3">
                @include('frontend.tschool-pages.sidebar')
            </div><!-- col-3 -->
        </div><!--  /Row -->
    </section>

@endsection
