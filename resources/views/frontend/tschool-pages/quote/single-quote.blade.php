@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">

<div class="row">
	<div class="col-md-9 mb-3">
		<div class="bg-white p-3 overflow-hidden">
			<h3 class="text-dark pb-2">প্রধান শিক্ষকের বাণী</h3>
			<div class="pb-4">
				@php
                     $getQuoteImage = \App\Image::where('id', $getQuote['images'])->get()->first();
                 @endphp
				<img class="img-fluid float-left mr-3" src="{{url('/')}}/{{$getQuoteImage['full_size_directory']}}" alt="">
				<?php echo $getQuote['description'] ;?>
			</div>
		</div>
		
	</div>
	<div class="col-md-3 mb-3">
		@include('frontend.tschool-pages.sidebar')
	</div>
</div>


</section>
@endsection