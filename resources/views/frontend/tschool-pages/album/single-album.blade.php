@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">
    <div class="pb-0">
        <h3 class="text-dark pb-2">{{ $albumName[0]->name }}</h3>
    </div>
    <div class="border-top-0 mb-3">
        <div class="card-body p-0">
            <div class="album-photo-gallery">
                <ul id="lightgallery" class="list-unstyled mb-0 row">
                    @foreach($albumPhoto as $photos)
                    @php
                        $img = \App\Image::where('id', $photos->media_id)->get()->first();
                        //dump($img);
                    @endphp
                    <li class="col-md-3 col-12" data-responsive="{{url('/')}}/{{$img->full_size_directory}}" data-src="{{url('/')}}/{{$img->full_size_directory}}" data-sub-html="<h4>{{$photos->caption}}</h4>">
                        <a href="">
                            <img class="img-fluid" src="{{url('/')}}/{{$img->full_size_directory}}">
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div><!-- End gallery-->
        </div>
    </div>
</section>

@endsection


@section('cusjs')

<link href="https://cdn.jsdelivr.net/lightgallery/1.3.9/css/lightgallery.min.css" rel="stylesheet">

<script>
    $(document).ready(function(){
        $('#lightgallery').lightGallery();
    });
</script>
<script src="{{asset('public/assets/js/gallery-lightbox.js')}}"></script>

@endsection