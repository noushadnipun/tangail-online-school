@extends('frontend.layouts.app')

@section('content')

<div class="container">

    <nav aria-label="breadcrumb" class="my-4">
      <ol class="row breadcrumb bg-white">
        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Gallery</li>
      </ol>
      <h2>Photo Albums</h2>
    </nav>
   
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                @foreach($albums as $album)
                <div class="col-lg-4 col-md-6 col-sm-12 col-12 py-3">
                    <a class="text-white" href="{{ route('album_photos' , $album->id) }}">
                        <div class="card h-100 text-white bg-danger overflow-hidden">
                            <div class="overlay-div"></div>
                            @php
                                $galleryFirstPhoto = \App\Gallery::where('category_id', $album->id)->orderBy('id', 'DESC')->get()->first();
                                $getImageById = \App\Image::where('id', $galleryFirstPhoto['media_id'])->get()->first();
                            @endphp
                            <div class="card-body py-5 album-card" style="background-image: url('{{url('/')}}/{{$getImageById['full_size_directory']}}'); background-size: cover;">
                               <h4 class="card-title position-relative text-center">{{ $album->name }}</h4>
                            </div>
                            <div class="card-footer text-center bg-danger position-relative font-weight-600">View all</div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
