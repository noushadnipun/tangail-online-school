@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">
	<div class="pb-4">
		<h3 class="text-dark pb-2">টাংগাইল জেলার পাবলিক পরীক্ষার ফলাফল</h3>
		<form action="{{route('page_result')}}" method="POST">
		{!! csrf_field() !!}
		  	<div class="form-row">
			    <div class="col-md-3 col-6">
		    		<label for="office">উপজেলা</label>
					@php
					  	$getUpozila = \App\Result::select('upozila')->distinct()->get();
					@endphp
				  	<select class="form-control" id="upozila" name="upozila">
					  	@foreach($getUpozila as $data)
							<option value="{{ $data->upozila }}">{{ $data->upozila }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-3 col-6">
			      <label for="name">পরীক্ষার নাম</label>
				  	@php
					  	$getExamName = \App\Result::select('exam_name')->distinct()->get();
					@endphp
				  	<select class="form-control" id="exam_name" name="exam_name">
					  	@foreach($getExamName as $data)
							<option value="{{ $data->exam_name }}">{{ $data->exam_name }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-3 col-6">
			      <label for="year">সাল</label>
				  	@php
					  	$getYear = \App\Result::select('year')->distinct()->get();
					@endphp
				  	<select class="form-control" id="year" name="year">
					  	@foreach($getYear as $data)
							<option value="{{ $data->year }}">{{ $data->year }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-1 text-center">
			      	<button class="btn btn-success data-table-submit">সার্চ</button>
			    </div>
		  	</div>
		</form>
	</div>

	<div class="card border-top-0 mb-3">
		<div class="card-body p-0 table-responsive">
			<table class="table table-bordered mb-0">
			  	<thead>
				    <tr>
				      <th scope="col">উপজেলা</th>
				      <th scope="col">পরীক্ষার নাম</th>
				      <th scope="col">সাল</th>
				      <th scope="col">মোট পরীক্ষার্থী</th>
				      <th scope="col">মোট পাস</th>
				      <th scope="col">পাসের হার</th>
				      <th scope="col">A+</th>
				      <th scope="col">A</th>
				      <th scope="col">A-</th>
				      <th scope="col">B</th>
				      <th scope="col">C</th>
				      <th scope="col">D</th>
				      <th scope="col">F</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@foreach($getResult as $data)
						<tr>
							<td>{{ $data->upozila }}</td>
							<td>{{ $data->exam_name }}</td>
							<td>{{ $data->year }}</td>
							<td>{{ $data->total_candidates }}</td>
							<td>{{ $data->total_pass }}</td>
							<td>{{ $data->percentage_pass }}</td>
							<td>{{ $data->a_plus }}</td>
							<td>{{ $data->a }}</td>
							<td>{{ $data->a_min }}</td>
							<td>{{ $data->b }}</td>
							<td>{{ $data->c }}</td>
							<td>{{ $data->d }}</td>
							<td>{{ $data->f }}</td>
						</tr>
				    @endforeach
			  	</tbody>
			</table>
		</div>
		<div class="card-footer border-top-0 bg-white">
			<nav aria-label="Page navigation" class="custom-paginition pb-4">
				<div class="pagination mb-0 justify-content-center pagination-sm">
					{{$getResult->links()}}
				</div>
			</nav>
		</div>
	</div>
</section>


@endsection