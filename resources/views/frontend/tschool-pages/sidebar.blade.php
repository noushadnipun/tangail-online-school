@include('frontend.homepage.notice-board')


<div class="card mb-2 p-3 border-1 d-none">
        <div class="live-class-content-header">
            <h4> বাণী </h4>
        </div>
        <div id="carouselMessage" class="carousel slide" data-ride="carousel" data-touch="true">

            <div class="carousel-inner">
                @php
                    $QuoteInsidebar = \App\Post::whereRaw("FIND_IN_SET('63' , categories)")->limit(3)->get();
                    //dump($QuoteInsidebar)    
                @endphp
                @foreach($QuoteInsidebar as $key=>$data)
                    @php
                     $getQuoteImage = \App\Image::where('id', $data['images'])->get()->first();
                     @endphp
                    <div class="carousel-item  {{ ($key == 1) ? 'active' : ' ' }}">
                        <div class="box-title mt-2">
                            <img src="{{url('/')}}/{{$getQuoteImage['full_size_directory']}}" alt="" class="img-fluid quote-images">
                        </div>
                        <div class="box-content mt-2 box-content-mb">
                            <h5> <a class="text-dark" href="{{ route('page_single_quote', $data['id']) }}">{{ $data['title'] }} </a></h5>
                            <p class="content">
                                <?php echo implode(' ', array_slice(explode(' ', $data['description']), 0, 25)) ?> <a href="{{ route('page_single_quote', $data['id']) }}">..বিস্তারিত  </a>
                            </p>
                        </div>
                    </div>   <!-- /item -->    
                @endforeach                                                            
            </div><!-- inner -->
            <ol class="carousel-indicators" style="bottom: -24px;">
                @foreach($QuoteInsidebar as $key=>$data)
                    <li data-target="#carouselMessage" data-slide-to="{{ $key++ }}" class="{{ ($key == 1) ? 'active' : '' }} bg-dark"></li>
                @endforeach
            </ol>
        </div>

</div>


@php 
    $getParentMenuName = DB::table('menus')->where('id' , '2')->first();
@endphp
@if(!empty($getParentMenuName))
     @php 
        $setSliderRightMenu =   get_parent_menus($getParentMenuName->id); 
     @endphp
    <div class="card slider-right-box mb-2">
        <div class="card-body box">
            <h4>{{ $getParentMenuName->name }}</h4>
            <div class="row">
                <div class="col-md-5 col-4">
                    <img class="img-fluid" src="{{url('/')}}/public/assets/images/edu-cap.png">
                </div>
                <div class="col-md-7 col-8">
                    <ul class="box-caption">
                        @foreach($setSliderRightMenu as $link)
                        <li><a href="{{ url($link->link) }}">{{ $link->label }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- End Slider Right Side Box -->
@endif


<div class="site-widget-image">
    {{-- <img class="img-fluid" src="{{url('/')}}/public/assets/images/National-Helpline.jpg" alt="" style="height: 428px; width: 100%;"> --}}
    <?php //echo dynamic_widget($widgets, ['id' => 1, 'heading' => NULL]); ?>
</div>

@include('frontend.homepage.live-class-statistics')