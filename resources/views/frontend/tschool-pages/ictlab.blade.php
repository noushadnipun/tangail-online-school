@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">
	<div class="pb-4">
		<h3 class="text-dark pb-2">টাংগাইল জেলার আইসিটি ল্যাব</h3>
		<form action="{{route('page_ict_lab')}}" method="POST">
		{!! csrf_field() !!}
		  	<div class="form-row">
			    <div class="col-md-3 col-6">
		    		<label for="office">উপজেলা</label>
					@php
					  	$getUpozila = \App\Ictlab::select('upozila')->distinct()->get();
					@endphp
				  	<select class="form-control" id="upozila" name="upozila">
					  	@foreach($getUpozila as $data)
							<option value="{{ $data->upozila }}">{{ $data->upozila }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-3 col-6">
			      <label for="thana">ল্যাবের ধরণ</label>
				  	@php
					  	$getLabType = \App\Ictlab::select('lab_type')->distinct()->get();
					@endphp
				  	<select class="form-control" id="lab_type" name="lab_type">
					  	@foreach($getLabType as $data)
							<option value="{{ $data->lab_type }}">{{ $data->lab_type }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-1 text-center">
			      	<button class="btn btn-success data-table-submit">সার্চ</button>
			    </div>
		  	</div>
		</form>
	</div>

	<div class="card border-top-0 mb-3">
		<div class="card-body p-0 table-responsive">
			<table class="table table-bordered mb-0">
			  	<thead>
				    <tr>
					  <th scope="col">প্রতিষ্ঠানের নাম</th>
				      <th scope="col">দায়িত্বপ্রাপ্ত শিক্ষক</th>
				      <th scope="col">মোবাইল</th>
				      <th scope="col">ই-মেইল</th>
				      <th scope="col">ল্যাপটপ</th>
				      <th scope="col">ডেস্কটপ</th>
				      <th scope="col">প্রজেক্টর</th>
				      <th scope="col">স্মার্ট টেলিভিশন</th>
				      <th scope="col">ইন্টারনেট</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@foreach($getIctlab as $data)
				    <tr>
				      <td>{{ $data['institute_name'] }}</td>
				      <td>
					  <?php 
						if(!empty($data->incharge_teacher)){
								$getTeacherName = \App\User::where('id', $data->incharge_teacher)->get();
								echo $getTeacherName[0]->name;
						}
					  ?>
					  </td>
				      <td>{{ $data['mobile'] }}</td>
				      <td>{{ $data['email'] }}</td>
				      <td>{{ $data['laptop'] }}</td>
				      <td>{{ $data['desktop'] }}</td>
				      <td>{{ $data['projector'] }}</td>
				      <td>{{ $data['smart_televison'] }}</td>
				      <td>{{ $data['internet'] }}</td>
				    </tr>
					@endforeach
			  	</tbody>
			</table>
		</div>
		<div class="card-footer border-top-0 bg-white">
			<nav aria-label="Page navigation" class="custom-paginition pb-4">
				<div class="pagination mb-0 justify-content-center pagination-sm">
					{{$getIctlab->links()}}
				</div>
			</nav>
		</div>
	</div>
</section>


@endsection