@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">
	<div class="pb-4">
		<h3 class="text-dark pb-2">টাংগাইল জেলার শিক্ষা প্রতিষ্ঠানসমূহ</h3>
		<form action="{{route('page_institutions')}}" method="POST">
		{!! csrf_field() !!}
		  	<div class="form-row">

			  <div class="col-md-3 col-6">
			      <label for="thana">স্ট্যাটাস</label>
				  	@php
					  	$getStatus = \App\Institutions::select('status')->distinct()->get();
					@endphp
				  	<select class="form-control" id="status" name="status">
					  	@foreach($getStatus as $data)
							<option value="{{ $data->status }}">{{ $data->status }}</option>
						@endforeach
				  	</select>
			    </div>

				<div class="col-md-3 col-6">
			      <label for="thana">ধরণ</label>
				  	@php
					  	$getType = \App\Institutions::select('type')->distinct()->get();
					@endphp
				  	<select class="form-control" id="type" name="type">
					  	@foreach($getType as $data)
							<option value="{{ $data->type }}">{{ $data->type }}</option>
						@endforeach
				  	</select>
			    </div>

				<div class="col-md-3 col-6">
			      <label for="thana">স্তর</label>
				  	@php
					  	$getLevel = \App\Institutions::select('level')->distinct()->get();
					@endphp
				  	<select class="form-control" id="level" name="level">
					  	@foreach($getLevel as $data)
							<option value="{{ $data->level }}">{{ $data->level }}</option>
						@endforeach
				  	</select>
			    </div>

			    <div class="col-md-2 col-6">
		    		<label for="office">উপজেলা</label>
					@php
					  	$getUpozila = \App\Institutions::select('upozila')->distinct()->get();
					@endphp
				  	<select class="form-control" id="upozila" name="upozila">
					  	@foreach($getUpozila as $data)
							<option value="{{ $data->upozila }}">{{ $data->upozila }}</option>
						@endforeach
				  	</select>
			    </div>

			    <div class="col-md-1 text-center">
			      	<button class="btn btn-success data-table-submit">সার্চ</button>
			    </div>
		  	</div>
		</form>
	</div>

	<div class="card border-top-0 mb-3">
		<div class="card-body p-0 table-responsive">
			<table class="table table-bordered mb-0">
			  	<thead>
				    <tr>
					  <th scope="col">স্ট্যাটাস</th>
				      <th scope="col">ধরণ</th>
				      <th scope="col">স্তর</th>
				      <th scope="col">উপজেলা</th>
				      <th scope="col">প্রতিষ্ঠানের নাম</th>
				      <th scope="col">প্রতিষ্ঠান প্রধান</th>
				      <th scope="col">পদবী</th>
				      <th scope="col">প্রতিষ্ঠার তারিখ</th>
				      <th scope="col">মোবাইল</th>
				      <th scope="col">ই-মেইল</th>
				      <th scope="col">মোট শিক্ষক</th>
				      <th scope="col">মোট শিক্ষার্থী</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@foreach($getInstitutions as $data)
				    <tr>
				      <td>{{ $data['status'] }}</td>
				      <td>{{ $data['type'] }}</td>
				      <td>{{ $data['level'] }}</td>
				      <td>{{ $data['upozila'] }}</td>
				      <td>{{ $data['institutions_name'] }}</td>
				      <td>{{ $data['institutions_head'] }}</td>
				      <td>{{ $data['position'] }}</td>
				      <td>{{ $data['establishment_date'] }}</td>
				      <td>{{ $data['mobile'] }}</td>
				      <td>{{ $data['email'] }}</td>
				      <td>{{ $data['total_teacher'] }}</td>
				      <td>{{ $data['total_student'] }}</td>
				    </tr>
					@endforeach
			  	</tbody>
			</table>
		</div>
		<div class="card-footer border-top-0 bg-white">
			<nav aria-label="Page navigation" class="custom-paginition pb-4">
				<div class="pagination mb-0 justify-content-center pagination-sm">
					{{$getInstitutions->links()}}
				</div>
			</nav>
		</div>
	</div>
</section>


@endsection