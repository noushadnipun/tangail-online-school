@extends('frontend.layouts.app')

@section('content')
<section class="mt-2">
	<div class="row">
		<div class="col-md-2">
			@include('frontend.tschool-pages.news.news-left-sidebar')
		</div><!-- End Col 2 -->
		<div class="col-md-7">
			<h2>{{ $getNews->title }}</h2>
			<?php echo $getNews['description'];?>
		</div><!-- End Col 7 -->
		<div class="col-md-3">
			@include('frontend.tschool-pages.news.news-right-sidebar')
		</div><!-- End Col 3 -->
	</div><!-- End Row -->
</section>
@endsection