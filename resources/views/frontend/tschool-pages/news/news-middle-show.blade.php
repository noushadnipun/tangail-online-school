@php 
	$getAllNews = \App\Post::whereRaw("FIND_IN_SET('65',categories)")->orderBy('id', 'DESC')->paginate(10);
@endphp
@foreach($getAllNews as $key => $news)
	@php
		$getNewsImage = \App\Image::where('id', $news['images'])->get()->first();
	@endphp
	@if($key++ == 0)
		<div class="news-feed-block mb-3">
			<div class="news-banner-inner">
				<img class="img-fluid" src="{{url('/')}}/{{$getNewsImage['full_size_directory']}}">
				<div class="news-banner-caption">
					<h2><a href="{{ route('page_single_news', $news->id) }}">{{ $news->title }}</a></h2>
				</div>
			</div>
		</div><!-- news feed block	 -->
	@else
		<div class="news-feed-horizontal mb-3">
			<div class="d-flex justify-content-between flex-wrap p-1">
				<div class="article-image pr-2">
					<a href="{{ route('page_single_news', $news->id) }}">
						<img src="{{url('/')}}/{{$getNewsImage['icon_size_directory']}}" class="img-fluid">
					</a>
				</div>
				<div class="article-description">
					<a href="{{ route('page_single_news', $news->id) }}">
						<h4>{{ $news->title }}</h4>
					</a>
					<div class="additional-info">
						<div class="news-domain">
							<a href="">
								<span>
									@php 
										$offsetParent = explode(",", $news->categories);
										//$gettermName = \App\Term::whereRaw("FIND_IN_SET('71', id)")->first();
										$getTermName = \App\Term::whereIn('id', array_slice($offsetParent, 1))->first();
										//dump($gettermName);
									@endphp
									{{ $getTermName->name }}
								</span>
							</a>
						</div>
					<i class="fa fa-clock-o"></i> <span class="pub-date">{{ $news->updated_at->diffForHumans() }}</span>
					</div>
				</div>
			</div>
		</div><!-- end news list blcok -->
	@endif
@endforeach
<nav aria-label="Page navigation" class="custom-paginition pb-4">
	<div class="pagination mb-0 justify-content-center pagination-sm">
		{{$getAllNews->links()}}
	</div>
</nav>