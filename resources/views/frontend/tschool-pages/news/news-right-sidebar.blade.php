@php 
	$getTrendNews = \App\Post::whereRaw("FIND_IN_SET('65',categories)")->limit(6)->orderBy('id', 'DESC')->get();
@endphp
<div class="trending-news-horizonrtal">
	
	<div class="bg-danger px-3 py-1 text-white">সর্বশেষ সংবাদ</div>
	@foreach($getTrendNews as $news)
	<div class="news-feed-horizontal my-2">
		<div class="d-flex justify-content-between flex-wrap p-1">
			<div class="article-image pr-2">
				<a href="{{ route('page_single_news', $news->id) }}">
					@php
						$getNewsImage = \App\Image::where('id', $news['images'])->get()->first();
					@endphp
					<img src="{{url('/')}}/{{$getNewsImage['icon_size_directory']}}" class="img-fluid">
				</a>
			</div>
			<div class="article-description">
				<a href="{{ route('page_single_news', $news->id) }}">
					<h6>{{ $news['title'] }}</h6>
				</a>
				<div class="additional-info">
					<div class="news-domain">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach
</div> <!-- End trending-news-horizonrtal -->
{{-- <div class="site-widget-image">
	<img src="{{url('/')}}/public/assets/images/dengu.jpg" alt="" class="img-fluid">
</div><!-- End site image -->
<div class="site-widget-image">
	<img src="{{url('/')}}/public/assets/images/corona-new.jpg" alt="" class="img-fluid">
</div><!-- End site image --> --}}