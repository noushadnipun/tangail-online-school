<div class="news-side-menu">
	@php
		$getTermNewsSubCat = \App\Term::where('parent', '65')->whereNotIn('id', ['72'])->get();
	@endphp
	<ul class="row px-3">
		@foreach($getTermNewsSubCat as $data)
			<li class="col-md-12 col-4"><a href="{{ route('page_archive_news', $data->id) }}"><i class="fa fa-newspaper-o"></i> {{ $data->name }}</a></li>
		@endforeach
	</ul>
</div>