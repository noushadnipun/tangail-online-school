@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">

<div class="row">
	<div class="col-md-9 mb-3">
		<div class="bg-white p-3">
			<h3 class="text-dark pb-2">{{ $getVideo->video_title }}</h3>

			<div class="pb-4">
				<div class="embed-responsive embed-responsive-16by9">
  					<iframe class="embed-responsive-item" src="https://www.facebook.com/plugins/video.php?href={{ $getVideo->video_link }}%2F&show_text=false&width=734&height=413&appId"  scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-3 mb-3">
		@include('frontend.tschool-pages.sidebar')
	</div>
</div>


</section>
@endsection