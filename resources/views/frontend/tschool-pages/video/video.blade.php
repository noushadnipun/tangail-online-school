@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">
	<div class="pb-4">
		<h3 class="text-dark pb-2">
			{{ (!empty($getVideo[0]->name)) ? $getVideo[0]->name : 'শ্রেণি ভিত্তিক ক্লাশ সমূহ' }}
		</h3>
		{{-- {{ Form::open(array('url' => route('page_video_search'), 'method' => 'post', 'value' => 'PATCH', 'id' => '')) }} --}}
		<form method="get" action="{{ route('page_video_search') }}">
		 	{!! csrf_field() !!}
		  	<div class="form-row">
			    <div class="col-md-5 col-6">
				  	<input type="text" class="form-control" name="video_title" placeholder="সার্চ করো">
			    </div>
			    <div class="col-md-2 col-6">
				  	@php
                        $classId = \App\Classes::get();
                    @endphp
                    <select name="class_id" class="form-control" id="class_id">
                        @foreach($classId as $data)
                            <option value="{{$data->id}}">
                                {{$data->name}}
                            </option>
                        @endforeach
                    </select>
			    </div>
			    <div class="col-md-2 col-6">
			  	 		@php
                            $subjectId = \App\Subjects::get();
                        @endphp
                        <select name="subject_id" class="form-control" id="subject_id">
                            @foreach($subjectId as $data)
                                <option value="{{$data->id}}">
                                    {{$data->name}}
                                </option>
                            @endforeach
                        </select>
			    </div>
			    <div class="col-md-2 col-6">
				  	<select class="form-control" name="subject_lession">
				    			<option value="1"> ১ম পাঠ </option>
                                <option value="2"> ২য় পাঠ </option>
                                <option value="3"> ৩য় পাঠ </option>
                                <option value="4"> ৪র্থ পাঠ </option>
                                <option value="5"> ৫ম পাঠ </option>
                                <option value="6"> ৬ষ্ঠ পাঠ </option>
                                <option value="7"> ৭ম পাঠ </option>
                                <option value="8"> ৮ম পাঠ </option>
                                <option value="9"> ৯ম পাঠ </option>
                                <option value="10"> ১০ম পাঠ </option>
                                <option value="11"> ১১তম পাঠ </option>
                                <option value="12"> ১২তম পাঠ </option>
                                <option value="13"> ১৩তম পাঠ </option>
                                <option value="14"> ১৪তম পাঠ </option>
                                <option value="15"> ১৫তম পাঠ </option>
                                <option value="16"> ১৬তম পাঠ </option>
                                <option value="17"> ১৭তম পাঠ </option>
                                <option value="18"> ১৮তম পাঠ </option>
                                <option value="19"> ১৯তম পাঠ </option>
                                <option value="20"> ২০তম পাঠ </option>
				  	</select>
			    </div>
			    <div class="col-md-1 text-center">
			      	<button class="btn btn-success">সার্চ</button>
			    </div>
		  	</div>
		</form>
		{{-- {{ Form::close() }} --}}
	</div>
		<div class="row pb-4">
			@if(!empty($getVideo[0]->video_link))
				@foreach($getVideo as $data)
				<div class="col-md-3">
					<div class="card mb-2 p-1">
						<div class="embed-responsive embed-responsive-16by9">
	  						<iframe class="embed-responsive-item" src="https://www.facebook.com/plugins/video.php?href={{ $data->video_link }}%2F&show_text=false&width=734&height=413&appId"  scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>
						</div>
					    <div class="card-body p-2">
					      <h5 class="card-title mb-0"><a href="{{route('page_single_video', $data->id )}}" class="text-dark">{{ $data->video_title }}</a></h5>
					    </div>
				  </div>
				</div>
				@endforeach
			@else 
				<div class="col-md-6 mx-auto text-center alert alert-warning">No Videos uploaded yet.</div>
			@endif
		</div>
		<nav aria-label="Page navigation" class="custom-paginition pb-4">
			<div class="pagination mb-0 justify-content-center pagination-sm">
				{{$getVideo->links()}}
			</div>
		</nav>
</section>

@endsection