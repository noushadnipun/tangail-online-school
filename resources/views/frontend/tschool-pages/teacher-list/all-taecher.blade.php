@extends('frontend.layouts.app')

@section('content')

<section class="mt-2">
	<div class="pb-4">
		<h3 class="text-dark pb-2">শিক্ষক মন্ডলী</h3>
		<form action="{{route('page_teacher_list_headmaster')}}" method="POST">
		{!! csrf_field() !!}
		  	<div class="form-row">
			    <div class="col-md-3 col-6">
		    		<label for="office">অফিস</label>
					@php
					  	$getOffice = \App\User::select('company')->whereRaw("FIND_IN_SET('teacher' , teacher_type)")->get();
					@endphp
				  	<select class="form-control" id="office" name="office">
					  	@foreach($getOffice as $data)
							<option value="{{ $data->company }}">{{ $data->company }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-3 col-6">
			      <label for="thana">থানা</label>
				  	@php
					  	$getThana = \App\User::select('thana')->whereRaw("FIND_IN_SET('teacher' , teacher_type)")->get();
					@endphp
				  	<select class="form-control" id="thana" name="thana">
					  	@foreach($getThana as $data)
							<option value="{{ $data->thana }}">{{ $data->thana }}</option>
						@endforeach
				  	</select>
			    </div>
				<div class="col-md-3 col-6">
			      <label for="thana">স্কুল / কলেজ</label>
				  	@php
					  	$getSchool = \App\User::select('school')->whereRaw("FIND_IN_SET('teacher' , teacher_type)")->get();
					@endphp
				  	<select class="form-control" id="school" name="school">
					  	@foreach($getSchool as $data)
							<option value="{{ $data->school }}">{{ $data->school }}</option>
						@endforeach
				  	</select>
			    </div>
			    <div class="col-md-2 col-6">
			      <label for="name">নাম</label>
				  	<input class="form-control" id="name" name="name" />
			    </div>
			    <div class="col-md-1 text-center">
			      	<button class="btn btn-success data-table-submit">সার্চ</button>
			    </div>
		  	</div>
		</form>
	</div>

	<div class="card border-top-0 mb-3">
		<div class="card-body p-0 table-responsive">
			<table class="table table-bordered mb-0">
			  	<thead>
				    <tr>
			    	  <th scope="col">ছবি</th>
		    		  <th scope="col">নাম</th>
				      <th scope="col">স্কুল / কলেজ</th>
				      <th scope="col">পদবী</th>
				      <th scope="col">বিষয়</th>
				      <th scope="col">থানা</th>
				      <th scope="col">নিজ জেলা</th> 
				      <th scope="col">জন্ম তারিখ</th>
				      <th scope="col">অত্রাফিসে যোগদান</th>
				      <th scope="col">মোবাইল</th>
				      <th scope="col">ই-মেইল</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@foreach($AllTeacherList as $data)
				    <tr>
				      <td></td>
				      <td>{{ $data['name'] }}</td>
				      <td>{{ $data['school'] }}</td>
				      <td>{{ $data['employee_title'] }}</td>
				      <td>
						<?php 
							if(!empty($data->subject_id)) {
								$subjectID = explode(',', $data->subject_id);
								foreach($subjectID as $subData){
									$getSubject = \App\Subjects::select('name')->where('id', $subData)->get();
									//var_dump($getSubject);
									echo $getSubject[0]->name. ' ';
								}
							}
						?>
					  </td>
				      <td>{{ $data['thana'] }}</td>
				      <td>{{ $data['district'] }}</td>
				      <td>{{ $data['birthday'] }}</td>
				      <td>{{ $data['join_date'] }}</td>
				      <td>{{ $data['phone'] }}</td>
				      <td>{{ $data['email'] }}</td>
				    </tr>
					@endforeach
			  	</tbody>
			</table>
		</div>
		<div class="card-footer border-top-0 bg-white">
			<nav aria-label="Page navigation" class="custom-paginition pb-4">
				<div class="pagination mb-0 justify-content-center pagination-sm">
					{{$AllTeacherList->links()}}
				</div>
			</nav>
		</div>
	</div>
</section>


@endsection