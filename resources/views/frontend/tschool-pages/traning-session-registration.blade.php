@extends('frontend.layouts.app')

@section('content')

	<section class="mt-2">
		@if(Session::has('success'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
		@if($errors->any())
            <div class="col-md-12">
                <div class="alert alert-danger ">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
		<div class="alert alert-info mx-2">
			<h3>শর্তাবলী:</h3>
			১. লাল তারকা চিহ্নিত অপশন পূরণ করা বাধ্যতামূলক। <br>
			২. আবেদনটি সাবমিট করার পর সংশোধন করার সুযোগ নেই। <br>
			৩. একটি ইমেইল আইডি দিয়ে শুধুমাত্র একটি আবেদন করা যাবে। <br>
			৪. সকল তথ্য পূরণ শেষে অবশ্যই সাবমিট বাটন ক্লিক করতে হবে।
			৫. একটি আবেদন সাবমিট করার পর একই লিংক পুনরায় ব্যবহার করে অন্যান্য শিক্ষকের আবেদন প্রেরণ করতে পারবেন। <br>
			৬. প্রতিটি প্রতিষ্ঠানের কম্পিউটারে দক্ষ একজন শিক্ষক রেজিষ্ট্রেশন ফরম পূরণ পূর্বক প্রশিক্ষণে অংশগ্রহণ করবেন। <br>
			৭. প্রশিক্ষণের তারিখ ও সময় এবং অংশগ্রহণের ইউজার আইডি ও পাসওয়ার্ড যথাসময়ে রেজিষ্ট্রেশন ফর্মে উল্লেখিত ইমেইলে জানানো হবে। <br>
		</div>
		<form action="{{ route('page_traning_session_registration_store') }}" method="POST">
			{{ csrf_field() }}
			<div class="mt-2 row mx-1">
					
					<div class="col-lg-6">

						<div class="form-group">
						    <label for="zilla">Zilla : 
						    	<span class="text-danger">*</span>
					    		<small class="text-muted d-block">আপনার জেলার নাম নির্বাচন করুন</small>
						    </label>
						    <select class="form-control form-control-sm" id="zilla" name="district" required>
						      <option disabled selected>নির্বাচন করুন</option>
						      <option value="Tangail">Tangail</option>
						    </select>
					  	</div>

					  	<div class="form-group">
						    <label for="Upozilla">Upazilla :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার উপজেলার নাম  নির্বাচন করুন</small>
						    </label>
						    <select class="form-control form-control-sm" id="Upozilla" name="upozilla" required>
						      <option disabled selected>নির্বাচন করুন</option>
						      <?php 
						      	$selectUpozila = ['BASAIL', 'BHUAPUR', 'DELDUAR', 'DHANBARI', 'GHATAIL', 'GOPALPUR', 'KALIHATI', 'MADHUPUR', 'MIRZAPUR', 'NAGARPUR', 'SAKHIPUR', 'TANGAIL SADAR']
						       ?>
						       	@foreach($selectUpozila as $data)
						      		<option value="{{ ucwords(strtolower($data)) }}">{{ $data }}</option>
				      			@endforeach
						    </select>
					  	</div>

					  	<div class="form-group">
						    <label for="Institute">Institute Category :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার প্রতিষ্ঠানের ধরন  নির্বাচন করুন</small>
						    </label>
						    <select class="form-control form-control-sm" id="Institute" name="institute_category" required>
						      <option disabled selected>নির্বাচন করুন</option>
						      <?php 
						      	$selectInstitute = ['School', 'College', 'School and College', 'Madrasah', 'Primary School', 'Others']
						       ?>
						       	@foreach($selectInstitute as $data)
						      		<option value="{{ ucwords(strtolower($data)) }}">{{ $data }}</option>
				      			@endforeach
						    </select>
					  	</div>

					  	<div class="form-group">
						    <label for="Institute">Institute Name :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার শিক্ষা প্রতিষ্ঠানের নাম ইংরেজিতে লিখুন</small>
						    </label>
						    <input class="form-control form-control-sm" type="text" placeholder="আপনার উত্তর" name="institute_name" value="{{ old('institute_name') }}" required>
					  	</div>

					  	<div class="form-group">
						    <label for="Teacher">Teacher Name :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার নাম ইংরেজিতে লিখুন</small>
						    </label>
						    <input class="form-control form-control-sm" type="text" placeholder="আপনার উত্তর" name="teacher_name" value="{{ old('teacher_name') }}" required>
					  	</div>

					</div>


					<div class="col-lg-6">

						<div class="form-group">
						    <label for="Designation">Designation Name :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার পদবী ইংরেজিতে লিখুন</small>
						    </label>
						    <input class="form-control form-control-sm" type="text" placeholder="আপনার উত্তর" name="designation_name" value="{{ old('designation_name') }}" required>
					  	</div>

					  	<div class="form-group">
						    <label for="Gender">Gender :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার লিঙ্গ নির্বাচন করুন</small>
						    </label>
						    <select class="form-control form-control-sm" id="Institute" name="gender" required>
						      <option disabled selected>নির্বাচন করুন</option>
						      <?php 
						      	$selectGender = ['Male', 'Female']
						       ?>
						       	@foreach($selectGender as $data)
						      		<option value="{{ $data }}">{{ $data }}</option>
				      			@endforeach
						    </select>
					  	</div>

					  	<div class="form-group">
						    <label for="Index Number">Index Number :
						    	<span class="text-muted">(Optional)</span>
						    	<small class="text-muted d-block">আপনার ইনডেক্স নম্বর লিখুন (বাধ্যতামূলক নয়)</small>
						    </label>
						    <input class="form-control form-control-sm" type="text" placeholder="আপনার উত্তর" name="index_number" value="{{ old('index_number') }}">
					  	</div>

					  	<div class="form-group">
						    <label for="E-mail Address">E-mail Address :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার সচল ইমেইল আইডি লিখুন</small>
						    </label>
						    <input class="form-control form-control-sm" type="text" placeholder="আপনার উত্তর" name="email" value="{{ old('email') }}" required>
					  	</div>

					  	<div class="form-group">
						    <label for="Mobile Number">Mobile Number :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">আপনার সচল মোবাইল নম্বর লিখুন</small>
						    </label>
						    <input class="form-control form-control-sm" type="text" placeholder="আপনার উত্তর" name="mobile" value="{{ old('mobile') }}" required>
					  	</div>

					</div>

					<div class="col-lg-12 mt-2">

						<div class="form-group">
						    <label for="">I confirm that the above information is correct. :
						    	<span class="text-danger">*</span>
						    	<small class="text-muted d-block">উপরোক্ত তথ্য সঠিক থাকলে Yes মার্ক করুন</small>
						    </label>
						    <div class="form-check">
					    		<input class="form-check-input" type="checkbox" required> 
					    		<label class="form-check-label">
					    			Yes
					    		</label>
					    	</div> 
					  	</div>
		  				<button type="submit" class="btn btn-danger mb-2">জমা দিন</button>
					</div>
				
			</div>
		</form>
	</section>

@endsection