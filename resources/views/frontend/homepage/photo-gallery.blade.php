<section class="mb-2 bg-white photo-gallery pb-1">
    <div class="clearfix">
        <div class="float-left">
            <h5 class="text-success ml-2"><i class="fa fa-camera text-dark mr-2"></i>ফটো গ্যালারী</h5>
        </div>
        <div class="float-right photo-gallery-navigation">
            <a class="btn prev btn-success btn-sm border-0"><i class="fa fa-angle-left"></i></a>
            <a class="btn next btn-success btn-sm  border-0 "><i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    
    <div id="photo-gallery" class="owl-carousel owl-theme">
      @foreach($albums as $album)
        <div class="item">
            @php
                $galleryFirstPhoto = \App\Gallery::where('category_id', $album->id)->orderBy('id', 'DESC')->get()->first();
                $getImageById = \App\Image::where('id', $galleryFirstPhoto['media_id'])->get()->first();
            @endphp
            <a href="{{ route('album_photos' , $album->id) }}" class="cursor-pointer">
                <img class="img-fluid pl-2" src="{{url('/')}}/{{$getImageById['icon_size_directory']}}" alt="">
                <div class="text-dark">{{ $album->name }}</div>
            </a>
        </div>
        @endforeach
    </div>
</section>
