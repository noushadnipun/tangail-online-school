<section class="row">
	<div class="col-md-9">
		<div class="bg-white">
            <?php /*
			<div class="row subject-grid mb-2 mx-0">
				@php
					$getSubject = \App\Subjects::get();
				@endphp
	            @foreach($getSubject as $key => $data)
	            <div class="col-6 col-md-2 box">
	                <a href="{{ route('page_video_subject', $data['id']) }}">
	                    {{-- <img class="img-fluid" src="{{url('/')}}/public/assets/images/world.png" alt="" width=""> --}}
	                   	<i class="fa fa-book text-success" style="xcolor: #ae5ed0; font-size: 35px;"></i>
                        <p>{{ $data['name'] }}</p>
                    </a>
	            </div><!-- End all ol-6 col-md-2 box -->
   				@endforeach
            </div><!-- subject  grid -->
			*/ ?>
            <?php echo dynamic_widget($widgets, ['id' => 2, 'heading' => NULL]); ?>
        </div><!-- bg white -->
	</div><!-- Col-9 -->
	
	<div class="col-md-3">
		@include('frontend.homepage.live-class-statistics')
	</div><!-- col md 3 -->

</section>