<section class="mt-2">
	<h2 class="text-success text-center py-4">টাংগাইল জেলার শিক্ষা সংক্রান্ত সংবাদ</h2>
	<div class="row">
		<div class="col-md-2">
			@include('frontend.tschool-pages.news.news-left-sidebar')
		</div><!-- End Col 2 -->
		<div class="col-md-7">
			@foreach($getAllNews as $key => $news)
				@php
					$getNewsImage = \App\Image::where('id', $news['images'])->get()->first();
				@endphp
				@if($key++ == 0)
					<div class="news-feed-block mb-3">
						<div class="news-banner-inner">
							<img class="img-fluid" src="{{url('/')}}/{{$getNewsImage['full_size_directory']}}">
							<div class="news-banner-caption">
								<h2><a href="{{ route('page_single_news', $news->id) }}">{{ $news->title }}</a></h2>
							</div>
						</div>
					</div><!-- news feed block	 -->
				@else
					<div class="news-feed-horizontal mb-3">
						<div class="d-flex justify-content-between flex-wrap p-1">
							<div class="article-image pr-2">
								<a href="{{ route('page_single_news', $news->id) }}">
									<img src="{{url('/')}}/{{$getNewsImage['icon_size_directory']}}" class="img-fluid">
								</a>
							</div>
							<div class="article-description">
								<a href="{{ route('page_single_news', $news->id) }}">
									<h4>{{ $news->title }}</h4>
								</a>
								<div class="additional-info">
									<div class="news-domain">
										@php 
											$offsetParent = explode(",", $news->categories);
											//$gettermName = \App\Term::whereRaw("FIND_IN_SET('71', id)")->first();
											$getTermName = \App\Term::whereIn('id', array_slice($offsetParent, 1))->first();
											//dump($gettermName);
										@endphp
										{{-- <a href="{{ route('page_archive_news', $getTermName->id) }}">
											<span>
												{{ $getTermName->name }}
											</span>
										</a> --}}
									</div>
								<i class="fa fa-clock-o"></i> <span class="pub-date">{{ $news->updated_at->diffForHumans() }}</span>
								</div>
							</div>
						</div>
					</div><!-- end news list blcok -->
				@endif
			@endforeach
		</div><!-- End Col 7 -->
		<div class="col-md-3">
			@include('frontend.tschool-pages.news.news-right-sidebar')
		</div><!-- End Col 3 -->
	</div><!-- End Row -->
</section>