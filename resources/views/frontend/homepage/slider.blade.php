<section class="row mb-2">
	<div class="col-md-9 video-slider">
	 	<!--Carousel Wrapper-->
	    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel" data-interval="false" data-touch="true">
	    	@php
				$getClass = DB::table('classes')->orderBY('id', 'ASC')->limit(7)->get();	

				function getVideo($classDataId){
					$getVideos = DB::table('videos')
		        				->where('class_id', $classDataId)
		        				->orderBY('updated_at', 'DESC')
		        				->limit(1)
								->get();
					return $getVideos;
				}
	    	@endphp
	    		
	      <!--Slides-->
	      	<div class="carousel-inner" role="listbox">
	      		@foreach($getClass  as $key=>$classData)
		        <div class="carousel-item <?php if($key == 0) {echo 'active';} ?>">					
		          	<div class="facebook-responsive">
		          		@foreach(getVideo($classData->id) as$video)
		          			<iframe src="https://www.facebook.com/plugins/video.php?href={{ $video->video_link }}%2F&show_text=false&width=734&height=413&autoplay=true&appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>
		          		@endforeach
		          	</div>

		        </div>
		    	@endforeach
	      	</div>
	      <!--/.Slides-->
	      <!--Controls-->
	      	<ol class="carousel-indicators video_slider_thumb_scroll ml-lg-0" id="video_slider_thumb_container">
	      		@foreach($getClass as $key => $data)
			        <li data-target="#carousel-thumb" data-slide-to="{{ $key++ }}" class="<?php if($key == 1) {echo 'active';} ?>"> 
						{{-- <img class="" src="{{url('/')}}/public/assets/images/video-thumb.png" class="img-fluid"> --}}
						
						@foreach(getVideo($data->id) as$video)
		          			<iframe style="position:relative; z-index: -1;" src="https://www.facebook.com/plugins/video.php?href={{ $video->video_link }}%2F&show_text=false&width=113&height=70&appId" width="113" height="70" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>
		          		@endforeach
			        	<p class="thumb-caption text-success">{{ $data->name }}</p>
			        </li>
		        @endforeach
	      	</ol>
	      	<button id="slideBack" class="d-block d-xl-none" type="button"><i class="fa fa-angle-left slide-arrow"></i></button>
            <button id="slideNext" class="d-block d-xl-none" type="button"><i class="fa fa-angle-right slide-arrow"></i></button>
	      	<!--/.Controls-->
	    </div>
	    <!--/.Carousel Wrapper-->
	</div><!-- End Col 8 -->
	
	<div class="col-md-3">
		@php 
			$getParentMenuName = 	DB::table('menus')->where('id' , '2')->first();
		@endphp
		@if(!empty($getParentMenuName))
			 @php 
		 		$setSliderRightMenu =   get_parent_menus($getParentMenuName->id); 
		 	 @endphp
			<div class="card slider-right-box mb-2">
				<div class="card-body box">
					<h4>{{ $getParentMenuName->name }}</h4>
					<div class="row">
						<div class="col-md-5 col-4">
							<img class="img-fluid" src="{{url('/')}}/public/assets/images/edu-cap.png">
						</div>
						<div class="col-md-7 col-8">
							<ul class="box-caption">
								@foreach($setSliderRightMenu as $link)
								<li><a href="{{ url($link->link) }}">{{ $link->label }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div><!-- End Slider Right Side Box -->
		@endif
		@include('frontend.homepage.notice-board')
		<div class="site-widget-image">
			{{-- <img class="img-fluid" src="{{url('/')}}/public/assets/images/National-Helpline.jpg" alt="" style="height: 428px; width: 100%;"> --}}
			<?php echo dynamic_widget($widgets, ['id' => 1, 'heading' => NULL]); ?>
		</div>
	</div><!-- Enmd Col 4 -->
	
</section><!-- End Row -->