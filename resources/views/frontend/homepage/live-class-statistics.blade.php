<div class="live-class-content-wrapper bg-white px-2 pt-2">
    <div class="live-class-content">
        <div class="live-class-content-header">
            <h4>লাইভ ক্লাস পরিসংখ্যান</h4>
        </div>
        <ul>
            @php
                $getAllClass = \App\Classes::get();
                //dump($getAllClass);
            @endphp
            @foreach($getAllClass as $data)
            <li>
                <a href="{{route('page_video_search').'?class_id='.$data->id}}">
                    <span class="highlight-icon">
                        <i class="fa fa-book text-success"></i>
                    </span>
                    {{$data->name.'র মোট ক্লাসের সংখ্যা'}}
                    <span class="most-read float-right">
                        @php
                            $countVideo = App\Videos::where('class_id', $data->id)->count();
                        @endphp
                        {{ App\CustomClass\BanglaConverter::en2bn($countVideo)}}
                    </span>
                </a>
            </li>
            @endforeach
        </ul>
    </div><!-- live-class-content -->
</div>