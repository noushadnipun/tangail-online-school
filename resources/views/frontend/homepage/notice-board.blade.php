<div class="live-class-content-wrapper bg-white pl-2 pt-2 mb-3">
    <div class="live-class-content">
        <div class="live-class-content-header">
            <h4>নোটিশ</h4>
        </div>
        <ul>
            @php
                $getNotice = \App\Post::WhereRaw("FIND_IN_SET('62', categories)")->orderBy('id', 'DESC')->limit(10)->get();
                //dump($getAllClass);
            @endphp
            @foreach($getNotice as $data)
            <li style="line-height: 1.4rem; padding: 5px 0px;">
                <a href="{{ route('page_single_news', $data->id) }}">
                    <span class="highlight-icon">
                        <i class="fa fa-chevron-circle-right text-success"></i>
                    </span>
                    {{$data['title']}}
                </a>
            </li>
            @endforeach
        </ul>
    </div><!-- live-class-content -->
</div>