<nav class="navbar navbar-expand-lg">
  <!-- <a class="navbar-brand" href="#">Mega Dropdown</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Mens
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <div class="sub-menu-list">
            <div class="sub-menu-content" style="">
              <ul class="sub-menu-content-list menu-generator-space ">
                <li style="">
                  <h6>
                    <a href="#"><b>Men Clothing</b></a>
                  </h6>
                  <ul class="menu-generator-space ">
                    <li><a href="#"> Top Wear</a></li>
                    <li><a href="#"> Bottom Wear</a></li>
                    <li><a href="#"> Seasonal Wear</a></li>
                    <li><a href="#"> Sports Wear</a></li>
                    <li><a href="#"> Tie</a></li>
                  </ul>
                </li>
                <li style="">
                  <h6>
                    <a href="#"><b>Men Accessories</b></a>
                  </h6>
                  <ul class="menu-generator-space ">
                    <li><a href="#"> Belts</a></li>
                    <li><a href="#"> Perfumes</a></li>
                    <li><a href="#"> Watch</a></li>
                    <li><a href="#"> Sun Glass</a></li>
                    <li><a href="#"> Bracelets</a></li>
                  </ul>
                </li>
                <li style="">
                  <h6>
                    <a href="#"><b>Men Accessories</b></a>
                  </h6>
                  <ul class="menu-generator-space ">
                    <li><a href="#"> Belts</a></li>
                    <li><a href="#"> Perfumes</a></li>
                    <li><a href="#"> Watch</a></li>
                    <li><a href="#"> Sun Glass</a></li>
                    <li><a href="#"> Bracelets</a></li>
                  </ul>
                </li>
                <li style="">
                  <h6>
                    <a href="#"><b>Men Accessories</b></a>
                  </h6>
                  <ul class="menu-generator-space ">
                    <li><a href="#"> Belts</a></li>
                    <li><a href="#"> Perfumes</a></li>
                    <li><a href="#"> Watch</a></li>
                    <li><a href="#"> Sun Glass</a></li>
                    <li><a href="#"> Bracelets</a></li>
                  </ul>
                </li>
                <li style="">
                  <h6>
                    <a href="#"><b>Men Accessories</b></a>
                  </h6>
                  <ul class="menu-generator-space ">
                    <li><a href="#"> Belts</a></li>
                    <li><a href="#"> Perfumes</a></li>
                    <li><a href="#"> Watch</a></li>
                    <li><a href="#"> Sun Glass</a></li>
                    <li><a href="#"> Bracelets</a></li>
                  </ul>
                </li>
                <li style="">
                  <h6>
                    <a href="#"><b>Men Accessories</b></a>
                  </h6>
                  <ul class="menu-generator-space ">
                    <li><a href="#"> Belts</a></li>
                    <li><a href="#"> Perfumes</a></li>
                    <li><a href="#"> Watch</a></li>
                    <li><a href="#"> Sun Glass</a></li>
                    <li><a href="#"> Bracelets</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Womens
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Health & Beauty
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        </div>
      </li>
    </ul>
  </div>
</nav>

<style type="text/css">
  ul.navbar-nav.mr-auto {
    margin-left: 20px;
  }

  div.dropdown-menu li.nav-item a.nav-link.active {
    font-size: 13px;
    color: #333;
  }

  div.dropdown-menu li.nav-item a.nav-link {
    padding: 0 !important;
    margin: 0 !important;
    color: #555;
    font-weight: normal;
    font-size: 13px;
  }

  .nav-link {
    color: maroon;
    font-weight: bold;
  }

  .navbar .dropdown-menu div[class*="col"] {
    margin-bottom: 0;
  }

  /* adds some margin below the link sets  */
  .navbar .dropdown-menu div[class*="col"] {
    margin-bottom: 1rem;
  }

  .dropdown-menu {
    /* position: absolute;
      top: 100%;
      left: 0;
      background-color: #fff;
      display: none;
      float: left; */
    z-index: 1000;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-clip: padding-box;
    border-radius: 0;
    border: 0px solid #000;
    border-bottom: 1px solid #ececec;
    border-top: 1px solid #ececec;
  }

  /* breakpoint and up - mega dropdown styles */
  @media screen and (min-width: 992px) {

    /* remove the padding from the navbar so the dropdown hover state is not broken */
    .navbar {
      padding-top: 0px;
      padding-bottom: 0px;
    }

    /* remove the padding from the nav-item and add some margin to give some breathing room on hovers */
    .navbar .nav-item {}

    /* makes the dropdown full width  */
    .navbar .dropdown {
      position: static;
    }

    .navbar .dropdown-menu {
      width: 100%;
      left: 0;
      right: 0;
      top: 38px;
      display: block;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.3s linear;
    }

    /* shows the dropdown menu on hover */
    .navbar .dropdown:hover .dropdown-menu,
    .navbar .dropdown .dropdown-menu:hover {
      display: block;
      visibility: visible;
      opacity: 1;
      transition: visibility 0s, opacity 0.3s linear;
    }

    .navbar .dropdown-menu {
      background-color: #fff;
    }
  }
</style>
<script type="text/javascript">
  jQuery(document).ready(function($) {
        $.noConflict();
        // executes when HTML-Document is loaded and DOM is ready
        // breakpoint and up  
        $(window).resize(function() {
          if ($(window).width() >= 980) {
            // when you hover a toggle show its dropdown menu
            $(".navbar .dropdown-toggle").hover(function() {
              $(this).parent().toggleClass("show");
              $(this).parent().find(".dropdown-menu").toggleClass("show");
            });
            // hide the menu when the mouse leaves the dropdown
            $(".navbar .dropdown-menu").mouseleave(function() {
              $(this).removeClass("show");
            });
            // do something here
          }
        });
</script>