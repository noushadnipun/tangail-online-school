<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link rel="stylesheet" href="{{asset('public/plugins/timepicker/jquery-ui.min.css')}}"> --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/assets/css/global.css') }}">
    <title>
        @if(View::hasSection('title')) 
            @yield('title') - 
        @endif

        {{config('app.name','Default')}}
    </title>
    <script type="text/javascript">
        var baseurl = "<?php echo url('/'); ?>";
    </script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @include('frontend.layouts.css')
    @include('frontend.layouts.js_head')
</head>

<body class="hold-transition skin-black-light sidebar-mini">
    <?php $tksign = '&#2547; '; ?>
    <div class="wrapper">

        @include('frontend.layouts.header')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header)
            <section class="content-header" style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">
                <h1>@yield('title')
                    <small>@yield('sub_title')</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">@yield('title')</li>
                </ol>
            </section> -->
            <div class="content">
                @yield('content')
            </div>
        </div>

        @include('frontend.layouts.footer')

    </div>
    <!-- ./wrapper -->
    @include('frontend.layouts.js')
    @yield('cusjs')
</body>
</html>