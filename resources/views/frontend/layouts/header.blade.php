<div class="container px-md-0 px-0 py-md-1 bg-white">
	<!-- Banner -->
	<div class=" banner jumbotron jumbotron-fluid mb-1 p-0 p-md-4" style="background: linear-gradient(rgb(0 0 0 / 30%), rgb(0 0 0 / 35%)), url({{url('/')}}/public/assets/images/banner-2.jpg) no-repeat;">
		<div class="container">
			<div class="row">
				<div class="col-md-7 col-7 pt-2">
					<h1 class="banner-text"> Tangail Online School</h1>
					<p class="lead font-solaimanlipi">আমার হাতে আমার স্কুল</p>
				</div>
				<div class="col-md-5 col-5 pt-2">
					<div class="row justify-content-end mr-md-3 align-items-center h-100 language">
						<div class="lang btn-group">
							<a class="language-btn btn btn-sm text-white bg-purple active" href=""> বাংলা </a>
							<a class="language-btn btn btn-sm text-white bg-purple " href=""> English </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@php
		$newsHeadlines = \App\Post::WhereRaw("FIND_IN_SET('62', categories)")->orderBy('id', 'DESC')->limit(7)->get();
	@endphp
	<div class="notice_ticler">
		<div class="notice_ticler_label">
			<strong>নোটিশ</strong>
		</div>
		<div class="notice_ticler_wrapper">
			<ul class="w-100">
				<marquee behavior="scroll" scrollamount="5" scrolldelay="0" direction="left" onmouseover="stop();" onmouseout="start();">
					@foreach($newsHeadlines as $news)
						<li><i class="fa fa-circle mr-2"></i> <a href="{{ route('page_single_news', $news->id) }}">{{ $news['title'] }}</a></li>
					@endforeach
				</marquee>
			</ul>
		</div>
	</div>
	<!-- Nav Menu -->
	<nav class="navbar navbar-expand-lg navbar-dark my-1 px-0 py-0 bg-purple">
		<button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<ul class="text-white navbar-nav text-center font-weight-700 text-uppercase">
				@php $parentMenu =    get_parent_menus(1); @endphp
				@foreach($parentMenu as $link)
					<li class="{{ (has_sub($link->id) > 0) ? 'nav-item dropdown' : 'nav-item' }}">
						<a class="{{ (has_sub($link->id) > 0) ? 'nav-link dropdown-toggle' : 'nav-link' }}" href="{{ url($link->link) }}" data-abc="true" @if(has_sub($link->id) > 0) id="{{ $link->id }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" @endif>
							{{ $link->label }}
						</a>
						@if(has_sub($link->id) > 0)
							@php $subMneu = get_sub_menus($link->id) @endphp
							<ul class="dropdown-menu first-dropdown" aria-labelledby="{{ $link->id }}">
								@foreach($subMneu as $link)
									<li class="{{ (has_sub($link->id) > 1) ? 'dropdown-item dropdown' : 'dropdown-item' }}">
										<a href="{{ url($link->link) }}" data-abc="true" @if(has_sub($link->id) > 1) class="dropdown-toggle" id="{{ $link->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" @endif>
											{{ $link->label }}
										</a>
										@php $subSubMneu =  get_sub_menus($link->id) @endphp
										<ul class="dropdown-menu" aria-labelledby="{{ $link->id }}">
											@foreach($subSubMneu as $link)
												<li class="dropdown-item">
													<a href="{{ url($link->link) }}"  data-abc="true">
														{{ $link->label }}
													</a>
												</li>
											@endforeach
										</ul>
									</li>
								@endforeach
							</ul>
						@endif
					</li>
				@endforeach
{{--				<li class="nav-item"><a class="nav-link" href="index.php">প্রচ্ছদ</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="message.php">বাণী</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="administration.php">প্রশাসন</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="index.php">এডমিন প্যানেল</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="administration.php">ICT4E অ্যাম্বাসেডর</a></li>--}}
{{--				<li class="nav-item dropdown">--}}
{{--					<a class="nav-link dropdown-toggle" id="about_us" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">প্রতিযোগিতা</a>--}}
{{--					<ul class="dropdown-menu" aria-labelledby="about_us">--}}
{{--						<li class="dropdown-item"><a href="single.php">বিতর্ক</a></li>--}}
{{--						<li class="dropdown-item"><a href="#">কুইজ</a></li>--}}
{{--						<li class="dropdown-item"><a href="#">অনলাইন টেস্ট</a></li>--}}
{{--						<li class="dropdown-item"><a href="result.php">ফলাফল</a></li>--}}
{{--					</ul>--}}
{{--				</li>--}}
{{--				<li class="nav-item dropdown">--}}
{{--					<a class="nav-link dropdown-toggle" id="gallery" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">শিক্ষা সংক্রান্ত</a>--}}
{{--					<ul class="dropdown-menu" aria-labelledby="gallery">--}}
{{--						<li class="dropdown-item"><a href="">প্রতিষ্ঠান সমূহ</a></li>--}}
{{--						<li class="dropdown-item"><a href="">শিক্ষক মন্ডলী</a></li>--}}
{{--						<li class="dropdown-item"><a href="">বই বিতরণ</a></li>--}}
{{--						<li class="dropdown-item"><a href="">আইসিটি ল্যাব</a></li>--}}
{{--						<li class="dropdown-item"><a href="result.php">ফলাফল</a></li>--}}
{{--					</ul>--}}
{{--				</li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="news.php">সংবাদ</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="result.php">পাবলিক পরীক্ষার ফলাফল</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="video.php">শ্রেণি ক্লাশ</a></li>--}}
{{--				<li class="nav-item"><a class="nav-link" href="contact.php">যোগাযোগ</a></li>--}}
			</ul>
		</div>
	</nav>
</div>
<div class="container px-md-2 py-md-1 py-1 bg-white" style="padding-bottom: 0px !important;">
