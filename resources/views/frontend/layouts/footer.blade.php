<section>
	<footer class="bg-purple">
		<div class="row">
			
			<?php echo dynamic_widget($widgets, ['id' => 3, 'heading' => NULL]); ?>
		</div>
	</footer>
	<section class="bottom-footer py-0">
		<div class="row">
			<div class="col-md-6 text-center text-md-left">

				<p>
					© {{ App\CustomClass\BanglaConverter::en2bn(date('Y'))}} সর্বস্বত্ব সংরক্ষিত
					টাংগাইল অনলাইন স্কুল
				</p>

			</div>

			{{-- <div class=" text-center text-md-left col-md-4">
				<img src="{{url('/')}}/public/assets/images/footer-logo-new.png" alt="a2i" width="75%">
			</div> --}}

			<div class="text-center text-md-right col-md-6">

				<p> কারিগরি সহযোগিতায় 
					<a href="http://tritiyo.com" target="_blank">তৃতীয় লিমিটেড</a>

				</p>

			</div>
		</div>
	</section>
</section><!-- Footer -->
</div>