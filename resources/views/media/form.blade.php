@extends('layouts.app')

@php
    $url_slug = \Request::segment(1);
    if($url_slug === 'add_showroom' || $url_slug ==='edit_showroom'){
        $os = true;
    }else{
        $os = false;
    }

@endphp

@section('title', (($os)? 'Shop': 'Post'))

@section('sub_title', (($os)? 'Shop': 'Post') .' add or modification form')
@section('content')
    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif


        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="col-md-8">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($user->id))
                        media_forms
                    @else
                    media_form
                    @endif
                @endslot

                @slot('title')
                    @if (!empty($media->id))
                        Edit media
                    @else
                        Add a new media
                    @endif

                @endslot

                @slot('route')
                    @if (!empty($media->id))
                        {{route('admin.medias.update',$media->id)}}
                    @else
                        {{route('admin.medias.store')}}
                    @endif
                @endslot
                    @slot('method')
                        @if (!empty($media->id))
                          {{method_field('PUT')}}
                        @endif
                    @endslot

                @slot('fields')
                    <div class="form-group">
                        {{ Form::label('link', 'Link', array('class' => 'link')) }}
                        {{ Form::text('link', (!empty($media->link) ? $media->link : NULL), ['class' => 'form-control', 'placeholder' => 'Enter link...']) }}
                    </div>                    
                @endslot
            @endcomponent
        </div>        
    </div>
@endsection
@push('scripts')    
@endpush
