@extends('layouts.app')

@php
    $url_slug = \Request::segment(1);
    if($url_slug === 'add_showroom' || $url_slug ==='edit_showroom'){
        $os = true;
    }else{
        $os = false;
    }

@endphp

@section('title', (($os)? 'Shop': 'Post'))

@section('sub_title', (($os)? 'Shop': 'Post') .'add or modification form')
@section('content')
    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif


        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="col-md-8">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($user->id))
                        post_forms
                    @else
                        post_form
                    @endif
                @endslot

                @slot('title')
                    @if (!empty($post->id))
                        Edit {{ ($os)? 'showroom': 'post' }}
                    @else
                        Add a new {{ ($os)? 'showroom': 'post' }}
                    @endif

                @endslot

                @slot('route')
                    @if (!empty($post->id))
                        {{route('admin.posts.update',$post->id)}}
                    @else
                        {{route('admin.posts.store')}}
                    @endif
                @endslot
                    @slot('method')
                        @if (!empty($post->id))
                          {{method_field('PUT')}}
                        @endif
                    @endslot

                @slot('fields')
                    <div class="form-group">
                        {{ Form::hidden('user_id', (!empty(\Auth::user()->id) ? \Auth::user()->id : NULL), ['type' => 'hidden']) }}
                        {{ Form::hidden('lang', (!empty($post->lang) ? $post->lang : 'en'), ['type' => 'hidden']) }}
                        {{ Form::label('title', 'Title', array('class' => 'title')) }}
                        {{ Form::text('title', (!empty($post->title) ? $post->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter title...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('sub_title', 'Sub Title', array('class' => 'sub_title')) }}
                        {{ Form::text('sub_title', (!empty($post->sub_title) ? $post->sub_title : NULL), ['class' => 'form-control', 'placeholder' => 'Enter sub_title...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('seo_url', 'Search Engine Friendly URL', array('class' => 'seo_url')) }}
                        {{ Form::text('seo_url', (!empty($post->seo_url) ? $post->seo_url : NULL), ['type' => 'text', 'required', 'class' => 'form-control', 'placeholder' => 'Enter seo URL...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('author', 'Author/Contact Person', array('class' => 'author')) }}
                        {{ Form::text('author', (!empty($post->author) ? $post->author : NULL), ['type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter author...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Description', array('class' => 'description')) }}
                        {{ Form::textarea('description', (!empty($post->description) ? $post->description : NULL), ['required', 'class' => 'form-control', 'id' => 'wysiwyg', 'placeholder' => 'Enter details content...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('images', 'Image ID/s', array('class' => 'images')) }}
                        @if(!empty($post->images))
                            <?php
                            //$im = image_ids($product->images, TRUE, TRUE);
                            $im = $post->images;
                            ?>
                            {{ Form::text('images', (!empty($im) ? $im : NULL), ['type' => 'text', 'id' => 'image_ids', 'class' => 'form-control', 'placeholder' => 'Enter image IDs...']) }}
                        @else
                            {{ Form::text('images', NULL, ['type' => 'text', 'id' => 'image_ids', 'class' => 'form-control', 'placeholder' => 'Enter image IDs...']) }}
                        @endif
                        <small id="show_image_names"></small>

                    </div>
                    <div class="form-group">
                        @if(!empty($post->images))
                            <?php
                            $images = explode(',', $post->images);

                            $html = null;
                            foreach ($images as $image) :
                                $img = App\Image::find($image);
                                //dump($img);
                               if($img):
                                $html .= '<img src="' . url($img->icon_size_directory) . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                                //$html .= '<span>' . $img->id . '</span>';
                                $html .= '<a href="' . url('delete_attribute', ['id' => $img->id]) . '">x</a>';
                                //$attributes_p = product_attributes($product, TRUE);
                                endif;
                            endforeach;
                            //die();
                            ?>
                            {!! $html !!}
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6">

                            <div class="nav-tabs-custom">

                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                                            Basic
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">
                                            Other
                                        </a>
                                    </li>

                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">


                                        <div class="form-group">
                                            {{ Form::label('youtube', 'Youtube', array('class' => 'youtube')) }}
                                            {{ Form::text('youtube', (!empty($post->youtube) ? $post->youtube : NULL), ['class' => 'form-control', 'placeholder' => 'Enter youtube...']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('lang', 'Language', array('class' => 'lang')) }}
                                            <div class="radio">
                                                <label>
                                                    {{ Form::radio('lang', 1, (!empty($post) ? ((!empty($post->lang) ? $post->lang == 1 : 1) ? TRUE : FALSE) : TRUE), ['class' => 'radio']) }}
                                                    English
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    {{ Form::radio('lang', 0, (!empty($post) ? ((!empty($post->lang) ? $post->lang == 0 : 0) ? FALSE : TRUE) : null), ['class' => 'radio']) }}
                                                    Bengali
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('is_auto_post', 'Is facebook auto post?', array('class' => 'is_auto_post')) }}
                                            <div class="radio">
                                                <label>
                                                    {{ Form::radio('is_auto_post', 1, (!empty($post) ? (($post->is_auto_post == 1) ? TRUE : FALSE) : TRUE), ['class' => 'radio']) }}
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    {{ Form::radio('is_auto_post', 0, (!empty($post) ? (($post->is_auto_post == 0) ? TRUE : FALSE) : null), ['class' => 'radio']) }}
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('short_description', 'Short Description', array('class' => 'short_description')) }}
                                            {{ Form::textarea('short_description', (!empty($post->short_description) ? $post->short_description : NULL), ['class' => 'form-control', 'placeholder' => 'Enter short description...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('is_upcoming', 'Is it upcoming?', array('class' => 'is_upcoming')) }}
                                            <div class="radio">
                                                <label>
                                                    {{ Form::radio('is_upcoming', 1, (!empty($post) ? (($post->is_upcoming == 1) ? TRUE : FALSE) : TRUE), ['class' => 'radio']) }}
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    {{ Form::radio('is_upcoming', 0, (!empty($post) ? (($post->is_upcoming == 0) ? TRUE : FALSE) : null), ['class' => 'radio']) }}
                                                    No
                                                </label>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label>
                                                {{ Form::label('h1tag', 'H1 Tag', array('class' => 'h1tag')) }}
                                            </label>    
                                            {{ Form::text('h1tag', $post->h1tag??'', ['class' => 'form-control', 'placeholder' => 'Enter h1 tag...']) }}
                                        </div>

                                        <div class="form-group">
                                            <label>
                                            {{ Form::label('h2tag', 'H2Tag', array('class' => 'h2tag')) }}
                                            </label>
                                            {{ Form::text('h2tag', $post->h2tag??'', ['class' => 'form-control', 'placeholder' => 'Enter h2tag...']) }}

                                        </div>

                                        <div class="form-group">
                                            <label>
                                                 {{ Form::label('seo_title', 'Seo Title', array('class' => 'seo_title')) }}
                                            </label>
                                                {{ Form::text('seo_title', $post->seo_title??'', ['class' => 'form-control', 'placeholder' => 'Enter seo title...']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('seo_description', 'Seo Description', array('class' => 'seo_description')) }}
                                            {{ Form::textarea('seo_description', (!empty($post->seo_description) ? $post->seo_description : NULL), ['class' => 'form-control', 'placeholder' => 'Enter seo description...']) }}
                                        </div>



                                    </div>
                                    <div class="tab-pane" id="tab_2">

                                        <div class="form-group">
                                            {{ Form::label('brand', 'Brand', array('class' => 'brand')) }}
                                            {{ Form::text('brand', (!empty($post->brand) ? $post->brand : NULL), ['class' => 'form-control', 'placeholder' => 'Enter brand...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('phone', 'Phone', array('class' => 'phone')) }}
                                            {{ Form::text('phone', (!empty($post->phone) ? $post->phone : NULL), ['class' => 'form-control', 'placeholder' => 'Enter phone...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('opening_hours', 'Opening Hours', array('class' => 'opening_hours')) }}
                                            {{ Form::text('opening_hours', (!empty($post->opening_hours) ? $post->opening_hours : NULL), ['class' => 'form-control', 'placeholder' => 'Enter opening hours...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('latitude', 'Latitude', array('class' => 'latitude')) }}
                                            {{ Form::text('latitude', (!empty($post->latitude) ? $post->latitude : NULL), ['class' => 'form-control', 'placeholder' => 'Enter latitude...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('longitude', 'Longitude', array('class' => 'longitude')) }}
                                            {{ Form::text('longitude', (!empty($post->longitude) ? $post->longitude : NULL), ['class' => 'form-control', 'placeholder' => 'Enter longitude...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('phone_numbers', 'Phone Numbers', array('class' => 'phone_numbers')) }}
                                            {{ Form::text('phone_numbers', (!empty($post->phone_numbers) ? $post->phone_numbers : NULL), ['class' => 'form-control', 'placeholder' => 'Enter phone numbers...']) }}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('address', 'Address', array('class' => 'address')) }}
                                            {{ Form::textarea('address', (!empty($post->address) ? $post->address : NULL), ['class' => 'form-control', 'placeholder' => 'Enter address...']) }}
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                {{ Form::label('is_sticky', 'Is it sticky?', array('class' => 'is_sticky')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('is_sticky', 1, (!empty($post) ? (($post->is_sticky == 1) ? TRUE : FALSE) : TRUE), ['class' => 'radio']) }}
                                        Yes. This post will be marked as selected top post
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('is_sticky', 0, (!empty($post) ? (($post->is_sticky == 0) ? TRUE : FALSE) : null), ['class' => 'radio']) }}
                                        No. This post will no be marked as selected top post
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('is_active', 'Will it be active?', array('class' => 'is_active')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('is_active', 1, (!empty($post) ? (($post->is_active == 1) ? TRUE : FALSE) : TRUE), ['class' => 'radio']) }}
                                        Yes. This post will publish
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('is_active', 0, (!empty($post) ? (($post->is_active == 0) ? TRUE : FALSE) : null), ['class' => 'radio']) }}
                                        No. This post will save as draft
                                    </label>
                                </div>
                            </div>
                            @if (!$os)
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Post Categories <span id="photoCounter"></span></h3>

                                        <div class="col-xs-12 post_cats" style="margin-top: 15px;">
                                            <div class="pre-scrollable">
                                                <?php
                                                //dump($categories);

                                                if (!empty($post->categories)) {
                                                    $ids = explode(',', $post->categories);
                                                }
                                                $parent = 2;
                                                $parents = ($parent) ? $parent : NULL;
                                                ?>
                                                {!! category_h_checkbox_html($categories, $parents, $name = 'categories', !empty($ids) ? $ids : array()) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            {{ Form::hidden('categories[]') }}



                            <div class="form-group">
                                {{ Form::label('publish_date', 'Published Date', array('class' => 'publish_date')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::text('created_at', $post->created_at??'', ['class' => 'form-control datepicker', 'placeholder' => 'Enter date...']) }}
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('offer_expire_date', 'Offer Expire Date', array('class' => 'offer_expire_date')) }}
                                <div class="radio">
                                    <label>
                                        {{ Form::text('offer_expire_date', $post->offer_expire_date??'', ['class' => 'form-control datepicker', 'placeholder' => 'Enter date...']) }}
                                    </label>
                                </div>
                            </div>


                            <div class="form-group">
                                {{ Form::label('seo_keywords', 'Seo Keywords', array('class' => 'seo_keywords')) }}
                                {{ Form::textarea('seo_keywords', (!empty($post->seo_keywords) ? $post->seo_keywords : NULL), ['class' => 'form-control', 'placeholder' => 'Enter short keywords...']) }}
                            </div>


                        </div>
                    </div>
                @endslot
            @endcomponent
        </div>
        <div class="col-md-4">
            @component('component.dropzone')
            @endcomponent
            @if(!empty($medias))
                <div class="row" id="reload_me">
                    @foreach($medias as $media)
                        <div class="col-xs-6 col-md-4">
                            <div href="#" class="thumbnail">
                                <img src="{{ url($media->icon_size_directory??'') }}"
                                     class="img-responsive"
                                     style="max-height: 80px; min-height: 80px;"/>
                                <div class="caption text-center">
                                    <p>
                                        <a
                                                href="javascript:void(0);"
                                                data-id="{{ $media->id }}"
                                                data-option="{{ $media->filename }}"
                                                class="btn btn-xs btn-primary"
                                                onclick="get_id(this);"
                                                role="button">
                                            Use
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ URL::asset('public/public/plugins/dropzone.js') }}"></script>
    <script src="{{ URL::asset('public/public/js/dropzone-config.js') }}"></script>
    <script type="text/javascript">
        function get_id(identifier) {
            //alert("data-id:" + jQuery(identifier).data('id') + ", data-option:" + jQuery(identifier).data('option'));


            var dataid = jQuery(identifier).data('id');
            jQuery('#image_ids').val(
                function (i, val) {
                    return val + (!val ? '' : ', ') + dataid;
                });
            var option = jQuery(identifier).data('option');
            jQuery('#show_image_names').html(
                function (i, val) {
                    return val + (!val ? '' : ', ') + option;
                }
            );
        }

        /**
         *
         */
        jQuery(document).ready(function ($) {
            $.noConflict();

            $('#title').blur(function () {
                var m = $(this).val();
                var cute1 = m.toLowerCase().replace(/ /g, '-').replace(/&amp;/g, 'and').replace(/&/g, 'and').replace(/ ./g, 'dec');
                var cute = cute1.replace(/[`~!@#$%^&*()_|+\=?;:'"”,.<>\{\}\[\]\\\/]/gi, '');

                $('#seo_url').val(cute);
            });

        });
    </script>
    <script>
        jQuery(document).ready(function ($) {

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                defaultViewDate: {
                    year: {{\Carbon\Carbon::parse($post->created_at??'')->format('Y')??''}},
                    month: {{\Carbon\Carbon::parse($post->created_at??'')->format('m')??''}},
                    day: {{\Carbon\Carbon::parse($post->created_at??'')->format('d')??''}}
                }
            })

        } );
    </script>
@endpush
