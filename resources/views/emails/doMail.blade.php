@component('mail::message')

@component('mail::table')

    <div class="">
        Showroom: {{$data['shipments']->distributor->title}}
        OrderId: {{$data['shipments']->orderMaster->id}}
    </div>



<table style="width: 700px;">
    <thead>
    <tr>
        <th>Product Code</th>
        <th>Product Name</th>
        <th>Price (TK)</th>
        <th>Quantity</th>
        <th>Discount</th>
        <th>SP Discount</th>
        <th>Total (TK)</th>
        <th>Company</th>
    </tr>
    </thead>

    <tbody>
    @foreach($data['shipments']->orderDetails as $order)
    <tr>
        <td>{{$order->product_code}}</td>
        <td>{{$order->product_name}}</td>
        <td>{{$order->local_purchase_price}}</td>
        <td>{{$order->qty}}</td>
        <td>{{$order->discount}}</td>
        <td>0</td>
        <td>{{$order->local_purchase_price*$order->qty}}</td>
        <td>-</td>
    </tr>
        @endforeach
    </tbody>
</table>

@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent

<style>
    table tr {
        border: 1px solid #ddd;
    }
</style>
