<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Require meta tag-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title Tag-->
    <title>Welcome to RFLBestBuy.com | RFLBestBuy
        <!-- Bootstrap CSS -->
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!--Bootstrap Slider css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.2/css/bootstrap-slider.css">
    <!-- Style css-->
    <link rel="stylesheet" href="{{asset('public/frontend/css/font.css')}}">
    <!-- Style css-->
    <link rel="stylesheet" href="{{asset('public/frontend/css/main.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('public/frontend/css/responsivemobilemenu.css')}}">
    <style>
        .page-link:hover {
            z-index: 10;
            color: #0056b3;
            text-decoration: none;
             background-color: #e9ecef;
             border-color: #dee2e6;
        }
    </style>

<body>

<div id="app">
    {!! ssr('frontend/js/server.js')
        ->context('packages', $packages)
        ->render() !!}
</div>

<!-- REQUIRED SCRIPTS -->
<script>
    window.context = {
        packages: @json($packages)
    }


    window.$_REVECHAT_API || (function(d, w) { var r = $_REVECHAT_API = function(c) {r._.push(c);}; w.__revechat_account='8167451';w.__revechat_version=2;
        r._= []; var rc = d.createElement('script'); rc.type = 'text/javascript'; rc.async = true; rc.setAttribute('charset', 'utf-8');
        rc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'static.revechat.com/widget/scripts/new-livechat.js?'+new Date().getTime();
        var s = d.getElementsByTagName('script')[0]; s.parentNode.insertBefore(rc, s);
    })(document, window);

</script>
<script src="{{asset('public'.mix('frontend/js/manifest.js'))}}"></script>
<script src="{{asset('public'.mix('frontend/js/vendor.js'))}}"></script>
<script src="{{asset('public'.mix('frontend/js/app.js'))}}"></script>
</body>
</html>
