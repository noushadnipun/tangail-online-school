@extends('layouts.app')
@section('title', 'Gallery')
@section('sub_title', 'all images')
@section('content')
    <?php
            //dd($image);
    ?>
    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($image->id)?route('admin.common.gallery.update',$image->id):route('admin.common.gallery.store')}}
                @endslot

                @slot('method')
                    {{-- @if (!empty($team->id))
                        {{ method_field('put') }}
                    @endif --}}
                @endslot

                @slot('fields')

                    <div class="form-group">
                        {{ Form::label('imageId', 'Image ID', array('class' => 'imageId')) }}
                        {{ Form::text('imageId', $image->media_id??'', ['required', 'class' => 'form-control', 'placeholder' => 'Enter ID...']) }}
                        @if($image->image??false)
                            <div><img src="{{asset($image->image->icon_size_directory)}}" width="80"/></div>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('caption', 'Caption', array('class' => 'uri')) }}
                        {{ Form::textarea('caption', $image->caption??'', ['', 'class' => 'form-control', 'placeholder' => 'Enter caption...']) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('album','Album',array('class' => 'album'))}}
                        <?php
                            $albums = \App\Album::select('id', 'name')->get();
                        ?>
                        <select class="form-control" name="album_id">
                            @foreach($albums as $album)
                                <option value="{{ $album->id }}" {{ (!empty($image->category_id) && $image->category_id == $album->id) ? 'selected="selected"' : '' }}>
                                    {{ $album->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        {{Form::label('status','Status',array('class' => 'status'))}}
                        {!! Form::select('active', [0 => 'Disable', 1 => 'Active'], $image->active??1,['class' => 'form-control']) !!}
                    </div>

                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Images <a href="{{route('admin.common.gallery.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Serial No</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Caption</th>
                                <th>Album</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <tbody id="myTable">
                            @foreach($gallery as $gal)
                                <tr class="item" data-id="{{$gal->id}}">
                                    <td class="index">{{$gal->serial}} </td>
                                    <td><img src="{{asset(($gal->image->icon_size_directory??''))}}" width="100"/></td>
                                    <td>{{$gal->image->original_name??''}}</td>
                                    <td>{{$gal->caption??''}}</td>
                                    <td>
                                        @php
                                        $albumName = \App\Album::where('id', $gal->category_id)->get();
                                        // dump($albumName)
                                        @endphp

                                        @if(!empty($albumName[0]))
                                            {{$albumName[0]->name}}
                                        @endif


                                    </td>
                                    <td>
                                        @if($gal->active??false)
                                            Active
                                        @else
                                            Disable
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.gallery.index')  }}?id={{$gal->id}}">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger"
                                           href="{{ route('admin.common.gallery.delete',$gal->id)  }}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                            {{ $gallery->links('component.paginator', ['object' => $gallery]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection



@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });

                jQuery.ajax({
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "item": item
                    },
                    type: 'POST',
                    url: '{{ route('admin.common.gallery.serialupdate') }}',
                    success: function (result) {
                        // jQuery('#reload_me').load('{{ route('admin.common.gallery.index') }}' + " " + '#reload_me');
                        location.reload(true);
                        jQuery('#myTable').sortable();
                    }
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>
@endpush