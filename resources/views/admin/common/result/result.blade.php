@extends('layouts.app')
@section('title', 'Public Exam Results')
@section('sub_title', '')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
        @if(Session::has('delete'))
            <div class="col-md-12">
                <div class="callout callout-danger">
                    {{ Session::get('delete') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
        @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($resultEdit->id)?route('admin.common.exam-results.update',$resultEdit->id):route('admin.common.exam-results.store')}}
                @endslot

                @slot('method')
                    {{-- @if (!empty($team->id))
                        {{ method_field('put') }}
                    @endif --}}
                @endslot

                @slot('fields')
                    @if (!empty($resultEdit->id))
                        {{ Form::hidden('result_id', $resultEdit->id, ['required']) }}
                    @endif
                    <div class="form-group">
                        {{ Form::label('upozila', 'Upozila', array('class' => 'upozila')) }}
                        {{ Form::text('upozila', (!empty($resultEdit->upozila) ? $resultEdit->upozila : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter upozila...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('exam_name', 'Name of Exam', array('class' => 'exam_name')) }}
                        {{ Form::text('exam_name', (!empty($resultEdit->exam_name) ? $resultEdit->exam_name : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Name of Exam...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('year', 'Exam Year', array('class' => 'year')) }}
                        {{ Form::text('year', (!empty($resultEdit->year) ? $resultEdit->year : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Exam Year...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('total_candidates', 'Total Candidates', array('class' => 'total_candidates')) }}
                        {{ Form::text('total_candidates', (!empty($resultEdit->total_candidates) ? $resultEdit->total_candidates : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Total Candidates...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('total_pass', 'Total Passed', array('class' => 'total_pass')) }}
                        {{ Form::text('total_pass', (!empty($resultEdit->total_pass) ? $resultEdit->total_pass : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Total Passed...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('percentage_pass', 'Percentage of Passed', array('class' => 'laptop')) }}
                        {{ Form::text('percentage_pass', (!empty($resultEdit->percentage_pass) ? $resultEdit->percentage_pass : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Percentage of Passed...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('a_plus', 'A+', array('class' => 'a_plus')) }}
                        {{ Form::text('a_plus', (!empty($resultEdit->a_plus) ? $resultEdit->a_plus : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('a', 'A', array('class' => 'a')) }}
                        {{ Form::text('a', (!empty($resultEdit->a) ? $resultEdit->a : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('a_min', 'A-', array('class' => 'a_min')) }}
                        {{ Form::text('a_min', (!empty($resultEdit->a_min) ? $resultEdit->a_min : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('b', 'B', array('class' => 'b')) }}
                        {{ Form::text('b', (!empty($resultEdit->b) ? $resultEdit->b : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('c', 'C', array('class' => 'c')) }}
                        {{ Form::text('c', (!empty($resultEdit->c) ? $resultEdit->c : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('d', 'D', array('class' => 'd')) }}
                        {{ Form::text('d', (!empty($resultEdit->d) ? $resultEdit->d : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('f', 'F', array('class' => 'f')) }}
                        {{ Form::text('f', (!empty($resultEdit->f) ? $resultEdit->f : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                @endslot
            @endcomponent
        </div>

        {{-- Raed Data --}}
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Result Sheet <a href="{{route('admin.common.exam-results.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Upozila</th>
                                <th>Name Of Exam</th>
                                <th>Exam Year</th>
                                <th>Total Candidates</th>
                                <th>Additional Info</th>
                                @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                   <th>Action</th>
                                @endif
                            </tr>
                            <tbody id="xmyTable">
                           
                            @foreach($resultSheet as $result)
                                <tr class="item" data-id="{{$result->id}}">
                                    <td class="index">{{$result->upozila}} </td>
                                    <td class="index">{{$result->exam_name}} </td>
                                    <td class="index">{{$result->year}} </td>
                                    <td class="index">
                                        <strong>Total Candidates: </strong> {{$result->total_candidates}} <br/>
                                        <strong>Total Passed: </strong> {{$result->total_pass}} <br/>
                                        <strong>Parcentage Candidates: </strong> {{$result->percentage_pass}} <br/>
                                    </td>
                                    <td class="index"> 
                                        <strong>A+: </strong> {{$result->a_plus}} <br/>
                                        <strong>A: </strong> {{$result->a}} <br/>
                                        <strong>A-: </strong> {{$result->a_min}} <br/>
                                        <strong>B: </strong> {{$result->b}} <br/>
                                        <strong>C: </strong> {{$result->c}} <br/>
                                        <strong>D: </strong> {{$result->d}} <br/>
                                        <strong>F: </strong> {{$result->f}} <br/>
                                    </td>             
                                    @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.exam-results.edit', $result->id) }}"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this record?')"
                                           href="{{ route('admin.common.exam-results.delete',$result->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                           {{ $resultSheet->links('component.paginator', ['object' => $resultSheet]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>

    


@endpush