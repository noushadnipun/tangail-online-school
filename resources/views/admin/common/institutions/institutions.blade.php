@extends('layouts.app')
@section('title', 'Public Exam Results')
@section('sub_title', '')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
        @if(Session::has('delete'))
            <div class="col-md-12">
                <div class="callout callout-danger">
                    {{ Session::get('delete') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
        @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($institutionsEdit->id)?route('admin.common.institutions.update',$institutionsEdit->id):route('admin.common.institutions.store')}}
                @endslot

                @slot('method')
                    {{-- @if (!empty($team->id))
                        {{ method_field('put') }}
                    @endif --}}
                @endslot

                @slot('fields')
                    @if (!empty($institutionsEdit->id))
                        {{ Form::hidden('institutions_id', $institutionsEdit->id, ['required']) }}
                    @endif
                    <div class="form-group">
                        {{ Form::label('status', 'স্ট্যাটাস', array('class' => 'status')) }}
                        {{ Form::text('status', (!empty($institutionsEdit->status) ? $institutionsEdit->status : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter status...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('type', 'ধরণ', array('class' => 'type')) }}
                        {{ Form::text('type', (!empty($institutionsEdit->type) ? $institutionsEdit->type : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter type...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('level', 'স্তর', array('class' => 'level')) }}
                        {{ Form::text('level', (!empty($institutionsEdit->level) ? $institutionsEdit->level : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter level...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('upozila', 'উপজেলা', array('class' => 'upozila')) }}
                        {{ Form::text('upozila', (!empty($institutionsEdit->upozila) ? $institutionsEdit->upozila : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter upozila...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('institutions_name', 'প্রতিষ্ঠানের নাম', array('class' => 'institutions_name')) }}
                        {{ Form::text('institutions_name', (!empty($institutionsEdit->institutions_name) ? $institutionsEdit->institutions_name : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter institutions name...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('institutions_head', 'প্রতিষ্ঠান প্রধান', array('class' => 'institutions_head')) }}
                        {{ Form::text('institutions_head', (!empty($institutionsEdit->institutions_head) ? $institutionsEdit->institutions_head : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter institutions head...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('position', 'পদবী', array('class' => 'position')) }}
                        {{ Form::text('position', (!empty($institutionsEdit->position) ? $institutionsEdit->position : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter position...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('establishment_date', 'প্রতিষ্ঠার তারিখ', array('class' => 'establishment_date')) }}
                        {{ Form::text('establishment_date', (!empty($institutionsEdit->establishment_date) ? $institutionsEdit->establishment_date : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter establishment date...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('mobile', 'মোবাইল', array('class' => 'mobile')) }}
                        {{ Form::text('mobile', (!empty($institutionsEdit->mobile) ? $institutionsEdit->mobile : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter mobile...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'ই-মেইল', array('class' => 'email')) }}
                        {{ Form::text('email', (!empty($institutionsEdit->email) ? $institutionsEdit->email : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter email...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('total_teacher', 'মোট শিক্ষক', array('class' => 'total_teacher')) }}
                        {{ Form::text('total_teacher', (!empty($institutionsEdit->total_teacher) ? $institutionsEdit->total_teacher : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter ...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('total_student', 'মোট শিক্ষার্থী', array('class' => 'total_student')) }}
                        {{ Form::text('total_student', (!empty($institutionsEdit->total_student) ? $institutionsEdit->total_student : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter total student...']) }}
                    </div>
                @endslot
            @endcomponent
        </div>

        {{-- Raed Data --}}
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Result Sheet <a href="{{route('admin.common.institutions.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>উপজেলা</th>
                                <th>প্রতিষ্ঠানের নাম</th>
                                <th>Additional Info</th>
                                @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                   <th>Action</th>
                                @endif
                            </tr>
                            <tbody id="xmyTable">
                           
                            @foreach($getInstitutions as $result)
                                <tr class="item" data-id="{{$result->id}}">
                                    <td class="index">{{$result->upozila}} </td>
                                    <td class="index">{{$result->institutions_name}} </td>
                                    <td class="index"> 
                                        <strong>স্ট্যাটাস: </strong> {{$result->status}} <br/>
                                        <strong>ধরণ: </strong> {{$result->type}} <br/>
                                        <strong>স্তর-: </strong> {{$result->level}} <br/>
                                        <strong>প্রতিষ্ঠান প্রধান: </strong> {{$result->institutions_head}} <br/>
                                        <strong>পদবী: </strong> {{$result->position}} <br/>
                                        <strong>প্রতিষ্ঠার তারিখ: </strong> {{$result->establishment_date}} <br/>
                                        <strong>মোবাইল: </strong> {{$result->mobile}} <br/>
                                        <strong>ই-মেইল: </strong> {{$result->email}} <br/>
                                        <strong>মোট শিক্ষক: </strong> {{$result->total_teacher}} <br/>
                                        <strong>মোট শিক্ষার্থী: </strong> {{$result->total_student}} <br/>
                                    </td>             
                                    @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.institutions.edit', $result->id) }}"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this record?')"
                                           href="{{ route('admin.common.institutions.delete',$result->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                           {{ $getInstitutions->links('component.paginator', ['object' => $getInstitutions]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>

    


@endpush