@extends('layouts.app')
@section('title', 'Album')
@section('sub_title', 'all image albums')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($album->id) ? route('admin.common.album.update',$album->id) : route('admin.common.album.store') }}
                @endslot

                @slot('method')
                    @if (!empty($album->id))
                        {{ method_field('put') }}
                    @endif
                @endslot

                @slot('fields')

                    @if (!empty($album->id))
                        {{ Form::hidden('album_id', $album->id, ['required']) }}
                    @endif

                    <div class="form-group">
                        {{ Form::label('album_name', 'Album Name', array('class' => 'album_name')) }}
                        {{ Form::text('album_name', (!empty($album->name) ? $album->name : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter album name...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('album_content', 'Album Content', array('class' => 'album_content')) }}
                        {{ Form::textarea('album_content', (!empty($album->description) ? $album->description : NULL), ['required', 'class' => 'form-control', 'id' => 'swysiwyg', 'placeholder' => 'Enter album content...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('album_css_class', 'Album CSS Class', array('class' => 'album_css_class')) }}
                        {{ Form::text('album_css_class', (!empty($album->cssclass) ? $album->cssclass : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter album css class. Use space for multiple class...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('album_id', 'Album CSS ID', array('class' => 'album_id')) }}
                        {{ Form::text('album_id', (!empty($album->cssid) ? $album->cssid : NULL), ['class' => 'form-control', 'placeholder' => 'Enter album single css ID...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('album_position', 'Album Position', array('class' => 'album_position')) }}

                        @if (!empty($album->id))
                            <?php $position = $album->position; ?>
                        @else
                            <?php
                                $position = \App\Album::orderBy('id', 'desc')->get()->first();
                                $position = !empty($position->id) ? $position->id + 1 : 1;
                            ?>
                        @endif

                        {{ Form::text('album_position', (!empty($position) ? $position : NULL), ['class' => 'form-control', 'placeholder' => 'Enter album name...', 'readonly' => true]) }}
                    </div>
                    <div class="form-group">
                        <?php //dump($album); ?>
                        {{ Form::label('is_active', 'Will it be Active?', array('class' => 'is_active')) }}
                        {{ Form::select('is_active', ['1' => 'True', '0' => 'False'], (!empty($album->is_active) && $album->is_active == 1 ? $album->is_active : 0), ['class' => 'form-control', 'placeholder' => 'Will it be active...']) }}
                    </div>

                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Albums <a href="{{route('admin.common.album.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Serial No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>CSS ID</th>
                                <th>CSS CLASS</th>
                                <th>Position</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <tbody id="myTable">
                            @foreach($albums as $gal)
                                <tr class="item" data-id="{{$gal->id}}">
                                    <td class="index">{{$gal->id}} </td>
                                    <td class="index">{{$gal->name}} </td>
                                    <td>{{ $gal->description }}</td>
                                    <td>{{ $gal->cssid }}</td>
                                    <td>{{ $gal->cssclass }}</td>
                                    <td>{{ $gal->special }}</td>
                                    <td>
                                        @if($gal->is_active??false)
                                            Active
                                        @else
                                            Disable
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.album.index')  }}?id={{$gal->id}}"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-xs btn-danger"
                                           href="{{ route('admin.common.album.delete',$gal->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                            {{ $albums->links('component.paginator', ['object' => $albums]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection



@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });

                {{--jQuery.ajax({--}}
                {{--    data: {--}}
                {{--        "_token": "{{ csrf_token() }}",--}}
                {{--        "item": item--}}
                {{--    },--}}
                {{--    type: 'POST',--}}
                {{--    url: '{{ route('admin.common.album.serialupdate') }}',--}}
                {{--    success: function (result) {--}}
                {{--        // jQuery('#reload_me').load('{{ route('admin.common.album.index') }}' + " " + '#reload_me');--}}
                {{--        location.reload(true);--}}
                {{--        jQuery('#myTable').sortable();--}}
                {{--    }--}}
                {{--});--}}
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>
@endpush