@extends('layouts.app')


@section('title', 'Teams')


@section('sub_title', 'all team member')


@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($team->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($team->id)?route('admin.common.team.update',$team->id):route('admin.common.team.store')}}
                @endslot

                @slot('method')
                    {{-- @if (!empty($team->id))
                        {{ method_field('put') }}
                    @endif --}}
                @endslot

                @slot('fields')

                    <div class="form-group">
                        {{ Form::label('name', 'Name', array('class' => 'name')) }}
                        {{ Form::text('name', $team->name??'', ['required', 'class' => 'form-control', 'placeholder' => 'Enter name...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('position', 'Position', array('class' => 'position')) }}
                        {{ Form::text('position', $team->position??'', ['required', 'class' => 'form-control', 'placeholder' => 'Enter position...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('company', 'Company', array('class' => 'company')) }}
                        {{ Form::text('company', $team->company??'', ['required', 'class' => 'form-control', 'placeholder' => 'Enter company...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('social', 'Social', array('class' => 'uri')) }}
                        {{ Form::textarea('social', $team->social??'', ['required', 'class' => 'form-control', 'placeholder' => 'Enter social...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('description', 'Description', array('class' => 'uri')) }}
                        {{ Form::textarea('description', $team->description??'', ['', 'class' => 'form-control', 'placeholder' => 'Enter social...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('image', 'Image', array('class' => 'image')) }}
                        {{ Form::text('image', $team->image??'', ['required', 'class' => 'form-control', 'placeholder' => 'Enter image...']) }}
                        @if($team->image??false)
                             <img src="{{$team->image}}" width="60"/>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('serial', 'Serial', array('class' => 'serial')) }}
                        {{ Form::number('serial', $team->serial??0, ['required', 'class' => 'form-control', 'min' => 0]) }}
                    </div>

                    <div class="form-group">
                        {{Form::label('status','Status',array('class' => 'status'))}}
                        {!! Form::select('active', [0 => 'Disable', 1 => 'Active'], $team->active??1,['class' => 'form-control']) !!}
                    </div>

                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Teams <a href="{{route('admin.common.team.index')}}" class="btn btn-xs btn-success"><i class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Serial</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th>Company</th>
                                <th>Social</th>                    
                                <th>Is Active</th>
                                <th>Action</th>
                            </tr>

                            @foreach($teams as $teamx)
                                @php 
                                $team = json_decode($teamx->mtvalue);
                                @endphp
                                <tr>
                                   <td>{{$team->serial??0}}</td>
                                   <td><img src="{{$team->image??''}}" width="60"/></td>
                                   <td>{{$team->name??''}}</td>
                                   <td>{{$team->position??''}}</td>
                                   <td>{{$team->company??''}}</td>
                                   <td>{!!($team->social??'')!!}</td>
                                   <td>
                                       @if($teamx->active??false)
                                        Active
                                       @else 
                                         Disable
                                       @endif
                                   </td>
                                   <td>
                                        <a class="btn btn-xs btn-primary"
                                   href="{{ route('admin.common.team.index')  }}?id={{$teamx->id}}">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger"
                                                href="{{ route('admin.common.team.delete',$teamx->id)  }}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                   </td>
                                </tr>
                                <tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection



@push('scripts')



@endpush