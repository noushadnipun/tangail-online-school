@extends('layouts.app')
@section('title', 'ICT Lab')
@section('sub_title', '')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
        @if(Session::has('delete'))
            <div class="col-md-12">
                <div class="callout callout-danger">
                    {{ Session::get('delete') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
        @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($ictlabEdit->id)?route('admin.common.ictlab.update',$ictlabEdit->id):route('admin.common.ictlab.store')}}
                @endslot

                @slot('method')
                    {{-- @if (!empty($team->id))
                        {{ method_field('put') }}
                    @endif --}}
                @endslot

                @slot('fields')
                    @if (!empty($ictlabEdit->id))
                        {{ Form::hidden('ictlab_id', $ictlabEdit->id, ['required']) }}
                    @endif
                    <div class="form-group">
                        {{ Form::label('upozila', 'Upozila', array('class' => 'upozila')) }}
                        {{ Form::text('upozila', (!empty($ictlabEdit->upozila) ? $ictlabEdit->upozila : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter upozila...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('lab_type', 'Type of ict lab', array('class' => 'lab_type')) }}
                        {{ Form::text('lab_type', (!empty($ictlabEdit->lab_type) ? $ictlabEdit->lab_type : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Type of Lab...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('institute_name', 'Institute Name', array('class' => 'institute_name')) }}
                        {{ Form::text('institute_name', (!empty($ictlabEdit->institute_name) ? $ictlabEdit->institute_name : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Name of Institute...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('incharge_teacher', 'Teacher', array('class' => 'incharge_teacher')) }}
                        @php
                            $getTeacherId = \App\Role_user::where('role_id', '4')->get();
                            //dd($getTeacherId);
                        @endphp
                        <select name="incharge_teacher" class="form-control" id="teacher_id">
                            <option selected="selected" disabled value="">Select Teacher...</option>
                            @foreach($getTeacherId as $data)
                                <option value="{{$data->user_id}}" {{(!empty($ictlabEdit->incharge_teacher) && $ictlabEdit->incharge_teacher == $data->user_id ? 'selected' : NULL)}}>
                                    @php
                                        $getTeacherName = \App\User::where('id', $data->user_id)->get();
                                    @endphp
                                    @if(!empty($getTeacherName[0]))
                                        {{ $getTeacherName[0]->name }}
                                    @endif
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{ Form::label('mobile', 'Mobile No', array('class' => 'mobile')) }}
                        {{ Form::text('mobile', (!empty($ictlabEdit->mobile) ? $ictlabEdit->mobile : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Mobile No...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Email', array('class' => 'email')) }}
                        {{ Form::text('email', (!empty($ictlabEdit->email) ? $ictlabEdit->email : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Email...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('laptop', 'laptop', array('class' => 'laptop')) }}
                        {{ Form::text('laptop', (!empty($ictlabEdit->laptop) ? $ictlabEdit->laptop : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter laptop...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('desktop', 'Desktop', array('class' => 'desktop')) }}
                        {{ Form::text('desktop', (!empty($ictlabEdit->desktop) ? $ictlabEdit->desktop : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter desktop...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('projector', 'Projector', array('class' => 'projector')) }}
                        {{ Form::text('projector', (!empty($ictlabEdit->projector) ? $ictlabEdit->projector : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter projector...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('smart_televison', 'Smart Televison', array('class' => 'smart_televison')) }}
                        {{ Form::text('smart_televison', (!empty($ictlabEdit->smart_televison) ? $ictlabEdit->smart_televison : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Smart Televison...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('internet', 'Internet', array('class' => 'internet')) }}
                        {{ Form::text('internet', (!empty($ictlabEdit->internet) ? $ictlabEdit->internet : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Internet...']) }}
                    </div>
                @endslot
            @endcomponent
        </div>

        {{-- Raed Data --}}
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Ict Lab <a href="{{route('admin.common.ictlab.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Upozila</th>
                                <th>Lab Type</th>
                                <th>Name Of Institution</th>
                                <th>Incharge Teacher</th>
                                <th>Additional Info</th>
                                @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                   <th>Action</th>
                                @endif
                            </tr>
                            <tbody id="xmyTable">
                           
                            @foreach($ictlablist as $ictlab)
                                <tr class="item" data-id="{{$ictlab->id}}">
                                    <td class="index">{{$ictlab->upozila}} </td>
                                    <td class="index">{{$ictlab->lab_type}} </td>
                                    <td class="index">{{$ictlab->institute_name}} </td>
                                    <td class="index">
                                        <?php 
                                            $getTeacher = App\User::where('id', $ictlab->incharge_teacher)->get();
                                            if(!empty($getTeacher[0])){
                                                echo $getTeacher[0]->name; 
                                            }
                                        ?>
                                    </td>
                                    <td class="index"> 
                                        <strong>Mobile: </strong> {{$ictlab->mobile}} <br/>
                                        <strong>Email: </strong> {{$ictlab->email}} <br/>
                                        <strong>Laptop: </strong> {{$ictlab->laptop}} <br/>
                                        <strong>Desktop: </strong> {{$ictlab->desktop}} <br/>
                                        <strong>Projector: </strong> {{$ictlab->projector}} <br/>
                                        <strong>Smart Television: </strong> {{$ictlab->smart_televison}} <br/>
                                        <strong>Internet: </strong> {{$ictlab->internet}} <br/>
                                    </td>             
                                    @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.ictlab.edit', $ictlab->id) }}"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this record?')"
                                           href="{{ route('admin.common.ictlab.delete',$ictlab->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                           {{ $ictlablist->links('component.paginator', ['object' => $ictlablist]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>

    


@endpush