@extends('layouts.app')
@section('title', 'Classes')
@section('sub_title', 'all classes')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($class->id) ? route('admin.common.class.update',$class->id) : route('admin.common.class.store') }}
                @endslot

                @slot('method')
                    @if (!empty($class->id))
                        {{ method_field('put') }}
                    @endif
                @endslot

                @slot('fields')

                    @if (!empty($class->id))
                        {{ Form::hidden('class_id', $class->id, ['required']) }}
                    @endif

                    <div class="form-group">
                        {{ Form::label('name', 'Classes Name', array('class' => 'name')) }}
                        {{ Form::text('name', (!empty($class->name) ? $class->name : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter class name...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Class Description', array('class' => 'description')) }}
                        {{ Form::textarea('description', (!empty($class->description) ? $class->description : NULL), ['required', 'class' => 'form-control', 'id' => 'swysiwyg', 'placeholder' => 'Enter class content...']) }}
                    </div>
                    <div class="form-group">                        
                        {{ Form::label('is_active', 'Will it be Active?', array('class' => 'is_active')) }}
                        {{ Form::select('is_active', ['1' => 'True', '0' => 'False'], (!empty($class->is_active) && $class->is_active == 1 ? $class->is_active : 0), ['class' => 'form-control', 'placeholder' => 'Will it be active...']) }}
                    </div>

                @endslot
            @endcomponent
        </div>
        <div class="col-md-9">

            <div class="box box-success">
                <div class="box-header with-border">
                    Classes <a href="{{route('admin.common.class.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Serial No</th>
                                <th>Name</th>
                                <th>Description</th>                                
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <tbody id="myTable">
                            @foreach($classes as $gal)
                                <tr class="item" data-id="{{$gal->id}}">
                                    <td class="index">{{$gal->id}} </td>
                                    <td class="index">{{$gal->name}} </td>
                                    <td>{{ $gal->description }}</td>                                                                        
                                    <td>
                                        @if($gal->is_active??false)
                                            Active
                                        @else
                                            Disable
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.class.index')  }}?id={{$gal->id}}"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-xs btn-danger"
                                           href="{{ route('admin.common.class.delete',$gal->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                            {{ $classes->links('component.paginator', ['object' => $classes]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection



@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>
@endpush