@extends('layouts.app')
@section('title', 'ICT Lab')
@section('sub_title', '')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif
        @if(Session::has('delete'))
            <div class="col-md-12">
                <div class="callout callout-danger">
                    {{ Session::get('delete') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        {{-- Raed Data --}}
        <div class="col-md-12">

            <div class="box box-success">
                <div class="box-header with-border">
                    Traning Session Registered Users <a href="{{route('admin.common.tsession-registration.export_excel')}}" class="btn btn-xs btn-success">Export As Excel</i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Serial No </th>
                                <th>Zilla </th>
                                <th>Upozilla</th>
                                <th>Institute Category</th>
                                <th>Institute Name</th>
                                <th>Teacher Name</th>
                                <th>Additional Info</th>
                                <th>Submitted Date</th>
                                @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                   <th>Action</th>
                                @endif
                            </tr>
                            <tbody id="xmyTable">
                           
                            @foreach($getRegData as $key => $data)
                                <tr class="item" data-id="{{$data->id}}">
                                    <td class="index">{{ $key + $getRegData->firstItem()}} </td>
                                    <td class="index">{{$data->district}} </td>
                                    <td class="index">{{$data->upozilla}} </td>
                                    <td class="index">{{$data->institute_category}} </td>
                                    <td class="index">{{$data->institute_name}} </td>
                                    <td class="index"> {{$data->teacher_name}} </td>
                                    <td class="index"> 
                                        <strong>Designation Name: </strong> {{$data->designation_name}} <br/>
                                        <strong>Gender: </strong> {{$data->gender}} <br/>
                                        <strong>Index Number: </strong> {{$data->index_number}} <br/>
                                        <strong>Mobile: </strong> {{$data->mobile}} <br/>
                                        <strong>Email: </strong> {{$data->email}} <br/>

                                    </td>    
                                    <td class="index"> {{$data->created_at}} </td>         
                                    @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                    <td>
                                        <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure to delete this record?')"
                                           href="{{ route('admin.common.tsession-registration.delete',$data->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                            Total Submited Data : {{ $RegDataCount }}
                           {{ $getRegData->links('component.paginator', ['object' => $getRegData]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>

    


@endpush