@extends('layouts.app')
@section('title', 'Videos')
@section('sub_title', 'all videos')
@section('content')

    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <div class="col-md-3" id="signupForm">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($image->id))
                        schedule_form333
                    @else
                        schedule_form333
                    @endif
                @endslot
                @slot('title')
                    Add/Edit
                @endslot

                @slot('route')
                    {{!empty($video->id) ? route('admin.common.video.update',$video->id) : route('admin.common.video.store') }}
                @endslot

                @slot('method')
                    @if (!empty($video->id))
                        {{ method_field('put') }}
                    @endif
                @endslot$videos

                @slot('fields')

                    @if (!empty($videos->id))
                        {{ Form::hidden('video_id', $video->id, ['required']) }}
                    @endif

                     {{ Form::hidden('user_id', Auth::user()->id, ['required']) }}

                    <div class="form-group">
                        {{ Form::label('video_title', 'Video Title', array('class' => 'video_title')) }}
                        {{ Form::text('video_title', (!empty($video->video_title) ? $video->video_title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Video Title...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('video_link', 'Video Link', array('class' => 'video_link')) }}
                        {{ Form::text('video_link', (!empty($video->video_link) ? $video->video_link : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter Video Link...']) }}
                    </div>
                     <div class="form-group">
                        {{ Form::label('teacher_id', 'Teacher', array('class' => 'teacher_id')) }}
                         @php
                            $getTeacherId = \App\Role_user::where('role_id', '4')->get();
                            //dd($getTeacherId);
                        @endphp
                        <select name="teacher_id" class="form-control" id="teacher_id">
                            <option selected="selected" disabled value="">Select Teacher...</option>
                            @foreach($getTeacherId as $data)
                                <option value="{{$data->user_id}}" {{(!empty($video->teacher_id) && $video->teacher_id == $data->user_id ? 'selected' : NULL)}}>
                                    @php
                                        $getTeacherName = \App\User::where('id', $data->user_id)->get();
                                    @endphp
                                    @if(!empty($getTeacherName[0]))
                                        {{ $getTeacherName[0]->name }}
                                    @endif
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{ Form::label('class', 'Class', array('class' => 'class_id')) }}
                        @php
                            $classId = \App\Classes::get();
                        @endphp
                        <select name="class_id" class="form-control" id="class_id">
                            @foreach($classId as $data)
                                <option value="{{$data->id}}" {{(!empty($video->class_id) && $video->class_id == $data->id ? 'selected' : NULL)}}>
                                    {{$data->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{ Form::label('subject', 'Subject', array('class' => 'subject_id')) }}
                         @php
                            $subjectId = \App\Subjects::get();
                        @endphp
                        <select name="subject_id" class="form-control" id="subject_id">
                            @foreach($subjectId as $data)
                                <option value="{{$data->id}}" {{(!empty($video->subject_id) && $video->subject_id == $data->id ? 'selected' : NULL)}}>
                                    {{$data->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{ Form::label('subject_lession', 'Lession', array('class' => 'subject_lession')) }}
                        {{ Form::select('subject_lession', [
                                    '1' => '১ম পাঠ',
                                    '2' => '২য় পাঠ',
                                    '3' => '৩য় পাঠ',
                                    '4' => '৪র্থ পাঠ',
                                    '5' => '৫ম পাঠ',
                                    '6' => '৬ষ্ঠ পাঠ',
                                    '7' => '৭ম পাঠ',
                                    '8' => '৮ম পাঠ',
                                    '9' => '৯ম পাঠ',
                                    '10' => '১০ম পাঠ',
                                    '11' => '১১তম পাঠ',
                                    '12' => '১২তম পাঠ',
                                    '13' => '১৩তম পাঠ',
                                    '14' => '১৪তম পাঠ',
                                    '15' => '১৫তম পাঠ',
                                    '16' => '১৬তম পাঠ',
                                    '17' => '১৭তম পাঠ',
                                    '18' => '১৮তম পাঠ',
                                    '19' => '১৯তম পাঠ',
                                    '20' => '২০তম পাঠ',
                        ], (!empty($video->subject_lession) && $video->subject_lession ? $video->subject_lession : NULL), ['class' => 'form-control', 'placeholder' => 'Select Subject Path...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('is_active', 'Will it be Active?', array('class' => 'is_active')) }}
                        {{ Form::select('is_active', ['1' => 'True', '0' => 'False'], (!empty($video->is_active) && $video->is_active == 1 ? $video->is_active : 0), ['class' => 'form-control', 'placeholder' => 'Will it be active...']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('video_image', 'Image ID/s', array('class' => 'video_image')) }}
                        @if(!empty($video->video_image))
                            <?php
                            //$im = image_ids($product->images, TRUE, TRUE);
                            $im = $video->video_image;
                            ?>
                            {{ Form::text('video_image', (!empty($im) ? $im : NULL), ['type' => 'text', 'id' => 'image_ids', 'class' => 'form-control', 'placeholder' => 'Enter image IDs...']) }}
                        @else
                            {{ Form::text('video_image', NULL, ['type' => 'text', 'id' => 'image_ids', 'class' => 'form-control', 'placeholder' => 'Enter image IDs...']) }}
                        @endif
                        <small id="show_image_names"></small>

                    </div>
                    <div class="form-group">
                        @if(!empty($post->images))
                            <?php
                            $images = explode(',', $video->video_image);

                            $html = null;
                            foreach ($images as $image) :
                                $img = App\Image::find($image);
                                //dump($img);
                               if($img):
                                $html .= '<img src="' . url($img->icon_size_directory) . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                                //$html .= '<span>' . $img->id . '</span>';
                                $html .= '<a href="' . url('delete_attribute', ['id' => $img->id]) . '">x</a>';
                                //$attributes_p = product_attributes($product, TRUE);
                                endif;
                            endforeach;
                            //die();
                            ?>
                            {!! $html !!}
                        @endif
                    </div>

                @endslot
            @endcomponent
        </div>
        {{-- Image Dropzone --}}
        <div class="col-md-3">
            @component('component.dropzone')
            @endcomponent
            @if(!empty($medias))
                <div class="row" id="reload_me">
                    @foreach($medias as $media)
                        <div class="col-xs-6 col-md-4">
                            <div href="#" class="thumbnail">
                                <img src="{{ url($media->icon_size_directory??'') }}"
                                     class="img-responsive"
                                     style="max-height: 80px; min-height: 80px;"/>
                                <div class="caption text-center">
                                    <p>
                                        <a
                                            href="javascript:void(0);"
                                            data-id="{{ $media->id }}"
                                            data-option="{{ $media->filename }}"
                                            class="btn btn-xs btn-primary"
                                            onclick="get_id(this);"
                                            role="button">
                                            Use
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
        </div>

        {{-- Raed Data --}}
        <div class="col-md-6">

            <div class="box box-success">
                <div class="box-header with-border">
                    Video <a href="{{route('admin.common.video.index')}}" class="btn btn-xs btn-success"><i
                                class="fa fa-plus"></i></a>
                </div>
                <div class="box-body" style="">
                    <div class="box-body table-responsive no-padding" id="reload_me">
                        <table class="table table-hover" id="for_reloader">
                            <tr>
                                <th>Serial No</th>
                                <th>Video Info</th>
                                <th>Status</th>
                                @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                   <th>Action</th>
                                @endif
                            </tr>
                            <tbody id="myTable">
                            @foreach($videos as $gal)
                                <tr class="item" data-id="{{$gal->id}}">
                                    <td class="index">{{$gal->id}} </td>
                                    <td class="index"> 
                                        {{-- Video Title --}}
                                        <strong>Video Title: {{ $gal->video_title }}</strong>
                                        <br>
                                        <!-- teacher Nan-->
                                        @php
                                            $className = \App\User::where('id', $gal->teacher_id)->get();
                                        //dump($className)
                                        @endphp

                                        @if(!empty($className[0]))
                                           <strong>Teacher:</strong>  {{$className[0]->name}}
                                        @endif 
                                        <br>
                                        {{-- Class Name:  --}}
                                        @php
                                            $className = \App\Classes::where('id', $gal->class_id)->get();
                                            //dump($className)
                                        @endphp

                                        @if(!empty($className[0]))
                                            <strong>Class: </strong> {{$className[0]->name}}
                                        @endif 
                                        <br>

                                        {{-- Subject Name --}}

                                        @php
                                            $subjectName = \App\Subjects::where('id', $gal->subject_id)->get();
                                        @endphp

                                        @if(!empty($subjectName[0]))
                                            <strong>Subject: </strong> {{$subjectName[0]->name}}
                                        @endif
                                        <br>

                                        {{-- Subject Path --}}
                                        <strong>Lession: </strong>
                                        @switch($gal->subject_lession )
                                            @case(1)
                                            ১ম পাঠ
                                            @break

                                            @case(2)
                                            ২য় পাঠ
                                            @break

                                            @case(3)
                                            ৩য় পাঠ
                                            @break

                                            @case(4)
                                            ৪র্থ পাঠ
                                            @break

                                            @case(5)
                                            ৫ম পাঠ
                                            @break

                                            @case(6)
                                            ৬ষ্ঠ পাঠ
                                            @break

                                            @case(7)
                                            ৭ম পাঠ
                                            @break

                                            @case(8)
                                            ৮ম পাঠ
                                            @break

                                            @case(9)
                                            ৯ম পাঠ
                                            @break

                                            @case(10)
                                            ১০ম পাঠ
                                            @break

                                            @case(11)
                                            ১১তম পাঠ
                                            @break

                                            @case(12)
                                            ১২তম পাঠ
                                            @break

                                            @case(13)
                                            ১৩তম পাঠ
                                            @break

                                            @case(14)
                                            ১৪তম পাঠ
                                            @break

                                            @case(15)
                                            ১৫তম পাঠ
                                            @break

                                            @case(16)
                                            ১৬তম পাঠ
                                            @break

                                            @case(17)
                                            ১৭তম পাঠ
                                            @break

                                            @case(18)
                                            ১৮তম পাঠ
                                            @break

                                            @case(19)
                                            ১৯তম পাঠ
                                            @break

                                            @case(20)
                                            ২০তম পাঠ
                                            @break

                                        @endswitch
                                        <br>

                                        {{-- Video Link  --}}
                                        <strong>Video Link: </strong>
                                        <input type="text" onclick="this.setSelectionRange(0, this.value.length)" value="{{ $gal->video_link }}">

                                        <br>

                                        {{-- Posted By --}}
                                        @php
                                            $posetedUserName = \App\User::where('id', $gal->user_id)->get();
                                        //dump($className)
                                        @endphp

                                        @if(!empty($posetedUserName[0]))
                                            <strong>Posted By: </strong> {{$posetedUserName[0]->name}}
                                        @endif 

                                    </td>
                                                                       
                                    <td>
                                        @if($gal->is_active??false)
                                            Active
                                        @else
                                            Disable
                                        @endif
                                    </td>
                                    @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                                    <td>
                                        <a class="btn btn-xs btn-primary"
                                           href="{{ route('admin.common.video.index')  }}?id={{$gal->id}}"><i
                                                    class="fa fa-pencil-square-o"></i></a>
                                        <a class="btn btn-xs btn-danger"
                                           href="{{ route('admin.common.video.delete',$gal->id)  }}"><i
                                                    class="fa fa-trash"></i></a>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                        <div class="box-footer clearfix">
                            {{ $videos->links('component.paginator', ['object' => $videos]) }}
                        </div>
                    </div>

                </div>
                <div class="box-footer clearfix">
                </div>
            </div>
        </div>
    </div>


@endsection


@push('scripts')
    <script type="text/javascript">
        jQuery('#myTable').sortable({
            axis: 'y',
            update: function (event, ui) {
                var data = jQuery(".item"),
                    item = []


                jQuery.each(data, function (index, itm) {
                    var autoid = itm.dataset.id;
                    var serial = index + 1;

                    item.push({
                        serial: serial,
                        id: autoid
                    });

                    //console.log(jQuery(itm)[0])
                });
            }
        });
    </script>
    <style>
        td:hover {
            cursor: move;
        }
    </style>

    {{-- dropzzoen --}}
    <script src="{{ URL::asset('public/public/plugins/dropzone.js') }}"></script>
    <script src="{{ URL::asset('public/public/js/dropzone-config.js') }}"></script>
    <script type="text/javascript">
        function get_id(identifier) {
            //alert("data-id:" + jQuery(identifier).data('id') + ", data-option:" + jQuery(identifier).data('option'));


            var dataid = jQuery(identifier).data('id');
            jQuery('#image_ids').val(
                function (i, val) {
                    return val + (!val ? '' : ', ') + dataid;
                });
            var option = jQuery(identifier).data('option');
            jQuery('#show_image_names').html(
                function (i, val) {
                    return val + (!val ? '' : ', ') + option;
                }
            );
        }

        /**
         *
         */
        jQuery(document).ready(function ($) {
            $.noConflict();

            $('#title').blur(function () {
                var m = $(this).val();
                var cute1 = m.toLowerCase().replace(/ /g, '-').replace(/&amp;/g, 'and').replace(/&/g, 'and').replace(/ ./g, 'dec');
                var cute = cute1.replace(/[`~!@#$%^&*()_|+\=?;:'"”,.<>\{\}\[\]\\\/]/gi, '');

                $('#seo_url').val(cute);
            });

        });
    </script>
    <script>
        jQuery(document).ready(function ($) {

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                defaultViewDate: {
                    year: {{\Carbon\Carbon::parse($video->created_at??'')->format('Y')??''}},
                    month: {{\Carbon\Carbon::parse($videos->created_at??'')->format('m')??''}},
                    day: {{\Carbon\Carbon::parse($video->created_at??'')->format('d')??''}}
                }
            })

        } );
    </script>


@endpush