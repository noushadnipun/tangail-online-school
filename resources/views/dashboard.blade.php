@extends('layouts.app')


@section('title', 'Dashboard')
@section('sub_title', 'a quick overview or charts of whole software')

@section('content')
    @if (Auth::user()->isAdmin())
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
{{--            @include('dashboard.dashboard_charts')--}}

            <div class="col-lg-8">
                <div class="box box-primary">
                    <div class="box-body">

                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>
{{--            @include('dashboard.dashboard_quickreports')--}}
        </div>
        

        <div class="row">
            <div class="col-lg-4">
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Quick Navigation <small>Click following links to quick access</small></h3>
                    </div>
                    <div class="box-body">
{{--                        @include('dashboard.dashboard_quicknavigation')--}}
                    </div>
                </div>
            </div>
        </div>

    @endif
@endsection

@section('cusjs')
<script type="text/javascript" src="{{ asset('public/js/googlecharts.js') }}"></script>

<script type="text/javascript">
    // google.charts.load('current', {packages: ['corechart', 'bar']});
    // google.charts.setOnLoadCallback(drawAnnotations);
    //
    // function drawAnnotations() {
    //     var data = new google.visualization.DataTable();
    //     data.addColumn('date', 'Day');
    //     data.addColumn('number', 'Total Ordered Amount');
    //     data.addColumn({type: 'string', role: 'annotation'});
    //     //data.addColumn('number', 'Total Ordered Quantity');
    //     //data.addColumn({type: 'string', role: 'annotation'});
    //
    //     data.addRows([
    //         [new Date('2020-01-01'), 25000, '10'],
    //         [new Date('2020-01-02'), 14500, '20'],
    //         [new Date('2020-01-03'), 55000, '25'],
    //         [new Date('2020-01-04'), 110000, '50'],
    //         [new Date('2020-01-05'), 15000, '10'],
    //     ]);
    //
    //     var options = {
    //         title: 'Date wise sales report',
    //         annotations: {
    //             alwaysOutside: true,
    //             textStyle: {
    //                 fontSize: 14,
    //                 color: '#000',
    //                 auraColor: 'none'
    //             }
    //         },
    //         hAxis: {
    //             title: 'Day of Month',
    //             format: 'M'
    //         },
    //         vAxis: {
    //             title: 'Rating (scale of 1-250000)'
    //         }
    //     };
    //
    //     var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    //     chart.draw(data, options);
    // }
    //
    // google.charts.load('current', {'packages':['corechart']});
    // google.charts.setOnLoadCallback(drawChart);
    //
    // function drawChart() {
    //
    // var data = google.visualization.arrayToDataTable([
    //     ['Successful Order in Total', 'Successful Order in Total'],
    //     ['Placed', 50000],
    //     ['Processing', 150000],
    //     ['Successful', 1500000],
    //     ['Canceled', 200000],
    // ]);
    //
    // var options = {
    //     title: 'Comparison between successful and unsuccessful orders',
    //     colors: ['#00FFF3', '#CEDC1E', 'green', 'red']
    // };
    //
    // var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    //
    // chart.draw(data, options);
    // }
</script>

@endsection