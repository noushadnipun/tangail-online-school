<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div id="map" style="height: 550px"></div>



<script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyC1buC1K_HSqoSaB8_IcC3R9WG3HkLShKk&libraries=geometry&callback=initMap"
        async defer></script>
<script>
    var myposition = {lat: 23.7745973, lng:90.4219106};
    var mycustomer = {lat: 23.7685653, lng: 90.404959};
    var distance = 2000*1.6;
    var map,searchmarker,searchcircle,markerlist;
    var finallist = []

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: myposition,
            mapTypeId: 'terrain',
        });
        var searchmarker = new google.maps.Marker({position: myposition, map: map,title:'Master Point'});
        var searchcircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: new google.maps.LatLng(myposition),
            radius: distance,
            position: myposition,
        });
        var citymap = {
            Gulshan1: {
                title:'badda',
                center: {lat: 23.7585653, lng: 90.404959},
            },
            BoroMoghbazar: {
                title:'Gulshan',
                center: {lat: 23.7585653, lng: 90.404959},
            },
            HollandCentre: {
                title:'RAMPUra',
                center: {lat: 23.7585653, lng: 90.404959},
            },
            TownHall: {
                title:'Moakhali',
                center: {lat: 23.755, lng: 90.404959},
            },
            comilla: {
                title:'comilla',
                center: {lat: 23.75856, lng: 90.404959},
            },
            mohammadpur: {
                title:'mohammadpur',
                center: {lat: 23.756, lng: 90.404959},
            },
            Bongshal: {
                title:'banani',
                center: {lat: 23.7190284, lng: 90.4068079},
            },
        };

        for (var city in citymap) {

            var markerlist = new google.maps.Marker({
                position:citymap[city].center,
                map: map,
                draggable:true,
                animation: google.maps.Animation.DROP,
                title:citymap[city].title,
            });

            if (google.maps.geometry.spherical.computeDistanceBetween(markerlist.getPosition(), searchcircle.getCenter())<= searchcircle.getRadius()) {
                //finallist = markerlist;
                //finallist = markerlist.title;
                finallist.push(markerlist.title)
                //console.log(markerlist.title)
            }
            else {
                console.log(markerlist.title+' is not in area');
            }
        }
        // console.log(finallist)
        //
        //
        // console.log(finallist.getTitle());
        //
        // google.maps.event.addListener(searchcircle, 'dragend', function() {
        //     for (var i = 0; i < citymap[city].length; i++) {
        //         console.log('Marker: ' + randomMarkers[i].marker.title + ', position: ' + randomMarkers[i].marker.getPosition());
        //
        //         if (google.maps.geometry.spherical.computeDistanceBetween(citymap[city].getPosition(), searchcircle.getCenter())<= searchcircle.getRadius()) {
        //             console.log('=> is in searchArea');
        //         } else {
        //             console.log('=> is NOT in searchArea');
        //         }
        //     }
        // });

                // var markerlist = new google.maps.Marker({
                //     position:mycustomer,
                //     map: map,
                //     draggable:true,
                //     animation: google.maps.Animation.DROP,
                // });
                //
                // if (google.maps.geometry.spherical.computeDistanceBetween(markerlist.getPosition(), searchcircle.getCenter())<= searchcircle.getRadius()) {
                //     console.log('=> is in searchArea');
                // } else {
                //     console.log('=> is NOT in searchArea');
                // }

    }
    google.maps.event.addDomListener(window, 'load', initMap);

</script>
</body>
</html>