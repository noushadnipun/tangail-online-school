<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-light">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Common</h3>
            <ul class="control-sidebar-menu">

                <li class="{{ Request::is('menus') ? 'active' : '' }}">
                    <a href="{{ url('menus') }}" class="text-muted">
                        <i class="fa fa-th-list"></i> Menus
                    </a>
                </li>
                <li class="{{ Request::is('widgets') ? 'active' : '' }}">
                    <a href="{{ url('widgets') }}" class="text-muted">
                        <i class="fa fa-book"></i> Widgets
                    </a>
                </li>
                <li class="{{ Request::is('admin/htmlgen') ? 'active' : '' }}">
                    <a href="{{ route('admin.htmlgen') }}" class="text-muted" target="_blank">
                        <i class="fa fa-book"></i> HTML Generator
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <h3 class="control-sidebar-heading">Settings</h3>

            @if (Auth::user()->isAdmin())
                <ul class="control-sidebar-menu">
                    <li class="{{ Request::is('settings') || Request::is('settings') ? 'active' : '' }}">
                        <a href="{{ url('settings') }}" class="text-muted">
                            <i class="fa fa-cogs"></i> <span>Global Settings</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('homesettings') || Request::is('homesettings') ? 'active' : '' }}">
                        <a href="{{ url('homesettings') }}" class="text-muted">
                            <i class="fa fa-cogs"></i> <span>Home Settings</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/routes') || Request::is('admin/routes') ? 'active' : '' }}">
                        <a href="{{ url('admin/routes') }}" class="text-muted">
                            <i class="fa fa-comments-o"></i> Routes
                        </a>
                    </li>

                </ul>
            @endif

        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<div class="control-sidebar-bg"></div>