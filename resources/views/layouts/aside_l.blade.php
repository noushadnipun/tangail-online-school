<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            @if (Auth::user()->isAdmin())

                <li class="treeview {{ Request::is('dashboard') || Request::is('most_sold') || Request::is('never_sold') ? 'active menu-open' : '' }}">
                    <a href="">
                        <i class="fa fa-newspaper-o"></i> <span> Reports Dashboard</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ url('/dashboard') }}">
                                <i class="fa fa-th"></i> <span>Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if (Auth::user()->isAdmin())
                <li class="{{ Request::is('terms') || Request::is('terms') ? 'active' : '' }}">
                    <a href="{{ url('terms') }}">
                        <i class="fa fa-th-list"></i> Categories
                    </a>
                </li>
            @endif
            @if (Auth::user()->isAdmin() || Auth::user()->isStaff())
                <li class="treeview {{ Request::is('admin/users') || Request::is('admin/newsletters')  ? 'active menu-open' : '' }}">
                <li class="treeview {{ Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/newsletters')  ? 'active menu-open' : '' }}">


                <li class="treeview {{ Request::is('admin/users') || Request::is('admin/newsletters')  ? 'active menu-open' : '' }}">
                <li class="treeview {{ Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/newsletters')  ? 'active menu-open' : '' }}">

                    <a href="">
                        <i class="fa fa-users"></i> <span>Contacts Management</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        @if (Auth::user()->isAdmin() || Auth::user()->isStaff())
                            <li class="{{ Request::is('admin/users') ? 'active' : '' }}">
                                <a href="{{ route('admin.users.index') }}">
                                    <i class="fa fa-users"></i> Users
                                </a>
                            </li>
                            <li class="{{ Request::is('admin/users/create') || Request::is('admin/users/create') ? 'active' : '' }}">
                                <a href="{{ route('admin.users.create') }}">
                                    <i class="fa fa-user-plus"></i> Add user
                                </a>
                            </li>
                            <li class="divider"></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if (Auth::user()->isAdmin() || Auth::user()->isStaff())
                <li class="{{ Request::is('admin/medias') || Request::is('admin/medias') ? 'active' : '' }}">
                    <a href="{{ route('admin.medias.index') }}">
                        <i class="fa fa-file-o"></i> Files
                    </a>
                </li>
            @endif

            @if (Auth::user()->isAdmin())
                <li class="{{ Request::is('admin/posts') || Request::is('admin/posts') ? 'active' : '' }}">
                    <a href="{{ route('admin.posts.index') }}">
                        <i class="fa fa-newspaper-o"></i> Posts
                    </a>
                </li>
            @endif

            @if (Auth::user()->isAdmin())
                <li class="{{ Request::is('admin/pages') || Request::is('admin/pages') ? 'active' : '' }}">
                    <a href="{{ route('admin.pages.index') }}">
                        <i class="fa fa-newspaper-o"></i> Pages
                    </a>
                </li>
            @endif

            @if (Auth::user()->isAdmin())

                <li class="treeview {{ Request::is('admin/sliders*') ||
                    Request::is('admin/common/album*') ||
                    Request::is('admin/common/gallery*') ? 'active menu-open' : '' }}">
                    <a href="">
                        <i class="fa fa-television"></i> <span> Uncommon Things</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/sliders') || Request::is('admin/sliders') ? 'active' : '' }}">
                                <a href="{{route('admin.sliders') }}">
                                    <i class="fa fa-sliders"></i> Sliders
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/common/album') || Request::is('admin/common/album') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.album.index') }}">
                                    <i class="fa fa-sliders"></i> Albums
                                </a>
                            </li>
                        @endif

                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/common/gallery') || Request::is('admin/common/gallery') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.gallery.index') }}">
                                    <i class="fa fa-sliders"></i> Galleries
                                </a>
                            </li>
                        @endif

                    </ul>
                </li>
            @endif

            @if (Auth::user()->isAdmin() || Auth::user()->isEditor() || Auth::user()->isStaff() || Auth::user()->isTaecher())

                <li class="treeview {{ Request::is('admin/classes*') ? 'active menu-open' : '' }}">
                    <a href="">
                        <i class="fa fa-television"></i> <span> Administration</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor() || Auth::user()->isTaecher())
                            <li class="{{ Request::is('admin/classes') || Request::is('admin/classes') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.class.index') }}">
                                    <i class="fa fa-sliders"></i> Classes
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor() || Auth::user()->isTaecher())
                            <li class="{{ Request::is('admin/subjects') || Request::is('admin/subjects') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.subject.index') }}">
                                    <i class="fa fa-sliders"></i> Subjects
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor() || Auth::user()->isStaff() || Auth::user()->isTaecher())
                            <li class="{{ Request::is('admin/videos') || Request::is('admin/videos') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.video.index') }}">
                                    <i class="fa fa-sliders"></i> Videos
                                </a>
                            </li>
                        @endif

                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/common/ictlab') || Request::is('admin/common/ictlab') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.ictlab.index') }}">
                                    <i class="fa fa-sliders"></i> ICT Lab
                                </a>
                            </li>
                        @endif

                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/common/exam-results') || Request::is('admin/common/exam-results') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.exam-results.index') }}">
                                    <i class="fa fa-sliders"></i> Results
                                </a>
                            </li>
                        @endif

                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/common/institutions') || Request::is('admin/common/institutions') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.institutions.index') }}">
                                    <i class="fa fa-sliders"></i> Institutions
                                </a>
                            </li>
                        @endif

                        @if (Auth::user()->isAdmin() || Auth::user()->isEditor())
                            <li class="{{ Request::is('admin/common/tsession-registration') || Request::is('admin/common/tsession-registration') ? 'active' : '' }}">
                                <a href="{{ route('admin.common.tsession-registration.index') }}">
                                    <i class="fa fa-sliders"></i> Traning Session Registered Users
                                </a>
                            </li>
                        @endif
                        
                    </ul>
                </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>