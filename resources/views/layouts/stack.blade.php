@push('scripts')
    <!-- jQuery 3 -->
    <script src="{{ URL::asset('public/cssc/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->

    <script src="{{ URL::asset('public/cssc/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ URL::asset('public/cssc/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts 
    <script src="{{ URL::asset('public/cssc/raphael/raphael.min.js') }}"></script>
    <script src="{{ URL::asset('public/cssc/morris.js/morris.min.js') }}"></script>-->
    <!-- Sparkline -->
    <script src="{{ URL::asset('public/cssc/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ URL::asset('public/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ URL::asset('public/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ URL::asset('public/cssc/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ URL::asset('public/cssc/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/cssc/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ URL::asset('public/cssc/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ URL::asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ URL::asset('public/cssc/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('public/cssc/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL::asset('public/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="{{ URL::asset('public/js/pages/dashboard2.js') }}"></script>-->
    <!-- AdminLTE for demo purposes -->
    <!--<script src="{{ URL::asset('public/js/demo.js') }}"></script>-->
@endpush