<link rel="stylesheet" href="{{ URL::asset('public/cssc/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ URL::asset('public/cssc/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ URL::asset('public/cssc/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ URL::asset('public/public/css/AdminLTE.min.css') }}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ URL::asset('public/public/css/skins/_all-skins.min.css') }}">
<!-- Morris chart -->
<!--<link rel="stylesheet" href="{{ URL::asset('public/cssc/morris.js/morris.css') }}">-->
<!-- jvectormap -->
<link rel="stylesheet" href="{{ URL::asset('public/cssc/jvectormap/jquery-jvectormap.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ URL::asset('public/cssc/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<!-- Color Picker -->
<link rel="stylesheet" href="{{ URL::asset('public/cssc/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ URL::asset('public/cssc/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/public/css/dropzone.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/public/css/tritiyo_app.css') }}">


<!-- bootstrap wysihtml5 - text editor || Wysiwyg Editor Plugin -->
{{--<link rel="stylesheet" href="{{ URL::asset('public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">--}}
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">--}}
<link href="{{ URL::asset('public/public/css/ui/trumbowyg.min.css') }}" rel="stylesheet">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements Mediaedia queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">