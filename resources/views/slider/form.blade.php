@extends('layouts.app')

@section('title', 'Slider')
@section('sub_title', 'slider add or modification form')
@section('content')
    <div class="row">
        @if(Session::has('success'))
            <div class="col-md-12">
                <div class="callout callout-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        @endif

        {{--@endif--}}
        @if($errors->any())
            <div class="col-md-12">
                <div class="callout callout-danger">
                    <h4>Warning!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="col-md-8">
            @component('component.form')
                @slot('form_id')
                    @if (!empty($user->id))
                        slider_forms
                    @else
                        slider_form
                    @endif
                @endslot
                @slot('title')
                    @if (!empty($slider->id))
                        Edit slider
                    @else
                        Add a new slider
                    @endif

                @endslot

                @slot('route')
                    @if (!empty($slider->id))
                            {{route('admin.slider_update_save',$slider->id)}}
                    @else
                        {{route('admin.slider_save')}}
                    @endif
                @endslot

                @slot('fields')
                    <div class="form-group">
                     
                        {{ Form::label('title', 'Title', array('class' => 'title')) }}
                        {{ Form::text('title', (!empty($slider->title) ? $slider->title : NULL), ['required', 'class' => 'form-control', 'placeholder' => 'Enter title...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Description', array('class' => 'description')) }}
                        {{ Form::textarea('content', (!empty($slider->content) ? $slider->content : NULL), ['required', 'class' => 'form-control', 'id' => 'wysiwyg', 'placeholder' => 'Enter details content...']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('images', 'Image ID/s', array('class' => 'images')) }}
                        @if(!empty($slider->images))
                            <?php
                            //$im = image_ids($product->images, TRUE, TRUE);
                            $im = $slider->images;
                            ?>
                            {{ Form::text('images', (!empty($im) ? $im : NULL), ['type' => 'text', 'id' => 'image_ids', 'class' => 'form-control', 'placeholder' => 'Enter image IDs...']) }}
                        @else
                            {{ Form::text('images', NULL, ['type' => 'text', 'id' => 'image_ids', 'class' => 'form-control', 'placeholder' => 'Enter image IDs...']) }}
                        @endif
                        <small id="show_image_names"></small>

                    </div>
                    <div class="form-group">
                        @if(!empty($slider->images))
                            <?php
                            $images = explode(',', $slider->images);

                            $html = null;
                            foreach ($images as $image) :
                                $img = App\Image::find($image);
                             if($img):
                                //dump($img);
                                $html .= '<img src="' . url($img->icon_size_directory??'') . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                                //$html .= '<span>' . $img->id . '</span>';
                                $html .= '<a href="' . url('delete_attribute', ['id' => $img->id]) . '">x</a>';
                             endif;
                                //$attributes_p = product_attributes($product, TRUE);
                            endforeach;
                            //die();
                            ?>
                            {!! $html !!}
                        @endif
                    </div>            
                @endslot
            @endcomponent
        </div>
        <div class="col-md-4">
            @component('component.dropzone')
            @endcomponent
            @if(!empty($medias))
                <div class="row" id="reload_me">
                    @foreach($medias as $media)
                        <div class="col-xs-6 col-md-4">
                            <div href="#" class="thumbnail">
                                <img src="{{url($media->icon_size_directory??'')}}"
                                     class="img-responsive"
                                     style="max-height: 80px; min-height: 80px;"/>
                                <div class="caption text-center">
                                    <p>
                                        <a
                                                href="javascript:void(0);"
                                                data-id="{{ $media->id }}"
                                                data-option="{{ $media->filename }}"
                                                class="btn btn-xs btn-primary"
                                                onclick="get_id(this);"
                                                role="button">
                                            Use
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ URL::asset('/public/public/plugins/dropzone.js') }}"></script>
    <script src="{{ URL::asset('/public/public/js/dropzone-config.js') }}"></script>
    <script type="text/javascript">
        function get_id(identifier) {
            //alert("data-id:" + jQuery(identifier).data('id') + ", data-option:" + jQuery(identifier).data('option'));


            var dataid = jQuery(identifier).data('id');
            jQuery('#image_ids').val(
                function (i, val) {
                    return val + (!val ? '' : ', ') + dataid;
                });
            var option = jQuery(identifier).data('option');
            jQuery('#show_image_names').html(
                function (i, val) {
                    return val + (!val ? '' : ', ') + option;
                }
            );
        }

        /**
         *
         */
        jQuery(document).ready(function ($) {
            $.noConflict();

            $('#title').blur(function () {
                var m = $(this).val();
                var cute1 = m.toLowerCase().replace(/ /g, '-').replace(/&amp;/g, 'and').replace(/&/g, 'and').replace(/ ./g, 'dec');
                var cute = cute1.replace(/[`~!@#$%^&*()_|+\=?;:'"”,.<>\{\}\[\]\\\/]/gi, '');

                $('#seo_url').val(cute);
            });

        });
    </script>
@endpush
