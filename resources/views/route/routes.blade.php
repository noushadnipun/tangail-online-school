@extends('layouts.app')

@section('title', 'Routes')
@section('sub_title', 'list of all routes')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Routes
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding" id="reload_me">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <th>Route URI</th>
                            <th>Route Name</th>
                            <th></th>
                            <th>Change Role Group</th>
                            <th>Current Group</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th>Date Updated</th>
                        </tr>
                        @php
                            $i = 1;
                        @endphp
                        @foreach($routes as $route)
                            @if (strpos($route->uri, 'api') === false && !exclude_unwanter_permission_link($route->uri))
                                @php
                                    $stored_route = \App\Route::where(['route_hash' => md5($route->getName())])->get()->first();
                                @endphp
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $route->uri }}</td>
                                    <td>{{ $route->getName() }}</td>
                                    <td><a class="btn btn-xs btn-info"> >> </a></td>
                                    <td>
                                        <a href="javascript:void(0)"
                                           class="btn btn-xs btn-primary role_changer"
                                           data-id="{{ !empty($stored_route->id) ? $stored_route->id : ''  }}"
                                           data-hash="{{ !empty($stored_route->route_hash) ? $stored_route->route_hash : md5($route->getName()) }}"
                                           data-name="{{ !empty($stored_route->route_name) ? $stored_route->route_name : $route->getName()  }}"
                                           data-uri="{{ !empty($stored_route->route_uri) ? $stored_route->route_uri : $route->uri  }}"
                                           data-role="{{ !empty($stored_route->in_group) ? $stored_route->in_group : 'admin' }}"
                                           data-status="{{ !empty($stored_route->status) ? $stored_route->status : 1 }}"
                                           data-toggle="modal"
                                           data-target="#modal-role-change"
                                        >
                                            Edit User Role Group
                                        </a>
                                    </td>
                                    <td>{{ !empty($stored_route->in_group) ? $stored_route->in_group : '' }}</td>
                                    <td>{{ !empty($stored_route->status) ? $stored_route->status : '0' }}</td>
                                    <td>{{ !empty($stored_route->created_at) ? $stored_route->created_at : '' }}</td>
                                    <td>{{ !empty($stored_route->created_at) ? $stored_route->created_at : '' }}</td>
                                </tr>
                            @endif
                            @php
                                $i++;
                            @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <form action="{{ route('admin.route.role_modify') }}" method="POST">
        <div class="modal fade" id="modal-role-change">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Change This Route for Proper Role</h4>
                    </div>
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="" id="id">
                        <input type="hidden" name="hash" value="" id="hash">
                        <input type="hidden" name="name" value="" id="name">
                        <input type="hidden" name="uri" value="" id="uri">
                        <div class="form-group">
                            <label>Change Role</label>
                            <select name="in_group" class="form-control" id="role">
                                <option value="admin">admin</option>
                                <option value="manager">manager</option>
                                <option value="member">member</option>
                                <option value="employee">employee</option>
                                <option value="vendor">vendor</option>
                                <option value="applicant">applicant</option>
                                <option value="showroom">showroom</option>
                                <option value="affiliator">affiliator</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Is Active?</label>
                            <div class="checkbox-inline">
                                <input type="radio" name="status" value="1" id="status"/> Yes (1)
                            </div>
                            <div class="checkbox-inline">
                                <input type="radio" name="status" value="0" id="status"/> No (0)
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </form>
@endsection


@push('scripts')
    <script>
        jQuery(document).ready(function ($) {
            $(".role_changer").click(function () {
                //alert(this.dataset.role);
                $('#id').val(this.dataset.id);
                $('#hash').val(this.dataset.hash);
                $('#name').val(this.dataset.name);
                $('#uri').val(this.dataset.uri);

                var roles = [
                    'admin',
                    'manager',
                    'member',
                    'employee',
                    'vendor',
                    'applicant',
                    'showroom',
                    'affiliator'
                ];
                var currentval = this.dataset.role;
                $('#role option').each(function () {
                    if (this.value == currentval) {
                        $('#role').val(currentval);
                    }
                    //alert(this.value);
                });

                var preSelect = this.dataset.status;
                $(function () {
                    $('input[name=status][value=' + preSelect + ']').prop('checked', true);
                });
            })
        })
    </script>
@endpush