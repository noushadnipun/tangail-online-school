<h4 class="text-center text-blue">ADDITION</h4>

<a class="btn btn-app" href="{{ url('add_product') }}">
    <i class="fa fa-plus-circle"></i> Add Product
</a>

<a class="btn btn-app" href="{{ route('admin.users.create') }}">
    <i class="fa fa-plus-circle"></i> Add User
</a>

<a class="btn btn-app" href="{{ route('admin.posts.create') }}">
    <i class="fa fa-plus-circle"></i> Add Post
</a>

<a class="btn btn-app" href="{{ route('admin.pages.create') }}">
    <i class="fa fa-plus-circle"></i> Add Page
</a>

<a class="btn btn-app" href="{{ url('settings') }}">
    <i class="fa fa-industry"></i> Site
</a>
<a class="btn btn-app" href="{{ url('settings') }}">
    <i class="fa fa-industry"></i> Administrator
</a>
<a class="btn btn-app" href="{{ url('settings') }}">
    <i class="fa fa-code"></i> SEO Setting
</a>
<a class="btn btn-app" href="{{ url('settings') }}">
    <i class="fa fa-facebook"></i> FB Setting
</a>
<a class="btn btn-app" href="{{ url('settings') }}">
    <i class="fa fa-google"></i> Google Setting
</a>



<h4 class="text-center text-blue">REPORTS</h4>
@php
$productCount = \App\Product::count();
$orderMastCount = \App\OrdersMaster::count();
@endphp
<a class="btn btn-app" href="{{ url('products') }}">
    <span class="badge bg-green">{{$productCount}}</span>
    <i class="fa fa-barcode"></i> Products
</a>
<a class="btn btn-app" href="{{ url('orders') }}">
    <span class="badge bg-teal">{{$orderMastCount}}</span>
    <i class="fa fa-inbox"></i> Orders
</a>


@php
$usersCount = \App\User::count();
@endphp

<a class="btn btn-app" href="{{ route('admin.users.index') }}">
    <span class="badge bg-purple">{{$usersCount}}</span>
    <i class="fa fa-users"></i> Users
</a>
<a class="btn btn-app" href="{{ route('admin.posts.index') }}">
    <i class="fa fa-list-alt"></i> Posts
</a>
@php
$termsCount = \App\Term::count();
$widgetCount = \App\Dashboard::count();
@endphp
<a class="btn btn-app" href="{{ route('terms') }}">
    <span class="badge bg-green">{{$termsCount}}</span>
    <i class="fa fa-list"></i> Categories
</a>
<a class="btn btn-app" href="{{ route('widgets') }}">
    <span class="badge bg-red">{{$widgetCount}}</span>
    <i class="fa fa-gears"></i> Widgets
</a>

@php

$pagesCount = \App\Page::count();
@endphp                            
<a class="btn btn-app" href="{{ route('admin.pages.index') }}">
    <span class="badge bg-green">{{$pagesCount}}</span>
    <i class="fa fa-list-alt"></i> Pages
</a>

@php 
$mediasCount = \App\Image::count();
@endphp
<a class="btn btn-app" href="{{ route('admin.medias.index') }}">
    <span class="badge bg-green">{{$mediasCount}}</span>
    <i class="fa fa-list-alt"></i> Medias
</a>

<h4 class="text-center text-blue">FORGE CACHE</h4>                    
<a class="btn btn-app" href="{{ route('clear-cache') }}">                        
    <i class="fa fa-times-circle"></i> Clear Cache
</a>
<a class="btn btn-app" href="{{ route('optimize') }}">                        
    <i class="fa fa-times-circle"></i> Optimize
</a>
<a class="btn btn-app" href="{{ route('route-clear') }}">                        
    <i class="fa fa-times-circle"></i> Route Clear
</a>
<a class="btn btn-app" href="{{ route('config-clear') }}">                        
    <i class="fa fa-times-circle"></i> Config Clear
</a>
<a class="btn btn-app" href="{{ route('config-cache') }}">                        
    <i class="fa fa-times-circle"></i> Config Cache
</a>                    