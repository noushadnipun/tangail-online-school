<div class="col-lg-8">
    <div class="box box-primary">                    
        <div class="box-body">
            <div id="chart_div"></div>
            <div id="piechart" style="width: 500px; height: 150px;"></div>
        </div>
        <div class="box-footer">
            <a href="{{ route('orders') }}">All Orders</a>
        </div>
    </div>
</div>