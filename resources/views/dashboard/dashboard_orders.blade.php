<table class="table table-hover">
    <thead>
    <tr>
        <th title="Order ID">OID</th>
        <th title="Customer Details">Customer</th>
        <th title="Shipment Information">Shipment Segments</th>
        <th title="Order Information">Order Infos</th>
        <th title="Total Purchased Amount">Amount</th>                                    
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $line)
        @php
            //dump($line);
            //$image_info  = App\ProductImages::Where(['main_pid' => $line->product_id])->get()->first();
            // $vendor  = App\User::Where(['id' => $line->vendor_id])->get()->first();
            $order_details  = App\OrdersDetail::Where(['order_random' => $line->order_random])->get();
            $total = 0;
                if(auth()->user()->isVendor()){
                    foreach ($order_details as $cod){
                    $vendor  = App\User::Where(['id' => $cod->vendor_id])->get()->first();
                        if(auth()->user()->id == $vendor->id) {
                            $total += $cod->local_purchase_price;
                        }
                    }
                } else {

                $total +=  $line->total_amount;

                }
        @endphp
        <tr>
            <td>{{ $line->id }}</td>
            <td>
                <small>
                    <b>{{ $line->customer_name }}</b><br/>
                    {!! nl2br($line->address) !!}<br/>
                    <b>P# </b>{{ $line->phone }}
                    <b>EP# </b>                                                
                </small>
            </td>
            <td>
                @php
                    $od_mast = \App\OrdersMaster::find($line->id);
                @endphp
                @foreach($od_mast->orderShipments as $dist)
                    <div class="bg-gray disabled color-palette" style="padding: 2px; margin-bottom: 2px;">
                        <small>
                            <strong>Shipment</strong> - {{ $dist->serial }},
                        </small>
                        <small>
                            <strong>Qty</strong> - {{ $dist->orderDetails->count() }},
                        </small>
                        <small>
                            <strong>Showroom</strong> - {{$dist->distributor->title??''}}
                        </small>
                    </div>
                @endforeach
            </td>
            <td>
                <b title="Payment Stauts">PS:</b> {{ $line->payment_term_status }}<br/>
                <b title="Order Date">OD:</b> {{ $line->created_at }}<br/>
                <b title="Payment Method">PM:</b> {{ $line->payment_method }} <br/>
            </td>
            <td>
                <div title="Total">
                    <b>Total : </b> ৳ {{ number_format($total) }}
                </div>
                @if(!(auth()->user()->isVendor()))
                    <div title="Grand Total">
                        <b>Grand Total : </b> ৳ {{  number_format($line->grand_total) }}
                    </div>
                @endif
            </td>                                        
        </tr>
    @endforeach
    </tbody>
</table>