<div class="col-lg-4">
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    @php
                        $total_product = App\Product::get()->count();
                        //dd($products);
                    @endphp
                    <h3>{{ $total_product }}</h3>
                    <p>Products</p>
                </div>
                <div class="icon">
                    <i class="fa fa-product-hunt"></i>
                </div>
                <a href="javascript:void(0)" class="small-box-footer">
                    Uploaded Products
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    @php
                        $total_orders = App\OrdersMaster::get()->count();
                        //dd($products);
                    @endphp
                    <h3>{{ $total_orders }}</h3>

                    <p>Orders</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="javascript:void(0)" class="small-box-footer">
                    Orders Till Today
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-light-blue">
                <div class="inner">
                    @php
                        $vendorApproved = App\Role::find(5)->users()->where('users.vendor_status',1)->count();
                        $vendorNotApproved = App\Role::find(5)->users()->where('users.vendor_status',null)->count();
                        //dd($products);
                    @endphp
                    <h3>{{$vendorApproved}}<p class="badge">{{ $vendorNotApproved }} new</p></h3>
                    <p>Vendors</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <a href="{{route('admin.users.index')}}?role=10" class="small-box-footer">
                    All Vendors
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    @php                        
                        $showrooms = App\Showroom::count();
                        //dd($showrooms);
                    @endphp
                    <h3>{{$showrooms}}</h3>
                    <p>Showrooms</p>
                </div>
                <div class="icon">
                    <i class="fa fa-shopping-cart"></i>
                </div>                
                <a href="{{route('admin.showroom.index')}}?role=10" class="small-box-footer">
                    All Showrooms
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>

        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <!-- @php
                        $query = DB::table('products')
                        ->leftJoin('orders_detail', 'products.id', '=', 'orders_detail.product_id')
                        ->whereRaw('orders_detail.product_id IS NULL')
                        ->get();
                        $never_sold = $query->count();
                    @endphp -->
                    <h3>303</h3>
                    <p>Most Sold Items</p>
                </div>
                <div class="icon">
                    <i class="fa  fa-hand-o-up"></i>
                </div>                
                <a href="{{ route('most_sold') }}" class="small-box-footer">
                    Most Sold Items
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>  


        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    @php
                        $query = DB::table('products')
                        ->leftJoin('orders_detail', 'products.id', '=', 'orders_detail.product_id')
                        ->whereRaw('orders_detail.product_id IS NULL')
                        ->get();
                        $never_sold = $query->count();
                    @endphp
                    <h3>{{$never_sold}}</h3>
                    <p>Never Sold Items</p>
                </div>
                <div class="icon">
                    <i class="fa  fa-hand-o-down"></i>
                </div>                
                <a href="{{ route('never_sold') }}" class="small-box-footer">
                    Never Sold Items
                    <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>                
    </div>
</div>
