<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> All Credentials</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 15, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">

                    <div class="jumbotron text-left">
                        <h3>Users Seed</h3>
                        <pre>php artisan db:seed</pre>
                        <p>
                            User Email: admin@tritiyo.com
                            <br/>User Password: secret
                            <br/><br/>User Email: employee@tritiyo.com
                            <br/>User Password: secret
                            <br/><br/>User Email: manager@tritiyo.com
                            <br/>User Password: secret
                        </p>
                    </div>
                </div><!--//doc-content-->                
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>