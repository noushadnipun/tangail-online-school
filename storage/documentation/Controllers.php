<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Controllers</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 25, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section class="doc-section" id="download-section">
                        <h2 class="section-title">List of controllers</h2>
                        <div class="section-block">
                            <p>Controllers are included all the individual part of NewPORTAL.</p>
                            <br/>
                            <h6>Core Controllers [application\core\]</h6>
                            <ul class="list">
                                <li>
                                    core\
                                    <ul>
                                        <li><h6>MY_Controller</h6>
                                            <p>MY_Controller extends CodeIgniter CI_Controller. <i>This controllers actually is the main controllers for the portal.</i>
                                                <br>We extends this controllers to all other controllers of portal. We using table names from this controllers along with some other common functionality such as-</p>
                                            <p>
                                        </li>
                                    </ul>
                                    Mainly, we listed all the common data pulling or inserting functionality globally on this controllers which we using all over the controllers when needed.
                                    </p>
                                </li>
                            </ul>
                            <hr/>
                            <br/>
                            <h6>Lists of Controllers [application\controllers\]</h6>
                            <ul class="list">                                
                                <li>np_lib
                                    <ul>
                                        <li class="text-danger">
                                            <h6>Ajaxloadmore</h6>
                                            <p>Possibly we can remove this functionality now. Because, we are not using any load more functionality on portal, indeed. This library designed for the load more ajax handler.</p>
                                        </li>
                                        <li>
                                            <h6>Community</h6>
                                            <p>This library helps to handle the search box of portal. Our NP search box pull data from our subscribers portal and view them as community section of NP.</p>
                                        </li>
                                        <li>
                                            <h6>Crm</h6>
                                            <p>This library helps to handle all the contacts relationship database management of the portal</p>
                                        </li>
                                        <li>
                                            <h6>Email</h6>
                                            <p>This library helps to handle all the email activity of the portal. For example - Bulk Emailing, email template design etc.</p>
                                        </li>
                                        <li>
                                            <h6>Marketing_tools</h6>
                                            <p>This library helps to handle all the marketing tools, avatar and demographics under edit profile section.</p>
                                        </li>
                                        <li>
                                            <h6>Profilemodifier</h6>
                                            <p>This library helps to handle all the edit profile configuration. Such as - basic information, bio, web links, biz card etc.</p>
                                        </li>
                                        <li>
                                            <h6>Single</h6>
                                            <p>This library helps to handle all single view of our modules. Such as - jobs, events, crowdfunding, stores, press release etc.</p>
                                        </li>
                                        <li>
                                            <h6>User</h6>
                                            <p>This library developed to handle user home page/ profile page along with concurrent status posting.</p>
                                        </li>
                                    </ul>
                                </li>

                                <li class="text-danger">
                                    <h6>Adaptive_payments</h6>
                                    <p>May be we can remove this controller. But, before remove it we have to re-check properly.</p>
                                </li>
                                <li class="text-danger">
                                    <h6>Administrator</h6>
                                    <p>Previously, we created some administrator view to check subscribers, reports or analytics on this view. We no longer using this library, I guess. But, we have to re check before delete this file.</p>
                                </li>
                                <li>
                                    <h6>Advertisement</h6>
                                    <p>This library built to manage the advertisement with NP</p>
                                </li>
                                <li>
                                    <h6>Auth</h6>
                                    <p>ION_AUTH (third party) controller to manage user authentication.</p>
                                </li>
                                <li>
                                    <h6>Business</h6>
                                    <p>Business controller using to post businesses, modify businesses and other businesses related functionality.</p>
                                </li>
                                <li>
                                    <h6>Coaching</h6>
                                    <p>Coaching controller using to post Coaching, modify Coaching and other Coaching related functionality.</p>
                                </li>
                                <li>
                                    <h6>Crm</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Crowdfunding</h6>
                                    <p>Crowdfunding controller using to post Crowdfunding, modify Crowdfunding and other Crowdfunding related functionality.</p>
                                </li>
                                <li>
                                    <h6>Dashboard</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Email</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Events</h6>
                                    <p>Events controller using to post Events, modify Events and other Events related functionality.</p>
                                </li>
                                <li>
                                    <h6>External</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Greendirectory</h6>
                                    <p>Greendirectory controller using to post Greendirectory, modify Greendirectory and other Greendirectory related functionality.</p>
                                </li>
                                <li>
                                    <h6>Groupchat</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Hauth</h6>
                                    <p>Hauth (third party) along with hybridauthlib.</p>
                                </li>
                                <li>
                                    <h6>Jobs</h6>
                                    <p>Jobs controller using to post Jobs, modify Jobs and other Jobs related functionality.</p>
                                </li>
                                <li>
                                    <h6>Payflow</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Payments_pro</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Paypal</h6>
                                    <p>This is very very important controller which contains the ipn() method to handle multiple Instant Payment Notification from paypal server. This IPN handler work for crowdfuding donation, quick donation, mass payment.</p>
                                </li>
                                <li>
                                    <h6>Press_release</h6>
                                    <p>Press release controller using to post Press release, modify Press release and other Press release related functionality.</p>
                                </li>
                                <li>
                                    <h6>Products</h6>
                                    <p>Products controller using to post Products, modify Products and other Products related functionality.</p>
                                </li>
                                <li class="text-danger">
                                    <h6>Productss</h6>
                                    <p>This library has one method called buy(). But, not sure whether this method using somewhere or not. I will check it once again.</p>
                                </li>
                                <li>
                                    <h6>Profileeditor</h6>
                                    <p>This library also helps to handle the edit profile pages. We have another edit profile page handling controller under <b>\np_sh\</b> named <b>profilemodifier</b>. Here profileeditor is old one, profilemodifier is new one. I was migrating/moving functionality under profilemodifier which is not complete yet.</p>
                                </li>
                                <li>
                                    <h6>Socialmedia</h6>
                                    <p>This library probably an unnecessary library. But, it has been created to handle the social media posting and manage users/subscribers social media configuration.</p>
                                </li>
                                <li>
                                    <h6>Staticpages</h6>
                                    <p>This library created to handle the statics pages of NP. Such as - privacy policy, faq, training etc.</p>
                                </li>
                                <li>
                                    <h6>Upload</h6>
                                    <p>This library helps to upload files on different modules. Such as - Jobs, Events, Advertisement, Profile Picture.</p>
                                </li>
                                <li>
                                    <h6>Webinars</h6>
                                    <p>Webinars controller using to post Webinars, modify Webinars and other Webinars related functionality.</p>
                                </li>
                                <li class="text-danger">
                                    <h6>Adaptive_payments</h6>
                                    <p></p>
                                </li>
                            </ul>

                        </div>
                    </section>
                </div><!--//doc-content-->
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>