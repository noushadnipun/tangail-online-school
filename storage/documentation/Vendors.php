<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Vendors</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 21, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section class="doc-section" id="download-section">
                        <h2 class="section-title">List of vendors</h2>
                        <div class="section-block">
                            <p>Vendors are handles all the individual data base insert and pulling functionality of NewPORTAL.</p>
                            <br/>
                            <h6>Lists of vendors [application\vendors\]</h6>
                            <ul class="list">
                                
                                <li>
                                    <h6>composer</h6>
                                    <p>
                                        Composer is commonly used vendor for PHP projects.
                                    </p>
                                </li>
                                <li>
                                    <h6>Paypal</h6>
                                    <p>
                                        This vendor is really important SDK for our project. We using this PayPal Merchant PHP SDK for all of our transaction related work on the portal through PayPal. You can get an idea about it's usages by navigating following path of the project file-
                                    <pre>application\libraries\np_lib\Np_payment</pre>
                                        <br/>
                                        We are using this PayPal Merchant PHP SDK for quick donation, IPN handling, express checkout with recurring billing for subscription, crowdfunding donation, mass payment etc.
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                    </section>
                </div><!--//doc-content-->
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>