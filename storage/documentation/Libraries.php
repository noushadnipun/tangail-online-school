<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Libraries</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 21, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section class="doc-section" id="download-section">
                        <h2 class="section-title">List of library</h2>
                        <div class="section-block">
                            <p>Libraries are included all the global activity of NewPORTAL.</p>
                            <br/>
                            <h6>Lists of Libraries [application\libraries\]</h6>
                            <ul class="list">                                

                                <li>\np_sh
                                    <ul>
                                        <li>
                                            <h6>Np_auth</h6>
                                            <p>This library using for the authenticate an user/subscriber to entire their profile. On the other hand, this library methods are using to prevent unwanted guest/visitor login attempt.</p>
                                        </li>
                                        <li>
                                            <h6>Np_autoload</h6>
                                            <p>This library used to load automatically few common acitivity of NewPORTAL.</p>
                                        </li>
                                        <li>
                                            <h6>Np_crm</h6>
                                            <p>This library handle all the Contacts Relationship Management data and manipulate other functionalities.</p>
                                            <p>For example- 
                                            <pre class="language-php">
public static function get_crm_combined_contacts_total(array $options = array()) {
}</pre>
                                            </p>
                                            <p>
                                            <pre class="language-php">
        $options = [
            'user_id' => null,
            'search_key' => null,
            'type' => 0,
            'order_by_field' => null,
            'order_by_type' => null,
            'offset' => 0,
            'limit' => 10
        ];
                                            </pre>
                                            </p>
                                        </li>
                                        <li>
                                            <h6>Np_ipn</h6>
                                            <p>
                                                This library includes a very common IPN verification system when Instant Payment Notification data from PayPal end to NewPORTAL end.
                                                PayPal sends chunk POST array to NewPORTAL this library methods used to verify these informations and data of that particular array
                                            </p>
                                        </li>
                                        <li>
                                            <h6>Np_marketingtools</h6>
                                            <p>
                                                This library methods handles different type of marketing tools data.
                                            </p>
                                        </li>
                                        <li>
                                            <h6>Np_payment</h6>
                                            <p>
                                                This library methods handles different type of payment transaction data.
                                            </p>
                                        </li>
                                        <li>
                                            <h6>Np_refferal</h6>
                                            <p>
                                                This library methods helps to referral tracking system.
                                            </p>
                                        </li>
                                        <li class="text-warning">
                                            <h6>Np_subscription</h6>
                                            <p>
                                                This library methods helps to maintain user/subscriber package and current package information. May be can merge this library methods with Np_user library.
                                            </p>
                                        </li>
                                        <li class="text-danger">
                                            <h6>Np_titles</h6>
                                            <p>
                                                This library seems unnecessary, we can test and remove this pretty soon.
                                            </p>
                                        </li>
                                        <li>
                                            <h6>Np_user</h6>
                                            <p>
                                                This library is very important library for NewPORTAL. We used this library to validate, authentication checks, users information pulling from database, users friend/connection information etc. Most importantly eWallet data and reports.
                                            </p>
                                        </li>
                                    </ul>
                                </li>
                                <li class="text-danger">\paypal
                                    <ul>
                                        <li>
                                            <h6>Paypal_adaptive</h6>
                                            <p>This library from Andrew Angell portal. This library was using for paypal adaptive, currently it seems unnecessary. Though we have to check it's all configuration before removing it finally.</p>
                                        </li>
                                        <li>
                                            <h6>Paypal_payflow</h6>
                                            <p>This library from Andrew Angell portal. This library was using for paypal payflow, currently it seems unnecessary. Though we have to check it's all configuration before removing it finally.</p>
                                        </li>
                                        <li>
                                            <h6>Paypal_pro</h6>
                                            <p>This library from Andrew Angell portal. This library was using for paypal pro, currently it seems unnecessary. Though we have to check it's all configuration before removing it finally.</p>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <h6>Authentication</h6>
                                    <p>This is a sub ordinate library for ION AUTH (third party) authentication library.</p>
                                </li>
                                <li>
                                    <h6>Bcrypt</h6>
                                    <p>This is a sub ordinate library for ION AUTH (third party) authentication library. There are some other functions to validate or verify different type of data of the portal.</p>
                                </li>
                                <li>
                                    <h6>Csvimport</h6>
                                    <p>This library methods helps to handle csv importing through from a CSV file.</p>
                                </li>
                                <li>
                                    <h6>Csvreader</h6>
                                    <p>This library is a subordinate library of Csvimport library.</p>
                                </li>
                                <li class="text-danger">
                                    <h6>Datatables</h6>
                                    <p>At the very first time, we planned to use data tables third party javascript library to use to view and maintain our data. But, this was really a big mess for the portal. We can remove this library. But we have to test our all reports and routes before remove it.</p>
                                </li>
                                <li>
                                    <h6>Excel</h6>
                                    <p>This library used to download or export our datas to excel format. Currently we are using only on eWallet part of portal, but we can use this library method for any other places in portal.</p>
                                </li>
                                <li>
                                    <h6>Hybridauthlib</h6>
                                    <p>This library is a third party package library, third party package name is <b>Hybrid Auth</b>. We using this library to authenticate social medias and this library helps to post concurrently to different social medias.</p>
                                </li>
                                <li>
                                    <h6>Initial</h6>
                                    <p>We can say this library as helping library to show/view/handle different type of activity of portal. Such as- string escape, image upload, image resizing etc.</p>
                                </li>
                                <li>
                                    <h6>Ion_auth</h6>
                                    <p>This library is a third party library called Ion_auth for CI</p>
                                </li>
                                <li>
                                    <h6>MY_Email</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>MY_Email_3_0_x</h6>
                                    <p>Subordinate library of MY_Email</p>
                                </li>
                                <li>
                                    <h6>MY_Email_3_1_x</h6>
                                    <p>Subordinate library of MY_Email</p>
                                </li>
                                <li>
                                    <h6>Mobile_Detect</h6>
                                    <p>This library used to detect the responsive view (screen resolution) using PHP. This library helps to pick or pull certain view to the users based on their device size.</p>
                                </li>
                                <li>
                                    <h6>Notifications</h6>
                                    <p><h1>Gaurav can explain better than me</h1></p>
                                </li>
                                <li class="text-danger">
                                    <h6>Paypal_ec</h6>
                                    <p>This library from Andrew Angell portal. This library was using for paypal express checkout, currently it seems unnecessary. Though we have to check it's all configuration before removing it finally.</p>
                                </li>
                                <li class="text-danger">
                                    <h6>Paypal_lib</h6>
                                    <p>This library from Andrew Angell portal. This library was using for paypal, currently it seems unnecessary. Though we have to check it's all configuration before removing it finally.</p>
                                </li>
                                <li>
                                    <h6>Pdf</h6>
                                    <p>It helps to generate PDF of file. Currently, business card and flyers using PDF library to generate PDF file.</p>
                                </li>
                                <li>
                                    <h6>Response</h6>
                                    <p>Our common dancing guy message of portal use this library. I mean ajax response handles this library.</p>
                                </li>
                                <li>
                                    <h6>Sendemail</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Social_login</h6>
                                    <p></p>
                                </li>
                                <li>
                                    <h6>Sync</h6>
                                    <p></p>
                                </li>                                
                            </ul>

                        </div>
                    </section>
                </div><!--//doc-content-->
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>