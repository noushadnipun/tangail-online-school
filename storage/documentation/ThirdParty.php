<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Third Party</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 21, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section class="doc-section" id="download-section">
                        <h2 class="section-title">List of third party</h2>
                        <div class="section-block">
                            <p>Third Party used to quick move with NP for several tasks. Like Social Authentication, PDF generating or User Authentication etc.</p>
                            <br/>
                            <h6>Lists of Third Party [application\thirdparty\]</h6>
                            <ul class="list">                                
                                <li>
                                    <h6>MX</h6>
                                    <p>Make NP modular compatibles. This third party has been used HMVC design pattern. This third party commonly used for CodeIgniter. Have a look on to <a href="https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc" target="_blank">their documentation</a> </p>
                                </li>
                                <li>
                                    <h6>PHPExcel</h6>
                                    <p>This third party library/package helps to handle excel generating and some other excel related functionality. Have a look on to <a href="https://github.com/PHPOffice/PHPExcel/tree/1.8/Documentation" target="_blank">their documentation</a> </p>
                                </li>
                                <li>
                                    <h6>hybridauth</h6>
                                    <p>This third party library/package helps to handle social media authentication with OAuth 2.0 technology standards and some other social media related functionality. Have a look on to <a href="https://github.com/hybridauth/hybridauth" target="_blank">their documentation</a> </p>
                                </li>
                                <li>
                                    <h6>mpdf</h6>
                                    <p>This third party library/package helps to handle PDF file creation and some other PDF related functionality. Have a look on to <a href="https://github.com/mpdf/mpdf" target="_blank">their documentation</a> </p>
                                </li>
                                <li class="text-warning">
                                    <h6>phpmailer</h6>
                                    <p>This third party library/package used to send emails safely and easily via PHP code from a web server. Have a look on to <a href="https://github.com/PHPMailer/PHPMailer" target="_blank">their documentation</a> </p>
                                </li>
                            </ul>

                        </div>
                    </section>
                </div><!--//doc-content-->
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>