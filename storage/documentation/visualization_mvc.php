<?php require_once 'include/header.php'; ?>
<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><span aria-hidden="true" class="icon icon_lifesaver"></span> Visualize MVC for NP</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 25th, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section id="general" class="doc-section">
                        <h2 class="section-title">General Information</h2>
                        <div class="section-block">
                            <p>In this page, I am going to describe how NewPORTAL Model View Controller are executing. First of all, you already know the MVC design pattern. Thus, it's easily understandable.</p>

                        </div>
                        <div class="section-block">
                            <p>In following section I will show you a form in view and the full process of this module. Lets pick <b>Bio and presentation</b> form under edit profile page.</p>

                            <h6>Form view location [\application\views\profile\personalinformation.php]</h6>
                            <p>
                                If you see the file, this form has 11 different fields. For example- Country, State/Province, Gender, User Photo etc. Have a look into the file for 10-15 minutes probably. Before go to file lets see some quick notes for you down below-
                            <ul>
                                <li>Followed CodeIgniter standard form helper to create the form fields</li>
                                <li>Form use bootstrap form validate along with ajax submission based on form `id` attribute</li>
                                <li>Form textarea able to extend wysiwyg editor by using `wysiwyg` for textarea id. eg. id="wysiwyg". If you need multiple wysiwyg editor on same page. Use wysiwyg1 to wysiwyg10</li>
                                <li>Every form fields follow the rules of common bootstrap validate plugin standard. You are feel free to use with every form.</li>
                                <li>
                                    Form submission with ajax 
                                    <p>
                                    <pre>
form_open_multipart(
    NULL, 
    array('class' => 'form-horizontal', 'id' => 'personalInformationForm')
);                                            
                                    </pre>
                                    Here, <b>`personalInformationForm` actually hitting the `ajaxImgFn` function inside __ajax.js file under [\footprints\js\__ajax.js]</b>
                                    </p>
                                </li>
                                <li>
                                    Form submission without ajax
                                    <p>
                                    <pre>
form_open_multipart(
    'modifypersonalprofile', 
    array('class' => 'form-horizontal', 'id' => '')
);                                            
                                    </pre>
                                    Here, <b>`modifypersonalprofile` actually hitting the CI route directly. If you use it directly through route then you loosing the bootstrap form fields validation and no ajax submission.</b>
                                    </p>
                                </li>
                            </ul>
                            </p>
                            <h6>Routes for bio & presentation management controller [\application\config\routes.php]</h6>
                            <p>
                            <ul>
                                <li>
                                    Here, routes lines only for bio & presentation form page and information modification routes shown as an example.
                                    <p>
                                    <pre>
$route['personalinformation'] = 'profileeditor/personal_information';
$route['modifypersonalinformation'] = 'profileeditor/modify_personal_information';
                                    </pre>
                                    </p>
                                </li>
                            </ul>
                            </p>
                            <h6>Controller methods for bio & presentation management [\application\controllers\profileeditor.php]</h6>
                            <p>
                            <ul>
                                <li>
                                    For form view:
                                    <p>
                                    <pre>
public function personal_information() {
    $this->np_auth->required_logged_in();        
    $this->data['userinformation'] = $this->basic_information_of_mine($this->me->id);
    $this->data['userinformation1'] = $this->personal_information_of_mine($this->me->id);
    $this->data['personalinformation'] = $this->personal_information_of_mine($this->me->id);
    $this->data['words'] = $this->check_keywords($this->data['personalinformation']);
    $this->data['keywords'] = $this->check_words($this->data['personalinformation']);
    $this->data['countries'] = $this->common_model->get_countries();
    $this->data['interests'] = $this->common_model->get_interests_dropdown_by_module_by(13);
    $this->data['categories'] = $this->common_model->get_interests_dropdown_by_module_by(8, 'ASC');        
    $this->data['cities'] = $this->common_model->get_cities('USA');
    $this->data['states'] = $this->common_model->get_states_by_country_id(@$this->data['personalinformation']->CountryId);
    $this->data['states1'] = $this->data['states'];

    $this->data['title'] = 'Bio & Presentation';
    $this->data['panel_heading'] = $this->panel_heading();

    $this->load->view('np_sh/layout/header', $this->data);        
    $this->load->view('profile/personalinformation', $this->data);
    $this->load->view('np_sh/layout/footer', $this->data);
}
                                    </pre>
                                    </p>
                                </li>
                                <li>                                    
                                    For form view:
                                    <p>
                                    <pre>
public function modify_personal_information() {
    $this->np_auth->required_logged_in();        

    if (!empty($this->session->userdata()['reffered_source'])) {
        $mm = redirect_status(
                TRUE, 
                TRUE, 
                !empty($this->session->userdata()['reffered_by']) ? 
                    $this->session->userdata()['reffered_by'] : NULL, 
                    $this->session->userdata()['reffered_singleitemid'], 
                which_module($this->session->userdata()['reffered_source'])
        );
    }


    $dateofbirth = datetoint($this->input->post('dateofbirth'));
    $config['upload_path'] = "uploads/pp";
    $config['allowed_types'] = "gif|jpg|jpeg|png";
    $config['max_size'] = "5000";
    $config['max_width'] = "1907";
    $config['max_height'] = "1280";
    // $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $purl = remove_white_spaces($this->input->post('newportalurl'));
    $ppurl = remove_special_chars($purl);

    if (isset($this->me->id)) {
        if (@$_FILES["userfile"]["error"] == 4) {
            $pp = $this->input->post('userphoto');
        } else {
            $this->upload->do_upload('userfile');
            $profilephoto = $this->upload->data();
        }
        $data = array(
            'UserId' => $this->me->id,
            'Address' => $this->input->post('address'),
            'CountryId' => $this->input->post('countryid'),
            'CityId' => $this->input->post('cityid'),
            'StateP' => $this->input->post('statep'),
            'DateOfBirth' => $dateofbirth,
            'Gender' => $this->input->post('gender'),
            'UserPhoto' => (isset($profilephoto['file_name']) ? $profilephoto['file_name'] : $pp),
            'UserVideo' => $this->input->post('uservideo'),
            'Interests' => $this->input->post('userinterests'),
            'GreenDirectoryInterests' => $this->input->post('directory_interests'),
            'Biography' => $this->input->post('biography'),
            'MaritalStatus' => $this->input->post('maritalstatus'),
            'NewportalURL' => $ppurl
        );
        $data1 = array(
            'route' => $ppurl,
            'param' => $this->me->id,
            'rtype' => 'user'
        );

        $this->results = $this->profile_model->insertRoute($data1);
        if ($this->input->post('infosid') == 'none') {
            $this->results = $this->profile_model->insertPersonalInformation($data);
        } else {
            $where = array(
                'InfosId' => $this->input->post('infosid')
            );
            $this->results = $this->profile_model->updatePersonalInformation($data, $where);
        }

        if ($this->results) {
            $this->status['status'] = 1;
            $this->status['msg'] = 'The universe is adjusting to your destiny';
        } else {
            $this->status['status'] = 0;
            $this->status['msg'] = 'OOPS... try again or call customer service 310-598-0316';
        }            
        echo jsonEncode($this->status);
    } else {
        $this->status['status'] = 0;
        $this->status['msg'] = $this->upload->display_errors();
        echo jsonEncode($this->status);
    }
}                                
                                    </pre>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="section-block">
                            <p>Most importantly, by this example I tried to show that our every module follows this format.</p>
                            <p>Thus, if you want to work with a form or add new field to a form. You have to follow following diagram-</p>
                            <p>
                                <img src="assets/images/diagrams/mvc.jpg" class="img-responsive"/>
                            </p>
                        </div>
                    </section>
                </div>
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div>
    </div>
</div>
<?php require_once 'include/footer.php'; ?>