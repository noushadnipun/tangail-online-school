<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Models</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 21, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section class="doc-section" id="download-section">
                        <h2 class="section-title">List of models</h2>
                        <div class="section-block">
                            <p>Models are handles all the individual data base insert and pulling functionality of NewPORTAL.</p>
                            <br/>
                            <h6>Lists of Models [application\models\]</h6>
                            <ul class="list">
                                <li>
                                    <h6>Common_model</h6>
                                    <p>This model extends CodeIgniter CI_Model. <i>This model actually is the main model for the portal.</i>
                                        <br>We extends this model to all other models of portal. We using table names from this model along with some other common functionality such as-</p>
                                    <p>
                                    <pre>
insertRecords(), 
getRecords(), 
getAllRecords(), 
loadDataInfile(), 
get_countries(), 
get_country(), 
get_states_by_country_id() 
                                    </pre>
                                    Mainly, we listed all the common data pulling or inserting functionality globally on this model which we using all over the models when needed.
                                    </p>
                                </li>
                                <hr/>
                                <br/>
                                <li>\np_sh\
                                    <ul>
                                        <li>
                                            <h6>Crm_model</h6>
                                            <p>This model handle all the Contacts Relationship Management data pull, push system. This model called from various controllers for their own purpose, this model works with all the CRM tables of the database. Such as- Users Table, AddressBook Table or Contacts Connectivity Table.</p>
                                        </li>
                                        <li>
                                            <h6>Status_model</h6>
                                            <p>This model handle all the concurrent posting and NP status posting functionality. Status Create, Read, Update and Delete.</p>
                                        </li>
                                        <li>
                                            <h6>Subscription_model</h6>
                                            <p>This model handle all the subscription/s functionality. Subscription/s Create, Read, Update and Delete.</p>
                                        </li>
                                    </ul>                         
                                </li>                                
                                <li>
                                    <h6>Addressbook_model</h6>
                                    <p>This model handle all the contacts functionality which imports via CSV or Email import. Addressbook Create, Read, Update and Delete.</p>
                                </li>
                                <li class="text-warning">
                                    <h6>Admin_model</h6>                                    
                                </li>
                                <li>
                                    <h6>Advertisement_model</h6>
                                    <p>This model handle all the Advertisement functionality. Advertisement Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Coaching_model</h6>
                                    <p>This model handle all the Coaching functionality. Coaching Create, Read, Update and Delete.</p>
                                </li>                                
                                <li>
                                    <h6>Crowdfunding_model</h6>
                                    <p>This model handle all the Crowdfunding functionality. Crowdfunding Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Directory_model</h6>
                                    <p>This model handle all the Directory functionality. Directory Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Email_model</h6>
                                    <p>This model handle all the Email functionality. Email/Email Template Management Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Emailstats_model</h6>
                                    <p>This model handle all the Email Analytics functionality. Email Analytics Create, Read, Update and Delete.</p>                                    
                                </li>
                                <li>
                                    <h6>Events_model</h6>
                                    <p>This model handle all the Events functionality. Events Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Groupchat_model</h6>
                                    <p>This model handle all the Group chat functionality. Group chat Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Ion_auth_model</h6>
                                    <p>Ion auth model (third party) used for user creation, deletion, update information of an user and get all information of an user.</p>
                                </li>
                                <li>
                                    <h6>Jobs_model</h6>
                                    <p>This model handle all the Jobs functionality. Jobs Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Marketingtools_model</h6>
                                    <p>This model handle all the Marketing tools functionality. Marketing tools Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Media_model</h6>
                                    <p>This model handle all the Media functionality. Media files Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Notifications_model</h6>
                                    <p>This model handle all the Notifications functionality. Notifications Read, Update.</p>
                                </li>
                                <li>
                                    <h6>Np_user_model</h6>
                                    <p>This model is a sub ordinate model of Ion_auth_model which we wrote for our own purposes. User data Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Panel_model</h6>
                                    <p>This model handle all the administrator functionality. Various administrator functionality (Read, Update and Delete) to manage NP properly.</p>
                                </li>
                                <li>
                                    <h6>Press_release_model</h6>
                                    <p>This model handle all the Press release functionality. Press release Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Products_model</h6>
                                    <p>This model handle all the Products/Stores functionality. Products/Stores Create, Read, Update and Delete.</p>
                                </li>                               
                                <li>
                                    <h6>Profile_model</h6>
                                    <p>This model handle all the Profile management functionality. Profile information Create, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Socialmedia_model</h6>
                                    <p>This model handle all the Social Media configuration. Social Media configuration Create/Store, Read, Update and Delete.</p>
                                </li>
                                <li>
                                    <h6>Webinars_model</h6>
                                    <p>This model handle all the Webinars functionality. Webinars Create, Read, Update and Delete.</p>
                                </li>                                
                            </ul>

                        </div>
                    </section>
                </div><!--//doc-content-->
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>