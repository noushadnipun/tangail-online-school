<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Helpers</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 21, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section class="doc-section" id="download-section">
                        <h2 class="section-title">List of helpers</h2>
                        <div class="section-block">
                            <p>Controllers are included all the individual part of NewPORTAL.</p>
                            <br/>
                            <h6>Lists of Controllers [application\helpers\]</h6>
                            <ul class="list">                                

                                <li>
                                    <h6>MY_email_helper</h6>
                                    <p>
                                        This helper has only two different method.<br/>
                                        1. For validate email address<br/>
                                        2. Name and email format which pass - Joshua Joseph &#60;ResortFinancing@aol.com&#62;
                                    </p>
                                </li>
                                <hr/>
                                <li>
                                    <h6>action_button_helper</h6>
                                    <h6>scene_helper</h6>
                                    <p>NP `action_button_helper`, you also can call it as button helper. This helper file has very common buttons handling methods. For example- </p>
                                    <p>
                                        <span class="text-info">1. in action_button_helper.php</span>
                                    <pre class="language-php">                  
if (!function_exists('e_purchase_message_button')) {

    /**
     * Echo the purchase button, Like: Add to cart, Remove from Cart, Login to purchase message etc
     * 
     * @param int $product_id
     */
    function e_purchase_message_button($product) {
        if (!get_instance()->ion_auth->logged_in()) {
            return e_goto_login('Purchase');
        }
        

        if (get_instance()->me->id == $product->PostedBy) { ?>            

            // Edit button
            // Delete button
            
            return;
        }
        
        
        $products_for_buy = get_instance()->session->userdata('items');
        if (isset($products_for_buy) && array_key_exists('item_' . $product->ProductId, $products_for_buy)):
            
            // remove button
            
            return false;
        endif;
        
            // message seller button
        
        return true;
    }

}
                                    </pre>
                                    </p>
                                    <p>
                                        <span class="text-info">2. Usages</span>
                                    <pre class="language-php">                  
echo e_purchase_message_button(array $product);
                                    </pre>
                                    </p>
                                    <p>Every product single page, where we need to print edit, delete/remove for product owner, message seller button for interested buyer of the product.</p>
                                    <p>Events, Jobs, Press Release, Business, Crowd Funding buttons also stored or generates through this button helper file.</p>
                                </li>
                                <hr/>
                                <li>
                                    <h6>alert_helper</h6>
                                    <p></p>
                                </li>
                                <hr/>
                                <li>
                                    <h6>json_error_helper</h6>
                                    <p></p>
                                </li>
                                <hr/>
                                <li>
                                    <h6>scene_helper</h6>
                                    <p>NP `scene_helper`, you also can call it as view helper. This helper file has a lot of view helper. I am giving you few example in this section. To know more about this scene helper file go to it and check the methods one by one. These all functions are hand written and used for basic usage. Let's give you an example below-</p>
                                    <p>
                                        <span class="text-info">1. in scene_helper.php</span>
                                    <pre class="language-php">                  
function amount_field() {
    return '<small class="nodots">(no $ sign, no dots, no periods, no commas)</small>';
}
                                    </pre>
                                    </p>
                                    <p>
                                        <span class="text-info">2. Usages</span>
                                    <pre class="language-php">                  
echo amount_field();
                                    </pre>
                                    </p>
                                    <p>Every form, where we showing amount field we just calling amount_field() to print this small piece of message.</p>
                                    <br/>
                                    <br/>
                                    OR,
                                    <p>
                                        <span class="text-info">1. in scene_helper.php</span>
                                    <pre class="language-php">                  
/**
 * @param $intvalue Pass integer value
 * @return bool|string return as date. e.g 03/27/2015
 */
function inttodate($intvalue) {
    $d = date('m/d/Y', $intvalue);
    return $d;
}
                                    </pre>
                                    </p>
                                    <p>
                                        <span class="text-info">2. Usages</span>
                                    <pre class="language-php">                  
echo inttodate($pass_integer_value);
                                    </pre>
                                    </p>
                                    <p>When you will want to return the value like month/day/year format. This small method can make your life easy.</p>
                                </li> 
                            </ul>

                        </div>
                    </section>
                </div><!--//doc-content-->
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>