<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Components</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 15, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">

                    <div class="jumbotron text-left">
                        <h3><a href="Libraries.php">Libraries</a></h3>                        
                    </div>
                    <div class="jumbotron text-left">
                        <h3><a href="Controllers.php">Controllers</a></h3>                        
                    </div>
                    <div class="jumbotron text-left">
                        <h3><a href="Models.php">Models</a></h3>                        
                    </div>
                    <div class="jumbotron text-left">
                        <h3><a href="Helpers.php">Helpers</a></h3>
                    </div>                    
                    <div class="jumbotron text-left">
                        <h3><a href="ThirdParty.php">Third Party</a></h3>
                    </div>
                    <div class="jumbotron text-left">
                        <h3><a href="Vendors.php">Vendors</a></h3>
                    </div>
                </div><!--//doc-content-->                
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>