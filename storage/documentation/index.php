<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <title>Tritiyo CMS documentation</title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="favicon.ico">  
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!-- Global CSS -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="assets/plugins/elegant_font/css/style.css">
        <link rel="stylesheet" href="assets/plugins/prism/prism.css">        
        <!-- Theme CSS -->
        <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head> 

    <body class="landing-page">   

        <!--FACEBOOK LIKE BUTTON JAVASCRIPT SDK-->
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <div class="page-wrapper">
            <header class="header text-center">
                <div class="container">
                    <div class="branding">
                        <h1 class="logo">
                            <span aria-hidden="true" class="icon_documents_alt icon"></span>
                            <span class="text-highlight">Tritiyo</span><span class="text-bold"> CMS</span>
                        </h1>
                    </div><!--//branding-->
                    <div class="tagline">
                        <p>A full documentation for Developers</p>
                        <p>Designed with <i class="fa fa-heart"></i> for developers</p>
                    </div>
                </div>
            </header>
            <?php //require_once('include/home.php'); ?>
            <section class="cards-section text-center">
                <div class="container">
                    <div id="cards-wrapper" class="cards-wrapper row">
                        <div class="item item-green col-md-4 col-sm-6 col-xs-6">
                            <div class="item-inner">
                                <div class="icon-holder">
                                    <i class="icon fa fa-paper-plane"></i>
                                </div><!--//icon-holder-->
                                <h3 class="title">Quick Start</h3>
                                <p class="intro">An overview of Tritiyo CMS</p>
                                <a class="link" href="start.php"><span></span></a>
                            </div><!--//item-inner-->
                        </div><!--//item-->
                        <div class="item item-pink item-2 col-md-4 col-sm-6 col-xs-6">
                            <div class="item-inner">
                                <div class="icon-holder">
                                    <span aria-hidden="true" class="icon icon_puzzle_alt"></span>
                                </div><!--//icon-holder-->
                                <h3 class="title">Diagrams</h3>
                                <p class="intro">Here you will find some diagrams to get a quick overview how Tritiyo CMS works</p>
                                <a class="link" href="diagrams.php"><span></span></a>
                            </div><!--//item-inner-->
                        </div><!--//item-->
                        <div class="item item-blue col-md-4 col-sm-6 col-xs-6">
                            <div class="item-inner">
                                <div class="icon-holder">
                                    <span aria-hidden="true" class="icon icon_datareport_alt"></span>
                                </div><!--//icon-holder-->
                                <h3 class="title">Components</h3>
                                <p class="intro">You will find libraries, controllers, models and modules list on this page</p>
                                <a class="link" href="components.php"><span></span></a>
                            </div><!--//item-inner-->
                        </div><!--//item-->
                        <div class="item item-primary col-md-4 col-sm-6 col-xs-6">
                            <div class="item-inner">
                                <div class="icon-holder">
                                    <span aria-hidden="true" class="icon icon_genius"></span>
                                </div><!--//icon-holder-->
                                <h3 class="title">Database</h3>
                                <p class="intro">Inside this page you will find all the table names and connectivity one table to other tables</p>
                                <a class="link" href="database.php"><span></span></a>
                            </div><!--//item-inner-->
                        </div><!--//item-->
                        <div class="item item-orange col-md-4 col-sm-6 col-xs-6">
                            <div class="item-inner">
                                <div class="icon-holder">
                                    <span aria-hidden="true" class="icon icon_gift"></span>
                                </div><!--//icon-holder-->
                                <h3 class="title">All Credentials</h3>
                                <p class="intro">Here you will find all the credentials of Tritiyo CMS</p>
                                <a class="link" href="credentials.php"><span></span></a>
                            </div><!--//item-inner-->
                        </div><!--//item-->
                        <div class="item item-purple col-md-4 col-sm-6 col-xs-6">
                            <div class="item-inner">
                                <div class="icon-holder">
                                    <span aria-hidden="true" class="icon icon_lifesaver"></span>
                                </div><!--//icon-holder-->
                                <h3 class="title">FAQs</h3>
                                <p class="intro">Whole portal FAQ's here are included</p>
                                <a class="link" href="faqs.php"><span></span></a>
                            </div><!--//item-inner-->
                        </div><!--//item-->                        
                    </div><!--//cards-->

                </div><!--//container-->
            </section><!--//cards-section-->
            <?php require_once('include/footer.php'); ?>