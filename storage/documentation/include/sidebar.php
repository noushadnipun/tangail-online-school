<div class="doc-sidebar">
    <nav id="doc-nav">
        <ul id="doc-menu" class="nav doc-menu hidden-xs affix-top" data-spy="affix">
            <li><a href="index.php">Home</a></li>
            <li><a href="start.php">Overview</a></li>
            <li><a href="technology.php">Technologies</a></li>
            <li><a href="diagrams.php">Diagrams</a></li>
            <li><a href="database.php">Database</a></li>
            <li>
                <a href="components.php">Components</a>
                <ul>
                    <li><a href="Controllers.php">Controllers</a></li>
                    <li><a href="Helpers.php">Helpers</a></li>
                    <li><a href="Libraries.php">Libraries</a></li>
                    <li><a href="Models.php">Models</a></li>
                    <li><a href="Modules.php">Modules</a></li>
                    <li><a href="ThirdParty.php">Third Party</a></li>
                    <li><a href="Vendors.php">Vendors</a></li>
                </ul>
            </li>
            <li><a href="visualization_mvc.php">Visualize MVC for NP</a></li>
            <li><a href="special_functionality.php">Special Functionality</a></li>
            <li><a href="credentials.php">All Credentials</a></li>
            <li><a href="faqs.php">FAQ's</a></li>
        </ul><!--//doc-menu-->
    </nav>
</div>