<h2 class="title">Getting started is easy!</h2>
<div class="intro">
    <p>Welcome to Tritiyo CMS documentation. This landing page is an example of how you can develop more or customize Tritiyo CMS to present segments of your documentation.</p>
    <div class="cta-container">
        <a class="btn btn-primary btn-cta" href="https://Joseph7733@bitbucket.org/newportalteam/np_aug17.git" target="_blank"><i class="fa fa-cloud-download"></i> BitBucket Repository</a>
    </div><!--//cta-container-->
</div><!--//intro-->