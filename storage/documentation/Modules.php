<?php require_once 'include/header.php'; ?>

    <div class="doc-wrapper">
        <div class="container">
            <div id="doc-header" class="doc-header text-center">
                <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Modules</h1>
                <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 21, 2017</div>
            </div><!--//doc-header-->
            <div class="doc-body">
                <div class="doc-content">
                    <div class="content-inner">
                        <section class="doc-section" id="download-section">
                            <h2 class="section-title">Users Modules</h2>
                            <div class="section-block">

                            </div>
                        </section>
                    </div><!--//doc-content-->
                </div>
                <?php require_once 'include/sidebar.php'; ?>
            </div><!--//doc-body-->
        </div><!--//container-->
    </div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>