<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Database</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 15, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section id="download-section" class="doc-section">
                        <h2 class="section-title">MySQL database tables</h2>
                        <p class="section-block">Current Live Database name of NewPORTAL: `newporta_newportalorg`</p>
                        <div class="section-block">
                            <h5>Tables to store Users Data</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | Tables to store Users Data       |
                                +----------------------------------+
                                | groups                           |
                                | u_basicinfos                     |
                                | u_educationhistory               |
                                | u_fb_page                        |
                                | u_full_service_subscription_data |
                                | u_othercriterias                 |
                                | u_subscriptions                  |
                                | u_workhistory                    |
                                | user_email_provider              |
                                | users                            |
                                | users_connections                |
                                | users_groups                     |
                                | users_posts                      |
                                | subscription_package             |
                                | subscription_purchase            |
                                | ci_sessions                      |
                                | last_seen                        |
                                | login_attempts                   |
                                +----------------------------------+
                            </pre>
                        </div>
                        <div class="section-block">
                            <h5>Tables to store data for different modules</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | different modules                |
                                +----------------------------------+
                                | jobs                             |
                                | ecommerce_products               |
                                | events                           |
                                | cf_projects                      |
                                | pressreleases                    |
                                | webinar_info                     |
                                | u_businesses                     |
                                +----------------------------------+
                            </pre>                            
                        </div>
                        <div class="section-block">
                            <h5>Chat Modules</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | chat & groupchat                 |
                                +----------------------------------+
                                | group_chat_group                 |
                                | group_chat_member                |
                                | group_chat_message               |
                                | messages                         |
                                | replies                          |
                                +----------------------------------+
                            </pre>                            
                        </div>
                        <div class="section-block">
                            <h5>Users CRM & connection/friendship management tables</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | users contacts & connectivity    |
                                +----------------------------------+
                                | addressbook                      |
                                | contacts                         |
                                | contacts_connectivity            |
                                +----------------------------------+
                            </pre>                            
                        </div>                        
                        <div class="section-block">
                            <h5>Marketing Tools</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | Marketing Tools                  |
                                +----------------------------------+
                                | mt_about_company                 |
                                | mt_customer_prospect             |
                                | mt_demographics                  |
                                | mt_funding                       |
                                | mt_ideal_customer                |
                                | mt_marketing                     |
                                +----------------------------------+
                            </pre>                            
                        </div>                        
                        <div class="section-block">
                            <h5>Tables for concurrent posting and social media connection</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | CC Posting & Social Connection   |
                                +----------------------------------+
                                | social_tokens                    |
                                | status                           |
                                | statuses                         |
                                | mt_funding                       |
                                | mt_ideal_customer                |
                                | mt_marketing                     |
                                +----------------------------------+
                            </pre>                            
                        </div>
                        <div class="section-block">
                            <h5>Payment Portal/eWallet</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | Payment Portal/eWallet           |
                                +----------------------------------+
                                | mass_payout                      |
                                | paymentportal                    |
                                +----------------------------------+
                            </pre>
                        </div>
                                              
                        <div class="section-block">
                            <h6>Helping Tables of above mentioned modules</h6>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | different modules                |
                                +----------------------------------+
                                | categories                       |
                                | job_natures                      |
                                | country                          |
                                | city                             |
                                | countrylanguage                  |
                                | modules                          |
                                | media                            |
                                +----------------------------------+
                            </pre>
                        </div>
                        <div class="section-block">
                            <h5>Other Tables</h5>
                            <pre class="language-javascript">
                                +----------------------------------+
                                | Other Tables                     |
                                +----------------------------------+
                                | advertisements                   |
                                | appointments                     |
                                | callhistory                      |
                                | cards                            |
                                | email_provider                   |
                                | email_template                   |
                                | flyers                           |
                                | investors                        |
                                | mailgun_events                   |
                                | problems                         |
                                | route_table                      |
                                | temp_appointments                |
                                +----------------------------------+
                            </pre>                            
                        </div>
                    </section>
                </div><!--//doc-content-->

            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>