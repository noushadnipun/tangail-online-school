<?php require_once 'include/header.php'; ?>
<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><span aria-hidden="true" class="icon icon_lifesaver"></span> FAQs</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 25th, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section id="general" class="doc-section">
                        <h2 class="section-title">General</h2>
                        <div class="section-block">
                            <h3 class="question"><i class="fa fa-question-circle"></i> Can I viverra sit amet quam eget lacinia?</h3>
                            <div class="answer">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</div>
                        </div><!--//section-block-->
                        <div class="section-block">
                            <h3 class="question"><i class="fa fa-question-circle"></i> What is the ipsum dolor sit amet quam tortor? <span class="label label-success">New</span></h3>
                            <div class="answer">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</div>
                        </div><!--//section-block-->
                        <div class="section-block">
                            <h3 class="question"><i class="fa fa-question-circle"></i> How does the morbi quam tortor work?</h3>
                            <div class="answer">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis. Vestibulum vitae justo quam. Nulla luctus nunc in neque suscipit dictum. Cras faucibus magna sem, non accumsan quam interdum elementum. Pellentesque sollicitudin orci mauris, sit amet mollis nisi vehicula ut. Fusce non accumsan massa, tempus dictum leo. Suspendisse ornare ex vel imperdiet cursus.</div>
                        </div><!--//section-block-->
                        <div class="section-block">
                            <h3 class="question"><i class="fa fa-question-circle"></i> How does the morbi quam tortor work? <span class="label label-warning">Updated</span></h3>
                            <div class="answer">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis.</div>
                        </div><!--//section-block-->
                        <div class="section-block">
                            <h3 class="question"><i class="fa fa-question-circle"></i> How do I submit gravida justo ut sem feugiat?</h3>
                            <div class="answer">Quisque rhoncus elit in ullamcorper finibus. Mauris diam ipsum, tristique id nibh at, sagittis suscipit odio. Donec lacinia consequat augue vulputate pellentesque. Donec sed vehicula purus. Aliquam ac enim non magna vulputate mattis. Proin non vulputate est, id aliquam tortor. </div>
                        </div><!--//section-block-->
                        <div class="section-block">
                            <h3 class="question"><i class="fa fa-question-circle"></i> Are there any gravida justo ut sem feugiat?</h3>
                            <div class="answer">Curabitur convallis sapien eget porttitor tincidunt. Aenean vehicula congue tortor, in ullamcorper metus porta a. Sed congue condimentum turpis vel hendrerit. Nam dapibus nunc eu tellus accumsan, viverra posuere nibh dapibus. Nam nec condimentum elit. Aliquam erat volutpat. Proin dictum est a lacus semper porta. Pellentesque nec efficitur lectus, at tincidunt felis. Etiam ultrices malesuada nulla, sit amet luctus leo semper sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                        </div><!--//section-block-->
                    </section><!--//doc-section-->                   
                </div><!--//content-inner-->
            </div><!--//doc-content-->
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->              
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>