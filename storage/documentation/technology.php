<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Technologies of NewPORTAL</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 15, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section id="download-section" class="doc-section">                        
                        <h2 class="section-title">Technologies used for NewPORTAL</h2>
                        <div class="section-block">
                            <h6>For Web Portal</h6>
                            <ul>
                                <li><b>PHP Framework</b>: CodeIgniter 3</li>
                                <li><b>CSS Framework</b>: Bootstrap 3.3.7</li>
                                <li><b>REST API</b>: OAuth 2.0</li>
                                <li><b>JS Framework</b>: jQuery 2.1.4</li>
                                <li><b>Wysiwyg Editor</b>: Froala</li>
                                <li><b>Chat Tools</b>: Chatigniter from envato</li>                                
                            </ul>
                            <h6>For Android App</h6>
                            <ul>
                                <li>Android Web View</li>
                            </ul>
                            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                        </div>
                    </section>
                </div>
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div><!--//doc-body-->   
    </div><!--//container-->
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>