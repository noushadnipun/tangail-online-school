<?php require_once 'include/header.php'; ?>

<div class="doc-wrapper">
    <div class="container">
        <div id="doc-header" class="doc-header text-center">
            <h1 class="doc-title"><i class="icon fa fa-paper-plane"></i> Diagrams</h1>
            <div class="meta"><i class="fa fa-clock-o"></i> Last updated: Nov 28, 2017</div>
        </div><!--//doc-header-->
        <div class="doc-body">
            <div class="doc-content">
                <div class="content-inner">
                    <section id="download-section" class="doc-section">
                        <h2 class="section-title">Quick tour via diagrams</h2>
                        <p class="section-block">Some common functionality explained through diagram</p>
                        <div class="section-block">
                            <h5>Quick flow chart of whole new portal</h5>
                            <p>
                                <img src="assets/images/diagrams/QuickDiagram.jpg" class="img-responsive" />
                            </p>
                            <hr>
                            <h5>Quick flow chart of MVC along with extra steps NP following</h5>
                            <p>
                                <img src="assets/images/diagrams/mvc.jpg" class="img-responsive" />
                            </p>
                            <hr>

                            <h5>Subscription PayPal flow diagram</h5>
                            <p>
                                <img src="assets/images/diagrams/Subscriptions.jpg" class="img-responsive" />
                            </p>
                            <h5>Quick Donation + Crowd Funding Donation PayPal flow diagram</h5>
                            <p>
                                <img src="assets/images/diagrams/Donation.jpg" class="img-responsive" />
                            </p>
                            <h5>Connection between users/subscribers</h5>
                            <p>
                                <img src="assets/images/diagrams/Connections.jpg" class="img-responsive" />
                            </p>
                            <h5>Flow chart of modules/affiliate links activity</h5>
                            <p>
                                <img src="assets/images/diagrams/Modules.jpg" class="img-responsive" />
                            </p>
                        </div>
                    </section>
                </div>
            </div>
            <?php require_once 'include/sidebar.php'; ?>
        </div>
    </div>
</div><!--//doc-wrapper-->
<?php require_once 'include/footer.php'; ?>