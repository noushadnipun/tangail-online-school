<?php
/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */
/** --------------- All the web backend routes are in below part -------------------- */
/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */

Route::get('route_list', function () {
    $routes = [];
    foreach (\Route::getRoutes()->getIterator() as $route) {
        if (strpos($route->uri, 'api') === false) {
            $routes[$route->uri] = $route->getName();
        }
    }
    dd($routes);
});

Route::get('set_vendor_access_permissions', '\App\Http\Controllers\Admin\UserController@set_vendor_access_permissions')->name('set_vendor_access_permissions');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'outlet']], function () {

    Route::resources([
        'pages' => 'PageController',
        'posts' => 'PostController',
        'medias' => 'MediaController',
        'upload' => 'ImageController',
        'users' => 'UserController',
        'routes' => 'RouteController',
        'album' => 'AlbumController'
    ]);

    // Namespace: \App\Http\Controllers\Admin\
    // URL Prefix: admin
    // Route name prefix: admin
    // middleware: auth,outlet
    // We will place here all the route that match this configuration...

    Route::get('routes/index', 'RouteController@index')->name('route.role_modify');
    Route::post('routes/index', 'RouteController@role_modify')->name('route.role_modify');

    // Media panel routes    
    Route::get('medias', 'MediaController@index')->name('medias.index');

    // users panel routes
    Route::post('search_users', 'UserController@index')->name('search_users');
    Route::get('modify_password_view/{id}', 'UserController@modify_password_view')->name('modify_password_view');
    Route::post('modify_password/{id}', 'UserController@modify_password')->name('modify_password');
    Route::get('vendor_url_existing', 'UserController@vendor_url_existing')->name('vendor_url');
    Route::get('modify_role_view/{id}', 'UserController@modify_role_view')->name('modify_role_view');
    Route::get('modify_role', 'UserController@modify_role')->name('modify_role');
    Route::post('modify_role', 'UserController@modify_role')->name('modify_role');
    Route::get('set_default_role/{id}', 'UserController@set_default_role')->name('set_default_role');

    // Slider  panel routes
    Route::get('sliders', 'SliderController@sliders')->name('sliders');
    Route::get('add_slider', 'SliderController@add_slider')->name('add_slider');
    Route::post('slider_save', 'SliderController@store')->name('slider_save');
    Route::get('edit_slider/{id}', 'SliderController@edit_slider')->name('edit_slider');
    Route::post('slider/{id}/update', 'SliderController@slider_update_save')->name('slider_update_save');
    Route::delete('slider_delete/{id}', 'SliderController@destroy')->name('slider_destroy');

    // Common Controller
    Route::get('common/team', 'CommonController@team')->name('common.team.index');
    Route::post('common/team/store', 'CommonController@teamStore')->name('common.team.store');
    Route::post('common/team/update/{id}', 'CommonController@teamUpdate')->name('common.team.update');
    Route::get('common/team/delete/{id}', 'CommonController@teamDelete')->name('common.team.delete');

    // Gallery
    Route::get('common/gallery', 'GalleryController@galleries')->name('common.gallery.index');
    Route::post('common/gallery/store', 'GalleryController@galleryStore')->name('common.gallery.store');
    Route::post('common/gallery/update/{id}', 'GalleryController@galleryUpdate')->name('common.gallery.update');
    Route::get('common/gallery/delete/{id}', 'GalleryController@galleryDelete')->name('common.gallery.delete');
    Route::post('common/gallery/serialUpdate', 'GalleryController@gallerySerialUpdate')->name('common.gallery.serialupdate');

    // Album Controller
    Route::get('common/album', 'AlbumController@index')->name('common.album.index');
    Route::post('common/album/store', 'AlbumController@store')->name('common.album.store');
    Route::put('common/album/update/{id}', 'AlbumController@update')->name('common.album.update');
    Route::get('common/album/delete/{id}', 'AlbumController@destroy')->name('common.album.delete');

    // Class Controller
    Route::get('common/classes', 'ClassesController@index')->name('common.class.index');
    Route::post('common/class/store', 'ClassesController@store')->name('common.class.store');
    Route::put('common/class/update/{id}', 'ClassesController@update')->name('common.class.update');
    Route::get('common/class/delete/{id}', 'ClassesController@destroy')->name('common.class.delete');

    // Subject Controller
    Route::get('common/subjects', 'SubjectsController@index')->name('common.subject.index');
    Route::post('common/subject/store', 'SubjectsController@store')->name('common.subject.store');
    Route::put('common/subject/update/{id}', 'SubjectsController@update')->name('common.subject.update');
    Route::get('common/subject/delete/{id}', 'SubjectsController@destroy')->name('common.subject.delete');

    // Video Controller
    Route::get('common/videos', 'VideosController@index')->name('common.video.index');
    Route::post('common/video/store', 'VideosController@store')->name('common.video.store');
    Route::put('common/video/update/{id}', 'VideosController@update')->name('common.video.update');
    Route::get('common/video/delete/{id}', 'VideosController@destroy')->name('common.video.delete');

    // All Single Routesvideo
    Route::get('htmlgen', 'HtmlGenController@index')->name('htmlgen');

    //ICT Lab Route
    Route::get('common/ictlab', 'IctlabController@index')->name('common.ictlab.index');
    Route::post('common/ictlab/store', 'IctlabController@store')->name('common.ictlab.store');
    Route::get('common/ictlab/edit/{id}', 'IctlabController@edit')->name('common.ictlab.edit');
    Route::post('common/ictlab/update/{id}', 'IctlabController@update')->name('common.ictlab.update');
    Route::get('common/ictlab/delete/{id}', 'IctlabController@destroy')->name('common.ictlab.delete');

    //Results Route
    Route::get('common/exam-results', 'ResultController@index')->name('common.exam-results.index');
    Route::post('common/exam-results/store', 'ResultController@store')->name('common.exam-results.store');
    Route::get('common/exam-results/edit/{id}', 'ResultController@edit')->name('common.exam-results.edit');
    Route::post('common/exam-results/update/{id}', 'ResultController@update')->name('common.exam-results.update');
    Route::get('common/exam-results/delete/{id}', 'ResultController@destroy')->name('common.exam-results.delete');

    //Institutions Route
    Route::get('common/institutions', 'InstitutionsController@index')->name('common.institutions.index');
    Route::post('common/institutions/store', 'InstitutionsController@store')->name('common.institutions.store');
    Route::get('common/institutions/edit/{id}', 'InstitutionsController@edit')->name('common.institutions.edit');
    Route::post('common/institutions/update/{id}', 'InstitutionsController@update')->name('common.institutions.update');
    Route::get('common/institutions/delete/{id}', 'InstitutionsController@destroy')->name('common.institutions.delete');

    //Training Session Registration User
    Route::get('common/tsession-registration', 'TrainingSessionRegistrationController@index')->name('common.tsession-registration.index');
    Route::get('common/tsession-registration/delete/{id}', 'TrainingSessionRegistrationController@destroy')->name('common.tsession-registration.delete');

    Route::get('common/tsession-registration/export-excel/', 'TrainingSessionRegistrationController@exportExcel')->name('common.tsession-registration.export_excel');


});


Route::middleware(['outlet'])->group(function () {
    // dashboard, menus, widgets
    Route::get('dashboard', ['uses' => '\App\Http\Controllers\Admin\DashboardController@index'])->name('dashboard');
    Route::get('menus', ['uses' => '\App\Http\Controllers\Admin\DashboardController@menus'])->name('menus');
    Route::get('widgets', '\App\Http\Controllers\Admin\DashboardController@widgets')->name('widgets');
    Route::post('widget_save', ['uses' => '\App\Http\Controllers\Admin\DashboardController@store'])->name('widget_save');
    Route::get('edit_widget/{id}', ['uses' => '\App\Http\Controllers\Admin\DashboardController@edit_widget'])->name('edit_widget');
    Route::post('widget/{id}/update', ['uses' => '\App\Http\Controllers\Admin\DashboardController@widget_update_save'])->name('widget_update_save');
    Route::delete('delete_widget/{id}', ['uses' => '\App\Http\Controllers\Admin\DashboardController@destroy']);
    // dashboard, menus, widgets close

    // terms/category management routes
    Route::get('terms', '\App\Http\Controllers\Admin\TermController@terms')->name('terms');
    Route::post('term_save', ['uses' => '\App\Http\Controllers\Admin\TermController@store'])->name('term_save');
    Route::get('edit_term/{id}', ['uses' => '\App\Http\Controllers\Admin\TermController@edit_term'])->name('edit_term');
    Route::post('term/{id}/update', ['uses' => '\App\Http\Controllers\Admin\TermController@term_update_save'])->name('term_update_save');
    Route::get('delete_term/{id}', ['uses' => '\App\Http\Controllers\Admin\TermController@destroy'])->name('delete_term');
    Route::get('get_categories_on_search', ['uses' => '\App\Http\Controllers\Admin\TermController@get_categories_on_search'])->name('get_categories_on_search');
   
    // ajax
    Route::get('/check_if_cat_url_exists', ['uses' => '\App\Http\Controllers\Admin\TermController@check_if_cat_url_exists'])->name('check_if_cat_url_exists');

    // setting management routes
    Route::get('settings', ['uses' => '\App\Http\Controllers\Admin\SettingController@settings'])->name('settings');
    Route::post('setting_save', ['uses' => '\App\Http\Controllers\Admin\SettingController@store'])->name('setting_save');
    Route::get('edit_setting/{id}', ['uses' => '\App\Http\Controllers\Admin\SettingController@edit_setting'])->name('edit_setting');
    Route::post('setting/{id}/update', ['uses' => '\App\Http\Controllers\Admin\SettingController@setting_update_save'])->name('setting_update_save');
    Route::delete('setting_delete/{id}', ['uses' => '\App\Http\Controllers\Admin\SettingController@destroy']);

    Route::get('homesettings', 'Admin\HomeSettingController@settings')->name('settings');
    Route::post('homesetting_save', ['uses' => '\App\Http\Controllers\Admin\HomeSettingController@store'])->name('setting_save');
    Route::get('edit_homesetting/{id}', ['uses' => '\App\Http\Controllers\Admin\HomeSettingController@edit_setting'])->name('edit_setting');
    Route::post('homesetting/{id}/update', ['uses' => '\App\Http\Controllers\Admin\HomeSettingController@setting_update_save'])->name('setting_update_save');
    Route::delete('homesetting_delete/{id}', ['uses' => '\App\Http\Controllers\Admin\HomeSettingController@destroy'])->name('homesetting_delete');

    // user related routes
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('/signout', '\App\Http\Controllers\Auth\LoginController@logout');
});


// Cache Remover
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    return redirect('dashboard');
})->name('clear-cache');
//Reoptimized class loader:
Route::get('/optimize', function () {
    $exitCode = Artisan::call('optimize');
    return redirect('dashboard');
})->name('optimize');

//Route cache:
Route::get('/route-cache', function () {
    $exitCode = Artisan::call('route:cache');
    return redirect('dashboard');
})->name('route-cache');

//Clear Route cache:
Route::get('/route-clear', function () {
    $exitCode = Artisan::call('route:clear');
    return redirect('dashboard');
})->name('route-clear');
//
//Clear View cache:
Route::get('/view-clear', function () {
    $exitCode = Artisan::call('view:clear');
    return redirect('dashboard');
})->name('view-clear');

//Clear Config cache:
Route::get('/config-clear', function () {
    $exitCode = Artisan::call('config:clear');
    return redirect('dashboard');
})->name('config-clear');
//Clear Config cache:
Route::get('/config-cache', function () {
    $exitCode = Artisan::call('config:cache');
    return redirect('dashboard');
})->name('config-cache');
