<?php
use Illuminate\Support\Facades\Artisan;

Auth::routes();

use App\Http\Resources\Route;
use Illuminate\Http\Request;


// Role based routes
require_once 'admin.php';
require_once 'frontend.php';
