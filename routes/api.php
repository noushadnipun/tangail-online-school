<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */




Route::group(['namespace' => 'API'],function() {

    //CommonController 

    Route::get('common/globals','CommonController@globals');
    Route::get('common/shopType','CommonController@shopType');
    Route::get('common/divisions','CommonController@getDivision');
    Route::get('common/getDistrictsByDivision','CommonController@getDistrictsByDivision');
    Route::get('common/getThanaByDistrict','CommonController@getThanaByDistrict');
    Route::get('common/getPage','CommonController@getPage');
    Route::get('common/jobs','CommonController@jobIndex');
    Route::get('common/job','CommonController@getJobById');
    Route::get('common/ads','CommonController@getAds');
    // http://103.106.237.112:8080/samnuxt/secure/api/common/advertisement


    Route::get('common/getNewsEvents','CommonController@getNewsEvents');
    Route::get('common/getMostReadNews','CommonController@mostReadNews');
    Route::get('common/getNewsArchive','CommonController@getNewsArchive');
    Route::get('common/getNewsById','CommonController@getNewsById');


    Route::get('common/getOffers', 'CommonController@getOffers');
    Route::get('common/getMostViewsOffer', 'CommonController@getMostViewsOffer');
    Route::get('common/getOffersArchive', 'CommonController@getOffersArchive');
    Route::get('common/getOffersById', 'CommonController@getOffersById');



    Route::get('common/getShowroomByThana','CommonController@getShowroomByThana');
    Route::post('common/sendContactEmail','CommonController@sendContactEmail');
    Route::get('common/sitemap','CommonController@sitemap');

    Route::get('common/tellUsSomethingWhere','CommonController@tellUsSomethingWhere');
    Route::get('common/tellUsSomethingWhat/{id}','CommonController@tellUsSomethingWhat');
    Route::post('common/tellUsSomethingWhat/store','CommonController@tellUsSomethingSave');


    Route::post('common/subscribeNewsletter','CommonController@subscribeNewsLetter');


    /*Gallery*/
    Route::get('common/galleryImages', 'CommonController@gallery');


    ///AuthController
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/register', 'AuthController@register');
    Route::post('auth/passwordResetRequest', 'AuthController@passwordResetRequest');
    Route::post('auth/passwordReset', 'AuthController@passwordReset');


    ///HomeController
    Route::get('home/index','HomeController@index');
    Route::get('home/categorySet', 'HomeController@categorySet');
    Route::get('home/justForYou','HomeController@justForYou');
    Route::get('home/bestBuyChoices','HomeController@bestBuyChoices');


   ///ShopController
    Route::get('shop/search_products','ShopController@search_product');
    Route::get('shop/add_to_cart','ShopController@add_to_cart');
    Route::get('shop/remove_cart_item','ShopController@remove_cart_item');
    Route::post('shop/update_cart','ShopController@update_cart');
    Route::get('shop/cart','ShopController@cart');
    Route::get('shop/add_to_compare','ShopController@add_to_compare');
    Route::get('shop/view_compare','ShopController@view_compare');
    Route::get('shop/removeCompare','ShopController@removeCompare');

    Route::get('shop/wishlists','ShopController@wishlists');
    Route::get('shop/addToWishList','ShopController@addWishList');
    Route::get('shop/removeFromWishList','ShopController@removeFromWishList');


    Route::get('shop/checkoutUserDefaultDetails','ShopController@checkoutUserDefaultDetails');
    Route::post('shop/submitCheckoutUserDetails','ShopController@submitCheckoutUserDetails');
    Route::get('shop/checkoutUserDetails','ShopController@getCheckoutUserDetails');
    Route::get('shop/getPaymentSetting','ShopController@getPaymentSetting');
    Route::get('shop/product_details','ShopController@product_details');
    Route::get('shop/productVariationPricing','ShopController@productVariationPricing');
    Route::get('shop/recentlyViewProducts','ShopController@recentlyViewProducts');
    Route::post('shop/storePaymentMethod','ShopController@storePaymentMethod');
    Route::get('shop/checkoutUserPaymentMethod','ShopController@checkoutUserPaymentMethod');
    Route::get('shop/pay_now','ShopController@pay_now');
    Route::get('shop/orderSuccess','ShopController@orderSuccess');
    Route::get('shop/allCategories','ShopController@allCategories');
    Route::get('shop/getUserDeliveryLocation','ShopController@getUserDeliveryLocation');
    Route::post('shop/storeUserDeliveryLocation','ShopController@storeUserDeliveryLocation');

    Route::get('shop/apply_coupon_voucher','ShopController@apply_coupon_voucher');
    Route::get('shop/newArrivals','ShopController@newArrivals');


    Route::get('shop/flashSchedules','ShopController@flashSchedules');

    Route::get('shop/product/{id}/qa','ShopController@questionAnswer');
    Route::get('shop/product/reviews','ShopController@getProductReviews');

    //WholeSale
    Route::post('shop/wholeSale','ShopController@storeWholeSale');
    Route::get('shop/getAllBrands','ShopController@getAllBrands');
    Route::get('shop/brandProducts', 'ShopController@brandProducts');
    Route::get('shop/getDiscountedAllProducts','ShopController@getDiscountedAllProducts');
    Route::get('shop/trackMyOrder','ShopController@trackMyOrder');


    ///Vendor
    Route::get('shop/vendor/details','ShopController@vendorDetails');
    Route::get('shop/vendor/allProducts','ShopController@vendorAllProducts');

    ///Payment IPN
    Route::post('shop/sslCommerzSuccessListener','ShopController@sslCommerzSuccessListener');
    
    //PreBooking 
    Route::get('shop/prebookingProducts', 'ShopController@prebookingProduct');


});



Route::group(['namespace' => 'API','middleware' => ['auth:api']],function() {


    Route::get('auth/user', 'AuthController@user');
    Route::post('auth/logout','AuthController@logout');

    //ShopController
    //Route::get('shop/apply_coupon_voucher','ShopController@apply_coupon_voucher');
    Route::post('shop/product/reviews/store','ShopController@storeProductReviews');
    Route::post('shop/product/reviews/likeDislikes','ShopController@reviewLikeDislikes');
    Route::post('shop/product/reviews/UploadPhoto','ShopController@reviewPhotoUpload');
    Route::post('shop/product/reviews/deleteImage','ShopController@deleteReviewUploadImage');
    Route::post('shop/product/reviews/deleteImages','ShopController@deleteReviewUploadImages');
    Route::post('shop/convertPointToCoupon', 'ShopController@convertPointToCoupon');
    Route::get('shop/rewardPointHistoryByUser', 'ShopController@rewardPointHistoryByUser');


    ///CommonController
    Route::post('/common/applyJob', 'CommonController@applyJob');

    ///UserController
    Route::get('user/account','UserController@account');
    Route::post('user/updateAccount','UserController@updateAccount');
    Route::post('user/updatePassword','UserController@updatePassword');
    Route::post('user/updateBillingAddress','UserController@updateBillingAddress');
    Route::get('user/orderHistory','UserController@orderHistory');
    Route::get('user/selfDashboard','UserController@selfDashboard');
    Route::post('shop/product/qa','ShopController@submitQuestion');
    Route::get('shop/product/qa/likeDislike','ShopController@questionLikeDislike');



    ///ShowroomController 

    Route::get('showroom/dashboard','ShowroomController@dashboard');
    Route::get('showroom/shipment/index','ShowroomController@shipment_index');
    Route::get('showroom/shipment/show','ShowroomController@shipment_show');
    Route::get('showroom/shipment/status','ShowroomController@shipment_status');

});
