/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */
/** -------------- All the web frontend routes are in below part -------------------- */
/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */
//Login
Route::get('login','Auth\LoginController@login')->name('login');
//For Social Login
Route::get('/social/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/social/callback/{provider}', 'SocialController@callback');


// Main
Route::get('/reset_password', '\App\Http\Controllers\Site\CommonController@reset_password')->name('reset_password');
Route::post('/reset_password', '\App\Http\Controllers\Site\CommonController@reset_password')->name('reset_password');
Route::get('/password_confirmation', '\App\Http\Controllers\Site\CommonController@password_confirmation')->name('password_confirmation');
Route::post('/retrieve_password', '\App\Http\Controllers\Site\CommonController@retrieve_password')->name('retrieve_password');

// Contact Page
//Route::get('/contact', '\App\Http\Controllers\EmailController@contact')->name('contact');
Route::post('/send_email', '\App\Http\Controllers\EmailController@send_email')->name('send_email');
//Route::get('/page/{id}/{slug}', '\App\Http\Controllers\Site\HomeController@get_webpage')->name('webpage');
// Route::get('/my_account', '\App\Http\Controllers\Site\CommonController@my_account')->name('my_account');

Route::get('/profile_update', '\App\Http\Controllers\Site\CommonController@profile_update')->name('profile_update');
Route::post('/profile_update', '\App\Http\Controllers\Site\CommonController@profile_update')->name('profile_update');

Route::get('/login_now', '\App\Http\Controllers\Site\CommonController@login_now')->middleware('guest')->name('login_now');
Route::get('/create_an_account', '\App\Http\Controllers\Site\CommonController@create_an_account')->middleware('guest')->name('create_an_account');
Route::post('/web_login', 'Site\CommonController@web_login')->name('web_login');
Route::post('/web_signup', '\App\Http\Controllers\Site\CommonController@web_signup')->name('web_signup');
Route::get('/logout', 'Site\CommonController@web_logout')->name('web_logout');
Route::post('/subscribe_email', '\App\Http\Controllers\Site\CommonController@subscribe_email')->name('subscribe_email');
Route::get('/order_history', '\App\Http\Controllers\Site\CommonController@order_history')->name('order_history');
Route::get('/return', '\App\Http\Controllers\Site\CommonController@return')->name('return');
Route::get('/return_save', '\App\Http\Controllers\Site\CommonController@return_save')->name('return_save');
Route::post('/return_save', '\App\Http\Controllers\Site\CommonController@return_save')->name('return_save');
//Route::get('/wishlist', '\App\Http\Controllers\Site\CommonController@wishlist')->name('wishlist');
Route::get('/gift_voucher', '\App\Http\Controllers\Site\CommonController@gift_voucher')->name('gift_voucher');
//Route::get('/compare', '\App\Http\Controllers\Site\CommonController@compare')->name('compare');
//Route::get('/sitemap', '\App\Http\Controllers\Site\CommonController@sitemap')->name('sitemap');
Route::get('/faq', '\App\Http\Controllers\Site\CommonController@faq')->name('faq');
Route::get('/paymentmethod', '\App\Http\Controllers\Site\CommonController@paymentmethod')->name('paymentmethod');
Route::get('/our_store', '\App\Http\Controllers\Site\CommonController@our_store')->name('our_store');
Route::get('/order_information', '\App\Http\Controllers\Site\CommonController@order_information')->name('order_information');

Route::post('/subscribe_email', '\App\Http\Controllers\Site\CommonController@subscribe_email')->name('subscribe_email');
Route::get('/item_cart_load', '\App\Http\Controllers\Site\CommonController@item_cart_load')->name('item_cart_load');
Route::get('/item_wishlist_load', '\App\Http\Controllers\Site\CommonController@item_wishlist_load')->name('item_wishlist_load');
Route::get('/item_compare_load', '\App\Http\Controllers\Site\CommonController@item_compare_load')->name('item_compare_load');

/**
 * Shop Controller
 */
//Route::get('/product/{slug}', '\App\Http\Controllers\Site\ShopController@product_details')->name('product_details');
Route::get('/p/{slug}', '\App\Http\Controllers\Site\ShopController@product_details')->name('product_details');
Route::post('/save_review', '\App\Http\Controllers\Site\ShopController@save_review')->name('save_review');

//Route::get('/product/{id}/{slug}', '\App\Http\Controllers\Site\ShopController@product_details')->name('product_details');
//Route::get('/product/{id}', '\App\Http\Controllers\Site\ShopController@product_details')->name('product_details');
Route::post('/apply_coupon_voucher', '\App\Http\Controllers\Site\ShopController@apply_coupon_voucher')->name('apply_coupon_voucher');

Route::namespace('Site')->group(function () {
    Route::get('track_order', 'ShopController@track_order')->name('track_order');
    Route::post('track_order', 'ShopController@track_order_store');

    Route::get('advertisements', 'CommonController@advertisementindex')->name('advertisements.index');
    Route::get('advertisements/{advertisements}', 'CommonController@advertisementshow')->name('advertisements.show');

    Route::get('testimonials', 'CommonController@testimonialindex')->name('testimonials.index');
    Route::get('testimonials/{testimonials}', 'CommonController@testimonialshow')->name('testimonials.show');

    Route::get('protfolios', 'CommonController@protfolioindex')->name('protfolios.index');
    Route::get('protfolios/{protfolios}', 'CommonController@protfolioshow')->name('protfolios.show');

    Route::get('store_location', 'ShopController@store_location');
    Route::get('new_arrival', 'ShopController@new_arrival');

    // News and events
    Route::get('news_events', 'CommonController@newseventsindex')->name('newsevents.index');
    Route::get('news_events/{news_event}', 'CommonController@newseventsshow')->name('newsevents.show');

    // preload
    Route::get('cookie', 'CommonController@preload')->name('cookie');
});

// Social Login
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

// Career page
//Route::get('career', 'Site\CareerController@index')->name('site.career.index');
//Route::get('career/apply/{id}', 'Site\CareerController@apply')->name('site.career.apply');
//Route::post('career', 'Site\CareerController@store')->name('site.career.store')->middleware('auth');




Route::get('reward_points', 'Site\CommonController@reward_points')->name('reward_points');

Route::get('/main_search_product_ajax', '\App\Http\Controllers\Site\ShopController@main_search_product_ajax')->name('main_search_product_ajax');
Route::get('/main_search_form', '\App\Http\Controllers\Site\ShopController@main_search_form')->name('main_search_form');

//Route::get('/category/{id}/{slug}', '\App\Http\Controllers\Site\ShopController@get_products_by_category')->name('get_products_by_category'); // removed
//Route::get('/category/{id}', '\App\Http\Controllers\Site\ShopController@get_products_by_category')->name('get_products_by_category'); // removed

Route::get('/c/{slug}', '\App\Http\Controllers\Site\ShopController@search_product')->name('search_product'); // category based product lists
Route::get('search_url_make', '\App\Http\Controllers\Site\ShopController@search_url_make')->name('search_url_make'); // category based product lists
Route::get('/category/{slug}', '\App\Http\Controllers\Site\ShopController@search_product')->name('search_product'); // category based product lists
Route::get('/search', '\App\Http\Controllers\Site\ShopController@search_product')->name('search_product'); // category based product lists with search
Route::get('/get_tab_data', '\App\Http\Controllers\Site\ShopController@get_tab_data')->name('get_tab_data'); // category based product lists with search

Route::get('/change_combinition', '\App\Http\Controllers\Site\ShopController@change_combinition')->name('change_combinition');
Route::get('/modify_variation', '\App\Http\Controllers\Site\ShopController@modify_variation')->name('modify_variation');
Route::get('/get_size_price_data', '\App\Http\Controllers\Site\ShopController@get_size_price_data')->name('get_size_price_data');

Route::get('/set_emi', '\App\Http\Controllers\Site\ShopController@set_emi')->name('set_emi');
Route::get('/unset_emi', '\App\Http\Controllers\Site\ShopController@unset_emi')->name('unset_emi');
Route::get('/add_to_cart', '\App\Http\Controllers\Site\ShopController@add_to_cart')->name('add_to_cart');
Route::get('/add_to_compare', '\App\Http\Controllers\Site\ShopController@add_to_compare')->name('add_to_compare');
Route::get('/add_to_wishlist', '\App\Http\Controllers\Site\ShopController@add_to_wishlist')->name('add_to_wishlist');
Route::get('/view_compare', '\App\Http\Controllers\Site\ShopController@view_compare')->name('view_compare');
Route::get('/view_wishlist', '\App\Http\Controllers\Site\ShopController@view_wishlist')->name('view_wishlist');
Route::get('/remove_cart_item', '\App\Http\Controllers\Site\ShopController@remove_cart_item')->name('remove_cart_item');
Route::get('/remove_compare_product', '\App\Http\Controllers\Site\ShopController@remove_compare_product')->name('remove_compare_product');
Route::get('/remove_wishlist_product', '\App\Http\Controllers\Site\ShopController@remove_wishlist_product')->name('remove_wishlist_product');
Route::post('/update_qty', '\App\Http\Controllers\Site\ShopController@update_qty')->name('update_qty');
Route::get('/view_cart', '\App\Http\Controllers\Site\ShopController@view_cart')->name('view_cart');
Route::get('/checkout/address', '\App\Http\Controllers\Site\ShopController@checkout_address')->name('checkout_address');
Route::post('/checkout/delivery_address', '\App\Http\Controllers\Site\ShopController@checkout_delivery_address')->name('checkout_delivery_address');

Route::post('/checkout/store_payment_method', '\App\Http\Controllers\Site\ShopController@store_payment_method')->name('store_payment_method');
Route::get('/checkout/review_order', '\App\Http\Controllers\Site\ShopController@review_order')->name('review_order');
Route::get('/checkout/pay_now', '\App\Http\Controllers\Site\ShopController@pay_now')->name('pay_now');
Route::get('/checkout/bkash_pay_now', '\App\Http\Controllers\Site\ShopController@bkash_pay_now')->name('bkash_pay_now');
Route::get('/invoice', '\App\Http\Controllers\Site\ShopController@success')->name('success');
Route::post('/pdf/invoice', '\App\Http\Controllers\Site\ShopController@pdf_invoice')->name('pdf_invoice');
Route::get('flash_products', '\App\Http\Controllers\Site\ShopController@flash_products')->name('flash_products');

Route::get('/sc/{slug}', '\App\Http\Controllers\Site\HomeController@specific_category')->name('specific_category');
//Route::get('/affiliates', '\App\Http\Controllers\Site\HomeController@dealers')->name('dealers');

Route::get('/sc/{id}/{slug}', '\App\Http\Controllers\Site\HomeController@specific_category')->name('specific_category');
Route::get('/get_district_by_division', '\App\Http\Controllers\Site\HomeController@get_district_by_division')->name('get_district_by_division');
Route::get('/get_thana_by_district', '\App\Http\Controllers\Site\HomeController@get_thana_by_district')->name('get_thana_by_district');
Route::get('/get_showroom_by_thana', '\App\Http\Controllers\Site\HomeController@get_showroom_by_thana')->name('get_showroom_by_thana');

Route::get('/district_by_division', 'Site\ShopController@district_by_division')->name('district_by_division');
Route::get('/thana_by_district', 'Site\ShopController@thana_by_district')->name('thana_by_district');

// checkout start
Route::post('/checkout/success', '\App\Http\Controllers\Site\ShopController@success')->name('success');
Route::get('/checkout/success', '\App\Http\Controllers\Site\ShopController@success')->name('success');
Route::post('/checkout/fail', '\App\Http\Controllers\Site\ShopController@fail')->name('fail');
Route::post('/checkout/cancel', '\App\Http\Controllers\Site\ShopController@cancel')->name('checkout_cancel');
// checkout end

// Bkash Payment
Route::post('/bkash_createpayment', '\App\Http\Controllers\Site\ShopController@bkash_createpayment')->name('bkash_createpayment');
Route::get('/bkash_createpayment', '\App\Http\Controllers\Site\ShopController@bkash_createpayment')->name('bkash_createpayment');
Route::get('/bkash_token', '\App\Http\Controllers\Site\ShopController@bkash_token')->name('bkash_token');
Route::post('/bkash_token', '\App\Http\Controllers\Site\ShopController@bkash_token')->name('bkash_token');
Route::post('/bkash_executepayment', '\App\Http\Controllers\Site\ShopController@bkash_executepayment')->name('bkash_executepayment');
Route::get('/bkash_executepayment', '\App\Http\Controllers\Site\ShopController@bkash_executepayment')->name('bkash_executepayment');
Route::get('/checkout/payment_method', '\App\Http\Controllers\Site\ShopController@checkout_payment_method')->name('checkout_payment_method');
// Bkash Payment

// Be a vendor
Route::post('be_a_vendor', '\App\Http\Controllers\Site\CommonController@be_a_vendor')->name('be_a_vendor');
Route::get('be_a_vendor', '\App\Http\Controllers\Site\CommonController@be_a_vendor')->name('be_a_vendor');
Route::get('save_vendor', '\App\Http\Controllers\Site\CommonController@save_vendor')->name('save_vendor');
Route::post('save_vendor', '\App\Http\Controllers\Site\CommonController@save_vendor')->name('save_vendor');
Route::get('shop/{slug}/{cat?}', '\App\Http\Controllers\Site\ShopController@shop')->name('shop');
Route::get('about_shop/{slug}', '\App\Http\Controllers\Site\ShopController@about_shop')->name('about_shop');
Route::get('shop_showroom/{slug}', '\App\Http\Controllers\Site\ShopController@shop_showroom')->name('shop_showroom');
//Be a vendor

// Comment Template
Route::get('comments', ['as' => 'comments', 'uses' => '\App\Http\Controllers\Site\CommentController@comments'])->name('comments');
Route::get('add_comment', ['as' => 'add_comment', 'uses' => '\App\Http\Controllers\Site\CommentController@add_comment'])->name('add_comment');
Route::post('comment_save', ['as' => 'comment_save', 'uses' => '\App\Http\Controllers\Site\CommentController@store'])->name('comment_save');
Route::get('comment_save', ['as' => 'comment_save', 'uses' => '\App\Http\Controllers\Site\CommentController@store'])->name('comment_save');
Route::get('edit_comment/{id}', ['as' => 'edit_comment', 'uses' => '\App\Http\Controllers\Site\CommentController@edit_comment'])->name('edit_comment');
Route::post('comment/{id}/update', ['as' => 'comment/{id}/update', 'uses' => '\App\Http\Controllers\Site\CommentController@comment_update_save'])->name('comment_update_save');
Route::delete('comment_delete/{id}', ['as' => 'comment_post', 'uses' => '\App\Http\Controllers\Site\CommentController@destroy']);
// Comment Template

// Helping & Ajax Routes
Route::get('/check_if_email_exists', '\App\Http\Controllers\Site\ShopController@check_if_email_exists')->name('check_if_email_exists');
Route::get('/mini_cart', '\App\Http\Controllers\Site\ShopController@mini_cart')->name('mini_cart');
Route::get('/mini_compare', '\App\Http\Controllers\Site\ShopController@mini_compare')->name('mini_compare');
Route::get('/get_product_pricing', '\App\Http\Controllers\Site\ShopController@get_product_pricing')->name('get_product_pricing');

Route::get('login_redirect','HomeController@login_redirect')->name('login.redirect');
//Route::get('/{url?}', 'HomeController@index')->where('url', '.*');

/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */
/** ---------------- All the web test routes are in below part ---------------------- */
/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */

// Tests
Route::get('todos', '\App\Http\Controllers\TodoController@getAllTodos');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products/categories', '\App\Http\Controllers\Site\HomeController@all_data')->name('product_categories');

// All Data
Route::get('all_data', '\App\Http\Controllers\Site\HomeController@all_data')->name('all_data');

Route::get('htmlgen', '\App\Http\Controllers\Admin\HtmlGenController@index')->name('htmlgen');
