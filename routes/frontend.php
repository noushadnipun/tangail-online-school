<?php

/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */
/** --------------- All the web frontend routes are in below part -------------------- */
/** --------------------------------------------------------------------------------- */
/** --------------------------------------------------------------------------------- */

// Main
Route::get('/page/{id}/{slug}', '\App\Http\Controllers\Site\HomeController@get_webpage')->name('webpage');
Route::get('/my_account', '\App\Http\Controllers\Site\CommonController@my_account')->name('my_account');
Route::get('/login_now', '\App\Http\Controllers\Site\CommonController@login_now')->name('login_now');
Route::get('/create_an_account', '\App\Http\Controllers\Site\CommonController@create_an_account')->name('create_an_account');
Route::post('/web_login', 'Auth\LoginController@login')->name('web_login');
Route::post('/web_signup', '\App\Http\Controllers\Site\CommonController@web_signup')->name('web_signup');
Route::get('/logout', 'Auth\LoginController@logout')->name('web_logout');


Route::get('/', '\App\Http\Controllers\Site\HomeController@index');

/** --------------- Nipun ----------
 * T School Pages route List
 -----------------------------------**/

//Gallery Photo
Route::get('/albums/{id}', '\App\Http\Controllers\Site\GalleryController@albumPhoto')->name('album_photos');
Route::get('/page/photos', '\App\Http\Controllers\Site\GalleryController@photos')->name('gallery_photos');
Route::get('/page/photos/{id}', '\App\Http\Controllers\Site\GalleryController@photos')->name('gallery_photos');

//Quote Page
Route::get('/page/quote', '\App\Http\Controllers\Site\TschoolController@pageQuote')->name('page_quote');
Route::get('/page/quote/load/{id}', '\App\Http\Controllers\Site\TschoolController@pageSingleQuote')->name('page_single_quote');

//Administation Page
Route::get('page/administration', '\App\Http\Controllers\Site\TschoolController@pageAdministration')->name('page_administration');
Route::post('page/administration', '\App\Http\Controllers\Site\TschoolController@pageAdministration')->name('page_administration');

//Result Page
Route::get('page/result', '\App\Http\Controllers\Site\TschoolController@pageResult')->name('page_result');
Route::post('page/result', '\App\Http\Controllers\Site\TschoolController@pageResult')->name('page_result');

//News Page
Route::get('page/news', '\App\Http\Controllers\Site\TschoolController@pageNews')->name('page_news');
Route::get('page/news/load/{id}', '\App\Http\Controllers\Site\TschoolController@pageSingleNews')->name('page_single_news');
Route::get('page/news/category/{id}', '\App\Http\Controllers\Site\TschoolController@pageArchiveNews')->name('page_archive_news');


//Video Page
Route::get('page/video', '\App\Http\Controllers\Site\TschoolController@pageVideo')->name('page_video');
Route::get('page/video-search', '\App\Http\Controllers\Site\TschoolController@pageVideoSearch')->name('page_video_search');
Route::get('page/video/load/{id}', '\App\Http\Controllers\Site\TschoolController@pageSingleVideo')->name('page_single_video');


//Subject Video [Homepage grid]
Route::get('page/video/subject/{id}', '\App\Http\Controllers\Site\TschoolController@pageSubjectVideo')->name('page_video_subject');

/* -----------Tschool Teacher List --------*/

//Headmaster List
Route::get('page/tlist-headmaster', '\App\Http\Controllers\Site\TschoolController@pageTeacherListHeadmaster')->name('page_teacher_list_headmaster');
Route::post('page/tlist-headmaster', '\App\Http\Controllers\Site\TschoolController@pageTeacherListHeadmaster')->name('page_teacher_list_headmaster');


//ICT4E  Ambassador list
Route::get('page/tlist-ict4e-ambassador', '\App\Http\Controllers\Site\TschoolController@pageTeacherListIctAmbassador')->name('page_teacher_list_ict4e_ambassador');
Route::post('page/tlist-ict4e-ambassador', '\App\Http\Controllers\Site\TschoolController@pageTeacherListIctAmbassador')->name('page_teacher_list_ict4e_ambassador');

//All Teacher list
Route::get('page/teacher', '\App\Http\Controllers\Site\TschoolController@pageAllTeacherList')->name('page_teacher_list_all');
Route::post('page/teacher', '\App\Http\Controllers\Site\TschoolController@pageAllTeacherList')->name('page_teacher_list_all');



//All ICT LAb
Route::get('page/ictlab', '\App\Http\Controllers\Site\TschoolController@pageIctLabList')->name('page_ict_lab');
Route::post('page/ictlab', '\App\Http\Controllers\Site\TschoolController@pageIctLabList')->name('page_ict_lab');

//All institutions
Route::get('page/institutions', '\App\Http\Controllers\Site\TschoolController@pageInstitutions')->name('page_institutions');
Route::post('page/institutions', '\App\Http\Controllers\Site\TschoolController@pageInstitutions')->name('page_institutions');




//Training Session Registrtion
Route::get('page/traning-session-registration', '\App\Http\Controllers\Site\TschoolController@TraningSessionRegistration')->name('page_traning_session_registration');
Route::post('page/traning-session-registration/store', '\App\Http\Controllers\Site\TschoolController@TraningSessionRegistrationstore')->name('page_traning_session_registration_store');