<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';
    protected $fillable = [
        'video_title' ,'video_link', 'video_image', 'class_id', 'subject_id', 'subject_lession', 'teacher_id', 'user_id', 'is_active'
    ];
}
