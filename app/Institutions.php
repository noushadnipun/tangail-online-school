<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institutions extends Model
{
    protected $table = 'institutions';
    protected $fillable = ['status', 'type', 'level', 'upozila', 'institutions_name', 'institutions_head', 'position', 'establishment_date', 'mobile', 'email', 'total_teacher', 'total_student'];
}
