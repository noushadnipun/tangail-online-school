<?php
/**
 * Created by PhpStorm.
 * User: Samrat
 * Date: 12/1/2017
 * Time: 8:51 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class EloquentMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'sam:eloquent';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository eloquent class';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {

        return __DIR__ . '/stubs/eloquent.stub';
    }

    /**
     * Get console argument
     */
    protected function fire() {
        $username = $this->argument('username');
        dd($username);
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Repositories';
    }


}