<?php
namespace App\Repositories\Role_user;

interface Role_userInterface
{
    public function getAll();

    public function getById($id);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function roleUpdateOrCreate($userId,$roleId);

    public function delete($id);
}