<?php

namespace App\Repositories\Media;

interface MediaInterface
{
    public function getAll();

    public function getById($id);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function delete($id);

    public function getMediaOnSearch(array $options);

    public function whereIn(array $ids);
}