<?php

namespace App\Repositories\User;


use App\User;

class EloquentUser implements UserInterface
{
    private $model;


    /**
     * EloquentMedia constructor.
     * @param Media $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     *
     */
    public function getAll(Array $options = array())
    {
        $default = array(
            'column' => null,
            'search_key' => null,
            'role' => null,
            'is_active' => null
        );


//        if (!empty($new['order_status'])) {
//            $os = " is_active = '" . $new['order_status'] . "'";
//        } else {
//            $os = " is_active IS NOT NULL OR is_active IS NULL";
//        }
//


        $new = array_merge($default, $options);

        //dd($new);


        $data = $this->model;
        if ($new['role']) {



            $data = $data->select('users.*');
            $data = $data->leftJoin('role_user as ru', 'ru.user_id', '=', 'users.id');

            $data = $data->where(['ru.role_id' => $new['role']]);


            if (!empty($new['search_key']) && !empty($new['column'])) {
                $data = $data->where(function ($query) use ($new) {
                    $query->where('users.' . $new['column'], 'like', '%' . $new['search_key'] . '%');
                });
            }


            $data = $data->orderBy('users.id', 'desc');


        } else {
            if (!empty($new['search_key']) && !empty($new['column'])) {
                $data = $data->where(function ($query) use ($new) {
                    $query->where($new['column'], 'like', '%' . $new['search_key'] . '%');

                });
            }

            $data = $data->orderBy('id', 'desc');
        }


        $data = $data->paginate(10);


        return $data;




    }

    /**
     * @param $id
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $email
     */
    public function getByEmail($email)
    {
        return $this->model->where('email', $email)->get()->first();
    }

    /**
     * @param array $att
     */
    public function create(array $att)
    {
        return $this->model->create($att);
    }

    /**
     * @param $id
     * @param array $att
     */
    public function update($id, array $att)
    {
        $todo = $this->getById($id);
        $todo->update($att);
        return $todo;
    }

    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }

    public function existing_col(array $options)
    {
        $default = array(
            'id' => [],
            'column' => null,
            'col_valu' => null
        );
        $new = array_merge($default, $options);

        $data = $this->model->where([$new['column'] => $new['col_valu']])->whereNotIn('id', $new['id'])->get();
        if($data->count() > 0){
            return false;
        } else{
            return true;
        }


    }


}