<?php

namespace App\Repositories\Term;


use App\Term;

class EloquentTerm implements TermInterface
{
    private $model;


    /**
     * EloquentTerm constructor.
     * @param Term $model
     */
    public function __construct(Term $model)
    {
        $this->model = $model;
    }

    /**
     *
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('name', 'asc')
            //->take(100)
            ->paginate(500);
    }

    /**
     * @param $id
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function getByAny($column, $value)
    {
        return $this->model->where($column, $value)->with('sub_categories.sub_categories')->get();
    }

    /**
     * @param array $att
     */
    public function create(array $att)
    {
        return $this->model->create($att);
    }

    /**
     * @param $id
     * @param array $att
     */
    public function update($id, array $att)
    {
        $todo = $this->getById($id);
        $todo->update($att);
        return $todo;
    }

    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }


    /**
     * Extra Methods
     */

    public function get_terms_by_options(array $options = array())
    {
        $defaults = array(
            'type' => 'category',
            'limit' => 100,
            'offset' => 0
        );
        $optionss = array_merge($defaults, $options);
        //$this->model->orderBy('id', 'desc');
        return $this->model->where('type', ($optionss['type'] === 'category') ? 'category' : 'others')->take(!empty($optionss['limit']) ? $optionss['limit'] : 20)->get();
    }

    public function getWhereIn(array $whare = array()){
       return $this->model->whereIn('id',$whare)->orderBy('name', 'ASC')->get();

    }
    
      public function getTarmByVendor(array $options = array())
    {
        $defaults = array(
            'type' => 'category',
            'vendor_id' => null,
            'cat_id' => 8
        );
        $optionss = array_merge($defaults, $options);
        $result = $this->model;



        $result = $result->leftJoin('productcategories AS pc', function ($join) {
                    $join->on('pc.term_id', '=', 'terms.id');
                })
                ->leftJoin('products AS p', function ($join) {
                    $join->on('p.id', '=', 'pc.main_pid');
                });
        $result = $result->select('terms.*');

        $result = $result->where([
            'terms.type' => $optionss['type'],
            'p.user_id' => $optionss['vendor_id'],

        ]);




        if($optionss['cat_id']){
            $result = $result->where([
                'terms.parent' => $optionss['cat_id']
            ]);


        }else{

            if($result->where(['terms.parent' => 1])->get()->count() > 0){
                $result = $result->where(['terms.parent' => 1]);
            }

        }


        $result = $result->groupBy('terms.id');
        $result = $result->take(50)->get();
       // dd($result);


        return $result;


    }


}