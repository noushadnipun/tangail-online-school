<?php

namespace App\Repositories\Gallery;


use App\Gallery;

class EloquentGallery implements GalleryInterface
{
    private $model;


    public function __construct(Gallery $model)
    {
        $this->model = $model;
    }

 
    public function getAll()
    {
        return $this->model
            ->orderBy('serial', 'asc')->with('image')
            //->take(100)
            ->paginate(10);
    }


    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }


    public function getByAny($column, $value)
    {
        return $this->model->where($column, $value)->with('image')->get();
    }

    public function create(array $att)
    {
        return $this->model->create($att);
    }

    
    public function update($id, array $att)
    {
        $todo = $this->getById($id);
        $todo->update($att);
        return $todo;
    }

    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }
}
