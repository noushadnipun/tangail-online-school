<?php

namespace App\Repositories\Post;

use App\Post;

class EloquentPost implements PostInterface
{
    private $model;
    /**
     * @var Attribute
     */
    private $attribute;


    /**
     * EloquentPost constructor.
     * @param Post $model
     */
    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    /**
     *
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('id', 'desc')
            //->take(100)
            ->paginate(10);
    }

    /**
     * @param $id
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $att
     */
    public function create(array $att)
    {
        return $this->model->create($att);
    }

    /**
     * @param $id
     * @param array $att
     */
    public function update($id, array $att)
    {
        $todo = $this->getById($id);
        $todo->update($att);
        return $todo;
    }

    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }

    /**
     * @param array $options
     * @return mixed
     */
    public function getPostsOnCategories(array $options)
    {
        $default = array(
            'search_key' => null,
            'limit' => 10,
            'categories' => array(),
            'offset' => 0,
            'shop_type' => null,
            'district' => null
        );



        $no = array_merge($default, $options);
       // dd($no);
        if (!empty($no['categories'])) {
            $data =  $this->model;
            if($no['shop_type']){
                $data =  $data->where(['shop_type' => $no['shop_type']]);
            }
            if($no['district']){
                $data =  $data->where(['district' => $no['district']]);
            }
            $data = $data->whereIn('categories', $no['categories'])
                ->orderBy('id', 'desc')
                ->groupBy('id')
                ->skip($no['offset'])
                ->take($no['limit'])
                ->get();


            return $data;



        } else {
            $this->model
                ->orWhere('categories', 'like', "%{$no["search_key"]}%")
                ->orWhere('title', 'like', "%{$no["search_key"]}%")
                ->orWhere('sub_title', 'like', "%{$no["search_key"]}%")
                ->orWhere('seo_url', 'like', "%{$no["search_key"]}%")
                ->orWhere('description', 'like', "%{$no["search_key"]}%")
                ->whereIn('categories', $no['categories'])->orderBy('id', 'desc')
                ->groupBy('id')
                ->skip($no['offset'])
                ->take($no['limit'])
                ->get();
        }
    }


    /**
     *
     */
    public function getByArr(array $options)
    {
        $default = array(
            'search_key' => null,
            'limit' => 10,
            'categories' => null,
            'offset' => 0
        );
        $no = array_merge($default, $options);
        //dd( $no );
        $data = $this->model;
        if($no['categories']){
            $data = $data->where(['categories' => $no['categories']]);
        }
        $data = $data->orderBy('id', 'desc')->paginate($no['limit']);

        return $data;
    }

}