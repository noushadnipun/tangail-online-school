<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'routes';
    protected $fillable = [
        'id', 'route_uri', 'route_name', 'route_hash', 'in_group', 'description', 'status'
    ];
}
