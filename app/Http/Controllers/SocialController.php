<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }


    public function callback($provider)
    {

        $getInfo = Socialite::driver($provider)->user();


        $user = $this->addUser($provider, $getInfo);
        $url = Config::get('app.url') . '/login?email='.$user->email.'&token='.$user->login_token.'&provider='.$provider.'&provider_id='.$user->provider_id.'&hash='.time();
        return redirect($url);
    }

    public function addUser($provider, $getInfo)
    {
        $user = User::where('email', $getInfo->email)->first();

        if (!$user) {
            $user = User::create([
                'name' => $getInfo->name,
                'email' => $getInfo->email,
                'provider' => $provider,
                'provider_id' => $getInfo->id,
                'is_active' => true,
                'password' => Hash::make('W3lc0m3@rflbestbuysocialnetworklogin')
            ]);
        }


        $user->login_token = Hash::make(time());
        $user->save();


        return $user;
    }
}
