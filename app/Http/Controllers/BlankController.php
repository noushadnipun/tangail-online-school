<?php

namespace App\Http\Controllers;

class BlankController extends Controller
{

    public function index()
    {
        $blank = ['Samrat', 'Tritiyo Limited', 'Programmer', 'Managing Director'];
        return view('blank', compact('blank'));
    }


}
