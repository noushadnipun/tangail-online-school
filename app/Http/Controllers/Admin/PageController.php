<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Repositories\Media\MediaInterface;
use App\Repositories\Page\PageInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * @var MediaInterface
     */
    private $media;

    /**
     * @var PageInterface
     */
    private $page;

    /**
     * PageController constructor.
     * @param PageInterface $page
     * @param MediaInterface $media
     * @internal param PageInterface $page
     */
    public function __construct(PageInterface $page, MediaInterface $media)

    {
        $this->media = $media;
        $this->page = $page;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = $this->page->getAll();
        return view('page.pages', compact('pages'))->with(['pages' => $pages]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function create()
    {
        $medias = $this->media->getAll();
        return view('page.form')->with('medias', $medias);
    }

    /**
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        if (isset($id)) {
            $page = $this->page->getById($id);
            $medias = $this->media->getAll();
            return view('page.form')
                ->with(['page' => $page, 'medias' => $medias]);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {


        $d = $this->page->getById($id);
        //owndebugger($d); die();
        // validate
        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'seo_url' => 'required',
                'description' => 'required',
                'lang' => 'required'
            ]
        );

        // process the login
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $attributes = [
                'user_id' => $request->get('user_id'),
                'title' => $request->get('title'),
                'sub_title' => $request->get('sub_title'),
                'seo_url' => $request->get('seo_url'),
                'description' => $request->get('description'),
                'is_sticky' => $request->get('is_sticky'),
                'lang' => $request->get('lang'),
                'is_active' => $request->get('is_active'),
                'images' => $request->get('images'),
            ];
            try {
                $page = $this->page->update($d->id, $attributes);
                return back()->with('success', 'Successfully save changed');
            } catch (\Illuminate\Database\QueryException $ex) {
                return back()->withErrors($ex->getMessage());
            }
        }
    }

    /**
     * @param Request $request
     * @return $this
     * @internal param Request $request
     */
    public function store(Request $request)
    {
        //dd($request);
        // validate
        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'seo_url' => 'required',
                'description' => 'required',
                'lang' => 'required'
            ]
        );

        // process the login
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $attributes = [
                'user_id' => $request->get('user_id'),
                'title' => $request->get('title'),
                'sub_title' => $request->get('sub_title'),
                'seo_url' => $request->get('seo_url'),
                'description' => $request->get('description'),
                'is_sticky' => $request->get('is_sticky'),
                'lang' => $request->get('lang'),
                'is_active' => $request->get('is_active'),
                'images' => $request->get('images'),
            ];

            try {
                $page = $this->page->create($attributes);
                return back()->with('success', 'Successfully Added');

            } catch (\Illuminate\Database\QueryException $ex) {
                return back()->withErrors($ex->getMessage());
            }
        }

    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {


        try {

            $this->page->delete($id);
            return back()->with('success', 'Successfully deleted');
        } catch (\Illuminate\Database\QueryException $ex) {
            return back()->withErrors($ex->getMessage());
        }
    }
}
