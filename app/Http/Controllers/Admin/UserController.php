<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Role\RoleInterface;
use App\Repositories\Role_user\Role_userInterface;
use App\Repositories\User\UserInterface;
use App\User;
use Illuminate\Http\Request;
use Validator;
use DB;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    private $user;
    /**
     * @var RoleInterface
     */
    private $role;
    /**
     * @var User
     */
    private $umodel;
    /**
     * @var Role_userInterface
     */
    private $role_user;
    /**
     * UserController constructor.
     * @param UserInterface $user
     * @param RoleInterface $role
     * @param Role_userInterface $role_user
     * @param User $umodel
     */
    public function __construct(UserInterface $user, RoleInterface $role, Role_userInterface $role_user, User $umodel)
    {

        $this->user = $user;
        $this->role = $role;

        // loading models
        $this->umodel = $umodel;
        $this->role_user = $role_user;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

//        dd('e');
        if (!empty($request)) {
            $column = $request->get('column');
            $default = [
                'column' => $column,
                'search_key' => $request->get('search_key'),
                'role' => $request->get('role')
            ];
            $users = $this->user->getAll($default);
            $users->appends(['role' => $request->role]);
            return view('user.index')->with(['users' => $users]);
        } else {
            $users = $this->user->getAll([]);
            $users->appends(['role' => $request->role]);
            return view('user.index')->with(['users' => $users]);
        }
        //$users = $this->user->getAll();
        //return view('user.index')->with(['users' => $users]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function create()
    {
        $roles = $this->role->getAll();
        return view('user.form', compact('roles'));
    }

    /**
     * @param $id
     * @return $this
     */
    public function edit($id)
    {
        if (isset($id)) {
            $user = $this->user->getById($id);
            //$role_id = $this->role_user->getById($user->id)->role_id;
            //dd($role_id);
            $roles = $this->role->getAll();
            return view('user.form')
                ->with(['user' => $user, 'roles' => $roles]);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $d = $this->user->getById($id);
        //dd($request->teacher_type);
        // validate
        // read more on validation at
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                //'email' => 'required'
            ]
        );
         
        // process the login
        if ($validator->fails()) {
            return redirect('users')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            if(!empty($request->teacher_type)) {
                $teacher_type_array = implode(',', $request->teacher_type);
            } else {
                $teacher_type_array = '';
            }
            //dd($teacher_type_array);
            if(!empty($request->subject_id)) {
                $subject_type_array = implode(',', $request->subject_id);
            } else {
                $subject_type_array = '';
            }
           
            $attributes = [
                'name' => $request->get('name'),
                'employee_no' => $request->get('employee_no'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'seo_url' => ($request->get('seo_url')) ? $request->get('seo_url') : null,
                'birthday' => date('Y-m-d', strtotime($request->get('birthday'))),
                'gender' => $request->get('gender'),
                'marital_status' => $request->get('marital_status'),
                'join_date' => date('Y-m-d', strtotime($request->get('join_date'))),
                'father' => $request->get('father'),
                'mother' => $request->get('mother'),
                'company' => $request->get('company'),
                'school' => $request->get('school'),
                'address' => $request->get('address'),
                'district' => $request->get('district'),
                'thana' => $request->get('thana'),
                'phone' => $request->get('phone'),
                'employee_title' => $request->get('employee_title'),
                'vendor_banner' => ($request->get('vendor_banner')) ? $request->get('vendor_banner') : null,
                'vendor_web' => ($request->get('vendor_web')) ? $request->get('vendor_web') : null,
                'vendor_logo' => ($request->get('vendor_logo')) ? $request->get('vendor_logo') : null,
                'vendor_title' => ($request->get('vendor_title')) ? $request->get('vendor_title') : null,
                'vendor_description' => ($request->get('vendor_description')) ? $request->get('vendor_description') : null,
                'vendor_dis_img' => ($request->get('vendor_dis_img')) ? $request->get('vendor_dis_img') : null,
                'vendor_dis_img_title' => ($request->get('vendor_dis_img_title')) ? $request->get('vendor_dis_img_title') : null,
                'vendor_shop_type' => $request->vendor_shop_type ?? null,

                'vendor_mark_as_authorized' => ($request->get('vendor_mark_as_authorized')) ? $request->get('vendor_mark_as_authorized') : null,
                'vendor_trade_license_no' => ($request->get('vendor_trade_license_no')) ? $request->get('vendor_trade_license_no') : null,
                'vendor_vat_certificate' => ($request->get('vendor_vat_certificate')) ? $request->get('vendor_vat_certificate') : null,
                'vendor_etin_certificate' => ($request->get('vendor_etin_certificate')) ? $request->get('vendor_etin_certificate') : null,
                'vendor_bank_solvency_certificate' => ($request->get('vendor_bank_solvency_certificate')) ? $request->get('vendor_bank_solvency_certificate') : null,
                'vendor_incorporation_certificate' => ($request->get('vendor_incorporation_certificate')) ? $request->get('vendor_incorporation_certificate') : null,
                'vendor_article_of_memorandum' => ($request->get('vendor_article_of_memorandum')) ? $request->get('vendor_article_of_memorandum') : null,
                'vendor_bank_account_details' => ($request->get('vendor_bank_account_details')) ? $request->get('vendor_bank_account_details') : null,
                'vendor_nid' => ($request->get('vendor_nid')) ? $request->get('vendor_nid') : null,
                'vendor_md_photo' => ($request->get('vendor_md_photo')) ? $request->get('vendor_md_photo') : null,
                'vendor_md_signature' => ($request->get('vendor_md_signature')) ? $request->get('vendor_md_signature') : null,
                'is_active' => $request->get('is_active'),
                'vendor_status' => $request->vendor_status ?? 0,
                'mark_verifed' => $request->mark_verifed ?? 0,
                'teacher_type' => $teacher_type_array,
                'subject_id' => $subject_type_array,
            ];

            try {
                $this->user->update($d->id, $attributes);
                $this->role_user->roleUpdateOrCreate($d->id, $request->get('user_role'));

                return redirect()->back()->with('success', 'Successfully save changed');
            } catch (\Illuminate\Database\QueryException $ex) {
                return redirect()->back()->withErrors($ex->getMessage());
            }
        }
    }


    /**
     * @param Request $request
     * @return $this
     * @internal param Request $request
     */
    public function store(Request $request)
    {
        //dd($request->subject_id);

        // validate
        // read more on validation at
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required|alpha_dash'
            ]
        );

        $validator1 = Validator::make(
            $request->all(),
            [
                'role_id' => 'required',
                'user_id' => 'required'
            ]
        );

        // process the login
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            if(!empty($request->teacher_type)) {
                $teacher_type_array = implode(',', $request->teacher_type);
            } else {
                $teacher_type_array = '';
            }

            if(!empty($request->subject_id)) {
                $subject_type_array = implode(',', $request->subject_id);
            } else {
                $subject_type_array = '';
            }
            
            $attributes = [
                'name' => $request->get('name'),
                'employee_no' => $request->get('employee_no'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'seo_url' => ($request->get('seo_url')) ? $request->get('seo_url') : null,
                'birthday' => date('Y-m-d', strtotime($request->get('birthday'))),
                'gender' => $request->get('gender'),
                'marital_status' => $request->get('marital_status'),
                'join_date' => date('Y-m-d', strtotime($request->get('join_date'))),
                'father' => $request->get('father'),
                'mother' => $request->get('mother'),
                'company' => $request->get('company'),
                'school' => $request->get('school'),
                'address' => $request->get('address'),
                'district' => $request->get('district'),
                'thana' => $request->get('thana'),
                'phone' => $request->get('phone'),
                'employee_title' => $request->get('employee_title'),
                'password' => bcrypt($request->get('password')),
                'mark_verifed' => $request->mark_verifed ?? 0,
                'teacher_type' => $teacher_type_array,
                'subject_id' => $subject_type_array,
                'vendor_shop_type' => $request->vendor_shop_type ?? null
            ];

//            dd($attributes);

            try {
                $user = $this->user->create($attributes);
                if (!empty($user)) {
                    $attributes_role = [
                        'role_id' => $request->get('user_role'),
                        'user_id' => $user->id
                    ];
                    try {
                        $this->role_user->create($attributes_role);
                        return redirect()->back()->with('success', 'Successfully save changed');
                    } catch (\Illuminate\Database\QueryException $ex) {
                        return redirect()->back()->withErrors($ex->getMessage());
                    }
                } else {
                    return redirect()->back();
                }
            } catch (\Illuminate\Database\QueryException $ex) {
                return redirect()->back()->withErrors($ex->getMessage());
            }
        }
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $this->user->delete($id);
            return back()->with('success', 'Successfully deleted');
        } catch (\Illuminate\Database\QueryException $ex) {
            return back()->withErrors($ex->getMessage());
        }
    }


    /**
     * Extra Methods
     **/

    public function vendor_url_existing(Request $request)
    {
        if ($request->get('id') && $request->get('seo_url')) {
            $option = [
                'id' => [$request->get('id')],
                'column' => 'seo_url',
                'col_valu' => $request->get('seo_url')
            ];
            $data = $this->user->existing_col($option);
            if ($data) {
                return response()->json(['has_data' => 'No', 'massage' => '<p class="text-success">"' . $request->get('seo_url') . '" url is available.</p>']);

            } else {
                return response()->json(['has_data' => 'Yes', 'massage' => '<p class="text-danger">"' . $request->get('seo_url') . '" url is taken. Try another.</p>']);
            }
        }


    }


    public function modify_password_view($id)
    {
        if (isset($id)) {
            $user = $this->user->getById($id);
            //dd($user);
            //$role_id = $this->role_user->getById($user->id)->role_id;
            //dd($role_id);
            $roles = $this->role->getAll();
            return view('user.modify_password_view')
                ->with(['user' => $user, 'roles' => $roles]);
        }
    }

    public function modify_password(Request $request, $id)
    {


        $d = $this->user->getById($id);

        // validate
        // read more on validation at
        $validator = Validator::make(
            $request->all(),
            [
                'new_password' => 'required',
                'new_password_confirm' => 'required'
            ]
        );

        // process the login
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {

            if ($request->get('new_password') == $request->get('new_password_confirm')) {

                // store
                $attributes = [
                    'password' => bcrypt($request->get('new_password'))
                ];
                //dd($attributes);

                $attributes_role = [
                    'role_id' => $request->get('user_role')
                ];

                try {
                    $this->user->update($d->id, $attributes);
                    $this->role_user->update($request->get('id_of_role_user'), $attributes_role);

                    return back()->with('success', 'Successfully save changed');
                } catch (\Illuminate\Database\QueryException $ex) {
                    return back()->withErrors($ex->getMessage());
                }

            } else {
                return back()->withErrors('Unable to change password');
            }
        }
    }
    // Insert Undefined Roles
    //INSERT INTO role_user (role_id, user_id)
    //SELECT 8, id FROM users WHERE id NOT IN(SELECT user_id FROM role_user)

}