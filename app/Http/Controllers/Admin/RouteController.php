<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Route\RouteInterface;
use App\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    /**
     * @var RouteInterface
     */
    private $route;

    /**
     * RouteController constructor.
     * @param RouteInterface $route
     */
    public function __construct(RouteInterface $route)
    {
        $this->route = $route;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = \Route::getRoutes()->getIterator();
        return view('route.routes')->with('routes', $routes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\route $route
     * @return \Illuminate\Http\Response
     */
    public function show(route $route)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\route $route
     * @return \Illuminate\Http\Response
     */
    public function edit(Route $route)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\route $route
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Route $route)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\route $route
     * @return \Illuminate\Http\Response
     */
    public function destroy(Route $route)
    {
        //
    }

    public function role_modify(Request $request)
    {
        $route = Route::updateOrCreate(
            [
                'id' => $request->id,
                'route_hash' => $request->hash
            ],
            [
                'route_uri' => $request->uri,
                'route_name' => $request->name,
                'route_hash' => $request->hash,
                'in_group' => $request->in_group,
                'status' => $request->status
            ]
        );

        return redirect()->back()->with('success', 'Route data successfully saved..');


    }
}
