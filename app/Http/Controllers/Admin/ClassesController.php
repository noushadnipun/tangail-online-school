<?php

namespace App\Http\Controllers\Admin;

use App\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\ClassesInterface;
use Validator;

class ClassesController extends Controller
{

    private $classes;

    public function __construct(ClassesInterface $classes)
    {
        $this->classes = $classes;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $classes = $this->classes->getAll();

        if (!empty($request->id)) {
            $class = $this->classes->getById($request->id);
        } else {
            $class = '';
        }

        return view('admin.common.classes.classes')->with(['classes' => $classes, 'class' => $class]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $attr = [
            'name' => $request->name,
            'description' => $request->description,            
            'is_active' => $request->is_active
        ];

        $this->classes->create($attr);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Classes $classes
     * @return \Illuminate\Http\Response
     */
    public function show(Classes $classes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Classes $classes
     * @return \Illuminate\Http\Response
     */
    public function edit(Classes $classes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Classes $classes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $d = $this->classes->getById($id);

        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'description' => 'required',
            ]
        );

        // process the login
        if ($validator->fails()) {
            return redirect('classes')
                ->withErrors($validator)
                ->withInput();
        } else {

            $attr = [
                'name' => $request->name,
                'description' => $request->description,            
                'is_active' => $request->is_active
            ];
            $this->classes->update($d->id, $attr);

            return redirect()->back();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Classes $classes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Classes::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Successfully deleted');
    }


    
}
