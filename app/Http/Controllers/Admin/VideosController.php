<?php

namespace App\Http\Controllers\Admin;
use App\Videos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Videos\VideosInterface;
use Validator;

class VideosController extends Controller
{
    private $videos;

    public function __construct(VideosInterface $videos)
    {
        $this->videos = $videos;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $videos= $this->videos->getAll();

        if (!empty($request->id)) {
            $video = $this->videos->getById($request->id);
        } else {
            $video = '';
        }

        return view('admin.common.videos.videos')->with(['videos' => $videos, 'video' => $video]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                //'video_title' => 'required',
                'video_link' => 'required',
                //'class_id' => 'required',
                //'subject_id' => 'required',
                //'teacher_id' => 'required',
                //'subject_lession' => 'required',
                //'email' => 'required'
            ]
        );
        if ($validator->fails()) {
            return redirect('back')
                ->withErrors($validator)
                ->withInput();
        } else {
            $attr = [
                'video_title' => $request->video_title,
                'video_link' => $request->video_link,
                'video_image' => $request->video_image,
                'class_id' => $request->class_id,
                'subject_id' => $request->subject_id,
                'subject_lession' => $request->subject_lession,
                'teacher_id' => $request->teacher_id,
                'user_id' => $request->user_id,
                'is_active' => $request->is_active
            ];

            $this->videos->create($attr);

            return redirect()->back();
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Videos $videos
     * @return \Illuminate\Http\Response
     */
    public function show(Videos $videos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Videos $videos
     * @return \Illuminate\Http\Response
     */
    public function edit(Videos $videos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Classes $classes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $d = $this->videos->getById($id);

        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                //'video_title' => 'required',
                'video_link' => 'required',
                //'video_image' => 'required',
                //'class_id' => 'required',
                //'subject_id' => 'required',
                //'subject_lession' => 'required',
                //'teacher_id' => 'required',
                //'user_id' => 'required',
            ]
        );

        // process the login
        if ($validator->fails()) {
            return redirect('classes')
                ->withErrors($validator)
                ->withInput();
        } else {

            $attr = [
                'video_title' => $request->video_title,
                'video_link' => $request->video_link,
                'video_image' => $request->video_image,
                'class_id' => $request->class_id,
                'subject_id' => $request->subject_id,
                'subject_lession' => $request->subject_lession,
                'teacher_id' => $request->teacher_id,
                'user_id' => $request->user_id,
                'is_active' => $request->is_active
            ];
            $this->videos->update($d->id, $attr);

            return redirect()->back()->with('success', 'Update successfully');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Classes $classes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Videos::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Successfully deleted');
    }

}
