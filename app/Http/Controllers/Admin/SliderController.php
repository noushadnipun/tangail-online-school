<?php

namespace App\Http\Controllers\Admin;

use DB;
use Validator;
use App\Repositories\Media\MediaInterface;
use App\Repositories\Slider\SliderInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SliderController extends Controller
{
    /**
     * @var SliderInterface
     */
    private $slider;
    /**
     * @var MediaInterface
     */
    private $media;

    /**
     * sliderController constructor.
     * @param SliserInterface $slider
     * @param MediaInterface $media
 */
    public function __construct(SliderInterface $slider, MediaInterface $media)
    {
        $this->slider = $slider;
        $this->media = $media;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sliders()
    {
        $sliders = $this->slider->getAll();
        return view('slider.sliders', compact('sliders'))->with(['sliders' => $sliders]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function add_slider()
    {
        $medias = $this->media->getAll();


        return view('slider.form')->with(['medias' => $medias]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function edit_slider($id)
    {
        if (isset($id)) {
            $slider = $this->slider->getById($id);
            $medias = $this->media->getAll();

            return view('slider.form')
                ->with(['slider' => $slider, 'medias' => $medias]);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function slider_update_save(Request $request, $id)
    {


        $d = $this->slider->getById($id);
        //owndebugger($d); die();
        // validate
        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'content' => 'required',

            ]
        );

        // process the login
        if ($validator->fails()) {
            return redirect('sliders')
                ->withErrors($validator)
                ->withInput();
        } else {

            $attributes = [

                'title' => $request->get('title'),
                'content' => $request->get('content'),
                'images' => $request->get('images'),
                
            ];

            //dd($attributes);
            try {
                $slider = $this->slider->update($d->id, $attributes);
                //dd($slider);
                return redirect('admin/sliders')->with('success', 'Successfully save changed');
            } catch (\Illuminate\Database\QueryException $ex) {
                return redirect('admin/sliders')->withErrors($ex->getMessage());
            }
        }
    }

    /**
     * @param Request $request
     * @return $this
     * @internal param Request $request
     */
    public function store(Request $request)
    {

        //dd($request);

        // validate
        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'content' => 'required',

            ]
        );
        $random = str_random(5);

        // process the login
        if ($validator->fails()) {
            return redirect('sliders')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $attributes = [
                'title' => $request->get('title'),
                'code'=> $random,
                'content' => $request->get('content'),
                'images' => $request->get('images'),
            ];

            try {
                $this->slider->create($attributes);
                return redirect('admin/add_slider')->with('success', 'Successfully Added');
            } catch (\Illuminate\Database\QueryException $ex) {
                return redirect('admin/sliders')->withErrors($ex->getMessage());
            }
        }

    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $this->slider->delete($id);
            return redirect('admin/sliders')->with('success', 'Successfully deleted');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect('admin/sliders')->withErrors($ex->getMessage());
        }
    }

}
