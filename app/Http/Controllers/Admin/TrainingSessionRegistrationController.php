<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrainingSessionRegistration;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SessionRegistrationExport;

class TrainingSessionRegistrationController extends Controller
{
    public function index(){
    	$RegDataCount = TrainingSessionRegistration::all()->count();
        $getRegData = TrainingSessionRegistration::orderBy('id', 'DESC')->paginate('15');
        return view ('admin.common.tsession-registration.index', compact('getRegData', 'RegDataCount'));
    }
 /*
    public function store(Request $request){
        $data = new TrainingSessionRegistration();
        $data->district = $request->district;
        $data->upozilla = $request->upozilla;
        $data->institute_category = $request->institute_category;
        $data->institute_name = $request->institute_name;
        $data->teacher_name = $request->teacher_name;
        $data->designation_name = $request->designation_name;
        $data->index_number = $request->index_number;
        $data->email = $request->email;
        $data->mobile = $request->mobile;
        //dd($request->all());
        $data->save();
        return redirect()->back()->with('success', 'Data Added Successfully');
    }

    public function edit($id){
        $ictlabEdit = Ictlab::find($id);
        $ictlablist = Ictlab::paginate('10');
        return view ('admin.common.ictlab.ictlab', compact('ictlablist', 'ictlabEdit'));
    }

     public function update(Request $request){
        $data = Ictlab::find($request->ictlab_id);
        $data->upozila = $request->upozila;
        $data->lab_type = $request->lab_type;
        $data->institute_name = $request->institute_name;
        $data->incharge_teacher = $request->incharge_teacher;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->laptop = $request->laptop;
        $data->desktop = $request->desktop;
        $data->projector = $request->projector;
        $data->smart_televison = $request->smart_televison;
        $data->internet = $request->internet;
        //dd($request->all());
        $data->save();
        return redirect()->back()->with('success', 'Data Updated Successfully');
    }
	*/
    public function destroy($id)
    {
        $getRegDataDelete = TrainingSessionRegistration::find($id);
        $getRegDataDelete->delete();
        return redirect()->route('admin.common.tsession-registration.index')->with('delete', 'Data Deleted Successfully');
    }

    public function exportExcel(Request $request){
    	//$data = TrainingSessionRegistration::all();
    	/*
		Excel::create('registered-list', function($excel) use($data){

			$excel->sheet('Sheet 1', function ($sheet) use ($data){
				$sheet->fromArray($data);
			});
		})->download('xls');
		*/
		return Excel::download(new SessionRegistrationExport, 'users.xlsx',  \Maatwebsite\Excel\Excel::XLSX);

		//return redirect()->back()->with('success', 'Export Successfully');
    }
    
}
