<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Gallery\GalleryInterface;

class CommonController extends Controller
{


    private $request;
    private $gallery;

    public function __construct(Request $request, GalleryInterface $gallery)
    {
        $this->request = $request;
        $this->gallery = $gallery;
    }

    public function galleries()
    {
        $gallery = $this->gallery->getAll();
        $image = false;

        if ($this->request->id) {
            $image = $this->gallery->getById($this->request->id);
        }

        return response()->view('admin.common.gallery.index',compact('gallery','image'));
    }


    public function galleryStore()
    {

        $existedGallery = \App\Gallery::orderBy('serial','DESC')->first();
        $serial = ($existedGallery->serial??0)+1;

        $attr  = [
            'media_id' => $this->request->imageId,
            'category_id' => null,
            'serial' => $serial,
            'caption' => $this->request->caption,
            'active' => $this->request->active
        ];

        $this->gallery->create($attr);

        return redirect()->back();
    }


    public function galleryUpdate($id)
    {
        $gallery = $this->gallery->getById($id);
        $gallery->media_id = $this->request->imageId;
        $gallery->category_id = null;
        $gallery->caption = $this->request->caption;
        $gallery->active = $this->request->active;
        $gallery->save();

        return redirect()->back();
    }


    public function galleryDelete($id)
    {
        $gallery = $this->gallery->delete($id);
        
        return redirect()->back();
    }


    public function gallerySerialUpdate(Request $request) {

        $datas = $request->get('item');

        foreach($datas as $data) {
            $gallery = $this->gallery->getById($data['id']);
            $gallery->serial = $data['serial'];
            $gallery->save();            
        }
    }

}
