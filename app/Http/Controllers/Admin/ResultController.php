<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Result;

class ResultController extends Controller
{
    public function index(){
        $resultSheet = Result::paginate('10');
        return view ('admin.common.result.result', compact('resultSheet'));
    }


     public function store(Request $request){
        $data = new Result();
        $data->upozila = $request->upozila;
        $data->exam_name = $request->exam_name;
        $data->year = $request->year;
        $data->total_candidates = $request->total_candidates;
        $data->total_pass = $request->total_pass;
        $data->percentage_pass = $request->percentage_pass;
        $data->a_plus = $request->a_plus;
        $data->a = $request->a;
        $data->a_min = $request->a_min;
        $data->b = $request->b;
        $data->c = $request->c;
        $data->d = $request->d;
        $data->f = $request->f;
        //dd($request->all());
        $data->save();
        return redirect()->back()->with('success', 'Data Added Successfully');
    }

    public function edit($id){
        $resultEdit = Result::find($id);
        $resultSheet = Result::paginate('10');
        return view ('admin.common.result.result', compact('resultSheet', 'resultEdit'));
    }

     public function update(Request $request){
        $data = Result::find($request->result_id);
        $data->upozila = $request->upozila;
        $data->exam_name = $request->exam_name;
        $data->year = $request->year;
        $data->total_candidates = $request->total_candidates;
        $data->total_pass = $request->total_pass;
        $data->percentage_pass = $request->percentage_pass;
        $data->a_plus = $request->a_plus;
        $data->a = $request->a;
        $data->a_min = $request->a_min;
        $data->b = $request->b;
        $data->c = $request->c;
        $data->d = $request->d;
        $data->f = $request->f;
        //dd($request->all());
        $data->save();
        return redirect()->route('admin.common.exam-results.index')->with('success', 'Data Updated Successfully');
    }

    public function destroy($id)
    {
        $ictLabDelete = Result::find($id);
        $ictLabDelete->delete();
        return redirect()->route('admin.common.exam-results.index')->with('delete', 'Data Deleted Successfully');
    }
}
