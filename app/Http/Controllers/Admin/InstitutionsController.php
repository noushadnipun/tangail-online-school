<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Institutions;

class InstitutionsController extends Controller
{
    public function index(){
        $getInstitutions = Institutions::paginate('10');
        return view ('admin.common.institutions.institutions', compact('getInstitutions'));
    }

     public function store(Request $request){
        $data = new Institutions();
        $data->status = $request->status;
        $data->type = $request->type;
        $data->level = $request->level;
        $data->upozila = $request->upozila;
        $data->institutions_name = $request->institutions_name;
        $data->institutions_head = $request->institutions_head;
        $data->position = $request->position;
        $data->establishment_date = $request->establishment_date;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->total_teacher = $request->total_teacher;
        $data->total_student = $request->total_student;
        //dd($request->all());
        $data->save();
        return redirect()->back()->with('success', 'Data Added Successfully');
    }


    public function edit($id){
        $institutionsEdit = Institutions::find($id);
        $getInstitutions = Institutions::paginate('10');
        return view ('admin.common.institutions.institutions', compact('getInstitutions', 'institutionsEdit'));
    }


     public function update(Request $request){
        $data = Institutions::find($request->institutions_id);
        $data->status = $request->status;
        $data->type = $request->type;
        $data->level = $request->level;
        $data->upozila = $request->upozila;
        $data->institutions_name = $request->institutions_name;
        $data->institutions_head = $request->institutions_head;
        $data->position = $request->position;
        $data->establishment_date = $request->establishment_date;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->total_teacher = $request->total_teacher;
        $data->total_student = $request->total_student;
        //dd($request->all());
        $data->save();
        return redirect()->route('admin.common.institutions.index')->with('success', 'Data Updated Successfully');
    }

    public function destroy($id)
    {
        $institutionsDelete = Institutions::find($id);
        $institutionsDelete->delete();
        return redirect()->route('admin.common.institutions.index')->with('delete', 'Data Deleted Successfully');
    }


}
