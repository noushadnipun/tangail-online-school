<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ictlab;

class IctlabController extends Controller
{

    public function index(){
        $ictlablist = Ictlab::paginate('10');
        return view ('admin.common.ictlab.ictlab', compact('ictlablist'));
    }

    public function store(Request $request){
        $data = new Ictlab();
        $data->upozila = $request->upozila;
        $data->lab_type = $request->lab_type;
        $data->institute_name = $request->institute_name;
        $data->incharge_teacher = $request->incharge_teacher;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->laptop = $request->laptop;
        $data->desktop = $request->desktop;
        $data->projector = $request->projector;
        $data->smart_televison = $request->smart_televison;
        $data->internet = $request->internet;
        //dd($request->all());
        $data->save();
        return redirect()->back()->with('success', 'Data Added Successfully');
    }

    public function edit($id){
        $ictlabEdit = Ictlab::find($id);
        $ictlablist = Ictlab::paginate('10');
        return view ('admin.common.ictlab.ictlab', compact('ictlablist', 'ictlabEdit'));
    }

     public function update(Request $request){
        $data = Ictlab::find($request->ictlab_id);
        $data->upozila = $request->upozila;
        $data->lab_type = $request->lab_type;
        $data->institute_name = $request->institute_name;
        $data->incharge_teacher = $request->incharge_teacher;
        $data->mobile = $request->mobile;
        $data->email = $request->email;
        $data->laptop = $request->laptop;
        $data->desktop = $request->desktop;
        $data->projector = $request->projector;
        $data->smart_televison = $request->smart_televison;
        $data->internet = $request->internet;
        //dd($request->all());
        $data->save();
        return redirect()->back()->with('success', 'Data Updated Successfully');
    }

    public function destroy($id)
    {
        $ictLabDelete = Ictlab::find($id);
        $ictLabDelete->delete();
        return redirect()->route('admin.common.ictlab.index')->with('delete', 'Data Deleted Successfully');
    }


}
