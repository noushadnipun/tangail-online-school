<?php

namespace App\Http\Controllers\Admin;

use App\Subjects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Subjects\SubjectsInterface;
use Validator;

class SubjectsController extends Controller
{
    private $subjects;

    public function __construct(SubjectsInterface $subjects)
    {
        $this->subjects = $subjects;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $subjects= $this->subjects->getAll();

        if (!empty($request->id)) {
            $subject = $this->subjects->getById($request->id);
        } else {
            $subject = '';
        }

        return view('admin.common.subjects.subjects')->with(['subjects' => $subjects, 'subject' => $subject]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $attr = [
            'name' => $request->name,
            'description' => $request->description,
            'is_active' => $request->is_active
        ];

        $this->subjects->create($attr);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Subjects $subjects
     * @return \Illuminate\Http\Response
     */
    public function show(Subjects $subjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Subjects $subjects
     * @return \Illuminate\Http\Response
     */
    public function edit(Subjects $subjects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Subjects $subjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $d = $this->subjects->getById($id);

        // read more on validation at
        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'description' => 'required',
            ]
        );

        // process the login
        if ($validator->fails()) {
            return redirect('subjects')
                ->withErrors($validator)
                ->withInput();
        } else {

            $attr = [
                'name' => $request->name,
                'description' => $request->description,
                'is_active' => $request->is_active
            ];
            $this->subjects->update($d->id, $attr);

            return redirect()->back()->with('success', 'Update successfully');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Subjects $subjects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Subjects::find($id);
        $data->delete();
        return redirect()->back()->with('success', 'Successfully deleted');
    }

}
