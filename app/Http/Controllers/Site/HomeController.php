<?php

namespace App\Http\Controllers\Site;

use App\Repositories\Dashboard\DashboardInterface;
use App\Repositories\HomeSetting\HomeSettingInterface;
use App\Repositories\Page\PageInterface;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Slider\SliderInterface;
use App\Repositories\Term\TermInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Image;
use App\Album;
use App\Videos;
use App\term;
use App\Post;
/**
 * @property  page
 */
class HomeController extends Controller
{
    private $data = [];
    private $setting;
    private $page;
    /**
     * @var DashboardInterface
     */
    private $dashboard;
    /**
     * @var TermInterface
     */
    private $term;
    /**
     * @var HomeSettingInterface
     */
    private $homesetting;

    /**
    * @var SliderInterface
    */
    private $slider;    

    /**
     * HomeController constructor.
     * @param PageInterface $page
     * @param DashboardInterface $dashboard
     * @param TermInterface $term
     * @param HomeSettingInterface $homesetting
     * @internal param array $data
     */
    public function __construct(
        SettingInterface $setting,
        PageInterface $page,
        DashboardInterface $dashboard,
        TermInterface $term,
        HomeSettingInterface $homesetting,
        SliderInterface $slider
    ) {
        $this->setting = $setting;
        $this->page = $page;
        $this->dashboard = $dashboard;
        $this->term = $term;
        $this->homesetting = $homesetting;
        $this->slider = $slider;
    }

    public function index()
    {
        $settings = $this->setting->getAll();
        $pages = $this->page->getAll();
        $widgets = $this->dashboard->getAll();
        $homesettig = $this->homesetting->getAll();
        $slider = $this->slider->getById(12);
        $albums = Album::orderBy('id', 'DESC')->where('is_active', '1')->get();
        $getTermNewsSubCat = Term::where('parent', '65')->get();
        $getAllNews = Post::whereRaw("FIND_IN_SET('72',categories)")->orderBy('id', 'DESC')->limit('3')->get();
        //dd($getTermNewsSubCat);
        return view('frontend.index')
            ->with(['settings' => $settings,
                'pages' => $pages,
                'widgets' => $widgets,
                'homesettig' => $homesettig[0],
                'slider' => $slider,
                'albums' => $albums,
                'getTermNewsSubCat' => $getTermNewsSubCat,
                'getAllNews'  => $getAllNews,
            ]);
        //return view('frontend.basic.home')->with();
    }

    public function get_webpage($id, $slug)
    {
        // dd($id);
        $settings = $this->setting->getAll();
        $pages = $this->page->getById($id);
        
                //dd($pages);
        // $posts = $this->post->getAll();
        // $products = $this->product->getAll();
        $widgets = $this->dashboard->getAll();
        // $product_details = $this->product->getById($id);

        return view('frontend.pages.page')
            ->with(['settings' => $settings, 'page' => $pages, 'widgets' => $widgets]);
    }

    public function specific_category($id)
    {
        $category = $this->term->getById($id);
        $divisions = DB::table('districts')->distinct()->select('division')->get();

        $settings = $this->setting->getAll();
        $posts = $this->post->getAll();
        $widgets = $this->dashboard->getAll();

        $sposts = [
            'limit' => 500,
            'categories' => [$id],
            'offset' => 0
        ];

        $sposts = $this->post->getPostsOnCategories($sposts);
        //dd($posts);

        return view('frontend.pages.scategory')
            ->with(['settings' => $settings, 'divisions' => $divisions, 'posts' => $posts, 'sposts' => $sposts, 'widgets' => $widgets, 'category' => $category]);
    }

    public function dealers()
    {
        //dd('sdfs');
        $category = $this->term->getById(623);
        $divisions = DB::table('districts')->distinct()->select('division')->get();

        $settings = $this->setting->getAll();
        $posts = $this->post->getAll();
        $widgets = $this->dashboard->getAll();

        $sposts = [
            'limit' => 500,
            'categories' => [623],
            'offset' => 0
        ];

        $sposts = $this->post->getPostsOnCategories($sposts);
        //dd($posts);

        return view('frontend.pages.dealers')
            ->with(['settings' => $settings, 'divisions' => $divisions, 'posts' => $posts, 'sposts' => $sposts, 'widgets' => $widgets, 'category' => $category]);
    }

    public function get_district_by_division(Request $request)
    {
        //dd($request->division);
        $list = DB::table('districts')->distinct()->select('district')->groupBy('thana')->where('division', $request->division)->get();

        $html = '';
        $html .= '<option value="" selected disabled>Select District</option>';
        foreach ($list->toArray() as $dist) {
            $html .= '<option value="' . $dist->district . '">' . $dist->district . '</option>';
        }
        return response()->json($html);
    }

    public function get_thana_by_district(Request $request)
    {
        //dd($request->district);
        $list = DB::table('districts')->distinct()->select('thana')->where('district', $request->district)->get();

        $html = '';
        $html .= '<option value="" selected disabled>Select Upazila</option>';
        foreach ($list->toArray() as $thana) {
            $html .= '<option value="' . $thana->thana . '">' . $thana->thana . '</option>';
        }
        return response()->json($html);
    }

    public function get_showroom_by_thana(Request $request)
    {
          $list = DB::table('posts')->where(['shop_type' => $request->shop_type, 'thana' => $request->thana ])->paginate(10);

        // dd($list);
        if ($list->count()) {
            $html = '';
            foreach ($list as $location) {
                // dd($location);
                if ($location->categories == 623) {

                    
                    
                    
                    
                    
                    
                    $html .= '<div class="address-shower">';
                    $html .= '<div class="col-md-12">';
                    $html .= '<div class="single-shop-areai" style="display: flex;align-items: center;">';
                    
                    $html .= '<div class="col-md-1">';
                    $html .= '<a href="javascript:void(0)" class="btn btn-sm btn-danger" style="border-radius:3px;" onclick="mapGenerate(' . $location->id . ', ' . $location->latitude . ', ' . $location->longitude . ');">';
                    $html .= '<i class="fa fa-map-pin"></i></a>';
                    $html .= '</div>';
                    
                    $html .= '<div class="col-md-8">';
                    
                    $html .= '<div class="title-about-us1">';
                    $html .= '<h3>' . $location->title . '</h3>';
                    $html .= '</div>';
                    $html .= '<div class="half-title">Address: ';
                    $html .= '<span class="half-content">' . $location->sub_title . '</span>';
                    $html .= '<p><i class="fa fa-phone"></i> ' . $location->phone . '</p>';
                    $html .= '</div>';
                    $html .= '</div>';
                    
                    $html .= '<div class="col-md-3">';
                    $html .= '<div class="content-about-us_one">';

                    if ($location->images) {
                        $html .= '<div class="image-about-us image-stor-us">';
                        $image = Image::find($location->images);

                        $html .= '<img src="' . url($image->full_size_directory) . '" alt="Image">';
                        $html .= '</div>';
                    }
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
        } else {
            $html = '<h2 class="text-center">No data found!</h2>';
        }

        return response()->json($html);
    }

    /**
     * @return $this
     * All the data dumped on this method
     * Please check whatever you would like to check
     * Make your decision and work
     */
    public function all_data()
    {
        $settings = $this->setting->getAll();
        $paymentsetting = $this->paymentsetting->getAll();
        $pages = $this->page->getAll();
        $posts = $this->post->getAll();
        $products = $this->product->getAll();
        $widgets = $this->dashboard->getAll();

        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $categories = $this->term->getAll()->toArray();
        //dd($paymentsetting);

        return view('frontend.all_data')
            ->with([
                'settings' => $settings,
                'pages' => $pages,
                'posts' => $posts,
                'products' => $products,
                'widgets' => $widgets,
                'cart' => $oldcart,
                'paymentsetting' => $paymentsetting,
                'cats' => $categories
            ]);
        //return view('frontend.basic.home')->with();
    }
}
