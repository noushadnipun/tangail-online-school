<?php

namespace App\Http\Controllers\Site;

use App\Repositories\Career\CareerRepository;
use App\Repositories\Career\EloquentCareer;
use App\Repositories\Dashboard\DashboardInterface;
use App\Repositories\Job\EloquentJob;
use App\Repositories\Job\JobRepository;
use App\Repositories\Setting\SettingInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CareerController extends Controller
{




    private $settings;
    private $dashboard;
    private $job;
    private $jobRepository;

    public function __construct(SettingInterface $settings,DashboardInterface $dashboard,EloquentJob $job,JobRepository $jobRepository)
    {
        $this->settings             = $settings;
        $this->dashboard            = $dashboard;
        $this->job                  = $job;
        $this->jobRepository        = $jobRepository;
    }



    public function index(){

        $settings                  = $this->settings->getAll();
        $dashboard                 = $this->dashboard->getAll();
        $widgets                   = $this->dashboard->getAll();

        $jobs                      = $this->job->getAllActive();


        return response()->view('frontend.career.index',compact(
            'settings',
            'dashboard',
            'widgets',
            'jobs'
        ));
    }


    public function apply($id){
        $settings                  =    $this->settings->getAll();
        $dashboard                 =    $this->dashboard->getAll();
        $widgets                   =    $this->dashboard->getAll();
        $job                       =    $this->job->getById($id);
        $applied                   =    $job->applied()->where('user_id',Auth::id())->count();

        return response()->view('frontend.career.apply',compact(
            'settings',
            'dashboard',
            'widgets',
            'job',
            'applied'
        ));
    }



    public function store(Request $request){

        $career = $this->jobRepository->apply($request);

        return redirect()->back()->with('status', isset($career->id) ? 'Data Successfully Submitted' : 'Data Failed To Submit, please check file format.');
    }


}
