<?php

namespace App\Http\Controllers\Site;

use App\Repositories\Dashboard\DashboardInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Album;
use App\Gallery;

class GalleryController extends Controller
{
    private $dashboard;

     public function __construct( DashboardInterface $dashboard){
        $this->dashboard = $dashboard;
    }
    //Single Album Photo
    public function albumPhoto($id)
    {
        $widgets = $this->dashboard->getAll();
    	$albumPhoto = Gallery::where('category_id', $id)->orderBy('id', 'DESC')->where('active', '1')->get();
    	$albumName = Album::where('id', $id)->get();

    	return view ('frontend.tschool-pages.album.single-album', compact('albumPhoto', 'albumName', 'widgets'));
    }

    //Gallery Photo Index
    public function photos()
    {	
        $widgets = $this->dashboard->getAll();
    	$albums = Album::orderBy('id', 'DESC')->where('is_active', '1')->get();
    	return view ('frontend.tschool-pages.album.photos', compact('albums', 'widgets'));
    }
   
}
