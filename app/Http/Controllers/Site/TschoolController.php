<?php

namespace App\Http\Controllers\Site;

use App\Repositories\Dashboard\DashboardInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use App\Videos;
use App\Post;
use App\User;
use App\Role_user;
use App\Ictlab;
use App\Result;
use App\Institutions;
use App\TrainingSessionRegistration;

class TschoolController extends Controller
{
    private $dashboard;

     public function __construct( DashboardInterface $dashboard){
        $this->dashboard = $dashboard;
    }
    
    //Result Page
    public function pageResult(Request $request){
        $widgets = $this->dashboard->getAll();
        if(!empty($request->all())){
            $getResult = Result::where('upozila', 'Like', '%'.$request->upozila.'%')
                                ->where('exam_name', $request->exam_name)
                                ->where('year', $request->year)
                                ->paginate(16); 
            return view('frontend.tschool-pages.result', compact('getResult', 'widgets'));

        } else {
            $getResult = Result::paginate(20);
            return view('frontend.tschool-pages.result', compact('getResult', 'widgets'));
        } 
    }

    /*-------------------------------------
   ----------------Quote -----------------
   --------------------------------------*/
    //Quote Page
    public function pageQuote(){
        $widgets = $this->dashboard->getAll();
        //dd($bitcoin);
        $getQuote = Post::whereRaw("FIND_IN_SET('63' , categories)")->paginate(9);
        return view('frontend.tschool-pages.quote.quote', compact('getQuote', 'widgets'));
    }
    // Single Quote Page
    public function pageSingleQuote($id){
        $widgets = $this->dashboard->getAll();
        $getQuote = Post::find($id);
        return view('frontend.tschool-pages.quote.single-quote', compact('getQuote', 'widgets'));
    }

    /*-------------------------------------
    ----------------News -----------------
    --------------------------------------*/

    //News Page
    public function pageNews(){
        $widgets = $this->dashboard->getAll();
        return view('frontend.tschool-pages.news.news', compact('widgets'));
    }
    // Single News Page
    public function pageSingleNews($id){
        $widgets = $this->dashboard->getAll();
        $getNews = Post::find($id);
        return view('frontend.tschool-pages.news.single-news', compact('getNews', 'widgets'));
    }
    //Archive News
    public function pageArchiveNews($id){
        $widgets = $this->dashboard->getAll();
        if(!empty($id)){
            $getAllNews = Post::whereRaw("FIND_IN_SET($id , categories)")->paginate(10);
            return view('frontend.tschool-pages.news.news-archive', compact('getAllNews', 'widgets'));
            //dd($getAllNews);
        }
    }
    /*-------------------------------------
    ----------------Video -----------------
    --------------------------------------*/
    //Video Page 
    public function pageVideo(){
        $widgets = $this->dashboard->getAll();
         $getVideo = Videos::orderBy('id', 'DESC')->where('is_active', '1')->paginate(16); 
         return view('frontend.tschool-pages.video.video', compact('getVideo', 'widgets'));
    }
    //Search Video
    public function pageVideoSearch(Request $request){
        $widgets = $this->dashboard->getAll();
        $getVideo = Videos::where('video_title', 'Like', '%'.$request->video_title.'%')
                                ->where('class_id', $request->class_id)
                                ->where('subject_id', 'Like', '%'.$request->subject_id.'%')
                                ->where('subject_lession', 'Like', '%'.$request->subject_lession.'%')
                                ->paginate(16); 
        return view('frontend.tschool-pages.video.video', compact('getVideo', 'widgets'));
    }

    // Single Video Page
    public function pageSingleVideo($id){
        $widgets = $this->dashboard->getAll();
        $getVideo = Videos::find($id);
        return view('frontend.tschool-pages.video.single-video', compact('getVideo', 'widgets'));
    }

    //Subject Video [Homepage Grid]

    public function pageSubjectVideo($id){
        $widgets = $this->dashboard->getAll();
        $getVideo = DB::table('subjects')->leftjoin('videos', 'videos.subject_id', '=', 'subjects.id')
                            ->select('subjects.*', 'videos.*')
                            ->where('subjects.id', $id)
                            ->paginate(16);
        //dd($su);
        return view('frontend.tschool-pages.video.video', compact('getVideo', 'widgets'));
    }


    /*----------------------------------------
    --------------- Teacher List ------------
    ----------------------------------------*/
    //Administration Page
    public function pageAdministration(Request $request){
        $widgets = $this->dashboard->getAll();
        if(!empty($request->all())){
            $AllAdministratorList = User::where('company', 'Like', '%'.$request->office.'%')
                                ->where('thana', $request->thana)
                                ->where('name', 'Like', '%'.$request->name.'%')
                                //->where('subject_lession', 'Like', '%'.$request->subject_lession.'%')
                                ->paginate(16); 
            return view('frontend.tschool-pages.administration', compact('AllAdministratorList', 'widgets'));
        } else {
            $AllAdministratorList =  User::whereRaw("FIND_IN_SET('administrator' , teacher_type)")->paginate(20); 
            return view('frontend.tschool-pages.administration', compact('AllAdministratorList', 'widgets'));
        }
    }

    // Headmaster Page
    public function pageTeacherListHeadmaster(Request $request)
    { 
         $widgets = $this->dashboard->getAll();
        if(!empty($request->all())){
            $headmasterList = User::where('company', 'Like', '%'.$request->office.'%')
                                ->where('thana', $request->thana)
                                ->where('name', 'Like', '%'.$request->name.'%')
                                //->where('subject_lession', 'Like', '%'.$request->subject_lession.'%')
                                ->paginate(16); 
            //return $request->all();
            return view('frontend.tschool-pages.teacher-list.headmaster', compact('headmasterList', 'widgets'));
        } else{
            $headmasterList = User::whereRaw("FIND_IN_SET('headmaster' , teacher_type)")->paginate(20);
            //return $headmasterList;
            return view('frontend.tschool-pages.teacher-list.headmaster', compact('headmasterList', 'widgets'));
        }

    }


    // Ict4E Ambassador List
    public function pageTeacherListIctAmbassador(Request $request)
    {
         $widgets = $this->dashboard->getAll();
        if(!empty($request->all())){
            $ictAmbassadorList = User::where('company', 'Like', '%'.$request->office.'%')
                                ->where('thana', $request->thana)
                                ->where('name', 'Like', '%'.$request->name.'%')
                                //->where('subject_lession', 'Like', '%'.$request->subject_lession.'%')
                                ->paginate(16); 
            return view('frontend.tschool-pages.teacher-list.ict4e-ambassador', compact('ictAmbassadorList', 'widgets'));
        } else {
            $ictAmbassadorList = User::whereRaw("FIND_IN_SET('ict-ambassador' , teacher_type)")->paginate(20);
            return view('frontend.tschool-pages.teacher-list.ict4e-ambassador', compact('ictAmbassadorList', 'widgets'));
        }
        
    }

    // All Teacher List
     public function pageAllTeacherList(Request $request)
    {
         $widgets = $this->dashboard->getAll();
       if(!empty($request->all())){
            $AllTeacherList = User::where('company', 'Like', '%'.$request->office.'%')
                                ->where('thana', $request->thana)
                                ->where('name', 'Like', '%'.$request->name.'%')
                                //->where('subject_lession', 'Like', '%'.$request->subject_lession.'%')
                                ->paginate(16); 
            return view('frontend.tschool-pages.teacher-list.all-taecher', compact('AllTeacherList', 'widgets')); 
        } else {
            $AllTeacherList =  User::whereRaw("FIND_IN_SET('teacher' , teacher_type)")->paginate(20); 
            return view('frontend.tschool-pages.teacher-list.all-taecher', compact('AllTeacherList', 'widgets')); 
        }           
    }

    
    /*======================================
    =============ICT LAB====================
    =======================================*/
    public function pageIctLabList(Request $request){
         $widgets = $this->dashboard->getAll();
        if(!empty($request->all())){
            $getIctlab = Ictlab::where('upozila', 'Like', '%'.$request->upozila.'%')
                                ->where('lab_type', $request->lab_type)
                                ->paginate(16); 
            return view('frontend.tschool-pages.ictlab', compact('getIctlab', 'widgets'));
        } else {
            $getIctlab =  Ictlab::paginate(16);
            return view('frontend.tschool-pages.ictlab', compact('getIctlab', 'widgets'));
        }
    }


    /*======================================
    ============= All Institutions ====================
    =======================================*/
    public function pageInstitutions(Request $request){
         $widgets = $this->dashboard->getAll();
        if(!empty($request->all())){
            $getInstitutions = Institutions::where('status', 'Like', '%'.$request->status.'%')
                                ->where('type', $request->type)
                                ->where('level', $request->level)
                                ->where('upozila', $request->upozila)
                                ->paginate(16); 
            return view('frontend.tschool-pages.institutions', compact('getInstitutions', 'widgets'));
        } else {
            $getInstitutions =  Institutions::paginate(16);
            return view('frontend.tschool-pages.institutions', compact('getInstitutions', 'widgets'));
        }
    }


    /*=============================================================
    ============= Traning Session Registration ====================
    ===============================================================*/
    public function TraningSessionRegistration(){
        $widgets = $this->dashboard->getAll();
        return view ('frontend.tschool-pages.traning-session-registration', compact( 'widgets'));
    }

    public function TraningSessionRegistrationstore(Request $request){
        $widgets = $this->dashboard->getAll();
        $data = new TrainingSessionRegistration();

         $validator = Validator::make(
            $request->all(),
            [
                'district' => 'required',
                'upozilla' => 'required',
                'institute_category' => 'required',
                'institute_name' => 'required',
                'teacher_name' => 'required',
                'designation_name' => 'required',
                'email' => 'required|unique:tsession_registration
',
                'mobile' => 'required|min:11|max:11',
            ]
        );
        // process the login
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
                $data->district = $request->district;
                $data->upozilla = $request->upozilla;
                $data->institute_category = $request->institute_category;
                $data->institute_name = $request->institute_name;
                $data->teacher_name = $request->teacher_name;
                $data->designation_name = $request->designation_name;
                $data->gender = $request->gender;
                $data->index_number = $request->index_number;
                $data->email = $request->email;
                $data->mobile = $request->mobile;
                //dd($request->all());
                $data->save();
                return redirect()->back()->with('success', 'তথ্য সফলভাবে জমা দেওয়া হয়েছে।');
            }
    }



}
