<?php

namespace App\Http\Controllers\API;

use App\Repositories\Coupon\CouponInterface;
use App\Repositories\OrdersDetail\OrdersDetailInterface;
use App\Repositories\RewardsPointHistory\RewardsPointHistoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $orderDetails;
    private $rpHistory;
    private $coupon;

    public function __construct(
        OrdersDetailInterface $orderDetails,
        RewardsPointHistoryInterface $rpHistory,
        CouponInterface $coupon
    )
    {
        $this->orderDetails = $orderDetails;
        $this->rpHistory = $rpHistory;
        $this->coupon = $coupon;
    }

    public function account()
    {
        $user = auth()->guard('api')
            ->user();

        $orderMaster = $user->orderMaster;

        $totalOrders = $orderMaster->count();

        $totalPending = $user->orderMaster()
            ->where('order_status', 'processing')
            ->count();

        $totalComplete = $user->orderMaster()
            ->where('order_status', 'done')
            ->count();

        $totalSpend = $user->orderDetails()
            ->sum('local_purchase_price');

        return response()->json(
            compact(
                'user',
                'totalOrders',
                'totalPending',
                'totalComplete',
                'totalSpend'
            )
        );
    }

    public function updateAccount(Request $request)
    {
        $user = auth()->guard('api')->user();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'phone' => 'required',
            'emergency_phone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validator->errors(),
                'result' => null
            ]);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->emergency_phone = $request->emergency_phone;
        $user->save();
        return response()->json([
            'success' => true,
            'messages' => null,
            'result' => $user
        ]);
    }

    public function updatePassword(Request $request)
    {
        $user = auth()->guard('api')->user();
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:5|confirmed',
            'current_password' => ['required', function ($attr, $val, $fail) use ($user) {
                if (!Hash::check($val, $user->password)) {
                    return $fail(__('The current password is incorrect'));
                }
            }]
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validator->errors(),
                'result' => null
            ]);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'success' => true,
            'messages' => null,
            'result' => $user
        ]);
    }

    public function updateBillingAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validator->errors(),
                'result' => null
            ]);
        }

        $user = auth()->guard('api')->user();
        $user->address = $request->address;
        $user->address_2 = $request->address_2;
        $user->district = $request->district;
        $user->postcode = $request->postcode;
        $user->save();

        return response()->json([
            'success' => true,
            'messages' => null,
            'result' => $user
        ]);
    }


    public function orderHistory()
    {
        $user = auth()->guard('api')
            ->user();

        $orderMaster = $user->orderMaster()->paginate(10);

        return response()->json(compact(
            'orderMaster'));
    }


    public function selfDashboard()
    {
        $user = auth()->guard('api')
            ->user();

        if ($user->isShowroom()) {
            $response = 'showroom/dashboard';
        } else {
            $response = 'dashboard';
        }
        return response()->json($response);
    }


    public function rpToMoney(Request $request)
    {
        if ($request->points < 10) {
            return response()->json([
                'success' => false,
                'message' => 'Rewards point can not be less than 10..',
                'results' => null
            ]);
        }

        $user = auth()->guard('api')->user();
        if ($user) {
            $user->reward_points -= $request->points;
            $user->save();

            $money = round($request->points / 10);

            $coupon = [
                'coupon_code' => 'bbuc' . $user->id . $money . $request->points . uniqid(),
                'coupon_type' => 'Coupon',
                'amount_type' => 'Fixed',
                'price' => $money,
                'purchase_min' => 1,
                'used_limit' => 1,
                'start_date' => Carbon::now()->toDateTimeString(),
                'end_date' => Carbon::now()->addDays(90)->toDateTimeString(),
                'coupon_for' => json_encode(['selected' => 'All', 'values' => []]),
                'comment' => 'Personal Coupon By ' . $user->name . ' user id ' . $user->id,
                'is_active' => true
            ];

            $createCoupon = $this->coupon->create($coupon);

            if ($createCoupon) {
                $rpHistory = [];
            }
        }
    }

}
