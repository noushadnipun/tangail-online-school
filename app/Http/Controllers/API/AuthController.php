<?php

namespace App\Http\Controllers\API;

use App\Notifications\PasswordResetRequest;
use App\PasswordReset;
use App\Repositories\Role_user\Role_userInterface;
use App\Repositories\User\UserInterface;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Boolean;

class AuthController extends Controller
{

    private $role_user;
    private $user;


    public function __construct(Role_userInterface $role_user, UserInterface $user)
    {
        $this->role_user = $role_user;
        $this->user = $user;
    }


    public function login(Request $request)
    {

        $http = new Client(['verify' => false]);

        if ($request->provider_id && $request->token) {

            $userExist = User::where('email', $request->email)->where('provider_id', $request->provider_id)
                ->where('login_token', $request->token)
                ->first();

            if ($userExist) {
                $userExist->login_token = null;
                $userExist->save();

                $request->password = 'W3lc0m3@rflbestbuysocialnetworklogin';
            }
        }

        try {

//            $authUrl = str_replace('https','http',URL::to('/oauth/token'));
            $authUrl = URL::to('/oauth/token');
            $response = $http->post($authUrl, [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('API_CLIENT_ID')??8,
                    'client_secret' => env('API_CLIENT_KEY')??'2wcADYZGPCvubNYGZNAd7XlYjMXRskRyuaZphg8Y',
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => '*',
                ],
                'http_errors' => false
            ]);

            // dd((string) $response->getBody());

            $json = json_decode((string)$response->getBody(), true);

        } catch (BadResponseException $e) {

            if ($e->getCode() === 400) {

                return response()->json([
                    'success' => false,
                    'message' => 'There something error',
                    'request' => $request
                ]);

            } elseif ($e->getCode() === 401) {

                return response()->json([
                    'success' => false,
                    'message' => 'Invalid credentials',
                    'request' => $request
                ]);

            }
        }


        $success = !isset($json['error']);
        $user = $success ? User::whereEmail($request->email)->first() : false;

        $data =
            [
                'success' => $success,
                'user' => $user,
                'token' => $json
            ];

        setcookie('user', json_encode($data), time() + (86400 * 30), "/");

        return response()->json($data);
    }


    public function loggedOut()
    {
        $user = \auth()->guard('api')->token()->revoke();

        return response()->json(true);
    }


    public function user()
    {
        $user = auth()->guard()->user();

        return response()->json($user);
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'emergency_phone' => 'required',
            'password' => 'required|min:5|confirmed',
            'term_check' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
                'result' => null
            ]);
        }

        $data = [
            'name' => $request->name,
            'ukey' => md5(uniqid().time()),
            'email' => $request->email,
            'username' => explode('@', $request->email)[0],
            'phone' => $request->phone,
            'emergency_phone' => $request->emergency_phone,
            'password' => Hash::make($request->password),
            'company' => $request->company,
            'address' => $request->address,
            'address_2' => $request->address_2,
            'district' => $request->district,
            'reward_points' => 1000,
            'postcode' => $request->postcode
        ];

        $result = \App\User::insert($data);

        if ($request->account_type == 'vendor') {
            $user = $this->user->getByEmail($request->get('email'));
            $this->role_user->roleUpdateOrCreate($user->id, 10);
        }

        if ($request->account_type == 'affiliator') {
            $user = $this->user->getByEmail($request->get('email'));
            $this->role_user->roleUpdateOrCreate($user->id, 13);
        }


        return response()->json([
            'success' => (Boolean)$result,
            'message' => null,
            'result' => $result
        ]);

    }


    public function passwordResetRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
                'result' => null
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if ($user) {


            $resetToken = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                ['token' => str_random(60)]
            );


            if ($resetToken) {
                $user->notify(new PasswordResetRequest($resetToken->token));
            }

        }

        return response()->json([
            'success' => (bool)($user->email ?? false),
            'message' => null,
            'result' => $user ? 'Sent..' : 'Failed..'
        ]);
    }


    public function passwordReset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed',
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => true,
                'message' => $validator->errors(),
                'result' => null
            ]);
        }

        $tokenExist = PasswordReset::where('token', $request->token)->first();

        if ($tokenExist && $tokenExist->token) {

            $user = User::where('email', $tokenExist->email)->first();
            if ($user) {
                $user->password = bcrypt($request->password);
                $user->save();
                $tokenExist->token = null;
                $tokenExist->save();

                return response()->json([
                    'success' => true,
                    'message' => 'Successfully Updated..',
                    'result' => 'updated'
                ]);
            }

        }


        return response()->json([
            'success' => false,
            'message' => null,
            'result' => null
        ]);


    }


}
