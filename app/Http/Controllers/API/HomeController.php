<?php

namespace App\Http\Controllers\API;

use App\Post;
use App\Product;
use App\ProductCategories;

use App\Repositories\HomeSetting\HomeSettingInterface;
use App\Repositories\Media\MediaInterface;
use App\Repositories\Slider\SliderInterface;
use App\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
    private $home_setting;
    private $slider;
    private $image;
    private $sponsored;
    private $flashSchedule;
    private $sessionToDB;

    public function __construct(
        HomeSettingInterface $home_setting,
        SliderInterface $slider,
        MediaInterface $image,
        SponserInterface $sponsored,
        FlashSheduleInterface $flashSchedule,
        SessionTaxonomyInterface $sessionToDB
    ) {
        $this->home_setting     = $home_setting;
        $this->slider           = $slider;
        $this->image            = $image;
        $this->sponsored        = $sponsored;
        $this->flashSchedule    = $flashSchedule;
        $this->sessionToDB      = $sessionToDB;
    }

    public function index(Request $request)
    {

        $home_setting          = $this->home_setting->getAll()->first();

        $sliders               = $this->image->whereIn(explode(',', $this->slider->getByAny('id', 12)->first()->images ?? 0));
        $mobilesliders         = $this->image->whereIn(explode(',', $this->slider->getByAny('id', 13)->first()->images ?? 0));
        $sponsored             = $this->sponsored->getAll();
        $featured_shops        = [];
        $flash_sales           = $this->flashSchedule->getToday();
        $categories            = Term::whereIn('id', explode('|', $home_setting->home_category))->get();

        $banner_set_one        = [];
        $categories_of_month   = [];
        $category_one          = Term::where('id', explode('|', $home_setting->cat_first))->with('sub_categories', 'category_products', 'category_products.productImage', 'category_products.productRating')->first();
        $banner_set_two        = [];
        $category_two          = Term::where('id', explode('|', $home_setting->cat_second))->with('sub_categories', 'category_products', 'category_products.productImage', 'category_products.productRating')->first();
        $category_three        = Term::where('id', explode('|', $home_setting->cat_third))->with('sub_categories', 'category_products', 'category_products.productImage', 'category_products.productRating')->first();
        $banner_set_three      = [];
        $new_arrivals          = ProductCategories::leftJoin('products', 'productcategories.main_pid', '=', 'products.id')->with('product.image', 'product.ratingSum')->where(['products.new_arrival' => 'on'])->groupBy('products.sku')->orderBy('products.created_at', 'DESC')->limit(8)->get();
        $recently_views        = [];
        $support_section       = [];


        return response()->json(compact('sliders', 'mobilesliders', 'sponsored', 'featured_shops', 'flash_sales', 'categories', 'banner_set_one', 'categories_of_month', 'banner_set_two', 'banner_set_three', 'new_arrivals', 'recently_views', 'support_section'));
    }


    public function sliders()
    {
    }


    public function categorySet(Request $request)
    {

        $home_setting          = $this->home_setting->getAll()->first();

        $cat = $request->category;

        switch ($cat) {
            case 1:
                $catId = 'cat_first';
                break;
            case 2:
                $catId = 'cat_second';
                break;
            case 3:
                $catId = 'cat_third';
                break;
            default:
                $catId = 'cat_first';
        }
        
        $cat_products = function($query){
            return $query->orderBy('products.created_at','DESC')->with('productImage','productRating')->limit(8);
        };
    


        $category          = Term::where('id', explode('|', $home_setting->$catId))->select('name','id','seo_url')
                                        ->with(!$request->loadFromApp ?['category_products' => $cat_products, 'sub_categories'] : ['category_products' => $cat_products])
                                        ->first();


        return response()->json($category);
    }



    public function justForYou(Request $request)
    {
        return response()->json($this->getJustForYou($request->self_token, $request->limit));
    }

    private function getJustForYou($self_token, $limit = 8)
    {
        $session_data = $this->sessionToDB->getByKey($self_token . '_recently_views');
        $recently_views = $session_data ? json_decode($session_data, true) : [];
        $productsView = \App\Product::whereIn('id', $recently_views)
            ->with('image', 'ratingSum')
            //->where('recommended','on')
            ->orderBy('assured_quality', 'DESC')
            ->where('stock_status', 1)
            ->where('disable_buy', '!=', 'on')
            ->limit($limit > 8 ? 20 : 8)
            ->get()
            ->toArray();

        $viewProductCount = ceil((count($productsView) / 2));

        $productDB = \App\Product::where('recommended', 'on')->with('image', 'ratingSum')
            ->inRandomOrder()
            ->orderBy('assured_quality', 'DESC')
            ->where('stock_status', 1)
            ->where('disable_buy', '!=', 'on')
            ->limit($limit - $viewProductCount)
            ->get()
            ->toArray();

        $products = Arr::random(array_merge($productsView, $productDB), $limit ? $limit : 8);
        array_multisort(array_column($products, 'assured_quality'), SORT_DESC, array_column($products, 'local_discount'), SORT_DESC, $products);

        //dd(collect($products)->unique()->toArray());
        return collect($products)->unique()->toArray();
    }


    public function bestBuyChoices(Request $request)
    {

        $products = Product::where(['products.recommended' => 'on'])->with('image', 'ratingSum')
            ->limit($request->limit ?? 8)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($products);
    }
}