<?php

namespace App\Http\Controllers\API;

use App\Brand;
use App\Cart;
use App\Comparison;
use App\Repositories\Dashboard\DashboardInterface;
use App\Repositories\Page\PageInterface;
use App\Repositories\Post\PostInterface;
use App\Repositories\Role_user\Role_userInterface;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Term\TermInterface;
use App\Repositories\User\UserInterface;
use App\User;
use Carbon\Carbon;
use DOMDocument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Exception;
use phpDocumentor\Reflection\Types\Boolean;
use function Matrix\trace;

class ShopController extends Controller
{
    private $data = [];
    private $setting;
    private $page;
    /**
     * @var PostInterface
     */
    private $post;

    /**
     * @var DashboardInterface
     */
    private $dashboard;
    /**
     * @var TermInterface
     */
    private $term;
    /**
     * @var UserInterface
     */
    private $user;
    /**
     * @var Role_userInterface
     */
    private $role_user;
    /**
     * @var $sessionToDB
     * for save session related data to db
     * its required for api
     */
    private $sessionToDB;


    private $coupon;

    private $flashSchedule;


    private $QuestionAnswer;


    private $wholeSale;


    /**
     * ShopController constructor.
     * @param SettingInterface $setting
     * @param PageInterface $page
     * @param PostInterface $post
     * @param DashboardInterface $dashboard
     * @param TermInterface $term
     * @param UserInterface $user
     * @param Role_userInterface $role_user
     * @internal param array $data
     */
    public function __construct(
        SettingInterface $setting,
        PageInterface $page,
        PostInterface $post,
        DashboardInterface $dashboard,
        TermInterface $term,
        UserInterface $user,
        Role_userInterface $role_user
    )
    {
        $this->setting        = $setting;
        $this->page           = $page;
        $this->user           = $user;
        $this->post           = $post;
        $this->dashboard      = $dashboard;
        $this->term           = $term;
        $this->role_user      = $role_user;
        // $this->middleware('auth')->only('success');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function search_product(Request $request)
    {

        $seo_url      = $request->slug;
        $get_keyworld = $request->only(['page', 'price_min', 'price_max', 'sort_by', 'sort_show', 'keyword', 'fbclid']);

        $filteringDataArr = $request->all();
        $filteringData    = [];

        foreach ($filteringDataArr as $key => $val) {
            if(strpos($key, 'filterby_') !== false) {
                $filteringData[strtolower(str_replace('filterby_', '', $key))] = explode('|', $val);
            }
        }

        $get_keyworld = array_merge($get_keyworld, $filteringData);

        //return response()->json($get_keyworld);

        $category    = false;
        $products    = false;
        $categories  = false;
        $filters_att = false;
        $filters_cat = false;
        $view_cat    = false;

        $category = $this->term->getByAny('seo_url', $seo_url)->first();

        //dd($category);

        if(!empty($category->id)) {
            $filters_cat = get_filters_cat($category->id);
            // dd($filters_cat);

            $con_att      = $category->connected_with;
            $filters_atts = $this->attribute->getFilter($con_att);

            $filters_att = [];
            foreach ($filters_atts as $f_at) {
                $p_attr = explode('|', $f_at->default_value);

                $f_at->ProccessDefault = array_map(function ($pr_data) {
                    $e2 = explode(':', $pr_data);
                    return $e2;
                }, $p_attr);

                $filters_att[] = $f_at;
            }

            $view_cat = get_all_sub_cat($category->id);


            $default    = [
                'type'   => 'category',
                'limit'  => 500,
                'offset' => 0
            ];
            $cats       = $this->get_product_categories($default);
            $categories = $cats->toArray();

        }

        $products = $this->product->getProductByFilter($get_keyworld, $view_cat);


        return response()->json(compact(
                                    'category',
                                    'products',
                                    'categories',
                                    'filters_cat',
                                    'filters_att'
                                ));

    }

    /**
     * @param array $options
     * @return mixed
     */

    public function get_product_categories(array $options = [])
    {
        $default = [
            'type'   => 'category',
            'limit'  => 10,
            'offset' => 0
        ];

        $optionss = array_merge($default, $options);

        return $this->term->get_terms_by_options($optionss);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function add_to_cart(Request $request)
    {
        $self_token = $request->self_token;
        $slug       = $request->slug;
        $main_pid = $request->main_pid;

        $column = $slug ? 'seo_url' : 'id';
        $value = $slug ? $slug : $main_pid;
        
        if($request->sku){
            $column = 'sku';
            $value = $request->sku;
        }


        $product_ifo            = $this->product
            ->getByAny($column, $value)
            ->first();
            
            
    if(!$product_ifo || $product_ifo->stock_status != 1) {
            return response()->json(false);
    }

        $product = [
            'productid' => $product_ifo->id,
            'size'      => $request->get('size'),
            'color'     => $request->get('color'),
            'qty'       => $request->get('qty'),
        ];
        

        $get_product_data = [
            'main_pid' => $product_ifo->id,
            'color'    => $request->get('color'),
            'size'     => $request->get('size'),
            'type'     => null,
        ];
        $get_fp           = get_product_price($get_product_data);
        //dd($get_fp);



        if($product_ifo->disable_buy != 'on') {
            if($product_ifo['multiple_pricing'] == 'on') {
                $m_price_infos = \App\Pcombinationdata::where(['id' => $get_fp['multi_id']])->get();
                if($m_price_infos->count() == 1) {
                    $m_price_info = $m_price_infos->first();
                    if($get_fp['has_cs']) {

                        $add_cat = [
                            'productid'      => $get_fp['productid'],
                            'productcode'    => $product_ifo->product_code . 'CIC' . $m_price_info->id,
                            'size_colo'      => $get_fp['multi_id'],
                            'purchaseprice'  => $get_fp['s_price'],
                            'qty'            => $request->get('qty'),
                            'is_dp'          => $get_fp['is_dp'],
                            'flash_discount' => $get_fp['flash_discount'],
                            'item_code'      => $get_fp['item_code'],
                            'dis_tag'        => $get_fp['save'],
                            'pre_price'      => $get_fp['r_price']
                        ];
                    }
                    else {
                        $add_cat = [];
                    }
                }
                else {
                    $add_cat = [];
                }
            }
            else {
                $add_cat = [
                    'productid'      => $get_fp['productid'],
                    'productcode'    => $get_fp['productcode'],
                    'size_colo'      => $get_fp['multi_id'],
                    'purchaseprice'  => $get_fp['s_price'],
                    'qty'            => $request->get('qty'),
                    'is_dp'          => $get_fp['is_dp'],
                    'flash_discount' => $get_fp['flash_discount'],
                    'item_code'      => $get_fp['item_code'],
                    'dis_tag'        => $get_fp['save'],
                    'pre_price'      => $get_fp['r_price']
                ];
            }
        }
        else {
            $add_cat = [];
        }


        //dd($add_cat);
        if($add_cat) {
            $self_token = $request->get('self_token');


            $oldcart = null;
            $ocart   = $this->sessionToDB->getByKey($self_token . '_cart');
            $oldcart = $ocart?(Object)json_decode($ocart, true) : null;
            //dd($oldcart);


            $cart = new Cart($oldcart);
            //dd($cart);
            //$cart->add($product, $request->get('productcode'));
            $cart->add($add_cat, $add_cat['productcode']);
            //dd($cart);
            //return response()->json($cart);

            $this->sessionToDB->updateOrCreate($self_token . '_cart', json_encode($cart));

            $nscart  = $this->sessionToDB->getByKey($self_token . '_cart');
            $newcart = $nscart?(Object)json_decode($nscart, true) : null;
            //dd($newcart);
            //$request->session()->put('cart', $cart); because session save to db..
            //$newcart = Session::has('cart') ? Session::get('cart') : null;


            $ncart = new Cart($newcart);
            //dd($ncart);

            if(!empty($ncart->items)) {
                $total_qty        = array_sum(array_column($ncart->items, 'qty'));
                $individual_price = [];
                foreach ($ncart->items as $item) {
                    //dd($item);
                    $individual_price[] = $item['purchaseprice'] * $item['qty'];
                }
                $totalprice = array_sum($individual_price);
            }
            else {
                $total_qty  = 0;
                $totalprice = number_format(0);
            }


            $pro = $this->product->getById($request->get('main_pid'));


            $report = true;

            return response()->json(compact(
                                        'pro',
                                        'totalprice',
                                        'total_qty',
                                        'report',
            'ncart'
                                    ));
        }
        else {
            return response()->json(['pro' => null, 'totalprice' => null, 'total_qty' => null, 'report' => false]);
        }
    }


    public function remove_cart_item(Request $request)
    {
        $code       = $request->product_code;
        $self_token = $request->self_token;

        if(!$code || !$self_token) {
            return response()->json(false);
        }


        $ocart   = $this->sessionToDB->getByKey($self_token . '_cart');
        $oldcart = $ocart?(Object)json_decode($ocart, true) : null;
        $cart    = new Cart($oldcart);

        $removeItem = $cart->items[$code] ?? false;

        if($removeItem) {
            $cart->totalqty   = $cart->totalqty - $removeItem['qty'];
            $cart->totalprice = $cart->totalprice - ($removeItem['purchaseprice'] * $removeItem['qty']);
            unset($cart->items[$code]);
            $this->sessionToDB->updateOrCreate($self_token . '_cart', json_encode($cart));
        }

        return response()->json($removeItem);
    }


    public function update_cart(Request $request)
    {
        $self_token = $request->self_token;
        $ocart      = $this->sessionToDB->getByKey($self_token . '_cart');
        $cart       = $ocart?(Object)json_decode($ocart, true) : null;

        foreach ($request->items as $key => $item) {

            if($cart && isset($cart->items[$key])) {
                $previousQuantity = $cart->items[$key]['qty'];
                $cart->totalqty   -= $previousQuantity;
                $cart->totalprice -= $previousQuantity * $cart->items[$key]['purchaseprice'];

                $cart->items[$key]['qty'] = $item;

                $currentQuantity  = $cart->items[$key]['qty'];
                $cart->totalqty   += $currentQuantity;
                $cart->totalprice += $currentQuantity * $cart->items[$key]['purchaseprice'];

                $calculated = true;
            }

        }

        if(isset($calculated)) {
            $this->sessionToDB->updateOrCreate($self_token . '_cart', json_encode($cart));
        }

        return response()->json(isset($calculated));
    }


    public function cart(Request $request)
    {
        $self_token = $request->self_token;


        if(!$self_token) {
            return response()->json(false);
        }

        $session_data = $this->sessionToDB->getByKey($self_token . '_cart');
        $session_data = $session_data?(Object)json_decode($session_data, true) : false;

        $items = $this->proccessCartWithDeliveryCharge($session_data, $self_token);

        if(!$items) {
            return response()->json(false);
        }

        $groupByShippingTime = $this->cartGroupingByShipment($items);

        if(($self_token != 'thisistokenforonbehalfbuy') || ($self_token == 'thisistokenforonbehalfbuy' && $request->admin_delivery_charge != 1)){
            $totalDeliveryCharge = array_sum(array_column($groupByShippingTime, 'delivery_charge'));
            $this->sessionToDB->updateOrCreate($self_token . '_total_delivery_charge', json_encode($totalDeliveryCharge));
        }else{
            $totalDeliveryCharge = $this->sessionToDB->getByKey($self_token . '_total_delivery_charge');
            $totalDeliveryCharge = $totalDeliveryCharge ? json_decode($totalDeliveryCharge) : 0;
        }

        $totalqty     = $session_data->totalqty ?? 0;
        $totalprice   = $session_data->totalprice ?? 0;
        $discount     = $this->getCouponDiscount($self_token, $session_data);
        $paymentFirst = $this->paymentFirstProductAvailable($items);

        return response()->json(compact(
                                    'items',
                                    'totalqty',
                                    'totalprice',
                                    'discount',
                                    'groupByShippingTime',
                                    'totalDeliveryCharge',
                                    'paymentFirst'
                                ));
    }


    private function proccessCartWithDeliveryCharge($cart, $self_token)
    {

        $location_session_data = $this->sessionToDB->getByKey($self_token . '_user_location');
        $location_session_data = $location_session_data?(Object)json_decode($location_session_data, true) : (Object)['division' => 'Dhaka'];


        $items = $cart?array_map(function ($item) use ($location_session_data) {
            $product              = $this->product->getById($item['item']['productid']);
            $item['item']['info'] = $product;

            if($product && $location_session_data) {
                $delivery_location         = $product->getDeliveryLocationWithCharge()
                                                     ->where('delivery_location', $location_session_data->division ?? 'Dhaka')
                                                     ->first()
                ;

                $item['delivery_location'] = $delivery_location->delivery_location ?? '';
                $item['delivery_charge']   = $delivery_location->delivery_charge ?? 0;
                $item['timeFrame']         = ($product->delivery_process ?? '') . ' ' . ($product->delivery_time ?? '');

            }


            return $item;
        }, $cart->items) : false;

        return $items;
    }


    private function cartGroupingByShipment($items)
    {
        $groupByShippingTime = [];

        if(is_array($items)) {

            foreach ($items as $key => $item) {
                $groupByShippingTime[$item['timeFrame'] ?? 0]['items'][$key]       = $item;
                $groupByShippingTime[$item['timeFrame'] ?? 0]['delivery_charge'][] = $item['delivery_charge'] ?? 0;
                $groupByShippingTime[$item['timeFrame'] ?? 0]['timeFrame'] = $item['timeFrame'] ?? 0;
            }

        }

        $groupByShippingTime = array_map(function ($item) {
            $item['delivery_charge'] = max($item['delivery_charge']);
            return $item;
        }, $groupByShippingTime);

        return $groupByShippingTime;
    }

    private function paymentFirstProductAvailable($items)
    {
        foreach ($items as $item) {

            if(($item['item']['info']->payment_first ?? 0) == 1) {
                return true;
            }
        }

        return false;
    }


    public function add_to_compare(Request $request)
    {
        $product    = [
            'productid'   => $request->get('productid'),
            'productcode' => $request->get('productcode'),
            'seo_url'     => $request->get('seo_url'),
        ];
        $self_token = $request->self_token;
        $ocompare   = $this->sessionToDB->getByKey($self_token . '_compare');
        $oldcompare = $ocompare?(Object)json_decode($ocompare, true) : null;
        //dd($oldcompare);
        $compare = new Comparison($oldcompare);
        $compare->add($product, $request->get('productcode'));
        //dd($compare);

        $this->sessionToDB->updateOrCreate($self_token . '_compare', json_encode($compare));
        //$request->session()->put('comparison', $compare);

        $pro = $this->product->getById($request->get('productid'));

        $regularprice = $pro->local_selling_price;
        $save         = ($pro->local_selling_price * $pro->local_discount) / 100;
        $price        = $regularprice - $save;

        $ncompare      = $this->sessionToDB->getByKey($self_token . '_compare');
        $newcompare    = $ncompare?(Object)json_decode($ncompare, true) : null;
        $compareItems  = new Comparison($newcompare);
        $total_compare = count($compareItems->items);
        //dd($total_com);

        return response()->json(
            compact(
                'compareItems',
                'total_compare',
                'pro',
                'price'
            )
        );
    }

    public function removeCompare(Request $request)
    {
        $code       = $request->product_code;
        $self_token = $request->self_token;

        if(!$code || !$self_token) {
            return response()->json(false);
        }


        $ocompare   = $this->sessionToDB->getByKey($self_token . '_compare');
        $oldcompare = $ocompare?(Object)json_decode($ocompare, true) : null;
        $compare    = new Comparison($oldcompare);
        $removeItem = $compare->items[$code] ?? false;

        if($removeItem) {
            unset($compare->items[$code]);
            $this->sessionToDB->updateOrCreate($self_token . '_compare', json_encode($compare));
        }

        return response()->json($removeItem);
    }

    public function view_compare(Request $request)
    {
        $self_token = $request->self_token;

        if(!$self_token) {
            return response()->json(false);
        }

        $compare = $this->sessionToDB->getByKey($self_token . '_compare');
        $compare = $compare?(Object)json_decode($compare, true) : false;

        $products = $compare?array_map(function ($item) {
            // $product = $this->product->getById($item['item']['productid']);
            return $this->product->getById($item['item']['productid']);
        }, $compare->items) : false;


        $total_products = count($products?$products : []);

        return response()->json(
            compact(
                'products',
                'total_products'
            )
        );
    }


    public function wishlists()
    {
        $wishlists = auth()->guard('api')->user()->wishlists ?? [];
        return response()->json($wishlists);
    }


    public function addWishList(Request $request)
    {
        if(auth()->guard('api')->check() && $request->product_id) {

            $data = [
                'product_id' => $request->product_id,
                'user_id'    => auth()->guard('api')->id()
            ];

            $result = \App\Wishlist::create($data);
            $result->product->image;

            return response()->json($result);
        }
        return response()->json(false);
    }

    public function removeFromWishList(Request $request)
    {
        if(auth()->guard('api')->check() && $request->product_id) {
            $wish = \App\Wishlist::where('product_id', $request->product_id)->where('user_id', auth()->guard('api')->id())->first();

            return response()->json($wish?$wish->delete() : false);
        }

        return response()->json(false);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function apply_coupon_voucher(Request $request)
    {

        $self_token = $request->self_token;
        $coupon     = $request->coupon;
        $success    = false;
        $message    = '';

        if(!$self_token || !$coupon) {
            $message = 'There is something error, try again later.';
        }
        else {
            $coupon = $this->coupon->getByAny('coupon_code', $coupon)->first();

            if($coupon) {
                // dd(now()->timestamp , strtotime($coupon->start_date),now()->timestamp > strtotime($coupon->start_date));

                if(strtotime($coupon->start_date) <= now()->timestamp && strtotime($coupon->end_date) >= now()->timestamp && $coupon->usageHistory->count() < $coupon->used_limit) {
                    $this->sessionToDB->updateOrCreate($self_token . '_coupon', $coupon->id);
                    $success = true;
                    $message = "Coupon successfully applied.";
                }
                else {
                    $success = false;
                    $message = "The coupon is expired.";
                }

            }
            else {
                $message = 'Coupon Does not exist..';
            }

        }

        $response = [
            'success' => $success,
            'message' => $message
        ];
        return response()->json($response);
    }


    private function getCouponDiscount($self_token, $cart)
    {
        $session_data = $this->sessionToDB->getByKey($self_token . '_coupon');
        if(!$session_data) {
            return 0;
        }

        $coupon = $this->coupon->getById($session_data);
        //dd(strtotime($coupon->start_date),strtotime($coupon->end_date),now()->timestamp,strtotime($coupon->start_date) <= now()->timestamp);
        if(!$coupon || ($coupon
                        && (!(strtotime($coupon->start_date) <= now()->timestamp
                              && strtotime($coupon->end_date) >= now()->timestamp
                              && $coupon->usageHistory->count() < $coupon->used_limit)) || !($coupon->purchase_min <= $cart->totalprice ?? 0))) {
            // dd(0);
            return 0;
        }


        $couponFor = json_decode($coupon->coupon_for);

        if($couponFor->selected == 'All') {
            if($coupon->amount_type == 'Fixed') {
                return $coupon->price;
            }
            else {
                $totalPrice = $cart->totalprice ?? 0;
                $discount   = ($totalPrice * $coupon->price) / 100;
                $discount   = $coupon->upto_amount && $discount > $coupon->upto_amount?$coupon->upto_amount : $discount;
                return $discount;
            }

        }
        elseif($couponFor->selected == 'Product') {
            $productDiscount = 0;

            foreach ($cart->items as $item) {
                if(in_array($item['item']['productid'], $couponFor->values)) {
                    if($coupon->amount_type == 'Fixed') {
                        $productDiscount += $coupon->price * $item['qty'];
                    }
                    else {
                        $productDiscount += (($coupon->price * $item['purchaseprice']) / 100) * $item['qty'];
                    }
                }
            }

            return $coupon->upto_amount && $productDiscount > $coupon->upto_amount?$coupon->upto_amount : $productDiscount;

        }
        elseif($couponFor->selected == 'Category') {
            ///Will Do Later...
        }elseif($couponFor->selected == 'User'){
            
            $requestedUser = auth()->guard('api')->user();
            $requestedUserId = $requestedUser->id??0;

            if(in_array($requestedUserId, $couponFor->values)){
                return $coupon->price;
            }

            return 0;
        }


        return 0;
    }


    public function checkoutUserDefaultDetails(Request $request)
    {

        $self_token = $request->self_token;
        $logged     = auth()->guard('api')->check();
        $loggedUser = auth()->guard('api')->check()?auth()->guard('api')->user() : false;

        $session_data = $this->sessionToDB->getByKey($self_token . '_user_details');
        $session_data = $session_data?(Object)json_decode($session_data, true) : false;

        $location_session_data = $this->sessionToDB->getByKey($self_token . '_user_location');
        $location_session_data = $location_session_data?(Object)json_decode($location_session_data, true) : false;


        return response()->json(
            [
                'name'              => $loggedUser->name ?? ($session_data->name ?? ''),
                'phone'             => $loggedUser->phone ?? ($session_data->phone ?? ''),
                'emergency_phone'   => $loggedUser->emergency_phone ?? ($session_data->emergency_phone ?? ''),
                'email'             => $loggedUser->email ?? ($session_data->email ?? ''),
                'division'          => $location_session_data->division ?? '',
                'district'          => $location_session_data->district ?? '',
                'thana'             => $location_session_data->thana ?? '',
                'deliveryfee'       => $loggedUser->name ?? ($session_data->name ?? ''),
                'address'           => $loggedUser->address ?? ($session_data->name ?? ''),
                'dba'               => false,
                'dba_name'          => '',
                'dba_division'      => '',
                'dba_district'      => '',
                'dba_thana'         => '',
                'dba_address'       => '',
                'notes'             => '',
                'username'          => $loggedUser->username ?? ($session_data->name ?? ''),
                'password'          => '',
                'create'            => false,
                'logged'            => $logged
            ]
        );
    }


    public function submitCheckoutUserDetails(Request $request)
    {
        $self_token = $request->self_token;
        $user       = (object)$request->user;


        if(!$self_token || !$user) {
            return response()->json(false);
        }


        $validator = Validator::make($request->user, [
            'name'            => 'required|min:3|max:255',
            'phone'           => 'required|min:11|max:11',
            'emergency_phone' => 'required|min:11|max:11|different:phone',
            'email'           => 'required',
            'address'         => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(false);
        }


        $session_data   = $this->sessionToDB->getByKey($self_token . '_cart');
        $session_data   = $session_data?(Object)json_decode($session_data, true) : false;
        $existUser      = $this->user->getByEmail($user->email);
        $paymentSetting = $this->paymentsetting->getAll();
        $paymentSetting = $paymentSetting->first();

        $cart_price = 0;
        $cart_price += $session_data->totalprice ?? 0;

//
//        if($cart_price > $paymentSetting->decidable_amount)
//        {
//            $deliveryCharge = 0;
//        }
        $deliveryChargeFromCart = $this->sessionToDB->getByKey($self_token . '_total_delivery_charge');
        $deliveryChargeFromCart = isset($deliveryChargeFromCart)?json_decode($deliveryChargeFromCart, true) : 0;
        $deliveryCharge         = $deliveryChargeFromCart;

        $userDetailsAttributes = [
            'name'            => $user->name,
            'email'           => $user->email,
            'username'        => $user->username,
            'address'         => $user->address,
            'phone'           => $user->phone,
            'emergency_phone' => $user->emergency_phone,
            'district'        => $user->district,
            'deliveryfee'     => $deliveryCharge
        ];


        if(empty($existUser)) {
            ///User account must create if not exist...

            $nonExistAttributes = [
                'password'  => bcrypt($user->password ?? '12345'),
                'is_active' => 1
            ];

            $nonExistAttributes = array_merge($nonExistAttributes, $userDetailsAttributes);

            $createUser = $this->user->create($nonExistAttributes);


            if(!empty($createUser)) {
                $roleAttributes = [
                    'role_id' => 8,
                    'user_id' => $createUser->id
                ];

                $this->role_user->create($roleAttributes);

                $createUserId = [
                    'user_id' => [$createUser->id]
                ];

                $userDetailsAttributes = array_merge($userDetailsAttributes, $createUserId);

            }
        }

        if(!empty($existUser)) {
            $existUserId = [
                'user_id' => [$existUser->id]
            ];

            $userDetailsAttributes = array_merge($existUserId, $userDetailsAttributes);
        }

        $extraField = [
            'dba'               => $user->dba,
            'dba_name'          => $user->dba_name,
            'dba_address'       => $user->dba_address,
            'dba_division'      => $user->dba_division,
            'dba_district'      => $user->dba_district,
            'dba_thana'         => $user->dba_thana,
            'notes'             => $user->notes,
            'division'          => $user->division,
            'thana'             => $user->thana
        ];

        $userDetailsAttributes = array_merge($extraField, $userDetailsAttributes);

        $this->sessionToDB->updateOrCreate($self_token . '_user_details', json_encode($userDetailsAttributes));

        return response()->json($userDetailsAttributes);

    }


    public function getCheckoutUserDetails(Request $request)
    {
        $self_token = $request->self_token;

        $session_data = $this->sessionToDB->getByKey($self_token . '_user_details');
        $session_data = $session_data?(Object)json_decode($session_data, true) : false;

        return response()->json($session_data);
    }

    public function getPaymentSetting(Request $request)
    {
        $paymentSetting = $this->paymentsetting->getAll()->first();

        return response()->json($paymentSetting);
    }

    public function storePaymentMethod(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'self_token'     => 'required',
            'payment_method' => 'required',
            'term_check'     => 'required'
        ]);
        //return response()->json($validator->errors());

        if($validator->fails()) {
            return response()->json(['submit' => true, 'success' => false, 'result' => $request->all()]);
        }

        $self_token     = $request->self_token;
        $payment_method = $request->payment_method;
        $term_check     = $request->term_check;

        $user_details = $this->sessionToDB->getByKey($self_token . '_user_details');
        $user_details = $user_details?(Object)json_decode($user_details, true) : false;

        $cart = $this->sessionToDB->getByKey($self_token . '_cart');
        $cart = $cart?(Object)json_decode($cart, true) : false;


        $attributes = [
            'currency'       => 'BDT',
            'total_amount'   => $cart->totalprice ?? 0,
            'grand_total'    => ($cart->totalprice ?? 0) + ($user_details->deliveryfee ?? 0),
            'payment_method' => $payment_method,
            'terms_check'    => $term_check,
            'payment_id'     => null
        ];

        $this->sessionToDB->updateOrCreate($self_token . '_payment_method', json_encode($attributes));

        $session_data = $this->sessionToDB->getByKey($self_token . '_payment_method');
        $session_data = $session_data?(Object)json_decode($session_data, true) : false;

        return response()->json(['submit' => true, 'success' => true, 'result' => $session_data]);
    }


    public function checkoutUserPaymentMethod(Request $request)
    {
        $self_token = $request->self_token;

        $session_data = $this->sessionToDB->getByKey($self_token . '_payment_method');
        $session_data = $session_data?(Object)json_decode($session_data, true) : false;

        return response()->json($session_data);
    }


    public function product_details(Request $request)
    {
        $self_token = $request->self_token;
        $slug       = $request->slug;
        $productId = $request->productId;

        $column = $slug ? 'seo_url' : 'id';
        $value = $slug ? $slug : $productId;
        
        if($request->sku){
            $column = 'sku';
            $value = $request->sku;
        }


        $product            = $this->product
            ->getByAny($column, $value)
            ->first();
            
        if(!$product){
          return response()->json(false);
        }
            
        $product->flashTime = isset($product->flashItem)?$this->flashSchedule->getTodayFlashOnly() : null;
        $product->category  = $product->categories()->first()->term??null;
        $product->images;
        $product->attributes;
        $product->relatedProducts = $product->relatedProducts()->with('product.image', 'product.ratingSum')->get();

        $SizeCol         = \App\Product::where('id', $product->id)->with('colors.sizes')
                                       ->first();
        $product->colors = $SizeCol->colors;


        $session_data   = $this->sessionToDB->getByKey($self_token . '_recently_views');
        $recently_views = $session_data?json_decode($session_data, true) : [];

        $recently_views[time()] = $product->id;
        //$recently_views = array_unique($recently_views);
        krsort($recently_views);
        $recently_views = array_slice($recently_views, 0, 50);
        $this->sessionToDB->updateOrCreate($self_token . '_recently_views', json_encode($recently_views));


        // $similar_products                = $this->product->getSimilarProduct($product->id);

        $similarWord      = substr($product->title, 0, 6);
        $similar_products = \App\Product::where('title', 'like', '%' . $similarWord . '%')
                                        ->with('image', 'ratingSum')
                                        ->limit(8)
                                        ->get();


        $whoBoughts = $this->product
            ->getSimilarProduct($product->id);

        return response()->json(compact(
                                    'product',
                                    'similar_products',
                                    'whoBoughts'
                                ));

    }


    public function recentlyViewProducts(Request $request)
    {
        $self_token = $request->self_token;

        $session_data   = $this->sessionToDB->getByKey($self_token . '_recently_views');
        $recently_views = $session_data?json_decode($session_data, true) : [];
        krsort($recently_views);
        $products = \App\Product::whereIn('id', $recently_views)
                                ->with('image', 'ratingSum')
                                ->limit(15)
                                ->get();

        return response()->json($products);
    }


    public function pay_now(Request $request)
    {
        $self_token = $request->self_token;

        //dd($data);
        $rand       = str_random(15);
        $secret_key = str_random(50);
        //dd($request);

        $user_details = $this->sessionToDB->getByKey($self_token . '_user_details');
        $user_details = $user_details?(Object)json_decode($user_details, true) : null;

        $cart = $this->sessionToDB->getByKey($self_token . '_cart');
        $cart = $cart?(Object)json_decode($cart, true) : null;

        ///Coupon
        $totalDiscount = $this->getCouponDiscount($self_token, $cart);
        $couponId      = $this->sessionToDB->getByKey($self_token . '_coupon');
        $getCoupon     = $couponId?$this->coupon->getById($couponId) : false;

        $couponDetails = [
            'coupon_type'     => 'Coupon',
            'coupon_code'     => $getCoupon->coupon_code ?? null,
            'coupon_discount' => $totalDiscount
        ];


        $payment_method              = $this->sessionToDB->getByKey($self_token . '_payment_method');
        
        // dd($payment_method);
        
        if($payment_method){
                    $payment_method              = $payment_method?(Object)json_decode($payment_method, true) : null;
                    $payment_method->grand_total -= $totalDiscount;
        }



        $data = [
            'user_details'   => (array)$user_details,
            'cart'           => $cart,
            'payment_method' => (array)$payment_method,
            'coupon_details' => $couponDetails,
            'ob_payment_status' => $request->onbehalf_pm_status,
            'ob_order_status' => $request->onbehalf_od_status,
            'aff_key' => $request->aff_key
        ];


        $attributes = [
            '_token'         => json_encode($self_token),
            '_previous'      => json_encode('hello'),
            'cart'           => json_encode($cart),
            'coupon_details' => json_encode($couponDetails),
            'user_details'   => json_encode($user_details),
            'payment_method' => json_encode($payment_method)
        ];
        // dd($attributes);
        $temp_ensuring = $this->temporaryorder->create($attributes);
        if($temp_ensuring) {
            return response()->json($this->place_order($data, $rand, $secret_key, $self_token));
        }

        return response()->json([
                                    'success' => false,
                                    'result'  => null
                                ]);

    }


    /**
     * @param $data
     * @param $rand
     * @param $secret_key
     * @param $self_token
     * @return \Illuminate\Http\RedirectResponse
     */
    private function place_order($data, $rand, $secret_key, $self_token)
    {
        //dd($data);

        $user_id     = !empty($data['user_details']['user_id'])?$data['user_details']['user_id'][0] : null;
        $orderStatus = false;
        
        if($data['payment_method']){
            
           if($data['payment_method']['payment_method'] == 'cash_on_delivery') {
            $orderStatus = $this->paymentCashOnDelivery($data, $rand, $secret_key, $user_id, $self_token);
        }elseif($data['payment_method']['payment_method'] != 'cash_on_delivery')
        {
            $orderStatus = $this->paymentSSLCommerz($data, $rand, $secret_key, $user_id, $self_token);
        }         
            
            
        }

       

        return $orderStatus?isset($orderStatus['success']) && $orderStatus['success']?$orderStatus : false : false;
    }

    private function order_shipment_create($data, $rand, $secret_key, $user_id, $self_token)
    {
        if(!$data) {
            return false;
        }
        $cart = $data['cart'];

        $items           = $this->proccessCartWithDeliveryCharge($cart, $self_token);
        $groupByShipment = $this->cartGroupingByShipment($items);


        foreach ($groupByShipment as $time => $charge) {
            $shipmentCount       = \App\OrderShipment::where('order_random', $rand)->count();
            $groupByShipmentData = [
                'order_random'    => $rand,
                'time_frame'      => $time,
                'delivery_charge' => $charge['delivery_charge'] ?? 0,
                'status'          => 'Placed',
                'serial'          => $shipmentCount + 1
            ];

            $shipment = \App\OrderShipment::create($groupByShipmentData);
            
            if(!empty($shipment->id)) {
                
                $data['cart']->items  = $charge['items'];
                 
                $order_detail_created = order_detail_create($data, $rand, $secret_key, $user_id, $shipment->id);
                $this->deleteCartSessionHistory($self_token,true);
                
            }

        }
        
       
        
        if(isset($shipment))
        {
            $orderMaster = $shipment->orderMaster;

            if($self_token == 'thisistokenforonbehalfbuy'){
            $orderMaster->ob_order = 1;
            $orderMaster->payment_term_status = $data['ob_payment_status']?? 'Pending';
            $orderMaster->order_status = $data['ob_order_status']?? 'Placed';
            $orderMaster->save();
            }

            $message = \App\Dashboard::find(46)->description??'';
            $phones = \App\Dashboard::find(45)->description??'';
            $phones = explode(',',$phones);
            $phones[] = $orderMaster->phone??false;

            sendSmsFormatting($phones,$orderMaster->id??'',$orderMaster->customer_name??'',$message);
        }
        return isset($shipment);
    }

    private function deleteCartSessionHistory($self_token,$delete = false)
    {
        if($delete) {
            $this->sessionToDB->deleteByKey($self_token . '_cart');
            $this->sessionToDB->deleteByKey($self_token . '_user_details');
            $this->sessionToDB->deleteByKey($self_token . '_payment_method');
            $this->sessionToDB->deleteByKey($self_token . '_total_delivery_charge');
            $this->sessionToDB->deleteByKey($self_token . '_coupon');
        }

    }

    ////Payment Gateway


    /**
     * @param $data
     * @param $rand
     * @param $secret_key
     * @param $user_id
     * @param $self_token
     * @return array
     */
    private function paymentCashOnDelivery($data, $rand, $secret_key, $user_id, $self_token)
    {

        $orders_master_attributes = order_master_create($data, $rand, $secret_key, $user_id);

        try {
            //dd($orders_master_attributes);
            $orderplacing                      = $this->ordersmaster->create($orders_master_attributes);
            $orderplacing->payment_term_status = 'Pending';
            $orderplacing->save();
            //dd($orderplacing);
            $order_shipment_crated = $this->order_shipment_create($data, $rand, $secret_key, $user_id, $self_token);
            

            if(!empty($orderplacing)) {

                return [
                    'success' => true,
                    'result'  => $orderplacing
                ];

                //redirect('/checkout/success?order_id=' . $orderplacing->id . '&random_code=' . $rand . '&secret_key=' . $secret_key)->send();
            }
            else {
                return [
                    'success' => false,
                    'result'  => null
                ];
            }
        } catch (\Illuminate\Database\QueryException $ex) {

            return [
                'success' => false,
                'result'  => null
            ];

        }
    }


    /**
     * @param $data
     * @param $rand
     * @param $secret_key
     * @param $user_id
     * @param $self_token
     * @return array|string
     */
    private function paymentSSLCommerz($data, $rand, $secret_key, $user_id, $self_token)
    {
        $orders_master_attributes = order_master_create($data, $rand, $secret_key, $user_id);


        try {
            $orderplacing = $this->ordersmaster->create($orders_master_attributes);

            //dd($orderplacing);
            if (!empty($orderplacing)) {
                $order_shipment_crated = $this->order_shipment_create($data, $rand, $secret_key, $user_id, $self_token);
                //$msg_for_customer = 'Order has been placed. Order ID: ' . $orderplacing->id;
//                    sendSMS($orders_master_attributes['phone'] . ',' . $admins_nos, $msg_for_customer);

                $user_data = $this->user->getById($user_id);
                $cur_random_value = rand_string(18);
                $qty = [];
                $price = [];
                foreach ($data['cart']->items as $item) {
                    $qty[] = $item['qty'];
                    $price[] = $item['purchaseprice'];
                }
                //$total_amount = array_sum($price);
                //$delivery_charge = $data['user_details']['deliveryfee'];

                $grand_total = $data['payment_method']['grand_total'];

                //dd($grand_total);

                // Here you have to receive all the order data to initate the payment.
                // Lets your oder trnsaction informations are saving in a table called "orders"
                // In orders table order uniq identity is "order_id","order_status" field contain status of the transaction, "grand_total" is the order amount to be paid and "currency" is for storing Site Currency which will be checked with paid currency.
                $post_data = [];
                $post_data['total_amount'] = $grand_total; // You cant not pay less than 10
                $post_data['currency'] = 'BDT';
                $post_data['tran_id'] = $cur_random_value; // tran_id must be unique
                //Start to save these value  in session to pick in success page.
//                $_SESSION['payment_values']['tran_id'] = $post_data['tran_id'];
//                $orderplacing->trans_id = $post_data['tran_id'];
//                $orderplacing->save();
                //End to save these value  in session to pick in success page.
                $post_data['success_url'] = config('app.url').'/summary?order_id=' . $orderplacing->id . '&random_code=' . $rand . '&secret_key=' . $secret_key;;
                $post_data['fail_url'] = url('checkout/fail');
                $post_data['cancel_url'] = url('checkout/cancel');
                // CUSTOMER INFORMATION
                $post_data['cus_name'] = !empty($user_data->name) ? $user_data->name : 'Test Customer';
                $post_data['cus_email'] = !empty($user_data->email) ? $user_data->email : 'Test Customer Email';
                $post_data['cus_add1'] = !empty($user_data->address) ? $user_data->address : 'Test Customer Address';
                $post_data['cus_add2'] = '';
                $post_data['cus_city'] = '';
                $post_data['cus_state'] = !empty($user_data->district) ? $user_data->district : 'Test Customer District';
                $post_data['cus_postcode'] = '';
                $post_data['cus_country'] = 'Bangladesh';
                $post_data['cus_phone'] = !empty($user_data->phone) ? $user_data->phone : 'Test Customer District';
                $post_data['cus_fax'] = '';
                // SHIPMENT INFORMATION
                $post_data['ship_name'] = !empty($user_data->name) ? $user_data->name : 'Test Customer';
                $post_data['ship_add1 '] = !empty($user_data->address) ? $user_data->address : 'Test Customer Address';
                $post_data['ship_add2'] = '';
                $post_data['ship_city'] = '';
                $post_data['ship_state'] = !empty($user_data->district) ? $user_data->district : 'Test Customer District';
                $post_data['ship_postcode'] = '';
                $post_data['ship_country'] = 'Bangladesh';
                // OPTIONAL PARAMETERS
                $post_data['value_a'] = $orderplacing->id; // order master id
                $post_data['value_b'] = $rand; // order random id
                $post_data['value_c'] = $orderplacing->grand_total; // grand total
                $post_data['value_d'] = $orderplacing->total_amount; // total amount

                $sslc = new SSLCommerz();
                $payment_options = $sslc->initiate($post_data, false);

                return $payment_options;

            } else {
                return ['success' => false];
            }
        }catch (Exception $exception)
        {
            return ['success' => false];
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sslCommerzSuccessListener(Request $request)
    {
        $result = false;

        if (!empty($request->tran_id)) {
            $tran_id = $request->tran_id;
            $orders_master = $this->ordersmaster->getById($request->value_a);
            //dd($orders_master);
            $sslc = new SSLCommerz();

            if ($orders_master->payment_term_status == 'Pending') {
                $validation = $sslc->orderValidate($tran_id, $orders_master->grand_total, $orders_master->currency, $request->all());
                //dd($validation);
                if ($validation == true) {
                    /**
                     * That means IPN did not work or IPN URL was not set in your merchant panel. Here you need to update order status in order table as Processing or Complete.
                     * Here you can also sent sms or email for successful transaction to customer
                     */

                    $orders_master = $this->ordersmaster->update($request->value_a, ['payment_term_status' => 'Successful', 'trans_id' => $tran_id]);
                    $result = true;

                } else {
                    /**
                     * That means IPN did not work or IPN URL was not set in your merchant panel and Transation validation failed.
                     * Here you need to update order status as Failed in order table.
                     */
                    $orders_master = $this->ordersmaster->update($request->value_a, ['payment_term_status' => 'Failed', 'trans_id' => $tran_id]);
                    $msg = 'Validation Fail';
                    $result = false;
                }
            } elseif ($orders_master->payment_term_status == 'Processing' || $orders_master->payment_term_status == 'Complete') {
                /** That means through IPN Order status already updated.
                 * Now you can just show the customer that transaction is completed.
                 * No need to udate database.
                 */

                $msg = 'Transaction is successfully Complete';
                $result = true;

            } else {
                //That means something wrong happened. You can redirect customer to your product page.
                $msg = 'Invalid Transaction';
                $result = false;
            }
            //dd($orders_detail);
        }

        return response()->json(['success' => $result]);
    }


    public function orderSuccess(Request $request)
    {

        $order_id    = $request->order_id;
        $random_code = $request->random_code;
        $secret_key  = $request->secret_key;
        $self_token  = $request->self_token;

        if(!$order_id || !$random_code || !$secret_key || !$self_token) {
            return response()->json([
                                        'success' => false
                                    ]);
        }

        $orders_master   = $this->ordersmaster->getByRandom($random_code);
        $user            = $this->user->getById($orders_master->user_id);
        $order_shipments = $orders_master->orderShipments()->with('orderDetails.product.image')->get();

        $success = true;

        return response()->json(
            compact(
                'success',
                'orders_master',
                'user',
                'order_shipments'
            )
        );
    }

    public function getUserDeliveryLocation(Request $request)
    {
        $self_token = $request->self_token;
        $product_id = $request->product_id;

        $session_data = $this->sessionToDB->getByKey($self_token . '_user_location');
        $session_data = $session_data?(Object)json_decode($session_data, true) : false;

        if($product_id && !empty($session_data->division)) {
            $product                    = \App\Product::where('id', $product_id)->first();
            $delivery_location          = $product->getDeliveryLocationWithCharge()->where('delivery_location', $session_data->division)->first();
            $session_data->deliveryInfo = $delivery_location;

            if($delivery_location) {
                $session_data->deliveryInfo->timeFrame = $product->delivery_process . ' ' . $product->delivery_time;
            }
        }

        if(!$session_data) {
            $ifNotData = [
                'division' => 'Dhaka',
                'district' => 'Dhaka',
                'thana'    => 'Adabor',
                'first'    => true
            ];

            $session_data = (Object)$ifNotData;
        }
        else {
            $session_data->first = false;
        }

        return response()->json($session_data);
    }

    public function storeUserDeliveryLocation(Request $request)
    {
        $self_token = $request->self_token;
        $division   = $request->division;
        $district   = $request->district;
        $thana      = $request->thana;

        if(!$self_token || !$district || !$division || !$thana) {
            return response()->json(false);
        }

        $data = [
            'division' => $division,
            'district' => $district,
            'thana'    => $thana
        ];

        $this->sessionToDB->updateOrCreate($self_token . '_user_location', json_encode($data));

        return response()->json(true);
    }


    public function newArrivals()
    {
        $product = \App\Product::where('new_arrival', 'on')->with('image', 'ratingSum')
                               ->latest()
                               ->paginate(32);
        return response()->json($product);
    }


    public function flashSchedules()
    {

        $schedules = $this->flashSchedule
            ->getFromToday();

        return response()->json(compact('schedules'));
    }


    public function questionAnswer($id)
    {
        $questionAnswers = $this->QuestionAnswer->getAll(5, $id)->where('answer', !null);

        return response()->json(compact('questionAnswers'));
    }


    public function submitQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'question'   => 'required'
        ]);

        $data = [
            'product_id'       => $request->product_id,
            'question'         => $request->question,
            'question_user_id' => auth()->guard('api')->id()
        ];

        return response()->json([
                                    'success' => !$validator->fails(),
                                    'message' => $validator->errors(),
                                    'data'    => $data,
                                    'results' => !$validator->fails()?$this->QuestionAnswer->create($data) : null
                                ]);

    }

    public function questionLikeDislike(Request $request)
    {
        $question = $this->QuestionAnswer->getById($request->question_id);
        $userId   = auth()->guard('api')->id();
        $results  = false;

        if(!$userId) {
            return false;
        }
        $dreArr       = json_decode($question->dislikes);
        $disLikeArray = is_array($dreArr) && count($dreArr) > 0?$dreArr : [];
        $reArr        = json_decode($question->likes);
        $likeArray    = is_array($reArr) && count($reArr) > 0?$reArr : [];

        $likeDislikeRR = array_merge($disLikeArray, $likeArray);

        if($request->col == 'dislike') {
            if(!in_array($userId, $likeDislikeRR)) {
                $disLikeArray[]     = $userId;
                $question->dislikes = json_encode($disLikeArray);
                $results            = $question->save();
            }
        }
        else {
            if(!in_array($userId, $likeDislikeRR)) {
                $likeArray[]     = $userId;
                $question->likes = json_encode($likeArray);
                $results         = $question->save();
            }

        }
        return response()->json($results);
    }

    public function getProductReviews(Request $request)
    {

        $reviews = $this->review
            ->getReviewsByProductId($request->product_id, 10);

        return response()->json(compact('reviews'));
    }


    public function storeProductReviews(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'rating'     => 'required'
        ]);

        if(!$request->review_id) {

            $data = [
                'user_id'    => auth()->guard('api')->id(),
                'product_id' => $request->product_id,
                'rating'     => $request->rating,
                'comment'    => $request->comment,
                'is_active'  => 1
            ];

            $review = !$validator->fails()?$this->review->create($data) : false;
            $images = $this->reviewImageSaveToDB($review->id, $request->images);
        }
        else {
            $review                     = $this->review->getById($request->review_id);
            $review->rating             = $request->rating;
            $review->comment            = $request->comment;
            $review->additional_comment = $request->additionalReviews;
            $review->save();
        }
        //dd($request->images);


        return response()->json([

                                    'success' => !$validator->fails(),
                                    'message' => $validator->errors(),
                                    'result'  => $review?[
                                        'review' => $review ?? false,
                                        'images' => $images ?? false
                                    ] : null
                                ]);
    }

    public function reviewImageSaveToDB($review_id, $imaged)
    {
        if($review_id && $imaged) {
            $review = $this->review->getById($review_id);

            $images = [];
            foreach ($imaged as $image) {
                $images[] = [
                    'full_directory' => $image
                ];
            }

            $images = $review?$review->images()->createMany($images) : false;

            return $images;
        }

        return false;
    }

    public function reviewLikeDislikes(Request $request)
    {
        $review  = $this->review->getById($request->review_id);
        $userId  = auth()->guard('api')->id();
        $results = false;

        if(!$userId) {
            return false;
        }
        $dreArr       = json_decode($review->dislikes);
        $disLikeArray = is_array($dreArr) && count($dreArr) > 0?$dreArr : [];
        $reArr        = json_decode($review->likes);
        $likeArray    = is_array($reArr) && count($reArr) > 0?$reArr : [];

        $likeDislikeRR = array_merge($disLikeArray, $likeArray);

        if($request->col == 'dislike') {
            if(!in_array($userId, $likeDislikeRR)) {
                $disLikeArray[]   = $userId;
                $review->dislikes = json_encode($disLikeArray);
                $results          = $review->save();
            }
        }
        else {
            if(!in_array($userId, $likeDislikeRR)) {
                $likeArray[]   = $userId;
                $review->likes = json_encode($likeArray);
                $results       = $review->save();
            }

        }

        return response()->json($results);
    }


    public function reviewPhotoUpload(Request $request)
    {
        $userId = auth()->guard('api')->id();


        if($userId && $request->hasFile('image')) {
            $imageDir = 'uploads/review/images/' . $userId;

            $image       = $request->file('image');
            $name        = $image->getClientOriginalName() . time() . '.' . $image->getClientOriginalExtension();
            $upload      = $image->move(storage_path($imageDir), $name);
            $filenameDir = 'storage/' . $imageDir . '/' . $name;

            if($request->review_id && $upload) {
                $this->reviewImageSaveToDB($request->review_id, [$filenameDir]);
            }

            return response()->json($upload?$filenameDir : false);
        }


        return response()->json(false);
    }


    public function deleteReviewUploadImages(Request $request)
    {
        $images = $request->images;
        $delete = [];

        if($images) {
            foreach ($images as $image) {
                $delete[] = $this->deleteReviewUploadImageThis($image);
            }
        }

        return response()->json($delete);
    }


    public function deleteReviewUploadImage(Request $request)
    {
        if($request->review_id && $request->image) {
            $review = $this->review->getById($request->review_id);
            $image  = $review->images()
                             ->where('full_directory', $request->image)
                             ->first();
            $image->delete();
        }
        return response()->json($this->deleteReviewUploadImageThis($request->image));
    }


    private function deleteReviewUploadImageThis($image)
    {
        if($image && file_exists($image)) {
            return unlink($image);
        }

        return false;
    }


    public function storeWholeSale(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'         => 'required',
            'email'        => 'required',
            'phone'        => 'required',
            'address'      => 'required',
            'product_id'   => 'required',
            'product_code' => 'required',
            'quantity'     => 'required',
            'description'  => 'required'
        ]);

        $results = false;

        if(!$validator->fails()) {
            $results = $this->wholeSale->create([
                                                    'name'         => $request->name,
                                                    'email'        => $request->email,
                                                    'phone'        => $request->phone,
                                                    'address'      => $request->address,
                                                    'product_id'   => $request->product_id,
                                                    'product_code' => $request->product_code,
                                                    'quantity'     => $request->quantity,
                                                    'description'  => $request->description,
                                                    'completed'    => false
                                                ]);
        }

        return response()->json([
                                    'success' => !$validator->fails(),
                                    'message' => $validator->errors(),
                                    'results' => $results
                                ]);
    }


    public function allCategories()
    {

//        $terms = $this->term->getAll();
//         $megamenus = $this->term->getByAny('parent', 1);

        return response()->json([
//                                    'terms' => $terms,
//                                       'categorySub'
                                ]);
    }

}