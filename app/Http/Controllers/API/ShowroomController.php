<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowroomController extends Controller
{
    public function __construct()
    {

    }


    public function dashboard()
    {
        $showroom = auth()->guard('api')->check() ?
            auth()->guard('api')->user()
                ->showroomProfile()
                ->first() : false;

        if(!$showroom){
            response()->json(compact('showroom'));
        }

        $new = $showroom?$showroom->shipments()->where('status','Placed')->count():0;
        $processing = $showroom?$showroom->shipments()->where('status','Processing')->count():0;
        $completed = $showroom?$showroom->shipments()->where('status','Completed')->count():0;
        $total = $showroom?$showroom->shipments()->count():0;

        return response()->json(compact('showroom','new','processing','completed','total'));
    }

    public function shipment_index()
    {
        $sprofile = auth()->guard('api')->check() ?
            auth()->guard('api')->user()
                ->showroomProfile()
                ->first() : false;


        $shipments =    $sprofile? $sprofile->shipments()
            ->orderBy('created_at','DESC')->with('orderMaster:order_random,id')->withCount(['orderDetails'])
            ->paginate(30): [];


        return response()->json(compact('shipments'));
    }



    public function shipment_show(Request $request)
    {

        $sprofile = auth()->guard('api')->check() ?
            auth()->guard('api')->user()
                ->showroomProfile()
                ->first() : false;


        $shipment =    $sprofile? $sprofile->shipments()->withCount('orderDetails')->with(['orderDetails' => function($query){
            return $query->with(['product' => function($query) {
                return $query->with('image:main_pid,icon_size_directory')->select('id','sku','short_description');
            }]);

        }])->where('id',$request->id)->first() : false;

        $order = $shipment ? $shipment->orderMaster : false;

        return response()->json(compact('shipment','order'));
    }


    public function shipment_status(Request $request)
    {
        $sprofile = auth()->guard('api')->check() ?
            auth()->guard('api')->user()
                ->showroomProfile()
                ->first() : false;


        $shipment =    $sprofile? $sprofile->shipments()->where('id',$request->id)->first() : false;
        $status = $request->status;

        if(!$shipment){
            return response()->json(false);
        }
        $prevStatus = $shipment->status;
        $shipment->status = $status;
        $shipment->save();

        if(($prevStatus != 'Completed' && $status == 'Completed') || ($prevStatus == 'Completed' && $status != 'Completed'))
        {
            $reward_points = $shipment->orderDetails()->with('product')->get();
            $reward_points = collect($reward_points)->sum('product.reward_points');
            $this->addRewardPoints($shipment->orderMaster->user_id,$reward_points,$prevStatus,$status);
        }

        return response()->json(true);
    }

    public function addRewardPoints($user_id,$points,$prevStatus,$status)
    {

        $user = \App\User::find($user_id);
        if($prevStatus != 'Completed' && $status == 'Completed')
        {
            $user->reward_points += $points;
        }
        elseif($prevStatus == 'Completed' && $status != 'Completed')
        {
            $user->reward_points -= $points;
        }

        return $user->save();
    }
}
