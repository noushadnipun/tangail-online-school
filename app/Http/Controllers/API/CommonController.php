<?php

namespace App\Http\Controllers\API;

use App\District;
use App\Mail\Emailing;
use App\Repositories\Contact\ContactInterface;
use App\Repositories\Dashboard\DashboardInterface;
use App\Repositories\Page\PageInterface;
use App\Repositories\Role_user\Role_userInterface;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Term\TermInterface;
use App\Repositories\User\UserInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Boolean;

class CommonController extends Controller
{

    private $dashboard;
    private $settings;
    private $term;
    private $page;

    public function __construct(
        DashboardInterface $dashboard,
        SettingInterface $settings,
        TermInterface $term,
        PageInterface $page
    )
    {
        $this->dashboard = $dashboard;
        $this->settings = $settings;
        $this->term = $term;
        $this->page = $page;
    }

    public function globals(Request $request)
    {

        $widgets = $this->dashboard->getAll()->keyBy('id');
        $settings = $this->settings->getAll()->keyBy('id');
        $megamenus = $this->term->getByAny('parent', 1);
        $parent_menus = get_parent_menus(1);


        $menu1 = DB::table('menus')->where('id', 3)->first();
        $menu2 = DB::table('menus')->where('id', 5)->first();
        $menu3 = DB::table('menus')->where('id', 8)->first();

        $menus1 = [
            'menu' => $menu1,
            'menus' => get_parent_menus(3)
        ];
        $menus2 = [
            'menu' => $menu2,
            'menus' => get_parent_menus(5)
        ];
        $menus3 = [
            'menu' => $menu3,
            'menus' => get_parent_menus(8)
        ];

        return response()->json(
            compact(
                'widgets',
                'settings',
                'megamenus',
                'parent_menus',
                'menus1',
                'menus2',
                'menus3'
            )
        );
    }

    public function getPage(Request $request)
    {
        $id = $request->id;
        $slug = $request->slug;

        $pages = $this->page->getById($id);
        $pages->description = $this->pageFiltering($pages->description);

        return response()->json($pages);
    }


    public function gallery(Request $request)
    {
        $images = \App\Gallery::orderBy('created_at', $request->sortby ?? 'ASC')->with('image')->paginate(30);

        return response()->json($images);
    }
}

