<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = json_decode($_COOKIE['user'] ?? false);
        $self_token = uniqid();

        if (!isset($_COOKIE['self_token'])) {
            setcookie('self_token', $self_token, time() + (86400 * 30), "/");
        }
        $packages = [
            'user' => auth()->check() ? ['info' => $user->user, 'token' => $user->token] : false,
            'self_token' => $_COOKIE['self_token'] ?? $self_token

        ];

        return view('home', compact('packages'));
    }


    public function login_redirect()
    {
        $user = json_decode($_COOKIE['user']);
        return redirect()->to('/');
    }


    public function jobsJson()
    {
        $jobs = Job::all();
        return response()->json($jobs);
    }


    public function loginEntry()
    {
        $auth = json_decode($_COOKIE['user'] ?? false);
//        dd($auth);
        if (isset($auth->user->id)) {
            \auth()->loginUsingId($auth->user->id, true);
        }
        $redirect_url = Config::get('app.url') . "/login";

        if (auth()->check()) {
            $user = auth()->user();
            if ($user->isShowroom()) {
                $redirect_url = 'showroom/dashboard';
            } elseif ($user->isVendor()) {
                $redirect_url = '/vendor/dashboard';
            } elseif ($user->isAffiliator()) {
                $redirect_url = '/affiliator/dashboard';
            } else {
                $redirect_url = 'dashboard';
            }

        }

        return redirect($redirect_url);
    }


}
