<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;


class ShowroomMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check() && \Auth::user()->isShowroom()) {
            return $next($request);
        } else {
            return redirect(URL::to('/'));
        }
    }

}
