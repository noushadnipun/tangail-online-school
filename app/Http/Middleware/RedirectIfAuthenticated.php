<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            // return redirect('/dashboard');
            if (Auth::user()->isAdmin()) {
                return redirect('/dashboard');
            }elseif (Auth::user()->isVendor()){
                return redirect()->route('vendor.dashboard.index');
            }
            elseif (Auth::user()->isShowroom()){
                return redirect()->route('showroom.dashboard');
            }else {
                return redirect('/');
            }
        }

        return $next($request);
    }
}
