<?php

namespace App\Http\Middleware;

use Closure;

class RolePermissionChecker
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if (\Auth::check()) {

            $r = $request->route()->getName();

            $user = auth()->user();

            //dd($user);
            //$current_route = md5($r);

//            $matched = \App\MixTaxonomy::select('mtkey', 'mtvalue')
//                ->where('mtkey', 'useraccesspermission_' . $user['id'] . '_' .  md5($r))
//                ->first();

// ei bishoytai nei ekhane... :p
            //dd($matched);
            if(!(isset($matched['mtvalue']) && $matched['mtvalue'] == 'on' || $user->isAdmin()['name'] == 'admin')) {
                dd('You do not have permission to access this page!');
            }
            //return redirect('login_now');
        }

        //dd($request->route()->getName());
        return $next($request);



    }
}