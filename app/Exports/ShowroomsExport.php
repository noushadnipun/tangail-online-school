<?php

namespace App\Exports;

use App\Post;
use App\Showroom;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ShowroomsExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Showroom::select(
            'id', 'user_profile_id', 'title', 'sub_title', 'seo_url', 'author', 'description', 'short_description',
            'brand', 'phone', 'opening_hours','closing_hours','off_day', 'latitude', 'longitude', 'address',
            'division', 'district', 'thana', 'shop_type', 'is_active'
        )->get();
    }

    public function headings(): array
    {
        return [
            'Code',
            'ProfileID',
            'Title',
            'Sub Title',
            'SEO URL',
            'Contact Person',
            'Description',
            'Short Description',
            'Brand',
            'Phone',
            'Opening Hours',
            'Closing Hours',
            'Off Day',
            'Latitude',
            'Longitude',
            'Address',
            'Division',
            'District',
            'Thana',
            'Shop Type',
            'Is Active',
        ];
    }
}
