<?php

namespace App\Exports;

use App\OrdersDetail;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersExport implements FromCollection, WithHeadings
{

    private $f_date;

    public function __construct($from_date, $to_date) {
        $this->f_date = $from_date;
        $this->t_date = $to_date;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $fd = $this->f_date;
        $td = $this->t_date;

        return OrdersDetail::select(
            'om.id', 'orders_detail.product_name', 'orders_detail.product_code', 'orders_detail.qty', 'om.customer_name', 'om.phone', 'om.emergency_phone', 'om.address',
            'om.email', 'om.order_date', 'om.payment_method', 'om.grand_total', 'om.total_amount', 'om.coupon_type',
            'om.coupon_code', 'om.coupon_discount', 'om.district', 'om.order_status'
        )->leftJoin('orders_master AS om', function ($join) {
            $join->on('orders_detail.order_random', '=', 'om.order_random');
        })->whereBetween('orders_detail.order_date', [$fd, $td])->groupBy('orders_detail.product_code')->get();

        //dd($mmm);
    }

    public function headings(): array
    {
        return [
            'OMID',
            'Product Name',
            'PCode',
            'Qty',
            'CName',
            'Phone',
            'EPhone',
            'Address',
            'Email',
            'Order Date',
            'Payment Method',
            'GTotal',
            'Total',
            'CType',
            'CCode',
            'CDiscount',
            'District',
            'Action'
        ];
    }
}
