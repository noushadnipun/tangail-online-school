<?php

namespace App\Exports;

use App\ShowroomStock;
use Maatwebsite\Excel\Concerns\FromCollection;

class ShowroomStockExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Product::select(
            'showroom_id', 'product_code', 'quantity', 'color_id', 'size_id'
        )->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Showroom ID',
            'Product Code',
            'Quantity',
            'Color',
            'Size'
        ];
    }
}
