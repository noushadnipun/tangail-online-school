<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VendorProductsExport implements FromCollection, WithHeadings
{



    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $user = auth()->user();

        return $user->vendorProducts()->select('products.id', 'products.user_id', 'products.title', 'products.product_code', 'products.qty',
            'products.local_selling_price', 'products.local_discount', 'products.discount_tag', 'products.stock_status',
            'products.dp_price', 'products.delivery_area', 'products.pro_warranty', 'products.enable_comment',
            'products.enable_review', 'products.express_delivery', 'products.new_arrival', 'products.best_selling',
            'products.flash_sale', 'products.recommended', 'products.disable_buy', 'products.order_by_phone',
            'products.offer_details', 'products.offer_start_date', 'products.offer_end_date')->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'User ID',
            'Title',
            'Product Code',
            'Quantity',
            'Local Selling Price',
            'Local Discount', 'Discount Tag', 'Stock Status',
            'DP Price', 'Delivery Area', 'Warrenty', 'Enable Comment', 'Enable Review', 'Express Delivery',
            'New Arrival', 'Best Selling', 'Flash Sale', 'Recommended', 'Disable Buy', 'Order By Phone',
            'Offer Details', 'Offer Start Date', 'Offer End Date'
        ];
    }
}
