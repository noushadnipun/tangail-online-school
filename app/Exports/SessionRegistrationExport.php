<?php

namespace App\Exports;

use App\TrainingSessionRegistration;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SessionRegistrationExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return TrainingSessionRegistration::all();
        return TrainingSessionRegistration::select(
            'district', 'upozilla', 'institute_category', 'institute_name', 'teacher_name',  'designation_name', 'gender', 'index_number', 'mobile', 'email', 'created_at'
        )->get();
    }

    public function headings(): array
    {
        return [
            'Zilla',
            'Upozilla',
            'Institute category',
            'Institute Name',
            'Teacher Name',
            'Designation Name',
            'Gender',
            'Index Number',
            'Mobile',
            'Email',
            'Submitted at',
        ];
    }
}
