<?php

namespace App\Exports;

use App\Pcombinationdata;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsMultiPricingExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Pcombinationdata::select(
            'id', 'user_id', 'main_pid', 'color_codes', 'size',
            'regular_price', 'type', 'dp_price', 'selling_price',
            'discount_tag', 'item_code', 'stock', 'is_stock',
            'is_dp'
        )->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'User ID',
            'MPID',
            'CCode',
            'Size',            
            'Regular Price',
            'Type',
            'DP Price', 
            'Selling Price', 
            'Discount Tag', 
            'PCode', 
            'Stock', 
            'Is Stock',
            'Is DP'
        ];
    }
}
