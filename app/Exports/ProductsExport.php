<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Product::select(
            'id', 'user_id', 'title', 'product_code', 'qty',
            'local_selling_price', 'local_discount', 'discount_tag', 'stock_status',
            'dp_price', 'delivery_area', 'pro_warranty', 'enable_comment',
            'enable_review', 'express_delivery', 'new_arrival', 'best_selling',
            'flash_sale', 'recommended', 'disable_buy', 'order_by_phone',
            'offer_details', 'offer_start_date', 'offer_end_date'
        )->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'User ID',
            'Title',
            'Product Code',
            'Quantity',
            'Local Selling Price',
            'Local Discount', 'Discount Tag', 'Stock Status',
            'DP Price', 'Delivery Area', 'Warrenty', 'Enable Comment', 'Enable Review', 'Express Delivery',
            'New Arrival', 'Best Selling', 'Flash Sale', 'Recommended', 'Disable Buy', 'Order By Phone',
            'Offer Details', 'Offer Start Date', 'Offer End Date'
        ];
    }
}
