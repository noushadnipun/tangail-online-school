<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $table = 'terms';
    protected $fillable = [
        'name', 'seo_url','cat_theme', 'type', 'position', 'cssid', 'cssclass', 'description','term_short_description', 'parent', 'connected_with',
        'page_image', 'home_image', 'term_menu_icon', 'term_menu_arrow', 'with_sub_menu', 'sub_menu_width', 'banner1', 'banner2', 'column_count',
        'term_seo_keywords','term_seo_description','term_seo_title','term_seo_h2','term_seo_h1','thumb_image','onpage_banner'
    ];


    public function sub_categories()
    {

        return $this->hasMany(Term::class,'parent','id');
    }


    public function products()
    {

        return $this->hasMany(Product::class,'parent_id','id');
    }


    public function category_products()
    {

        //dd($this->get());

        return $this->hasMany(ProductCategories::class,'term_id','id')
                    ->leftJoin('products','products.id','=','productcategories.main_pid')
                    ->with('product.image','product.ratingSum')
                    ->where('products.recommended','=','on')
                    ->orderBy('products.created_at','desc')
         ;
    }



}
