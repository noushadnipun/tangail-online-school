<?php

namespace App\Providers;


use App\Repositories\Album\AlbumInterface;
use App\Repositories\Album\EloquentAlbum;
use App\Repositories\Classes\ClassesInterface;
use App\Repositories\Classes\EloquentClasses;
use App\Repositories\Subjects\SubjectsInterface;
use App\Repositories\Subjects\EloquentSubjects;
use App\Repositories\Videos\VideosInterface;
use App\Repositories\Videos\EloquentVideos;
use App\Repositories\Dashboard\DashboardInterface;
use App\Repositories\Dashboard\EloquentDashboard;
use App\Repositories\HomeSetting\EloquentHomeSetting;
use App\Repositories\HomeSetting\HomeSettingInterface;
use App\Repositories\Media\EloquentMedia;
use App\Repositories\Media\MediaInterface;
use App\Repositories\Page\EloquentPage;
use App\Repositories\Page\PageInterface;
use App\Repositories\Post\EloquentPost;
use App\Repositories\Post\PostInterface;
use App\Repositories\Role\EloquentRole;
use App\Repositories\Role\RoleInterface;
use App\Repositories\Role_user\EloquentRole_user;
use App\Repositories\Role_user\Role_userInterface;
use App\Repositories\Route\EloquentRoute;
use App\Repositories\Route\RouteInterface;
use App\Repositories\Setting\EloquentSetting;
use App\Repositories\Setting\SettingInterface;
use App\Repositories\Term\EloquentTerm;
use App\Repositories\Term\TermInterface;
use App\Repositories\User\EloquentUser;
use App\Repositories\User\UserInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Repositories\Gallery\EloquentGallery;
use App\Repositories\Gallery\GalleryInterface;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use App\Repositories\Slider\SliderInterface;
use App\Repositories\Slider\EloquentSlider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MediaInterface::class, EloquentMedia::class);
        $this->app->singleton(UserInterface::class, EloquentUser::class);
        $this->app->singleton(DashboardInterface::class, EloquentDashboard::class);
        $this->app->singleton(TermInterface::class, EloquentTerm::class);
        $this->app->singleton(SettingInterface::class, EloquentSetting::class);
        $this->app->singleton(RoleInterface::class, EloquentRole::class);
        $this->app->singleton(Role_userInterface::class, EloquentRole_user::class);
        $this->app->singleton(PostInterface::class, EloquentPost::class);
        $this->app->singleton(PageInterface::class, EloquentPage::class);
        $this->app->singleton(HomeSettingInterface::class, EloquentHomeSetting::class);
        $this->app->singleton(GalleryInterface::class, EloquentGallery::class);
        $this->app->singleton(RouteInterface::class, EloquentRoute::class);
        $this->app->singleton(SliderInterface::class, EloquentSlider::class);
        $this->app->singleton(AlbumInterface::class, EloquentAlbum::class);
        $this->app->singleton(ClassesInterface::class, EloquentClasses::class);
        $this->app->singleton(SubjectsInterface::class, EloquentSubjects::class);
        $this->app->singleton(VideosInterface::class, EloquentVideos::class);
    }
}
