<?php

namespace App\Imports;

use App\ShowroomStock;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;

class ShowroomStocks implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
//    public function model(array $row)
//    {
//        return new showroom_stocks([
//            //
//        ]);
//    }

public function collection(Collection $collection)
{
    $showrooms = $collection[0];
    $data = [];
    foreach ($showrooms as $key => $showroom)
    {
        foreach ($collection as $c)
        {

            if(intval($showroom) !== 0 && intval($c[0]))
            {
               ShowroomStock::updateOrCreate(
                    [
                        'showroom_id' => (int) $showroom,
                        'product_code' => (int) $c[0]
                    ],
                    [
                        'quantity' => (int)$c[$key]
                    ]);

            }

         }
    }
//    dd($data);

}


}
