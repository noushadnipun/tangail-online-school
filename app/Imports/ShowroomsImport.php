<?php

namespace App\Imports;

use App\Showroom;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ShowroomsImport implements ToModel,WithStartRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {

    	// echo "<pre>";

    	// var_dump($row);

    	// echo "</pre>";

    	// return;
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Showroom::updateOrCreate(
            [
                'id' => (int)$row[0]
            ], [
                'user_profile_id' => (int)$row[1],
                'title' => (string)$row[2],
                'sub_title' => (string)$row[3],
                'seo_url' => (string)$row[4],
                'author' => (string)$row[5],
                'description' => (string)$row[6],
                'short_description' => (string)$row[7],
                'brand' => (string)$row[8],
                'phone' => (string)$row[9],
                'opening_hours' => (string)$row[10],
                'closing_hours' => (string)$row[11],
                'off_day' => (string)$row[12],
                'latitude' => (string)$row[13],
                'longitude' => (string)$row[14],
                'address' => (string)$row[15],
                'division' => (string)$row[16],
                'district' => (string)$row[17],
                'thana' => (string)$row[18],
                'shop_type' => (string)$row[19],
                'is_active' => (string)$row[20]
            ]
        );
        DB::statement('SET FOREIGN_KEY_CHECKS=1');


        //return true;
        // 'id', 'user_id', 'title', 'product_code',
        //            'local_selling_price', 'local_discount',
        //            'intl_selling_price', 'intl_discount', 'is_active'

    }

    public function startRow(): int
    {
        return 2;
    }
}
