<?php

namespace App\Imports;

use App\Pcombinationdata;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;


class ProductsMultiPricingImport implements ToModel,WithStartRow
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {

        //dd($row);

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Pcombinationdata::updateOrCreate(
            [
                'id' => (int)$row[0]
            ], [
                'user_id' => (int)$row[1],
                'main_pid' => (string)$row[2],
                'color_codes' => (string)$row[3],
                'size' => (string)$row[4],
                'regular_price' => (string)$row[5],
                'type' => (string)$row[6],
                'dp_price' => (string)$row[7],
                'selling_price' => (string)$row[8],
                'discount_tag' => (string)$row[9],
                'item_code' => (string)$row[10],
                'stock' => (string)$row[11],
                'is_stock' => (string)$row[12],
                'is_dp' => (string)$row[13]           
            ]
        );
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
    
    public function startRow(): int
    {
        return 2;
    }
    
}