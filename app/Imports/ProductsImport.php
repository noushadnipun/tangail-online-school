<?php

namespace App\Imports;

use App\Product;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;


class ProductsImport implements ToModel, WithStartRow
{
    /**
     * @param Collection $collection
     */


    public function model(array $row)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $update = Product::updateOrCreate(
            [
                'id' => (int) $row[0]
            ],
            [
                'user_id' => (int) $row[1],
                'title' => (string) $row[2],
                'product_code' => (string) $row[3],
                'qty' => (string) $row[4],
                'local_selling_price' => (string) $row[5],
                'local_discount' => (string) $row[6],
                'discount_tag' => (float) $row[7],
                'stock_status' => (string) $row[8],
                'dp_price' => (string) $row[9],
                'delivery_area' => (string) $row[10],
                'pro_warranty' => (string) $row[11],
                'enable_comment' => (string) $row[12],
                'enable_review' => (string) $row[13],
                'express_delivery' => (string) $row[14],
                'new_arrival' => (string) $row[15],
                'best_selling' => (string) $row[16],
                'flash_sale' => (string) $row[17],
                'recommended' => (string) $row[18],
                'disable_buy' => (string) $row[19],
                'order_by_phone' => (string) $row[20],
                'offer_details' => (!empty($row[21]) ? (string) $row[21] : null),
                'offer_start_date' => (!empty($row[22]) ? (string) $row[22] : null),
                'offer_end_date' => (!empty($row[23]) ? (string) $row[23] : null),
            ]
        );
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        //dd($update);
    }


    public function startRow(): int
    {
        return 2;
    }
}
