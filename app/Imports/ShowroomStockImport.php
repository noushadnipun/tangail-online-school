<?php

namespace App\Imports;

use App\ShowroomStock;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ShowroomStockImport implements ToModel, WithStartRow
{

    public function model(array $row)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $update = ShowroomStock::updateOrCreate(
            [
                'id' => (int)$row[0]
            ],
            [
                'showroom_id' => (int)$row[1],
                'product_code' => (string)$row[2],
                'quantity' => (string)$row[3],
                'color_id' => (string)$row[4],
                'size_id' => (string)$row[5],
            ]
        );
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        //dd($update);
    }


    public function startRow(): int
    {
        return 2;
    }
}
