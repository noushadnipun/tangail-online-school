<?php

use App\Pcombinationdata;
use App\Image;
use App\Term;
use App\Review;
use App\OrdersDetail;
use App\FlashShedule;
use App\FlashItem;
use App\PaymentSetting;
use App\Coupon;
use Illuminate\Support\Facades\Session;
use App\Product;
use App\HomeSetting;
use App\Cart;
use Carbon\Carbon;
use App\Repositories\SessionTaxonomy\SessionTaxonomyInterface as sessionToDB;
/**
 * $term_id Term Id
 * $return
 * return Term Info
 */
if (!function_exists('get_term_info')) {
    function get_term_info($term_id, $return, $key = null)
    {
        if (!empty($key) && $key == true) {
            $cat_info = Term::where('seo_url', $term_id)->get()->first();
            if (!empty($cat_info)) {
                return $cat_info->$return;
            } else {
                return false;
            }
        } else {
            //$cat_id = Request::segment($term_id);
            $cat_info = Term::find($term_id);
            return $cat_info->$return;
        }
    }
}

if (!function_exists('coupon_voucher_verify')) {
    function coupon_voucher_verify($coupon_type, $coupon_code,$self_token = false)
    {
        if(!$self_token)
        {
            return false;
        }

        $session_data = sessionToDB::getByKey($self_token.'_cart');
        $oldcart = $session_data ? (Object) json_decode($session_data,true) : null;


//        $oldcart = Session::get('cart');


        $cart = new Cart($oldcart);
        foreach ($cart->items as $item) {
            $totalprice[] = $item['purchaseprice'] * $item['qty'];
        }
        $total_buy = array_sum($totalprice);

        $option = [
            'coupon_code' => $coupon_code,
            'coupon_type' => $coupon_type,
            'is_active' => 1
        ];

        date_default_timezone_set('Asia/Dhaka');
        $coupon = Coupon::where($option)
            ->where('start_date', '<=', date('Y-m-d H:i:s'))
            ->where('end_date', '>=', date('Y-m-d H:i:s'))
            ->first();
        // dump($coupon->used_limit);

        if ($coupon != null) {
            $count = $coupon->count();
        } else {
            $count = 0;
        }
        //dump($coupon);

        if ($count != 0 || (auth()->guard('api')->check() && !auth()->guard('api')->user()->isDP())) {
            //if ($count != 0) {
            //$user_id = auth()->user()->id;
            $coupon_used = App\OrdersMaster::where(['coupon_code' => $coupon['coupon_code']])->get()->count();
            if ($coupon_used < $coupon->used_limit) {


                $c_type = $coupon['amount_type'];
                $c_price = $coupon['price'];
                $purchase_min = $coupon['purchase_min'];
                $c_upto = $coupon['upto_amount'];
                $c_limit = $coupon['used_limit'];

                if ($c_type == 'Fixed') {
                    if ($total_buy >= $purchase_min) {
                        if ($total_buy > $c_price) {
                            $discount = $c_price;
                            $massage = '';
                            $action = ['action' => 'Yes', 'amount' => $discount, 'massage' => $massage];
                        } else {
                            $massage = 'Please buy more than ' . $c_price;
                            $action = ['action' => 'No', 'amount' => null, 'massage' => $massage];
                        }
                    } else {
                        $massage = 'Please buy more than ' . $purchase_min;
                        $action = ['action' => 'No', 'amount' => null, 'massage' => $massage];
                    }
                } else {
                    if ($total_buy >= $purchase_min) {
                        $persent = ($total_buy * $c_price) / 100;

                        if ($c_upto == 0 || $c_upto == null || $c_upto == '') {
                            $discount = $persent;
                            $massage = '';
                            $action = ['action' => 'Yes', 'amount' => $discount, 'massage' => $massage];
                        } else {
                            if ($persent > $c_upto) {
                                $discount = $c_upto;
                                $massage = '';
                                $action = ['action' => 'Yes', 'amount' => $discount, 'massage' => $massage];
                            } else {
                                //dd($discount);
                                $discount = $persent;
                                $massage = '';
                                $action = ['action' => 'Yes', 'amount' => $discount, 'massage' => $massage];
                            }
                        }
                    } else {
                        $massage = 'Please buy more than Tk.' . $purchase_min;
                        $action = ['action' => 'No', 'amount' => null, 'massage' => $massage];
                    }
                }
            } else {
                $massage = 'This ' . $coupon_type . 'is invalid or uses quantity is over.';
                $action = ['action' => 'No', 'amount' => null, 'massage' => $massage];
            }

        } else {
            $massage = 'This ' . $coupon_type . ' is invalid or uses quantity is over.';
            $action = ['action' => 'No', 'amount' => null, 'massage' => $massage];
        }


        if ($action['action'] == 'Yes') {
            $attributes = [
                'coupon_amount' => round($action['amount']),
                'coupon_type' => $coupon_type,
                'coupon_code' => $coupon_code
            ];


            $ouput = sessionToDB::updateOrCreate($self_token.'_coupon',json_encode($attributes));

            //$ouput = Session::put('my_coupon', $attributes);

            if ((Session::has('user_details')) && (Session::has('payment_method')))
            {

                $total_amount = Session::get('payment_method')['total_amount'];
                $deliveryfee = Session::get('user_details')['deliveryfee'];
                $grand_total = $total_amount + $deliveryfee - round($action['amount']);
                Session::put('payment_method.grand_total', $grand_total);
                Session::put('user_details.deliveryfee', $deliveryfee);
                Session::save();
            }

            if ($ouput) {
                return true;
            } else {
                return false;

            }


            //Session::save();
        } else {
            //dd($action);
            Session::flash('sweet_error', $action['massage']);

            if (Session::has('my_coupon')) {
                Session::forget('my_coupon');
            }
        }
    }
}

/**
 * return Product name by id
 */
if (!function_exists('get_product_by_id')) {
    function get_product_by_id($id)
    {
        $item = Product::find($id);
        return $item;
    }
}

/**
 * return Product name by id
 */
if (!function_exists('product_title')) {
    function product_title($id)
    {
        $item = Product::find($id);
        return $item->title;
    }
}

if (!function_exists('get_product_selling_price')) {
    function get_product_selling_price($id)
    {
        $item = Product::find($id);
        return $item->local_selling_price;
    }
}

if (!function_exists('get_product_code')) {
    function get_product_code($id)
    {
        $item = Product::find($id);
        return $item->product_code;
    }
}

if (!function_exists('get_product_info_by_key')) {
    function get_product_info_by_key($id, $key)
    {
        $item = Product::find($id);
        return $item->$key;
    }
}

/**
 *
 */
if (!function_exists('get_all_product_image')) {
    function get_all_product_image($product_array)
    {
        $images = explode(',', $product_array->images);
        $imgs = Image::find($images);

        $html = [];
        foreach ($imgs as $img):
            $html[] = url($img['full_size_directory']);
        endforeach;

        return $html;
    }
}

/**
 * return Product price by id
 */
if (!function_exists('product_price')) {
    function product_price($id)
    {
        $tksign = '&#2547; ';
        $item = Product::find($id);
        $i = product_attributes($item, false);
        $infoss[] = json_decode($i['values']);
        if (!empty($infoss[0][0]->discount)) {
            $percentage = ($infoss[0][0]->selling_price * $infoss[0][0]->discount) / 100;
            $discounted = $infoss[0][0]->selling_price - $percentage;
            $price = $discounted . ' <span>' . $tksign . $infoss[0][0]->selling_price . '</span>';
            //Tk. 90.00 <span>Tk. 100.00 </span>
        } else {
            $price = $infoss[0][0]->selling_price;
            //Tk. 90.00
        }

        return $price;
    }
}

///**
// * Returns product price with discount in percentage
// */
//if (!function_exists('product_normal_price')) {
//    function product_normal_price($values)
//    {
//        if (!empty($values->discount)) {
//            $price = $values->selling_price % $values->discount;
//            $html = $values->selling_price . ' <span>' . $price . ' </span>';
//        } else {
//            $html = $values->selling_price;
//        }
//
//        return $html;
//    }
//}

/**
 *
 */
if (!function_exists('get_first_product_image')) {
    function get_first_product_image($id, $product_array = null)
    {
        $images = get_product_info_by_key($id, 'images');
        $imagesss = explode(',', $images);
        $imgs = Image::find($imagesss)->first();
        $html = url($imgs['full_size_directory']);
        //dd($html);
        return $html;
    }
}

/**
 * Returns main image from attribute table where relation with image table
 */
if (!function_exists('main_image')) {
    function main_image($post)
    {
        $html = null;
        $i = 0;
        foreach ($post as $image) {
            if ($image->module_type === 'products' && $image->attribute === 'image') {
                if ($i <= 1) {
                    $img = Image::find($image->values);
                    $html = url($img->full_size_directory);
                }
            }
            $i++;
        }
        return $html;
    }
}
/**
 * Returns All Product Information from attributes table
 */
if (!function_exists('product_information_attributes')) {
    function product_information_attributes($post)
    {
        $html = [];
        $html['values'] = null;
        $html['id'] = null;
        foreach ($post as $variation) {
            //dd($variation);
            if ($variation->module_type === 'products' && $variation->attribute === 'product_information') {
                $html['values'] = $variation->values;
            }
            $html['id'] = $variation->id;
        }

        return $html;
        //Tk. 90.00 <span>Tk. 100.00 </span>
    }
}
/**
 * Returns selling price only
 */
if (!function_exists('get_category_name_by_id')) {
    function get_category_name_by_id($id)
    {
        $category = Term::findOrFail($id);
        return $category->name;
    }
}
/**
 * Returns selling price only
 */
if (!function_exists('value_by_key')) {
    function value_by_key($values, $key)
    {
        return $values->$key;
    }
}
/**
 * Returns selling price only
 */
if (!function_exists('local_selling_price')) {
    function local_selling_price($values, $withsign = false)
    {
        //dd($withsign);

        $tksign = '&#2547; ';

        if ($withsign === true) {
            return $values->local_selling_price;
        } elseif ($withsign === false) {
            return $tksign . $values->local_selling_price;
        } else {
            return $tksign . $values->local_selling_price;
        }
    }
}
/**
 * Returns discounted price only
 */
if (!function_exists('discounted_price')) {
    function discounted_price($values, $withsign = false)
    {
        $tksign = '&#2547; ';
        if ($withsign === true) {
            $save = ($values->local_selling_price * $values->local_discount) / 100;
            return ($values->local_selling_price - $save);
        } elseif ($withsign === false) {
            $save = ($values->local_selling_price * $values->local_discount) / 100;
            return $tksign . ($values->local_selling_price - $save);
        } else {
            $save = ($values->local_selling_price * $values->local_discount) / 100;
            return $tksign . ($values->local_selling_price - $save);
        }
    }
}

/**
 * Returns save price only
 */
if (!function_exists('save_price')) {
    function save_price($values, $withoutsign = false)
    {
        $tksign = '&#2547; ';
        if ($withoutsign === true) {
            $save = ($values->local_selling_price * $values->local_discount) / 100;
            return $save;
        } elseif ($withoutsign === false) {
            $save = ($values->local_selling_price * $values->local_discount) / 100;
            return $tksign . $save;
        } else {
            $save = ($values->local_selling_price * $values->local_discount) / 100;
            return $tksign . $save;
        }
    }
}

if (!function_exists('get_price_by_type_using_product_id')) {
    function get_price_by_type_using_product_id($id, $return)
    {
    }
}

/**
 * return category seo url by category id
 */
if (!function_exists('category_seo_url_by_id')) {
    function category_seo_url_by_id($id)
    {
        $cat_seo_url = Term::findOrFail($id);
        $seo_url = $cat_seo_url->cssclass;

        return url('c/' . $seo_url);
    }
}

/**
 * Returns product price with discount in percentage
 */
if (!function_exists('seo_url_by_id_with_product_code')) {
    function seo_url_by_id_with_product_code($id, $product_code)
    {
        $item = Product::find($id);
        return url('product/' . $id . '/' . $item->seo_url . '?product_code=' . $product_code);
    }
}

/**
 * Returns product price with discount in percentage
 */
if (!function_exists('seo_url_by_id')) {
    function seo_url_by_id($id)
    {
        $item = Product::find($id);
        return url('product/' . $id . (!empty($item->seo_url) ? '/' . $item->seo_url : null));
    }
}

/**
 * return url with $values
 */
if (!function_exists('product_seo_url')) {
    function product_seo_url($values, $id = null)
    {
        // previous
        //return url('product/' . $id . '/' . $values);

        // new
        return url('product/' . $values);
    }
}

if (!function_exists('you_may_also_like')) {
    function you_may_also_like(array $options = [])
    {
        $default = [
            'category_id' => [],
            'limit' => 5,
            'post_id' => null
        ];

        $options = array_merge($default, $options);
        $m = Product::whereRaw('FIND_IN_SET(' . implode(',', $options['category_id']) . ', categories)')->limit($options['limit'])->get();

        return $matched = !empty($m) ? $m : [];
    }
}

if (!function_exists('product_design')) {
    function product_design(array $options = [])
    {
        $default = [
            'bootstrap_cols' => null,
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; '
        ];
        $option = array_merge($default, $options);

        $pro = Product::where(['id' => $option['product_id']])->first();

        $get_price = get_product_price(['main_pid' => $option['product_id']]);


        //dump($get_price);
        if (is_employee() && ($get_price['d_price'] != null || $get_price['d_price'] > 0)) {
            // dump($get_price);
            $price = '<span class="price-new">' . $option['sign'] . number_format($get_price['d_price']) . ' ' . '</span>';
            $price .= '<span class="price-old">' . $option['sign'] . number_format($get_price['r_price']) . '</span>';
            $save = $get_price['r_price'] - $get_price['d_price'];
        } else {
            if ($option['flash_now'] == 'Yes') {
                $price = '<span class="price-new">' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</span>';
                $price .= '<span class="price-old">' . $option['sign'] . number_format($get_price['r_price']) . '</span>';
            } elseif ($option['flash_now'] == 'No') {
                $price = '<span class="price-new">???</span>';
                $price .= '<span class="price-old">' . $option['sign'] . number_format($get_price['r_price']) . '</span>';
            } else {
                if ($get_price['s_price'] < $get_price['r_price']) {
                    $price = '<span class="price-new">' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</span>';
                    $price .= '<span class="price-old">' . $option['sign'] . number_format($get_price['r_price']) . '</span>';
                } else {
                    $price = '<span class="price-new">' . $option['sign'] . number_format($get_price['s_price']) . '</span>';
                }
            }
            $save = number_format($get_price['r_price'] - $get_price['s_price']);
            //$save = 100;
        }

        $html = '<div class="' . $option['bootstrap_cols'] . '">';
        $html .= '<div class="item-inner product-layout transition product-grid">';
        $html .= '<div class="product-item-container">';

        $html .= '<div class="left-block left-b" style="overflow: hidden">';
        $html .= '<div class="box-label">';
        if ($get_price['save'] > 0) {
            $html .= '<span class="label-product label-sale">' . $get_price['save'] . '%' . '</span>';
        }

        //        if(!empty($option['emi_availability']) && $option['emi_availability'] == 'on') {
        //            $html .= '<span class="label-product-new"></span>';
        //        }
        $html .= '</div>';

        $html .= image_container([
            'seo_url' => $option['seo_url'],
            'straight_seo_url' => $option['straight_seo_url'],
            'title' => $option['title'],
            'first_image' => $option['first_image'],
            'second_image' => $option['second_image']
        ]);

        $html .= '</div>';

        $proid = '"' . trim($option['product_id']) . '"';
        $procode = '"' . $option['product_code'] . '"';
        $str_seo_url = '"' . $option['straight_seo_url'] . '"';

        $html .= '<div class="right-block">';

        $html .= '<div class="button-group so-quickview cartinfo--left">';
        $html .= addtocart_button_container([
            'straight_seo_url' => null,
            'title' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; ',
            'save_price' => $save,
            'multi_id' => $get_price['multi_id'],
            'pro' => $pro
        ]);

        $html .= addtocompare_button_container(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= addttowish_button_container(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= '</div>';

        $html .= '<div class="caption hide-cont">';
        $html .= product_review_count($option['product_id']);
        $html .= title_container(['seo_url' => $option['seo_url'], 'title' => $option['title']]);
        $html .= '</div>';

        $html .= '<p class="price">' . $price . '</p>';

        $html .= '<div class="description item-desc hidden">';
        $html .= '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est . </p>';
        $html .= '</div>';
        $html .= '<div class="list-block hidden">';
        $html .= addtocart_button_container([
            'straight_seo_url' => null,
            'title' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; ',
            'save_price' => $save,
            'multi_id' => $get_price['multi_id'],
            'pro' => $pro
        ]);
        $html .= addtocompare_button_container(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= addttowish_button_container(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= '</div>';

        $html .= flash_now_container(['flash_now' => $option['flash_now'], 'flash_old_count' => $option['flash_old_count'], 'flash_now_count' => $option['flash_now_count']]);

        $html .= '</div>';

        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}

if (!function_exists('product_design_new')) {
    function product_design_new(array $options = [])
    {
        $default = [
            'bootstrap_cols' => null,
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; '
        ];
        $option = array_merge($default, $options);
        $pro = Product::where(['id' => $option['product_id']])->first();
        $get_price = get_product_price(['main_pid' => $option['product_id']]);
        //dump($get_price);
        if (is_employee() && ($get_price['d_price'] != null || $get_price['d_price'] > 0)) {
            // dump($get_price);
            //<strike>৳400.00</strike> <b>' . $price . '</b>
            $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['d_price']) . ' ' . '</b></span>';
            $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
            $save = $get_price['r_price'] - $get_price['d_price'];
        } else {
            if ($option['flash_now'] == 'Yes') {
                $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</b></span>';
                $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
            } elseif ($option['flash_now'] == 'No') {
                $price = '<span class="price-new"><b>???</b></span>';
                $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
            } else {
                if ($get_price['s_price'] < $get_price['r_price']) {
                    $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</b></span>';
                    $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
                } else {
                    $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['s_price']) . '</b></span>';
                }
            }
            $save = number_format($get_price['r_price'] - $get_price['s_price']);
            //$save = 100;
        }


        $html = '<div class="' . $option['bootstrap_cols'] . '">';
        $html .= '<div class="thumb-wrapper item-hover">';
        $html .= '<div class="wish-icon">';
        if ($get_price['save'] > 0) {
            $html .= '<span class="discountTag"> - ' . $get_price['save'] . '%' . '</span>';
        }
        $html .= '</div>';

        $html .= image_container_new([
            'seo_url' => $option['seo_url'],
            'straight_seo_url' => $option['straight_seo_url'],
            'title' => $option['title'],
            'first_image' => $option['first_image'],
            'second_image' => $option['second_image']
        ]);

        $proid = '"' . trim($option['product_id']) . '"';
        $procode = '"' . $option['product_code'] . '"';
        $str_seo_url = '"' . $option['straight_seo_url'] . '"';

        $html .= '<div class="button-group so-quickview cartinfo--left hidden">';
        $html .= addtocart_button_container([
            'straight_seo_url' => null,
            'title' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; ',
            'save_price' => $save,
            'multi_id' => $get_price['multi_id'],
            'pro' => $pro
        ]);

        $html .= addtocompare_button_container_new(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= addttowish_button_container_new(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);

        $html .= '</div>';
        $html .= '<div class="thumb-content">';
        $html .= title_container(['seo_url' => $option['seo_url'], 'title' => $option['title']]);
//        $html .= product_review_count_new($option['product_id']);


        $html .= '<div class="star-rating">';
        $html .= '<ul class="list-inline">';
        $html .= '<li class="list-inline-item"><i class="fa fa-star"></i></li>';
        $html .= '<li class="list-inline-item"><i class="fa fa-star"></i></li>';
        $html .= '<li class="list-inline-item"><i class="fa fa-star"></i></li>';
        $html .= '<li class="list-inline-item"><i class="fa fa-star"></i></li>';
        $html .= '<li class="list-inline-item"><i class="fa fa-star"></i></li>';
        $html .= '</ul>';
        $html .= '</div>';

        $html .= '</div>';
        $html .= '<p class="item-price">' . $price . '</p>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}

if (!function_exists('product_design_theme3')) {
    function product_design_theme3(array $options = [])
    {
        $default = [
            'bootstrap_cols' => null,
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; '
        ];
        $option = array_merge($default, $options);
        $pro = Product::where(['id' => $option['product_id']])->first();

        $get_price = get_product_price(['main_pid' => $option['product_id']]);

        $vendorLogo = ['public/pmp_img/vendor1.png', 'public/pmp_img/vendor2.png', 'public/pmp_img/vendor3.jpeg'];

        //dump($get_price);
        if (is_employee() && ($get_price['d_price'] != null || $get_price['d_price'] > 0)) {

            // dump($get_price);
            //<strike>৳400.00</strike> <b>' . $price . '</b>
            $price = '<span><b>' . $option['sign'] . number_format($get_price['d_price']) . ' ' . '</b></span><br/>';
            $price .= '<strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike>';
            $save = $get_price['r_price'] - $get_price['d_price'];

        } else {
            if ($option['flash_now'] == 'Yes') {
                $price = '<span><b>' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</b></span><br>';
                $price .= '<strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike>';

            } elseif ($option['flash_now'] == 'No') {

                $price = '<span><b>???</b></span><br>';
                $price .= '<strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike>';

            } else {

                if ($get_price['s_price'] < $get_price['r_price']) {
                    $price = '<span><b>' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</b></span><br>';
                    $price .= '<strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike>';
                } else {
                    $price = '<span><b>' . $option['sign'] . number_format($get_price['s_price']) . '</b></span>';
                }

            }
            $save = number_format($get_price['r_price'] - $get_price['s_price']);
            //$save = 100;
        }
        if ($get_price['save'] > 0) {
            $price .= '<span class="off_sp"> - ' . $get_price['save'] . '% Off</span>';
        }

        $html = '<div class=" cat_pfive_pbox">
                                    <div class="' . $option['bootstrap_cols'] . '">
                                        <div class="row">';

        $html .= '<div class="col-lg-2 col-3">
                                                <div class="cat_layout_f_pimg">
                                                    <a href="' . $option['seo_url'] . '"><img class="img-fluid" src="' . url($option['first_image']) . '"></a>
                                                </div>
                                                <!--<div class="cat_layout_f_addc">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" class="form-check-input" value="">Add to Compare
                                                        </label>
                                                    </div>
                                                </div>-->
                                            </div>';

        $html .= '<div class="col-lg-6 col-5">
                                                <div class="single_pview_righttop">
                                                    <div class="sing_protitle">
                                                        <a href="' . $option['seo_url'] . '"><h3>' . $option['title'] . '</h3></a>
                                                    </div>
                                                    <div class="singp_brand">
                                                       <div class="assured">
                                                            <img src="' . asset($vendorLogo[rand(0, 2)]) . '" class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="ratings_single">
                                                        <div class="rat_inner">
                                                            <div class="rat_btn">
                                                                3.9 <i class="fa fa-star-o"></i>
                                                            </div>
                                                            <span class="ratshow">
                                                                11,338 ratings and 1,055 reviews
                                                            </span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="statusblock">
                                                        <p><b>Status:</b> <strick> ' . ($pro->stock_status == 1 ? 'In Stock' : 'Out Of Stock') . '</strick></p>
                                                            ' . $pro->short_description . '
                                                    </div>
                                                </div>
                                            </div>';

        $html .= '<div class="col-lg-4 col-4">
                                                <div class="sinp_price">
                                                    <p class="item-price">
                                                        ' . $price . ' 
                                                    </p>
                                                    <p class="upto_ex">Up to ৳36950 off on Exchance
                                                    </p>
                                                    <p class="upto_ex">ENI starting form ৳36950/month
                                                    </p>
                                                    <p class="item-price"><span>Offer</span></p>
                                                    <p class="sp-pricetitle">Special Price</p>
                                                    
                                                    <div class="button-group so-quickview cartinfo--left">';

        $proid = '"' . trim($option['product_id']) . '"';
        $procode = '"' . $option['product_code'] . '"';
        $str_seo_url = '"' . $option['straight_seo_url'] . '"';

        $html .= addtocart_button_container([
            'straight_seo_url' => null,
            'title' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; ',
            'save_price' => $save,
            'multi_id' => $get_price['multi_id'],
            'pro' => $pro
        ]);

        $html .= addtocompare_button_container_new(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= addttowish_button_container_new(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);


        $html .= '</div></div></div>';

        $html .= '</div></div></div>';

///End

        //Its For Test
        $defaultHTML = '<div class=" cat_pfive_pbox">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-3">
                                                <div class="cat_layout_f_pimg">
                                                    <a href="#"><img class="img-fluid" src="img/rflus-front-vivo-y81i-2gb-16gb-black.jpg"></a>
                                                </div>
                                                <div class="cat_layout_f_addc">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" class="form-check-input" value="">Add to Compare
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-5">
                                                <div class="single_pview_righttop">
                                                    <div class="sing_protitle">
                                                        <h3>Oppo A3s - 2GB</h3>
                                                    </div>
                                                    <div class="singp_brand">
                                                       <div class="assured">
                                                            <img src="img/fa_8b4b59.png" class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="ratings_single">
                                                        <div class="rat_inner">
                                                            <div class="rat_btn">
                                                                3.9 <i class="fa fa-star-o"></i>
                                                            </div>
                                                            <span class="ratshow">
                                                                11,338 ratings and 1,055 reviews
                                                            </span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="statusblock">
                                                        <p><b>Status:</b> <strick> In Stock</strick></p>
                                                        <ul class="more_off">
                                                            <li>Sensors: Accelerometer, proximity, compass Accelerometer, proximity, compass</li>
                                                            <li>Full Screen stimulates the senses</li>
                                                            <li>1 Year Brand Warranty</li>
                                                            <li>Full Screen stimulates the senses</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="sinp_price">
                                                    <p class="item-price">
                                                        <span><b>৳36950.00</b></span> <br> 
                                                        <strike>৳400.00</strike> 
                                                        <span class="off_sp">60% off</span>
                                                    </p>
                                                    <div class="sectionGapCommon"></div>
                                                    <p class="upto_ex">Up to ৳36950 off on Exchance
                                                    </p>
                                                    <p class="upto_ex">ENI starting form ৳36950/month
                                                    </p>
                                                    <p class="item-price"><span>Offer</span></p>
                                                    <p class="sp-pricetitle">Special Price</p>
                                                    
                                                    <div class="sectionGapCommon"></div>
                                                    <div class="button-group so-quickview cartinfo--left">
                                                        <a data-toggle="tooltip" value="Add to Cart" data-loading-text="Loading..." id="button-cart" class="addToCart" data-color_id="" data-size_id="" data-productid="516" data-qty="1" title="Add to cart" onclick="add_to_cart_data(this);" data-original-title="Add to Cart">
                                                            <i class="bb-cart-plus"></i>
                                                        </a>
                                                        <a onclick="add_to_compare_all(this);" data-url="laminated-board-dressing-table-dth-103" data-procode="99666" data-proid="516" href="javascript:void(0);" class="addToCart" title="Compare this Product">
                                                            <i class="bb-compare-plus"></i><span></span>
                                                        </a>
                                                        <a onclick="add_to_wish_list(this);" data-proid="516" href="javascript:void(0);" class="addToCart" title="Add to wish list">
                                                            <i class="bb-wishlist-plus"></i><span></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';

        return $html;

    }
}


if (!function_exists('product_design_imgsidebyside')) {
    function product_design_imgsidebyside(array $options = [])
    {
        $default = [
            'bootstrap_cols' => null,
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; '
        ];
        $option = array_merge($default, $options);
        $pro = Product::where(['id' => $option['product_id']])->first();
        $get_price = get_product_price(['main_pid' => $option['product_id']]);
        //dump($get_price);
        if (is_employee() && ($get_price['d_price'] != null || $get_price['d_price'] > 0)) {
            // dump($get_price);
            //<strike>৳400.00</strike> <b>' . $price . '</b>
            $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['d_price']) . ' ' . '</b></span>';
            $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
            $save = $get_price['r_price'] - $get_price['d_price'];
        } else {
            if ($option['flash_now'] == 'Yes') {
                $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</b></span>';
                $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
            } elseif ($option['flash_now'] == 'No') {
                $price = '<span class="price-new"><b>???</b></span>';
                $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
            } else {
                if ($get_price['s_price'] < $get_price['r_price']) {
                    $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['s_price']) . ' ' . '</b></span>';
                    $price .= '<span class="price-old"><strike>' . $option['sign'] . number_format($get_price['r_price']) . '</strike></span>';
                } else {
                    $price = '<span class="price-new"><b>' . $option['sign'] . number_format($get_price['s_price']) . '</b></span>';
                }
            }
            $save = number_format($get_price['r_price'] - $get_price['s_price']);
            //$save = 100;
        }

        $proid = '"' . trim($option['product_id']) . '"';
        $procode = '"' . $option['product_code'] . '"';
        $str_seo_url = '"' . $option['straight_seo_url'] . '"';

        $html = '<div class="' . $option['bootstrap_cols'] . '">';
        $html .= '<div class="arrivals_item pro2-hover">';
        $html .= '<div class="arrivals_img">';
        $html .= image_container_imgsidebysidenew([
            'seo_url' => $option['seo_url'],
            'straight_seo_url' => $option['straight_seo_url'],
            'title' => $option['title'],
            'first_image' => $option['first_image'],
            'second_image' => $option['second_image']
        ]);
        $html .= '</div>';
        $html .= '<div class="arrivals_caption">';
        $html .= title_container_sidebyside(['seo_url' => $option['seo_url'], 'title' => $option['title']]);
//        $html .= '<div class="star-rating">
//                    <ul class="list-inline">
//                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
//                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
//                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
//                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
//                        <li class="list-inline-item"><i class="fa fa-star"></i></li>
//                    </ul>
//                </div>';
        $html .= '<span class="pro_Price">' . $price . '</span>';
        $html .= '</div>';
        $html .= '<div class=" button-group so-quickview cartinfo--left hidden">';
        $html .= addtocart_button_container([
            'straight_seo_url' => null,
            'title' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; ',
            'save_price' => $save,
            'multi_id' => $get_price['multi_id'],
            'pro' => $pro
        ]);

        $html .= addtocompare_button_container_new(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= addttowish_button_container_new(['proid' => $proid, 'procode' => $procode, 'str_seo_url' => $str_seo_url]);
        $html .= '</div>';

        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}


if (!function_exists('addtocart_button_container')) {
    function addtocart_button_container(array $options = [])
    {
        $default = [
            'bootstrap_cols' => null,
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
            'discount_rate' => null,
            'price' => null,
            'old_price' => null,
            'product_code' => null,
            'product_sku' => null,
            'product_id' => null,
            'product_qty' => null,
            'flash_id' => null,
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
            'emi' => null,
            'sign' => '&#2547; ',
            'save_price' => null,
            'multi_id' => null,
            'pro' => null
        ];
        $option = array_merge($default, $options);
        //dump($option);

//        $color_info = Pcombinationdata::where(['main_pid' => $option['pro']->id])->orderBy('stock', 'DESC')->get();
//        if ($color_info->count() > 0) {
//            $color_id = $color_info->first()->id;
//        } else {
//            $color_id = null;
//        }

        $html = null;

        $disable_buy = (!empty($option['pro']->disable_buy) && $option['pro']->disable_buy == 'on') ? 'disabled' : '';
        $button_name = (!empty($option['pro']->disable_buy) && $option['pro']->disable_buy == 'on') ? 'Disable Buy Now' : 'Add to Cart';
        $color_info = Pcombinationdata::where(['id' => $option['multi_id']])->first();
        if ($color_info) {
            $data_size_id = ($color_info['size']) ? $option['multi_id'] : null;
            $data_color_id = ($color_info['color_codes']) ? $option['multi_id'] : null;
        } else {
            $data_size_id = null;
            $data_color_id = null;
        }

        if (!empty($option['pro']->disable_buy) && $option['pro']->disable_buy == 'on') {
            $html .= '<a data-toggle="tooltip" title="Add to cart" value="' . $button_name . '" class="addToCart" data-original-title="' . $button_name . '" ' . $disable_buy . ' ><i class="bb-cart-plus"></i></a>';
        } else {
            $html .= '<a data-toggle="tooltip" value="' . $button_name . '"
                        data-loading-text="Loading..." id="button-cart"
                        class="addToCart"
                        data-color_id = "' . $data_color_id . '"
                        data-size_id = "' . $data_size_id . '"
                        data-productid="' . $option['pro']->id . '"
                        data-qty="1"
                        title="Add to cart"
                        onclick="add_to_cart_data(this);"
                        data-original-title="' . $button_name . '" ' . $disable_buy . ' ><i class="bb-cart-plus"></i></a>';
        }

        return $html;
    }
}

if (!function_exists('addtowishlist_button_container')) {
    function addtowishlist_button_container(array $options = [])
    {
        $default = [
            'proid' => null,
            'procode' => null,
            'str_seo_url' => null
        ];
        $option = array_merge($default, $options);

        $html = null;
        $html .= '<button onclick="add_to_wishlist($option[\'proid\'], $option[\'procode\'], $option[\'str_seo_url\']);" type="button" class="wishlist btn-button" title="Add to Wish List">';
        $html .= '<i class="fa fa-heart-o"></i><span>Add to Wish List</span>';
        $html .= '</button>';

        return $html;
    }
}

if (!function_exists('addtocompare_button_container')) {
    function addtocompare_button_container(array $options = [])
    {
        $default = [
            'proid' => null,
            'procode' => null,
            'str_seo_url' => null
        ];
        $option = array_merge($default, $options);

        $html = null;
        $html .= '<button onclick="add_to_compare_all(this);" data-url=' . $option['str_seo_url'] . ' data-procode=' . $option['procode'] . ' data-proid=' . $option['proid'] . ' href="javascript:void(0);"  class="compare btn-button" title="Compare this Product">';
        $html .= '<i class="bb-compare-plus"></i><span></span>';
        $html .= '</button>';

        return $html;
    }
}

if (!function_exists('addtocompare_button_container_new')) {
    function addtocompare_button_container_new(array $options = [])
    {
        $default = [
            'proid' => null,
            'procode' => null,
            'str_seo_url' => null
        ];
        $option = array_merge($default, $options);

        $html = null;
        $html .= '<a onclick="add_to_compare_all(this);" 
        data-url=' . $option['str_seo_url'] . ' 
        data-procode=' . $option['procode'] . ' 
        data-proid=' . $option['proid'] . ' 
        href="javascript:void(0);"  
        class="addToCart"
        title="Compare this Product">';
        $html .= '<i class="bb-compare-plus"></i><span></span>';
        $html .= '</a>';

        return $html;
    }
}

if (!function_exists('addttowish_button_container')) {
    function addttowish_button_container(array $options = [])
    {
        $default = [
            'proid' => null,
            'procode' => null,
            'str_seo_url' => null
        ];
        $option = array_merge($default, $options);

        $html = null;

        $html .= '<button onclick="add_to_wish_list(this);"  data-proid=' . $option['proid'] . ' href="javascript:void(0);"  class="compare btn-button" title="Add to wish list">';
        $html .= '<i class="fa fa-heart"></i><span></span>';
        $html .= '</button>';

        return $html;
    }
}


if (!function_exists('addttowish_button_container_new')) {
    function addttowish_button_container_new(array $options = [])
    {
        $default = [
            'proid' => null,
            'procode' => null,
            'str_seo_url' => null
        ];
        $option = array_merge($default, $options);

        $html = null;
        $html .= '<a onclick="add_to_wish_list(this);"  
        data-proid=' . $option['proid'] . ' 
        href="javascript:void(0);"  
        class="addToCart"
        title="Add to wish list">';
        $html .= '<i class="bb-wishlist-plus"></i><span></span>';
        $html .= '</a>';

        return $html;
    }
}

if (!function_exists('title_container')) {
    function title_container(array $options = [])
    {
        $default = [
            'seo_url' => null,
            'title' => null
        ];
        $option = array_merge($default, $options);

        $html = '<h4><a href="' . $option['seo_url'] . '" title="' . $option['title'] . '" target="_self">' . $option['title'] . '</a></h4>';
        return $html;
    }
}

if (!function_exists('title_container_sidebyside')) {
    function title_container_sidebyside(array $options = [])
    {
        $default = [
            'seo_url' => null,
            'title' => null
        ];
        $option = array_merge($default, $options);
        $html = '<h4 class="pro_title"><a href="' . $option['seo_url'] . '" title="' . $option['title'] . '" target="_self">' . $option['title'] . '</a></h4>';
        return $html;
    }
}

if (!function_exists('title_container_new')) {
    function title_container_new(array $options = [])
    {
        $default = [
            'seo_url' => null,
            'title' => null
        ];
        $option = array_merge($default, $options);

        $html = '<h4><a href="' . $option['seo_url'] . '" title="' . $option['title'] . '" target="_self">' . $option['title'] . '</a></h4>';
        return $html;
    }
}

if (!function_exists('image_container')) {
    function image_container(array $options = [])
    {
        $default = [
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
        ];
        $option = array_merge($default, $options);

        $html = '<div class="product-image-container second_img">';

        $html .= '<a href="' . $option['seo_url'] . '" target="_self" title="' . $option['title'] . '">';
        $html .= '<img src="' . $option['first_image'] . '" class="img-1 img-responsive" alt="' . $option['title'] . '">';
        if (!empty($option['second_image'])) {
            $html .= '<img src="' . $option['second_image'] . '" class="img-2 img-responsive" alt="' . $option['title'] . '">';
        } else {
            $html .= '<img src="' . $option['first_image'] . '" class="img-2 img-responsive" alt="' . $option['title'] . '">';
        }
        $html .= '</a>';

        $html .= '</div>';

        return $html;
    }
}


if (!function_exists('image_container_new')) {
    function image_container_new(array $options = [])
    {
        $default = [
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
        ];
        $option = array_merge($default, $options);


        $html = '<div class="img-box">';

        $html .= '<a href="' . $option['seo_url'] . '" target="_self" title="' . $option['title'] . '">';
        $html .= '<img src="' . $option['first_image'] . '" class="img-1 img-responsive" alt="' . $option['title'] . '">';
        $html .= '</a>';

        $html .= '</div>';

        return $html;
    }
}


if (!function_exists('image_container_imgsidebysidenew')) {
    function image_container_imgsidebysidenew(array $options = [])
    {
        $default = [
            'seo_url' => null,
            'straight_seo_url' => null,
            'title' => null,
            'first_image' => null,
            'second_image' => null,
        ];
        $option = array_merge($default, $options);


        $html = '<div class="arrivals_img">';
        $html .= '<a href="' . $option['seo_url'] . '" target="_self" title="' . $option['title'] . '">';
        $html .= '<img class="img-fluid"
                         src="' . $option['first_image'] . '" alt="' . $option['title'] . '">';
        $html .= '</a>';
        $html .= '</div>';


        return $html;
    }
}


if (!function_exists('flash_now_container')) {
    function flash_now_container(array $options = [])
    {
        $default = [
            'flash_now' => null,
            'flash_old_count' => null,
            'flash_now_count' => null,
        ];
        $option = array_merge($default, $options);

        $html = '';
        if ($option['flash_now'] == 'Yes') {
            $sole = $option['flash_old_count'] - $option['flash_now_count'];
            $sole_par = $sole * 100 / $option['flash_old_count'];

            $html = '<div class="progress progress_flash">';
            $html .= '<div class="progress-bar progress_flash_bar" role="progressbar" style="width: ' . $sole_par . '%" aria-valuenow="' . $sole_par . '" aria-valuemin="0" aria-valuemax="100"></div>';
            $html .= '</div>';
            $html .= '<p class="pc-sold">Sold: <b>' . $sole . '</b></p>';
        }

        return $html;
    }
}

if (!function_exists('product_review_count')) {
    function product_review_count($id)
    {
        //$id = 560;
        $this_review = Review::where(['product_id' => $id, 'is_active' => 1])->get();
        //dump($id);

        $review_count = 0;
        $review_total = 0;
        foreach ($this_review as $review) {
            $review_total += $review->rating;
            ++$review_count;
        }
        if ($review_count == 0) {
            $avarage_ratting = 0;
        } else {
            $avarage_ratting = round($review_total / $review_count);
        }

        $html = '';
        $html .= '<div class="ratings">';
        $html .= '<div class="rating-box">';
        for ($x = 1; $x <= 5; $x++) {
            if ($x <= $avarage_ratting) {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>';
            } else {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
            }
        }
        $html .= '</div>';
        $html .= '<span class="rating-num">(' . $review_count . ')</span>';
        $html .= '</div>';
        //dump('hasan');
        return $html;
    }
}

if (!function_exists('product_review_count_new')) {
    function product_review_count_new($id)
    {
        //$id = 560;
        $this_review = Review::where(['product_id' => $id, 'is_active' => 1])->get();
        //dump($id);

        $review_count = 0;
        $review_total = 0;
        foreach ($this_review as $review) {
            $review_total += $review->rating;
            ++$review_count;
        }
        if ($review_count == 0) {
            $avarage_ratting = 0;
        } else {
            $avarage_ratting = round($review_total / $review_count);
        }

        $html = '';
        $html .= '<div class="ratings">';
        $html .= '<div class="rating-box">';
        for ($x = 1; $x <= 5; $x++) {
            if ($x <= $avarage_ratting) {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>';
            } else {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
            }
        }
        $html .= '</div>';
        $html .= '<span class="rating-num">(' . $review_count . ')</span>';
        $html .= '</div>';
        //dump('hasan');
        return $html;
    }
}

if (!function_exists('product_review_count_only')) {
    function product_review_count_only($id)
    {
        $this_review = Review::where(['product_id' => $id, 'is_active' => 1])->get();

        $review_count = 0;
        foreach ($this_review as $review) {
            ++$review_count;
        }

        return $review_count;
    }
}

if (!function_exists('product_review')) {
    function product_review($id)
    {
        $this_review = Review::where(['product_id' => $id, 'is_active' => 1])->get();

        $review_count = 0;
        $review_total = 0;
        foreach ($this_review as $review) {
            $review_total += $review->rating;
            ++$review_count;
        }
        if ($review_count == 0) {
            $avarage_ratting = 0;
        } else {
            $avarage_ratting = round($review_total / $review_count);
        }

        $html = '';
        $html .= '<div class="ratings">';
        $html .= '<div class="rating-box">';
        for ($x = 1; $x <= 5; $x++) {
            if ($x <= $avarage_ratting) {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>';
            } else {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
            }
        }
        $html .= '</div>';
        $html .= '</div>';
        //dump('hasan');
        return $html;
    }
}

if (!function_exists('review_star')) {
    function review_star($review)
    {
        $avarage_ratting = $review;
        $html = '';
        $html .= '<div class="ratings">';
        $html .= '<div class="rating-box">';
        for ($x = 1; $x <= 5; $x++) {
            if ($x <= $avarage_ratting) {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>';
            } else {
                $html .= '<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>';
            }
        }
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
}

if (!function_exists('order_master_create')) {
    function order_master_create($data, $rand, $secret_key, $user_id)
    {
        dd($data['user_details']);
        if ($data['payment_method']['payment_method'] == 'cash_on_delivery') {
            $order_status = 'placed';
        } else {
            $order_status = 'Placed';
        }
        if (!empty($data['user_details']) && $data['cart']) {
            return $orders_master_attributes = [
                'user_id' => $user_id,
                'order_random' => $rand,
                'customer_name' => $data['user_details']['name'],
                'phone' => $data['user_details']['phone'],
                'emergency_phone' => $data['user_details']['emergency_phone'],
                'address' => $data['user_details']['address'],
                'notes' => $data['user_details']['notes'],
                'email' => $data['user_details']['email'],
                'order_date' => date('Y-m-d'),
                'payment_method' => $data['payment_method']['payment_method'],
                'payment_term_status' => 'Pending',
                'payment_parameter' => null,
                'order_status' => $order_status,
                'params' => null,
                'secret_key' => $secret_key,
                'delivery_date' => null,
                'currency' => 'BDT',
                'total_qty' => $data['cart']->totalqty,
                'delivery_fee' => $data['user_details']['deliveryfee'],
                'grand_total' => $data['payment_method']['grand_total'],
                'total_amount' => $data['payment_method']['total_amount'],
                'coupon_type' => (isset($data['coupon_details']['coupon_type']) ? $data['coupon_details']['coupon_type'] : null),
                'coupon_code' => (isset($data['coupon_details']['coupon_type']) ? $data['coupon_details']['coupon_code'] : null),
                'coupon_discount' => (isset($data['coupon_details']['coupon_type']) ? $data['coupon_details']['coupon_discount'] : 0),
                'division' => $data['user_details']['division'],
                'district' => $data['user_details']['district'],
                'thana' => $data['user_details']['thana'],
                'dba' => $data['user_details']['dba']??'',
                'dba_name' => $data['user_details']['dba_name']??'',
                'dba_address' => $data['user_details']['dba_address']??'',
                'dba_division' => $data['user_details']['dba_division']??'',
                'dba_district' => $data['user_details']['dba_district']??'',
                'dba_thana' => $data['user_details']['dba_thana']??'',
                'trans_id' => $data['payment_method']['payment_id'],
                'is_active' => 1
            ];
        } else {
            redirect('my_account')->withErrors(['error' => 'You have not set any product on cart']);
        }
    }
}

if (!function_exists('order_detail_create')) {
    function order_detail_create($data, $rand, $secret_key, $user_id,$order_shipment_id = 0)
    {
        if (!empty($data['cart'])) {
            $details = [];
            // dump($data['cart']);
            foreach ($data['cart']->items as $item) {
                $product_info = Product::Where(['id' => $item['item']['productid']])->first();

                if ($item['item']['size_colo']) {
                    $multi_data = App\Pcombinationdata::Where(['id' => $item['item']['size_colo']])->first();
                } else {
                    $multi_data = false;
                }

                if ($data['payment_method']['payment_method'] == 'cash_on_delivery') {
                    $order_status = 'Placed';
                } else {
                    $order_status = 'Placed';
                }

                $aff_amount  = 0;

                if(isset($data['aff_key']) && $data['aff_key'] && $product_info->affiliate_commission > 0){
                    $aff_amount = ($product_info->affiliate_commission * $item['purchaseprice'])/100;
                }

                $details[] = [
                    'user_id' => $user_id,
                    'vendor_id' => $product_info->user_id,
                    'order_random' => $rand,
                    'order_shipment_id' => $order_shipment_id,
                    'product_id' => $item['item']['productid'],
                    'product_name' => product_title($item['item']['productid']),
                    'product_code' => $item['item']['productcode'],
                    'qty' => $item['qty'],
                    'order_date' => date('Y-m-d'),
                    'img' => null,
                    'local_selling_price' => $item['item']['pre_price'],
                    'local_purchase_price' => $item['purchaseprice'],
                    'delivery_charge' => null,
                    'discount' => $item['item']['dis_tag'],
                    'is_dp' => $item['item']['is_dp'],
                    'is_flash' => (($item['item']['flash_discount']) ? 'Yes' : 'No'),
                    'flash_id' => null,
                    'flash_discount' => $item['item']['flash_discount'],
                    'item_code' => (($multi_data) ? $multi_data->item_code : null),
                    'color_type' => (($multi_data) ? $multi_data->type : null),
                    'size_color_id' => (($multi_data) ? $multi_data->id : null),
                    'color' => (($multi_data) ? $multi_data->color_codes : null),
                    'size' => (($multi_data) ? $multi_data->size : null),
                    'item_jeson' => json_encode($item['item']),
                    'aff_key' => isset($data['aff_key']) && $aff_amount > 0 ? $data['aff_key'] : null,
                    'aff_amount' => $aff_amount,
                    'secret_key' => $secret_key,
                    'od_status' => $order_status,
                    'is_active' => 1
                ];

                // dd($details);

                //$this->ordersdetail->create($orders_detail_attributes);
            }
            //dump($details);
            //die();
            return OrdersDetail::insert($details);
        } else {
            redirect('my_account')->withErrors(['error' => 'You have not set any product on cart']);
        }
    }
}

if (!function_exists('get_filters_cat')) {
    function get_filters_cat($id)
    {
        // dd($id);
        $get_first = Term::where(['id' => $id])->get()->first();

        if ($get_first->parent == 1) {
            $parent_data = ['parent' => $get_first->id, 'step' => 1];

        } else {
            $parent_get = Term::where(['id' => $get_first->parent])->get()->first();
            if ($parent_get->parent == 1) {
                $parent_data = ['parent' => $parent_get->id, 'step' => 2];
            } else {
                $parent_get = Term::where(['id' => $parent_get->parent])->get()->first();
                $parent_data = [
                    'parent' => (isset($parent_get->id)) ? $parent_get->id : $parent_get['id'],
                    'step' => 3];
            }
        }

        if ($parent_data['step'] == 1) {
            $parent_cat = Term::where(['id' => $parent_data['parent']])->get()->first();
            $sub_menu = Term::where(['parent' => $parent_cat->id])->get();
            $child_menu = null;

        } elseif ($parent_data['step'] == 2) {
            //dd($id);
            $parent_cat = Term::where(['id' => $parent_data['parent']])->get()->first();
            $sub_menu = Term::where(['id' => $id])->get()->first();
            $child_menu = Term::where(['parent' => $sub_menu->id])->orderBy('name', 'ASC')->get();
        } elseif ($parent_data['step'] == 3) {
            $parent_cat = Term::where(['id' => $parent_data['parent']])->get()->first();
            $child_menu = Term::where(['id' => $id])->get()->first();
            // dd($child_menu);
            $sub_menu = Term::where([
                'id' => (isset($child_menu->parent)) ? $child_menu->parent : $parent_get['id']
            ])->get()->first();
        }

       // dd($get_first,$parent_cat,$sub_menu);


        $cat_data = ['parent_cat' => $parent_cat, 'sub_menu' => $sub_menu, 'child_menu' => $child_menu];

        return $cat_data;
    }
}

if (!function_exists('get_all_sub_cat')) {
    function get_all_sub_cat($id)
    {
        // dd($id);
        $get_first = Term::where(['id' => $id])->get()->first();

        if ($get_first->parent == 1) {
            $parent_data = ['parent' => $get_first->id, 'step' => 1];
        } else {
            $parent_get = Term::where(['id' => $get_first->parent])->get()->first();
            if ($parent_get->parent == 1) {
                $parent_data = ['parent' => $parent_get->id, 'step' => 2];
            } else {
                $parent_get = Term::where(['id' => $parent_get->parent])->get()->first();
                $parent_data = [
                    'parent' => (isset($parent_get->id)) ? $parent_get->id : $parent_get['id'],
                    'step' => 3];
            }
        }
        //dd($parent_data['step']);

        if ($parent_data['step'] == 1) {
            $parent_cat = Term::where(['id' => $id])->select('id')->get()->first();
            $sub_menu = Term::where(['parent' => $parent_cat->id])->select('id')->get();
            $all_cat = [];
            $all_cat[] .= $parent_cat->id;

            foreach ($sub_menu as $sub) {
                $all_cat[] .= $sub->id;
                $chil_menu = Term::where(['parent' => $sub->id])->select('id')->get();
                foreach ($chil_menu as $chil) {
                    $all_cat[] .= $chil->id;
                }
            }
        } elseif ($parent_data['step'] == 2) {
            $parent_cat = Term::where(['id' => $id])->select('id')->get()->first();
            $sub_menu = Term::where(['parent' => $parent_cat->id])->select('id')->get();
            $all_cat = [];
            $all_cat[] .= $parent_cat->id;

            foreach ($sub_menu as $sub) {
                $all_cat[] .= $sub->id;
            }
        } elseif ($parent_data['step'] == 3) {
            $parent_cat = Term::where(['id' => $id])->select('id')->get()->first();
            $all_cat = [];
            $all_cat[] .= $parent_cat->id;
        }

        $cat_data = $all_cat;

        return $cat_data;
    }
}

if (!function_exists('is_flash_item')) {
    function is_flash_item($id)
    {
        $flash_rule = [
            'fs_is_active' => 1
        ];
        $flash_schedule = FlashShedule::where($flash_rule)
            ->whereRaw('NOW() BETWEEN fs_start_date AND fs_end_date ')
            ->orderBy('fs_start_date', 'ASC')->get()->first();

        if (!empty($flash_schedule)) {
            $flash_itmes = FlashItem::where(['fi_shedule_id' => $flash_schedule->id, 'fi_product_id' => $id])->first();

            // dump($flash_itmes);
            if ($flash_itmes) {
                $data = [
                    'flash_item' => $flash_itmes->id,
                    'discount' => $flash_itmes->fi_discount,
                    'discount_tag' => $flash_itmes->fi_show_tag,
                    'old_qty' => $flash_itmes->fi_qty,
                    'end_time' => $flash_schedule->fs_end_date
                ];
            } else {
                $data = [];
            }

            $result = $data;
        } else {
            $result = false;
        }

        return $result;
    }
}

if (!function_exists('get_search_category')) {
    function get_search_category($options)
    {
        $dufault = [
            'keyword' => null,
            'cat' => null
        ];

        $new = array_merge($dufault, $options);

        $result = Product::where(['products.is_active' => 1])
            ->leftJoin('productcategories AS pc', function ($join) {
                $join->on('pc.main_pid', '=', 'products.id');
            })
            ->leftJoin('terms AS t', function ($join) {
                $join->on('t.id', '=', 'pc.term_id');
            });

        if ($new['cat'] != null) {
            $result = $result->where('pc.term_id', $new['cat']);
            $result = $result->where('pc.term_id', '!=', null);
        }

        $result = $result->where(function ($query) use ($new) {
            $query->orWhere('products.title', 'like', "%{$new['keyword']}%");
        });
        $result = $result->select('pc.term_id as cat_id', 't.seo_url as t_url');

        $result = $result->groupBy('pc.term_id')->get();

        $cat_list = [];
        foreach ($result as $cat) {
            $cat_list[] .= $cat->cat_id;
        }

        return $cat_list;
    }
}

if (!function_exists('get_product_pricing')) {
    function get_product_pricing($option)
    {
        $default = [
            'main_pid' => null,
            'color' => null,
            'size' => null,
            'type' => null,
        ];

        $key = array_merge($default, $option);

        $p_info = Product::where(['id' => $key['main_pid']])->first();

//         dd($p_info->offer_end_date);

        if ($p_info->multiple_pricing == 'on') {
            $get_price = Pcombinationdata::where(['main_pid' => $key['main_pid']]);

            if ($key['color'] != null) {
                $color_info = Pcombinationdata::where(['id' => $key['color']])->first();
                if ($color_info->color_codes) {
                    $get_price = $get_price->where(['color_codes' => $color_info->color_codes]);
                }
            }

            if ($key['size'] != null) {
                $size_info = Pcombinationdata::where(['id' => $key['size']])->first();

                if (isset($color_info->color_codes)) {
                    $get_price = $get_price->where(['size' => $size_info->size]);
                }
            }
            $get_price = $get_price->get()->first();
        }

        if (Auth::check() && auth()->user()->isDP()) {
            if ($p_info->multiple_pricing == 'on') {
                //  dd($get_price);
                $dp_price = $get_price->dp_price;
            } else {
                $dp_price = $p_info->dp_price;
            }
        } else {
            $dp_price = 0;
        }

        $p_html = '';
        $tksign = '&#2547; ';

        if ($p_info->multiple_pricing == 'on') {
            $r_price = $get_price->regular_price;
            $s_price = $get_price->selling_price;
        } else {
            $r_price = $p_info->local_selling_price;
            if ($p_info->local_discount >= 1) {
                $today = Carbon::today()->format('m/d/Y');
                // echo $p_info->today;
                // echo $p_info->offer_end_date;
                if ($p_info->offer_end_date != null && $p_info->offer_end_date >= $today) {
                    $s_price = $p_info->local_selling_price - (($p_info->local_selling_price * $p_info->local_discount) / 100);
                } else {
                    $s_price = $p_info->local_selling_price;
                }
            } else {
                $s_price = $p_info->local_selling_price;
            }
        }

        $is_flash = is_flash_itme($key['main_pid']);
        if ($is_flash) {
            $save = number_format($is_flash->fi_discount);
            $save_per = number_format($is_flash->fi_show_tag);
            // dump($get_price);
            if ($p_info->multiple_pricing == 'on') {
                $s_price = $get_price->selling_price - $is_flash->fi_discount;
            } else {
                $s_price = $p_info->local_selling_price - $is_flash->fi_discount;
            }
            // $s_price = number_format(100);
        } else {
            $save = $r_price - $s_price;
            $save_per = round(($save * 100) / $r_price);
        }

        // dd($key);

        $get_price = get_product_price($key);

        // dump($get_price);

        if ($get_price['save'] > 0) {
            $p_html .= '<span class="regularprice" itemprop="price"> Regular Price: ' . $tksign . number_format($get_price['r_price']) . '</span> <br>';
            $p_html .= '<div class="product_page_price price" itemprop="offerDetails">';
            $p_html .= '<span class="price-new" id="price_tag" itemprop="price"> Discount Price ( ' . $get_price['save'] . ' %): ' . $tksign . number_format($get_price['s_price']);

            $p_html .= '<span style="font-size: 13px; color: #444; margin-left: 5px;">(Save ' . $tksign . number_format($get_price['r_price'] - $get_price['s_price']) . ')</span>';
            $p_html .= '</span></div>';
            if ($dp_price > 0) {
                $p_html .= '<p class="price-dp">DP Price : <span>' . $tksign . number_format($dp_price) . '</span> </p>';
            }
        } else {
            $p_html .= '<p class="price"><span class="price-new"> Price : ' . $tksign . number_format($get_price['r_price']) . ' </span></p>';

            if ($dp_price > 0) {
                $p_html .= '<p class="price-dp">DP Price : <span>' . $tksign . number_format($dp_price) . ' </span></p>';
            }
        }

        //dd($p_html);

        $color = Pcombinationdata::where(['main_pid' => $key['main_pid']])->whereNotNull('color_codes')->groupBy('color_codes')->get();
        // dd($color);
        if ($key['color'] != null) {
            $get_color = Pcombinationdata::where(['id' => $key['color']])->first();
            $size = Pcombinationdata::where([
                'main_pid' => $key['main_pid'], 'color_codes' => $get_color->color_codes])
                ->whereNotNull('size')->groupBy('size')->get();
        } else {
            $size = Pcombinationdata::where(['main_pid' => $key['main_pid']])->whereNotNull('size')->groupBy('size')->get();
        }

        //dd($color->count());
        $c_html = '';

        if ($color->count() > 0) {
            $c_html .= '<div class="short_description form-group"> <h4>Color:</h4></div> <div class="coor-warp"><div class="cc-selector">';
            $c_html .= '<ul id="color_ul" class="milit_thum">';

            foreach ($color as $col) {
                // dd($col);
                if ($key['color'] == $col->id) {
                    $active = 'active';
                } else {
                    $active = '';
                }

                $c_html .= '<li class="item-color-front ' . $active . '" data-color="' . $col->id . '">';
                if ($col->type == 1) {
                    $c_html .= '<a data-index="0" class="multi_img_view etr img thumbnail" data-image="' . url('public/pmp_img/' . $col->color_codes) . '" data-zoom-image="' . url('public/pmp_img/' . $col->color_codes) . '">';
                    $c_html .= '<img src="' . url('public/pmp_img/' . $col->color_codes) . '" width="30px" height="30px"></a>';
                } else {
                    $c_html .= '<span class="colo-box-front" style="background: #' . $col->color_codes . '"></span>';
                }
                $c_html .= '</li>';
            }
            $c_html .= '</ul></div></div>';
        }

        $s_html = '';
        if ($size->count() > 0) {
            $s_html .= '<div class="size-area"><div class="short_description form-group"> <h4>Size:</h4> </div><div class="size-area-warp"> <div class="cc-selector">';
            $s_html .= '<ul id="color_ul">';
            foreach ($size as $siz) {
                if ($key['size'] == $siz->id) {
                    $active = 'active';
                } else {
                    $active = '';
                }
                $s_html .= '<li class="item-size-front ' . $active . '" data-color="' . $siz->id . '">';
                $s_html .= '<a href="javascript:void(0)" data-spm-anchor-id="2114.10010108.1000016.1">';
                $s_html .= '<span class="size-box-front">' . $siz->size . '</span>';
                $s_html .= '</li>';
            }
            $s_html .= '</ul></div></div></div>';
        }

        $data = [
            'color' => ($p_info->multiple_pricing == 'on') ? $c_html : null,
            'size' => ($p_info->multiple_pricing == 'on') ? $s_html : null,
            'price' => $p_html
        ];
        // dd($data);

        return $data;
    }
}

if (!function_exists('get_product_price')) {
    function get_product_price($option)
    {
        $default = [
            'main_pid' => null,
            'color' => null,
            'size' => null,
            'type' => null,
        ];

        $key = array_merge($default, $option);
        // dd($key);

        $p_info = Product::where(['id' => $key['main_pid']])->first();
        $has_color_size = true;
        if ($p_info->multiple_pricing == 'on') {
            $get_price = Pcombinationdata::where(['main_pid' => $key['main_pid']]);

            if ($key['color'] != null) {
                $color_info = Pcombinationdata::where(['id' => $key['color']])->first();
                if ($color_info->color_codes) {
                    $get_price = $get_price->where(['color_codes' => $color_info->color_codes]);
                }
            }

            if ($key['size'] != null) {
                $size_info = Pcombinationdata::where(['id' => $key['size']])->first();

                if (isset($color_info->color_codes)) {
                    $get_price = $get_price->where(['size' => $size_info->size]);
                }
            }
            $get_price = $get_price->get();

            if ($get_price->count() > 0) {
                $get_price = $get_price->first();

                $d_color = ($get_price->color_codes) ? 'Yes' : 'No';
                $d_size = ($get_price->size) ? 'Yes' : 'No';
                $k_color = ($key['color']) ? 'Yes' : 'No';
                $k_size = ($key['size']) ? 'Yes' : 'No';

                //   dump($get_price);

                // dump($k_color);
                // dump($d_color);
                // dump ($k_size );
                // dd($d_size);


                if ($k_color == $d_color && $k_size == $d_size) {
                    $has_color_size = true;
                } else {
                    $has_color_size = false;
                }

                $item_code = $get_price->item_code;
            } else {
                $get_price = null;
                $item_code = null;
            }
        }

        if (auth()->guard('api')->check() && auth()->guard('api')->user()->isDP()) {
            if ($p_info->multiple_pricing == 'on') {
                // dd($get_price);
                if ($get_price) {
                    $dp_price = $get_price->dp_price;
                } else {
                    $dp_price = $p_info->dp_price;
                    //  dump($dp_price);
                }
            } else {
                $dp_price = $p_info->dp_price;
            }
        } else {
            $dp_price = 0;
        }

        $p_html = '';
        $tksign = '&#2547; ';

        $today = Carbon::today()->format('m/d/Y');
        if ($p_info->multiple_pricing == 'on' && $get_price != null) {
            $r_price = $get_price->regular_price;
            if ($p_info->offer_start_date != null && $p_info->offer_start_date <= $today && ($p_info->offer_end_date >= $today || $p_info->offer_end_date == null)) {
                $s_price = $get_price->selling_price;
                if ($get_price->discount_tag) {
                    $save_per = $get_price->discount_tag;
                } else {
                    $save_per = ((($r_price - $s_price) * 100) / $r_price);
                }
            } else {
                $s_price = $get_price->regular_price;
                $save_per = null;
            }
            $multi_id = $get_price->id;
        } else {
            $r_price = $p_info->local_selling_price;
            if ($p_info->local_discount >= 1) {
                if ($p_info->offer_start_date != null && $p_info->offer_start_date <= $today && ($p_info->offer_end_date >= $today || $p_info->offer_end_date == null)) {
                    $s_price = $p_info->local_selling_price - $p_info->local_discount;
                    if ($p_info->discount_tag) {
                        $save_per = $p_info->discount_tag;
                    } else {
                        $save_per = ((($r_price - $s_price) * 100) / $r_price);
                    }
                } else {
                    $s_price = $p_info->local_selling_price;
                    $save_per = null;
                }
            } else {
                $s_price = $p_info->local_selling_price;
                $save_per = null;
            }
            $multi_id = null;
        }

        $is_flash = is_flash_itme($key['main_pid']);

        if ($is_flash) {
            $save = number_format($is_flash->fi_discount);
            $save_per = number_format($is_flash->fi_show_tag);
            if ($p_info->multiple_pricing == 'on') {
                $s_price = number_format($get_price->selling_price - $is_flash->fi_discount);
            } else {
                $s_price = $p_info->local_selling_price - $is_flash->fi_discount;
            }
        }

        if (auth()->guard('api')->check() && auth()->guard('api')->user()->isDP()) {
            if ($dp_price > 0) {
                $s_price = $dp_price;
                $save = $r_price - $s_price;
                $save_per = round(($save * 100) / $r_price);
            }
        }

        $data = [
            'productid' => $key['main_pid'],
            'productcode' => $p_info->product_code,
            'is_dp' => ($dp_price > 0) ? 'Yes' : 'No',
            'flash_discount' => $is_flash->fi_discount??null,
            'item_code' => (isset($item_code)) ? $item_code : null,

            's_price' => $s_price,
            'r_price' => $r_price,
            'd_price' => $dp_price,
            'save' => round($save_per),
            'multi_id' => $multi_id,
            'main_pro' => $key['main_pid'],
            'has_cs' => $has_color_size
        ];

        //dump($data);
        return $data;
    }
}

if (!function_exists('is_flash_itme')) {
    function is_flash_itme($id)
    {
        $data = FlashShedule::leftJoin('flash_items AS fi', function ($join) {
            $join->on('fi.fi_shedule_id', '=', 'flash_schedules.id');
        })->where(['flash_schedules.fs_is_active' => 1, 'fi.fi_product_id' => $id])
            ->whereRaw('NOW() BETWEEN fs_start_date AND fs_end_date ')
            ->orderBy('fs_start_date', 'ASC')->get();

        if ($data->count() > 0) {
            return $data->first();
        } else {
            return false;
        }
    }
}

if (!function_exists('get_buy_discount')) {
    function get_buy_discount($price)
    {
        $setting_dis = PaymentSetting::first();

        $second_range = $setting_dis->second_range;
        $first_range = $setting_dis->first_range;
        if ($price >= $second_range) {
            $get_discount = $setting_dis->second_range_discount;
        } elseif ($price >= $first_range) {
            $get_discount = $setting_dis->first_range_discount;
        } else {
            $get_discount = null;
        }

        if ($get_discount != null) {
            $total = ($get_discount * $price) / 100;
            return $total;
        } else {
            return false;
        }
    }
}

if (!function_exists('get_home_cat')) {
    function get_home_cat()
    {
        $cats_data = HomeSetting::first()->home_category;
        if ($cats_data != null) {
            $cats = explode('|', $cats_data);

            $data = '';
            foreach ($cats as $cat) {
                $cat_info = Term::where(['id' => $cat])->first();
                //dump($cat_info);
                if ($cat_info) {
                    $data .= '<div class="content-box">';
                    $data .= '<div class="image-cat">';
                    $data .= '<a href="' . url('/c/' . $cat_info->seo_url) . '" title="' . $cat_info->name . '">';
                    if ($cat_info->home_image) {
                        $data .= '<img src="' . $cat_info->home_image . '" >';
                    } else {
                        $data .= '<img src="' . url('public/frontend/image/catalog/demo/category/cate1.jpg') . '">';
                    }

                    $data .= '</a>';
                    $data .= '</div>';

                    $data .= '<div class="cat-title"><a href="' . url('/c/' . $cat_info->seo_url) . '" title="' . $cat_info->name . '">' . $cat_info->name . '</a> </div>';
                    $data .= '</div>';
                }
            }

            return $data;
        } else {
            return false;
        }
    }
}

if (!function_exists('get_cart_price')) {
    function get_cart_price()
    {
        $cart = Session::get('cart')->items;
        $total = 0;

        foreach ($cart as $c) {
            $total += $c['purchaseprice'];
        }
        return $total;
    }
}

if (!function_exists('seller_review')) {
    function seller_review($id)
    {
        $data = App\Review::where(['vendor_id' => $id])->get();

        $count = $data->count();
        if ($count > 0) {
            $review = $count * 5;
            $get = $data->sum('rating');
            $persent = number_format(round(($get * 100) / $review));
            $output = $persent . '%';
        } else {
            $output = 'No Review';
        }

        return $output;
    }
}

if (!function_exists('seller_review_count')) {
    function seller_review_count($id)
    {
        $data = App\Review::where(['vendor_id' => $id])->get();

        $count = $data->count();

        return $count;
    }
}