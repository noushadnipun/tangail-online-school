<?php


if (!function_exists('gen_field')) {
    function gen_field($fieldtype, array $data = array())
    {
        switch ($fieldtype) :
            case 'text':
                echo get_text($fieldtype, $data);
                break;
            case 'url':
                echo get_text($fieldtype, $data);
                break;
            case 'number':
                echo get_text($fieldtype, $data);
                break;
            case 'textarea':
                echo get_textarea($fieldtype, $data);
                break;
            case 'select':
                //echo get_select($fieldtype, $data);
                echo get_checkbox($fieldtype, $data);
                break;
            case 'checkbox':
                echo get_checkbox($fieldtype, $data);
                break;
            case 'radio':
                echo get_radio($fieldtype, $data);
                break;
            default:
                break;
        endswitch;
    }
}

if (!function_exists('get_text')) {
    function get_text($fieldtype, array $data = array())
    {


        //dump($data);

        $stored = App\ProductAttributesData::Where(['main_pid' => $data['main_pid'], 'attribute_id' => $data['field_id']])->get();
        //dump($stored);

        $html = '<div class="acf-field acf-field-true-false ' . $fieldtype . '">';

        $html .= '<div class="acf-label">';
        $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
        $html .= '<p><small>' . $data['instructions'] . '</small></p>';
        $html .= '</div>';

        $html .= '<div class="acf-input">';
        $html .= '<div class="row load_att_checkbox'.$data['field_id'].'">';
        $html .= '<div class="col-md-6">';

        if($stored->count() > 0){
            $html .= '<ul class="att-checkbox-hasan">';
            foreach ($stored as $list){


                $html .= '<li><a href="javascript:void(0)" class="btn btn-danger btn-xs" 
                                onclick="att_check_del(this);" 
                                data-id = "'.$list->id.'"
                                data-load_class = "load_att_checkbox'.$data['field_id'].'"
                             >Del</a>  '. $list->value.'</li>';
            }

            $html .= '</ul>';
        }

        if (!empty($data['is_required'])) {
            if ($data['is_required'] == 1) {
                $required = 'required="required"';
            } else {
                $required = null;
            }
        } else {
            $required = null;
        }
        $html .= '</div>';
        $html .= '<div class="col-md-6">';
        $html .= '<div class="input-group text-att-group">';
        $html .= '<input type="' . $fieldtype . '" value="" id="' . $data['css_id'] . '" class="form-control ' . $data['css_class'] . '" name="attr_save[' . $data['field_id'] . '][value]" />';
        $html .= ' <a href="javascript:void(0)" 
                           onclick="att_text_add(this);" 
                         data-field_id = "'. $data['field_id'].'"
                         data-attgroup_id = "'.$data['attgroup_id'].'"
                         data-field_name = "'.$data['field_name'].'"
                         data-user_id = "'.$data['user_id'].'"
                         data-main_pid = "'.$data['main_pid'].'"
                         data-data_save_id = "'.(!empty($data['data_save_id']) ? $data['data_save_id'] : null).'"
                         data-value = ""
                         data-default_value = "'. $data['default_value'].'"
                         data-load_class = "load_att_checkbox'.$data['field_id'].'"
 
                    class="btn input-group-addon"><i class="fa fa-plus" aria-hidden="true"></i></a>';


        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';


        $html .= '<input type="hidden" value="' . $data['field_id'] . '" name="attr_save[' . $data['field_id'] . '][attribute_id]" />';
        $html .= '<input type="hidden" value="' . $data['attgroup_id'] . '" name="attr_save[' . $data['field_id'] . '][attgroup_id]" />';
        $html .= '<input type="hidden" value="' . $data['field_name'] . '" name="attr_save[' . $data['field_id'] . '][key]" />';
        $html .= '<input type="hidden" value="' . $data['user_id'] . '" name="attr_save[' . $data['field_id'] . '][user_id]" />';
        $html .= '<input type="hidden" value="' . $data['main_pid'] . '" name="attr_save[' . $data['field_id'] . '][main_pid]" />';
        $html .= '<input type="hidden" value="' . $data['default_value'] . '" name="attr_save[' . $data['field_id'] . '][default_value]" />';
        $html .= '<input type="hidden" value="' . (!empty($data['data_save_id']) ? $data['data_save_id'] : null) . '" name="attr_save[' . $data['field_id'] . '][data_save_id]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][created_at]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][updated_at]" />';

        $html .= '</div>';
        return $html;
    }
}

// if (!function_exists('get_text')) {
//     function get_text($fieldtype, array $data = array())
//     {

//         //dump($data);

//         $html = '<div class="acf-field acf-field-true-false ' . $fieldtype . '">';

//         $html .= '<div class="acf-label">';
//         $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
//         $html .= '<p><small>' . $data['instructions'] . '</small></p>';
//         $html .= '</div>';

//         $html .= '<div class="acf-input">';

//         if (!empty($data['is_required'])) {
//             if ($data['is_required'] == 1) {
//                 $required = 'required="required"';
//             } else {
//                 $required = null;
//             }
//         } else {
//             $required = null;
//         }

//         $html .= '<input type="' . $fieldtype . '" value="' . (is_array($data['value']) ? $data['value'][0] : $data['value']) . '" id="' . $data['css_id'] . '" class="' . $data['css_class'] . '" name="attr_save[' . $data['field_id'] . '][value]" ' . $required . '/>';
//         $html .= '</div>';


//         $html .= '<input type="hidden" value="' . $data['field_id'] . '" name="attr_save[' . $data['field_id'] . '][attribute_id]" />';
//         $html .= '<input type="hidden" value="' . $data['attgroup_id'] . '" name="attr_save[' . $data['field_id'] . '][attgroup_id]" />';
//         $html .= '<input type="hidden" value="' . $data['field_name'] . '" name="attr_save[' . $data['field_id'] . '][key]" />';
//         $html .= '<input type="hidden" value="' . $data['user_id'] . '" name="attr_save[' . $data['field_id'] . '][user_id]" />';
//         $html .= '<input type="hidden" value="' . $data['main_pid'] . '" name="attr_save[' . $data['field_id'] . '][main_pid]" />';
//         $html .= '<input type="hidden" value="' . $data['default_value'] . '" name="attr_save[' . $data['field_id'] . '][default_value]" />';
//         $html .= '<input type="hidden" value="' . (!empty($data['data_save_id']) ? $data['data_save_id'] : null) . '" name="attr_save[' . $data['field_id'] . '][data_save_id]" />';
//         $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][created_at]" />';
//         $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][updated_at]" />';

//         $html .= '</div>';
//         return $html;
//     }
// }

if (!function_exists('get_textarea')) {
    function get_textarea($fieldtype, array $data = array())
    {

        $html = '<div class="acf-field acf-field-true-false ' . $fieldtype . '">';

        $html .= '<div class="acf-label">';
        $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
        $html .= '<p><small>' . $data['instructions'] . '</small></p>';
        $html .= '</div>';

        $html .= '<div class="acf-input">';

        if (!empty($data['is_required'])) {
            if ($data['is_required'] == 1) {
                $required = 'required="required"';
            } else {
                $required = null;
            }
        } else {
            $required = null;
        }

        $html .= '<textarea id="' . $data['css_id'] . '" class="' . $data['css_class'] . '" name="attr_save[' . $data['field_id'] . '][value]" ' . $required . '>' . (is_array($data['value']) ? implode(',', $data['value']) : $data['value']) . '</textarea>';
        $html .= '</div>';


        $html .= '<input type="hidden" value="' . $data['field_id'] . '" name="attr_save[' . $data['field_id'] . '][attribute_id]" />';
        $html .= '<input type="hidden" value="' . $data['attgroup_id'] . '" name="attr_save[' . $data['field_id'] . '][attgroup_id]" />';
        $html .= '<input type="hidden" value="' . $data['field_name'] . '" name="attr_save[' . $data['field_id'] . '][key]" />';
        $html .= '<input type="hidden" value="' . $data['user_id'] . '" name="attr_save[' . $data['field_id'] . '][user_id]" />';
        $html .= '<input type="hidden" value="' . $data['main_pid'] . '" name="attr_save[' . $data['field_id'] . '][main_pid]" />';
        $html .= '<input type="hidden" value="' . $data['default_value'] . '" name="attr_save[' . $data['field_id'] . '][default_value]" />';
        $html .= '<input type="hidden" value="' . (!empty($data['data_save_id']) ? $data['data_save_id'] : null) . '" name="attr_save[' . $data['field_id'] . '][data_save_id]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][created_at]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][updated_at]" />';

        $html .= '</div>';
        return $html;
    }
}

if (!function_exists('get_select')) {
    function get_select($fieldtype, array $data = array())
    {
        $default_value = explode('|', $data['default_value']);

        $field_name = $data['field_name'] . '[' . $data['field_id'] . '][]';

        $html = '<div class="acf-field acf-field-true-false ' . $fieldtype . '">';
        $html .= '<div class="acf-label">';
        $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
        $html .= '<p><small>' . $data['instructions'] . '</small></p>';
        $html .= '</div>';


        $html .= '<div class="acf-input">';

        if (!empty($data['is_required'])) {
            if ($data['is_required'] == 1) {
                $required = 'required="required"';
            } else {
                $required = null;
            }
        } else {
            $required = null;
        }

        $html .= ' <select id="' . $data['css_id'] . '" class="' . $data['css_class'] . '" name="attr_save[' . $data['field_id'] . '][value]" ' . $required . '>';

        foreach ($default_value as $value) {
            $option = explode(':', $value);

            if (is_array($data['value'])) {
                if (in_array($option[0], $data['value'])) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }
            } else {
                if ($data['value'] == $option[0]) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }
            }


            $html .= '<option value="' . $option[0] . '" ' . $selected . '> ' . $option[1] . '</option>';
        }

        $html .= '</select>';
        $html .= '</div>';

        $html .= '<input type="hidden" value="' . $data['field_id'] . '" name="attr_save[' . $data['field_id'] . '][attribute_id]" />';
        $html .= '<input type="hidden" value="' . $data['attgroup_id'] . '" name="attr_save[' . $data['field_id'] . '][attgroup_id]" />';
        $html .= '<input type="hidden" value="' . $data['field_name'] . '" name="attr_save[' . $data['field_id'] . '][key]" />';
        $html .= '<input type="hidden" value="' . $data['user_id'] . '" name="attr_save[' . $data['field_id'] . '][user_id]" />';
        $html .= '<input type="hidden" value="' . $data['main_pid'] . '" name="attr_save[' . $data['field_id'] . '][main_pid]" />';
        $html .= '<input type="hidden" value="' . (!empty($data['data_save_id']) ? $data['data_save_id'] : null) . '" name="attr_save[' . $data['field_id'] . '][data_save_id]" />';
        $html .= '<input type="hidden" value="' . $data['default_value'] . '" name="attr_save[' . $data['field_id'] . '][default_value]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][created_at]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][updated_at]" />';

        $html .= '</div>';
        return $html;
    }
}


if (!function_exists('get_checkbox')) {
    function get_checkbox($fieldtype, array $data = array())
    {
        // Last update by Mohammad Hasan

        $default_value = explode('|', $data['default_value']);
        $field_name = $data['field_name'] . '[' . $data['field_id'] . '][]';

        $html = '<div class="acf-field acf-field-true-false text">';
        $html .= '<div class="acf-label">';
        $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
        $html .= '<p><small>' . $data['instructions'] . '</small></p>';
        $html .= '</div>';


        $html .= '<div class="acf-input">';
        $html .= '<div class="row load_att_checkbox'.$data['field_id'].'">';
        $html .= '<div class="col-md-6">';
        $stored = App\ProductAttributesData::Where(['main_pid' => $data['main_pid'],'attribute_id' => $data['field_id']])->get();


        if($stored->count() > 0){
            $html .= '<ul class="att-checkbox-hasan">';
            foreach ($stored as $list){


                $html .= '<li><a href="javascript:void(0)" class="btn btn-danger btn-xs" 
                                onclick="att_check_del(this);" 
                                data-id = "'.$list->id.'"
                                data-load_class = "load_att_checkbox'.$data['field_id'].'"
                             >Del</a>  '. $list->value.'</li>';
            }

            $html .= '</ul>';
        }

        $html .= '</div>';
        $html .= '<div class="col-md-6">';
        $html .= '<div class="box box-warning">';
        $html .= '<div class="box-header"  style=" max-height: 150px; overflow-y: scroll;">';
        $html .= '<h3 class="box-title"><label for="product_categories_getter" class="product_categories_getter">Choose Options</label> </h3>';


        if (!empty($data['is_required'])) {
            if ($data['is_required'] == 1) {
                $required = 'required="required"';
            } else {
                $required = null;
            }
        } else {
            $required = null;
        }

        $seperated_value = explode(',', $data['value']);
        foreach ($default_value as $value) {
            $option = explode(':', $value);

            if (is_array($seperated_value)) {
                //dump($option);
                if (in_array($option[0], $seperated_value)) {
                    $selected = 'checked="checked"';
                } else {
                    $selected = '';
                }
            } else {
                if ($data['value'] == $option[0]) {
                    $selected = 'checked="checked"';
                } else {
                    $selected = '';
                }
            }

            $html .= '<div class="checkbox">';
            $html .= '<label>';
            $html .= '<a href="javascript:void(0)" 
                         ondblclick="att_check_add(this);" 
                         data-field_id = "'. $data['field_id'].'"
                         data-attgroup_id = "'.$data['attgroup_id'].'"
                         data-field_name = "'.$data['field_name'].'"
                         data-user_id = "'.$data['user_id'].'"
                         data-main_pid = "'.$data['main_pid'].'"
                         data-data_save_id = "'.(!empty($data['data_save_id']) ? $data['data_save_id'] : null).'"
                         data-value = "'. $option[0].'"
                         data-default_value = "'. $data['default_value'].'"
                         data-load_class = "load_att_checkbox'.$data['field_id'].'"
                      
               
                         
                         >';
            $html .= !empty($option[1]) ? $option[1] : null;
            $html .= '</a>';
            $html .= '</label>';

            $html .= '</div>';

        }
        $html .= '<div class="box-footer">Use double click to add a product as add a related products</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        
        $html .= '</div>';



        return $html;
    }
}



// if (!function_exists('get_checkbox')) {
//     function get_checkbox($fieldtype, array $data = array())
//     {
//         $default_value = explode('|', $data['default_value']);
//         $field_name = $data['field_name'] . '[' . $data['field_id'] . '][]';

//         $html = '<div class="acf-field acf-field-true-false text">';
//         $html .= '<div class="acf-label">';
//         $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
//         $html .= '<p><small>' . $data['instructions'] . '</small></p>';
//         $html .= '</div>';

//         //dump($data['value']);
//         $html .= '<div class="acf-input">';


//         if (!empty($data['is_required'])) {
//             if ($data['is_required'] == 1) {
//                 $required = 'required="required"';
//             } else {
//                 $required = null;
//             }
//         } else {
//             $required = null;
//         }

//         //dump($default_value);
//         //dd($default_value);
//         $seperated_value = explode(',', $data['value']);
//         //dump($seperated_value);
//         foreach ($default_value as $value) {
//             $option = explode(':', $value);

//             //dump($option[0]);

//             if (is_array($seperated_value)) {
//                 //dump($option);
//                 if (in_array($option[0], $seperated_value)) {
//                     $selected = 'checked="checked"';
//                 } else {
//                     $selected = '';
//                 }
//             } else {
//                 if ($data['value'] == $option[0]) {
//                     $selected = 'checked="checked"';
//                 } else {
//                     $selected = '';
//                 }
//             }

//             $html .= '<div class="checkbox">';
//             $html .= '<label>';
//             $html .= '<input type="checkbox" ' . $selected . ' id="' . $data['css_id'] . '" value="' . $option[0] . '" name="attr_save[' . $data['field_id'] . '][value][]" ' . $required . '>';
//             $html .= !empty($option[1]) ? $option[1] : null;
//             $html .= '</label>';
//             $html .= '</div>';

//         }
//         $html .= '</div>';

//         $html .= '<input type="hidden" value="' . $data['field_id'] . '" name="attr_save[' . $data['field_id'] . '][attribute_id]" />';
//         $html .= '<input type="hidden" value="' . $data['attgroup_id'] . '" name="attr_save[' . $data['field_id'] . '][attgroup_id]" />';
//         $html .= '<input type="hidden" value="' . $data['field_name'] . '" name="attr_save[' . $data['field_id'] . '][key]" />';
//         $html .= '<input type="hidden" value="' . $data['user_id'] . '" name="attr_save[' . $data['field_id'] . '][user_id]" />';
//         $html .= '<input type="hidden" value="' . $data['main_pid'] . '" name="attr_save[' . $data['field_id'] . '][main_pid]" />';
//         $html .= '<input type="hidden" value="' . (!empty($data['data_save_id']) ? $data['data_save_id'] : null) . '" name="attr_save[' . $data['field_id'] . '][data_save_id]" />';
//         $html .= '<input type="hidden" value="' . $data['default_value'] . '" name="attr_save[' . $data['field_id'] . '][default_value]" />';
//         $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][created_at]" />';
//         $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][updated_at]" />';

//         $html .= '</div>';


//         return $html;
//     }
// }


if (!function_exists('get_radio')) {
    function get_radio($fieldtype, array $data = array())
    {
        $default_value = explode('|', $data['default_value']);
        $field_name = $data['field_name'] . '[' . $data['field_id'] . '][]';

        $html = '<div class="acf-field acf-field-true-false text">';
        $html .= '<div class="acf-label">';
        $html .= '<label for="' . $data['field_name'] . '" class="' . $data['field_name'] . '">' . $data['field_label'] . '</label>';
        $html .= '<p><small>' . $data['instructions'] . '</small></p>';
        $html .= '</div>';

        $html .= '<div class="acf-input">';

        if (!empty($data['is_required'])) {
            if ($data['is_required'] == 1) {
                $required = 'required="required"';
            } else {
                $required = null;
            }
        } else {
            $required = null;
        }

        foreach ($default_value as $value) {
            //dd($value);
            $option = explode(':', $value);

            if (is_array($data['value'])) {
                if (in_array($option[0], $data['value'])) {
                    $selected = 'checked="checked"';
                } else {
                    $selected = '';
                }
            } else {
                if ($data['value'] == $option[0]) {
                    $selected = 'checked="checked"';
                } else {
                    $selected = '';
                }
            }

            $html .= '<div class="radio">';
            $html .= '<label>';
            $html .= '<input type="radio" ' . $selected . ' id="' . $data['css_id'] . '" value="' . $option[0] . '" name="attr_save[' . $data['field_id'] . '][value]" ' . $required . '>';
            $html .= $option[1];
            $html .= '</label>';
            $html .= '</div>';

        }
        $html .= '</div>';


        $html .= '<input type="hidden" value="' . $data['field_id'] . '" name="attr_save[' . $data['field_id'] . '][attribute_id]" />';
        $html .= '<input type="hidden" value="' . $data['attgroup_id'] . '" name="attr_save[' . $data['field_id'] . '][attgroup_id]" />';
        $html .= '<input type="hidden" value="' . $data['field_name'] . '" name="attr_save[' . $data['field_id'] . '][key]" />';
        $html .= '<input type="hidden" value="' . $data['user_id'] . '" name="attr_save[' . $data['field_id'] . '][user_id]" />';
        $html .= '<input type="hidden" value="' . $data['main_pid'] . '" name="attr_save[' . $data['field_id'] . '][main_pid]" />';
        $html .= '<input type="hidden" value="' . (!empty($data['data_save_id']) ? $data['data_save_id'] : null) . '" name="attr_save[' . $data['field_id'] . '][data_save_id]" />';
        $html .= '<input type="hidden" value="' . $data['default_value'] . '" name="attr_save[' . $data['field_id'] . '][default_value]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][created_at]" />';
        $html .= '<input type="hidden" value="' . date('Y-m-d H:i:s') . '" name="attr_save[' . $data['field_id'] . '][updated_at]" />';


        $html .= '</div>';
        return $html;
    }
}