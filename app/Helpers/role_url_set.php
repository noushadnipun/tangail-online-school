<?php

if (!function_exists('get_vendor_url_set')) {
    function get_vendor_url_set($userid)
    {
        $vendor_set = [
            ['d9a3e27592d7567b91ad890fc7b3c9b2', 'admin/modify_password_view/{id}', $userid],
            ['462b0c66d483ec5251fe733f0118e41b', 'admin/modify_password/{id}', $userid],
            ['a1052e22191baf7b89e9dbb3374e6ab1', 'admin/vendor_url_existing', $userid],
            ['69515b93b61ff8471540323025cfc2b1', 'admin/modify_role_view/{id}', $userid],
            ['64164b42c7c41b124bb649057487e397', 'admin/modify_role', $userid],
            ['ccd3b60a27756c461aec5ab0665cd4dc', 'products_express_delivery', $userid],
            ['700b117ece72198bf9a2f59875fb43f3', 'products_enable_comment', $userid],
            ['0fec2b6a5cf3bc73c1c99e58782a2e09', 'vendor.dashboard.index', $userid],
            ['7a4a5b3a189122d1983689c190ed088c', 'vendor.setting.index', $userid],
            ['f80c9ed288235cd066ab3beed3a53443', 'vendor.setting.vendor', $userid],
            ['6ff1173aabc20ef4e1b4d6f495d571d8', 'vendor.site.content', $userid],
            ['93809c33af7f3f829288004208841e7d', 'vendor.site.saveSlider', $userid],
            ['6fba9ace2123e90fec4169b94d613a82', 'vendor.site.deleteSlider', $userid],
            ['cbab40a986f5a9371e6f0f7169c3e765', 'vendor.media.index', $userid],
            ['4733362629c9378ced1375726fe44e0e', 'vendor.site.saveChoicesProducts', $userid],
            ['9b3b5a115de7b5be48b9385617258740', 'vendor.order.index', $userid],
            ['5ecce48347a056b17dae28dcb9cbabef', 'vendor.product.index', $userid],
            ['c408d9288c1ff7968e9b77cd1c63b084', 'vendor.product.create', $userid],
            ['97f0e63411a6becc868ac90c0ce27cb7', 'vendor.product.edit', $userid],
            ['f1d3a617efc231aa068df0816bc73bb1', 'vendor.product.store', $userid],
            ['77faa3c885c8b0485716f745d20ecef6', 'vendor.product.update.basic', $userid],
            ['a0a6996be88537b69c59f2fe038321d5', 'admin.medias.index', $userid],
            ['2dddd499efae5db416c5c5267ba7c6c0', 'admin.medias.create', $userid],
            ['7280858d998e585002c24b1a5155a77c', 'admin.medias.store', $userid],
            ['cba89a90e046ec3dc6994391cc14a99a', 'admin.medias.show', $userid],
            ['ad7e82b913b843290a68d5ad96f7271f', 'admin.medias.edit', $userid],
            ['693e655ad5a892a78094eff40b08045c', 'admin.medias.update', $userid],
            ['b0b4a18346418ca9aed58c6c7347e4ad', 'admin.medias.destroy', $userid],
            ['d92fa6da3bb9f510a2281519766ab814', 'admin.upload.index', $userid],
            ['12998503f132e7ef58562f6e203db986', 'admin.upload.create', $userid],
            ['3f33abc4a528034341e82f6251491f53', 'admin.upload.store', $userid],
            ['97cabf1c445d68541eea86dd3dff9a53', 'admin.upload.show', $userid],
            ['636f4d958bafb36fe0bd2c8c737871cc', 'admin.upload.edit', $userid],
            ['5acb8f97fb60dcfa44c7ba68b68ac005', 'admin.upload.update', $userid],
            ['23b4831ebb47cd2d807e6b369235cea9', 'admin.upload.destroy', $userid],
            ['cf53d19a8da9cb986a5cac73d1edde2f', 'admin.flash_management.flash_schedule_status', $userid],
            ['d9fd38ffb0fe0b9c527203bf66634940', 'admin.flash_management.add_schedule_products', $userid],
            ['c6258cd921989ebbb0fb2dfc53ce4a83', 'admin.flash_management.flash_item_save', $userid],
            ['d18c2d317ca4850751c97c6cd78c886f', 'admin.flash_management.store_flash_items', $userid],
            ['7a4ccf7e175472c739968241abb2efd6', 'admin.flash_management.delete_flash_item', $userid],
            ['3c2071bab3abe3d20d5b2e30888c3f2f', 'admin.flash_management.index', $userid],
            ['50ffb6a34a50e37ce5bca28078825199', 'admin.flash_management.store', $userid],
            ['d1e96fa4b2440fdf5797057e3a0e3d8d', 'admin.flash_management.show', $userid],
            ['8f844d07b8137b509cd5323fe1986736', 'admin.flash_management.edit', $userid],
            ['a88fbe761cb9ad3b3c138de7c9664eec', 'admin.flash_management.update', $userid]
        ];

        return $vendor_set;
    }
}
if (!function_exists('get_showroom_url_set')) {
    function get_showroom_url_set()
    {
        $showroom_set = [
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
            '', '',
        ];
    }
}


if (!function_exists('get_member_url_set')) {
    function get_member_url_set($id)
    {
        $member_set =
            [
                'useraccesspermission_' . $id . '_acbc05928fd65c2b461f8887b7c945e9',
                'useraccesspermission_' . $id . '_52942d983fa3c848d8190aafd4d4bb94',
                'useraccesspermission_' . $id . '_8813a2bf3a3de38a030c1f89a16c2927',
                'useraccesspermission_' . $id . '_d9fd38ffb0fe0b9c527203bf66634940',
                'useraccesspermission_' . $id . '_9f3b4d7f8533cb89d17058151fe923af',
                'useraccesspermission_' . $id . '_7df605dd6fed54c7f3e630fd55a1b4d6',
                'useraccesspermission_' . $id . '_e8cf90617299b1bbc66aca0953af35db',
                'useraccesspermission_' . $id . '_130140fc883609f0a76cac4418fc6ca2',
                'useraccesspermission_' . $id . '_e8cf90617299b1bbc66aca0953af35db',
                'useraccesspermission_' . $id . '_859d8d9ae4da587d17ff833f6092d62b',
                'useraccesspermission_' . $id . '_ccd3b60a27756c461aec5ab0665cd4dc',
                'useraccesspermission_' . $id . '_ccd3b60a27756c461aec5ab0665cd4dc',
                'useraccesspermission_' . $id . '_700b117ece72198bf9a2f59875fb43f3',
                'useraccesspermission_' . $id . '_700b117ece72198bf9a2f59875fb43f3',
                'useraccesspermission_' . $id . '_1650182ed08fd61ac533b4e71eceee88',
                'useraccesspermission_' . $id . '_1650182ed08fd61ac533b4e71eceee88',
                'useraccesspermission_' . $id . '_55078e63f33ba40ad0c280cae496cf8a',
                'useraccesspermission_' . $id . '_55078e63f33ba40ad0c280cae496cf8a',
                'useraccesspermission_' . $id . '_813e7e24fc6ee39bd1d263ed340be2d1',
                'useraccesspermission_' . $id . '_813e7e24fc6ee39bd1d263ed340be2d1',
                'useraccesspermission_' . $id . '_c5b42abe3337c65c239a751e53534dc0',
                'useraccesspermission_' . $id . '_c5b42abe3337c65c239a751e53534dc0',
                'useraccesspermission_' . $id . '_9675c2450b8e43aeeb2662566654bde1',
                'useraccesspermission_' . $id . '_9675c2450b8e43aeeb2662566654bde1',
                'useraccesspermission_' . $id . '_46b098c303dfbd98a0b8e79bfce92040',
                'useraccesspermission_' . $id . '_46b098c303dfbd98a0b8e79bfce92040',
                'useraccesspermission_' . $id . '_1912f7f6477d1410d8dd044a3f6450ae',
                'useraccesspermission_' . $id . '_6a7db6dcb8858bdd8eb2666b8d02893c',
                'useraccesspermission_' . $id . '_4c6b2d9211b839abc15877f3d81c6829',
                'useraccesspermission_' . $id . '_3297120fd0995d3ee3e4f47e17f68eb8',
                'useraccesspermission_' . $id . '_e57d69cfb3ea4324e53a77f0a1922f83',
                'useraccesspermission_' . $id . '_95befd766b2e5225b2e117882041a3f0',
                'useraccesspermission_' . $id . '_45d1d5e5b83317f532f4637803467ff1',
                'useraccesspermission_' . $id . '_45d1d5e5b83317f532f4637803467ff1',
                'useraccesspermission_' . $id . '_060ca65333c1f645aa99dd51c22fe3b4',
                'useraccesspermission_' . $id . '_829a57ef22b8f06eab111840a95d5fc2',
                'useraccesspermission_' . $id . '_acfc1f6b581a3743d510b1d5dd9f23af',
                'useraccesspermission_' . $id . '_17194ed4c3be35df75c6c149ce6e1d4a',
                'useraccesspermission_' . $id . '_2de1df9ce43a5fcfb28913e406c685ef',
                'useraccesspermission_' . $id . '_0b5600b5601b8c5d1d3732b0d02229fa',
                'useraccesspermission_' . $id . '_0bae8f7b5824175dad38b87f781a58cf',
                'useraccesspermission_' . $id . '_2485adbcff41e8f5aa5a3d0710f9b80b',
                'useraccesspermission_' . $id . '_30167727bdfaa68cadf02926412ac310',
                'useraccesspermission_' . $id . '_920b63af2b01f3f566263f1299e106af',
                'useraccesspermission_' . $id . '_da341a9fea8386bd3a4073217e807d0e',
                'useraccesspermission_' . $id . '_701fc6de8dc2d084fd01991165f16318',
                'useraccesspermission_' . $id . '_7ab90f0ec65d5485ca52fb0a0cce0c51',
                'useraccesspermission_' . $id . '_8309f609882e0c29d351ba149fc83adb',
                'useraccesspermission_' . $id . '_a2010ff18392b973a58d4cb544bae767',
                'useraccesspermission_' . $id . '_76cb19de7f90dfa83fe832b93b62b4f2',
                'useraccesspermission_' . $id . '_dfbc661b121e81a7b748a133c3df96b8',
                'useraccesspermission_' . $id . '_af9cd61ede76262b5ed4761f460f0217',
                'useraccesspermission_' . $id . '_314c59890e483c1bb9956e3e0a59e9c4',
                'useraccesspermission_' . $id . '_4733362629c9378ced1375726fe44e0e',
            ];

    }
}