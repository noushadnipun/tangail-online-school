<?php

function owndebugger($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function storageLink($path)
{
    return \Illuminate\Support\Facades\Config::get('app.url').'/'.$path;
}

if (!function_exists('clean')) {
    /**
     * @param $string
     * @return string|string[]|null
     */
    function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}

if (!function_exists('category_sidebar_menu')) {
    /**
     * @param $category See $cats variable by printing on banks.blade.php
     * @param int $parent parent id
     * @param string $seperator Space
     * @param $cid Current option id
     * @return string Full html options
     */
    function category_sidebar_menu($category, $parent = 0, $seperator = ' ', $cid = null)
    {
        $html = null;
        if ($parent === null) {
            $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent, true);
        } else {
            $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);
        }

        //dd($current_lvl_keys);
        if (!empty($current_lvl_keys)) {
            $html .= '<ul>';
            foreach ($current_lvl_keys as $key) {
                $html .= "<li><a href='#'>" . $category[$key]['name'] . '</a></li>';
                $html .= category_sidebar_menu($category, $category[$key]['id'], $seperator . '', $cid);
            }
            $html .= '</ul>';
        }
        //dump($html);
        return $html;
    }
}

if (!function_exists('category_sidebar_menu_on_category_page')) {
    /**
     * @param $category See $cats variable by printing on banks.blade.php
     * @param int $parent parent id
     * @param string $seperator Space
     * @param $cid Current option id
     * @return string Full html options
     */
    function category_sidebar_menu_on_category_page($category, $parent = 0, $seperator = ' ', $cid = null)
    {
        $html = null;
        $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);

        if (!empty($current_lvl_keys)) {
            if ($parent === 100) {
                $html .= '<ul class="nav nav-pills nav-stacked nav-tree" id="myTree" data-toggle="nav-tree" data-nav-tree-expanded="fa fa-minus" data-nav-tree-collapsed="fa fa-plus">';
            } else {
                $html .= '<ul class="nav nav-pills nav-stacked nav-tree sub-side-manu">';
            }
            foreach ($current_lvl_keys as $key) {
                $totalproduct = App\Product::whereRaw('parent_id IS NULL')->whereRaw('FIND_IN_SET(' . $category[$key]['id'] . ', categories)')->get();
                $total = $totalproduct->count();
                $activeness = !empty(Request::segment(2) == $category[$key]['seo_url']) ? 'active' : '';

                $html .= "<li class=' $activeness '>";

                $html .= "<span class='badge pull-right'>" . $total . '</span>';
                $html .= "<a href='" . category_seo_url_by_id($category[$key]['id']) . "'>" . $category[$key]['name'] . '</a>';
                $html .= category_sidebar_menu_on_category_page($category, $category[$key]['id'], $seperator, $cid = null);
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }
}

if (!function_exists('category_sidebar_menu_on_home_page')) {
    /**
     * @param $category See $cats variable by printing on banks.blade.php
     * @param int $parent parent id
     * @param string $seperator Space
     * @param $cid Current option id
     * @return string Full html options
     */
    function category_sidebar_menu_on_home_page($category, $parent = 0, $seperator = ' ', $cid = null)
    {
        $html = null;
        $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);

        if (!empty($current_lvl_keys)) {
            if ($parent === 100) {
                $html .= '<ul class="" id="">';
            } else {
                $html .= '<ul class="">';
            }
            foreach ($current_lvl_keys as $key) {
                $totalproduct = App\Product::whereRaw('parent_id IS NULL')->whereRaw('FIND_IN_SET(' . $category[$key]['id'] . ', categories)')->get();
                $total = $totalproduct->count();
                $activeness = !empty(Request::segment(2) == $category[$key]['seo_url']) ? 'active' : '';

                $html .= "<li class=' $activeness '>";
                $html .= "<a href='" . category_seo_url_by_id($category[$key]['id']) . "'>" . $category[$key]['name'];
                if ($category[$key]['parent'] == 100) {
                    $html .= "<span class='pull-right circle'><i class='fa fa-angle-right'></i></span>";
                }
                $html .= '</a>';
                $html .= category_sidebar_menu_on_home_page($category, $category[$key]['id'], $seperator, $cid = null);
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }
}

if (!function_exists('category_sticky_menu_on_home_page')) {
    /**
     * @param $category See $cats variable by printing on banks.blade.php
     * @param int $parent parent id
     * @param string $seperator Space
     * @param $cid Current option id
     * @return string Full html options
     */
    function category_sticky_menu_on_home_page($category, $parent = 0, $seperator = ' ', $cid = null)
    {
        $html = null;
        $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);

        if (!empty($current_lvl_keys)) {
            if ($parent === 100) {
                $html .= '<ul class="" id="">';
            } else {
                $html .= '<ul class="">';
            }
            foreach ($current_lvl_keys as $key) {
                $totalproduct = App\Product::whereRaw('parent_id IS NULL')->whereRaw('FIND_IN_SET(' . $category[$key]['id'] . ', categories)')->get();
                $total = $totalproduct->count();
                $activeness = !empty(Request::segment(2) == $category[$key]['seo_url']) ? 'active' : '';

                $html .= "<li class=' $activeness '>";
                $html .= "<a href='" . category_seo_url_by_id($category[$key]['id']) . "'>" . $category[$key]['name'];
                if ($category[$key]['parent'] == 100) {
                    $html .= "<span class='pull-right circle'><i class='fa fa-angle-down'></i></span>";
                }
                $html .= '</a>';
                $html .= category_sticky_menu_on_home_page($category, $category[$key]['id'], $seperator, $cid = null);
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }
}

if (!function_exists('select_option_html')) {
    /**
     * @param $category See $cats variable by printing on banks.blade.php
     * @param int $parent parent id
     * @param string $seperator Space
     * @param $cid Current option id
     * @return string Full html options
     */
    function select_option_html($category, $parent = 0, $seperator = ' ', $cid = null, $li = false, $others = false)
    {
        $html = '';
        if ($parent === null) {
            $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent, true);
        } else {
            $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);
        }

        //dd($current_lvl_keys);
        if (!empty($current_lvl_keys)) {
            if ($li == true) {
                $html .= '<ul class="on_terms">';
            }
            foreach ($current_lvl_keys as $key) {
                $is_selected = ($cid == $category[$key]['id']) ? 'selected="selected"' : '';
                if ($li == true) {
                    $editbtn = '<a type="button" href="' . url('/edit_term/' . $category[$key]['id']) . '" class="btn btn-box-tool"><i class="fa fa-pencil-square-o"></i></a>';
                    if ($category[$key]['id'] != 1 && $category[$key]['id'] != 2) {
                        $delbtn = '<span class="pull-right"><a class="btn btn-xs btn-danger delete_form"
                       href="' . url('delete_term/' . $category[$key]['id']) . '"
                       onclick="return confirm(\'You are attempting to remove this category forever. Are you Sure?\')"
                       title="Delete Now">
                        <i class="fa fa-times"></i>
                    </a></span>';
                    } else {
                        $delbtn = '';
                    }
//                    $delbtn = '<span class="pull-right">
//                        ' . Form::open(['method' => 'delete', 'route' => ['delete_term', $category[$key]['id']], 'class' => 'delete_form']) . '
//                        ' . Form::button('<i class="fa fa-times"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger')) . '
//                        ' . Form::close() . '</span>';
                    $html .= '<li>' . $delbtn . $seperator . '(' . $category[$key]['id'] . ') ' . $category[$key]['name'] . $editbtn . '</li>';
                    $html .= select_option_html($category, $category[$key]['id'], $seperator . '', $cid, true);
                } else {
                    $html .= '<option ' . $is_selected . " value='" . $category[$key]['id'] . "'>" . $seperator . $category[$key]['name'] . '</option>';
                    $html .= select_option_html($category, $category[$key]['id'], $seperator . '-&nbsp;', $cid, false);
                }
            }
            if ($li == true) {
                $html .= '</ul>';
            }
        }

        return $html;
    }
}

if (!function_exists('select_option_html_on_front')) {
    /**
     * @param $category See $cats variable by printing on banks.blade.php
     * @param int $parent parent id
     * @param string $seperator Space
     * @param $cid Current option id
     * @return string Full html options
     */
    function select_option_html_on_front($category, $parent = 0, $seperator = ' ', $cid = null, $li = false, $others = false)
    {
        $html = '';
        if ($parent === null) {
            $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent, true);
        } else {
            $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);
        }

        //dd($current_lvl_keys);
        if (!empty($current_lvl_keys)) {
            if ($li == true) {
                $html .= '<ul class="on_terms">';
            }
            foreach ($current_lvl_keys as $key) {
                $is_selected = ($cid == $category[$key]['id']) ? 'selected="selected"' : '';
                if ($li == true) {
                    $editbtn = '<a type="button" href="' . url('/edit_term/' . $category[$key]['id']) . '" class="btn btn-box-tool"><i class="fa fa-pencil-square-o"></i></a>';
                    if ($category[$key]['id'] != 1 && $category[$key]['id'] != 2) {
                        $delbtn = '<span class="pull-right"><a class="btn btn-xs btn-danger delete_form"
                       href="' . url('delete_term/' . $category[$key]['id']) . '"
                       onclick="return confirm(\'You are attempting to remove this category forever. Are you Sure?\')"
                       title="Delete Now">
                        <i class="fa fa-times"></i>
                    </a></span>';
                    } else {
                        $delbtn = '';
                    }
//                    $delbtn = '<span class="pull-right">
//                        ' . Form::open(['method' => 'delete', 'route' => ['delete_term', $category[$key]['id']], 'class' => 'delete_form']) . '
//                        ' . Form::button('<i class="fa fa-times"></i>', array('type' => 'submit', 'class' => 'btn btn-xs btn-danger')) . '
//                        ' . Form::close() . '</span>';
                    $html .= '<li>' . $delbtn . $seperator . '(' . $category[$key]['id'] . ') ' . $category[$key]['name'] . $editbtn . '</li>';
                    $html .= select_option_html($category, $category[$key]['id'], $seperator . '', $cid, true);
                } else {
                    $html .= '<option ' . $is_selected . " value='" . $category[$key]['id'] . "'>" . $seperator . $category[$key]['name'] . '</option>';
                    $html .= select_option_html($category, $category[$key]['id'], $seperator . '-&nbsp;', $cid, false);
                }
            }
            if ($li == true) {
                $html .= '</ul>';
            }
        }

        return $html;
    }
}

if (!function_exists('category_in_product_links_page')) {
    function category_in_product_links_page($parent_id, $product_id, $user_id)
    {
        $sub_terms = App\Term::where('parent', $parent_id)->orderBy('name', 'asc')->get();
        dump($sub_terms);
        foreach ($sub_terms as $sub_term):
            $html = '<option id="dblclick_cat" value="' . $sub_term->id . '" data-mainpid="' . $product_id . '" data-userid="' . $user_id . '" data-title="' . $sub_term->name . '" data-attgroup="' . $sub_term->connected_with . '">&nbsp;&nbsp;&nbsp;' . $sub_term->name . '</option>';
        endforeach;

        return $html;
    }
}

if (!function_exists('category_h_checkbox_html')) {
    /*
     * @param mixed $categories
     * @param int   $parent_id = 0
     * @param string  $name = 'name' Name of checkbox <example><input type="checkbox" value="name[]"> </examble>
     */

    function category_h_checkbox_html($category, $parent = 2, $name = 'name', $selected_category_ids = [])
    {
        $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);
        //dump($current_lvl_keys);
        if (!empty($current_lvl_keys)) :
            ?>

            <ul id="<?php echo $name ?>-id-<?php echo $parent; ?>" style="list-style: none;">
                <?php foreach ($current_lvl_keys as $key) :
                    ?>
                    <li>
                        <input type="checkbox" id="<?php echo $category[$key]['id']; ?>" name="<?php echo $name; ?>[]"
                               value="<?php echo $category[$key]['id']; ?>" <?php echo(in_array($category[$key]['id'], $selected_category_ids) ? ' checked="checked" ' : ''); ?>>
                        <label for="<?php echo $category[$key]['id']; ?>"><?php echo $category[$key]['name']; ?></label>
                    </li>
                    <?php echo category_h_checkbox_html($category, $category[$key]['id'], $name, $selected_category_ids); ?>
                <?php endforeach; ?>
            </ul>
        <?php
        endif;
    }
}

if (!function_exists('category_open_checkbox_html')) {
    /*
     * @param mixed $categories
     * @param int   $parent_id = 0
     * @param string  $name = 'name' Name of checkbox <example><input type="checkbox" value="name[]"> </examble>
     */

    function category_open_checkbox_html($category, $parent = 100, $name = 'name', $selected_category_ids = [])
    {
        $current_lvl_keys = array_keys(array_column($category, 'parent'), $parent);

        //dd($current_lvl_keys);
        if (!empty($current_lvl_keys)) :
            ?>

            <ul id="<?php echo $name ?>-id-<?php echo $parent; ?>" style="list-style: none;">
                <?php foreach ($current_lvl_keys as $key) :
                    ?>
                    <li>
                        <a href="<?php echo url('c/' . $category[$key]['seo_url']); ?>">
                            <?php echo $category[$key]['name']; ?>
                        </a>
                    </li>
                    <?php echo category_open_checkbox_html($category, $category[$key]['id'], $name, $selected_category_ids); ?>
                <?php endforeach; ?>
            </ul>
        <?php
        endif;
    }
}

if (!function_exists('limit_text')) {
    /**
     * @param $text Content Parameter
     * @param $limit Limit in Words
     * @return string NewContent Returns
     */
    function limit_text($text, $limit)
    {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return strip_tags($text);
    }
}

if (!function_exists('limit_character')) {
    /**
     * @param $text Content Parameter
     * @param $limit Limit in Words
     * @return string NewContent Returns
     */
    function limit_character($text, $limit)
    {
//        if (str_word_count($text, 0) > $limit) {
//            $words = str_word_count($text, 2);
//            $pos = array_keys($words);
//            $text = substr($text, 0, $pos[$limit]) . '...';
//        }
//        return strip_tags($text);

        $string = strip_tags($text);
        if (strlen($string) > $limit) {
            $stringCut = substr($string, 0, $limit);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }
        return $string;
    }
}

if (!function_exists('image_ids')) {
    /**
     * @param $post
     * @return string
     */
    function image_ids($post, $p = false, $onlyimage = false)
    {
        if ($p == true) {
            if (!empty($post->product_attributes)) {
                $ids = [];
                foreach ($post->product_attributes as $attribute) {
                    if (!empty($attribute) && $attribute->attribute === 'image') {
                        if ($attribute['module_type'] === 'products' && $onlyimage == true) {
                            $ids[] = !empty($attribute['values']) ? $attribute['values'] : null;
                        }
                    }
                }
                return implode(', ', $ids);
            } else {
                return 0;
            }
        } else {
            if (!empty($post->attributes)) {
                $ids = [];
                foreach ($post->attributes as $attribute) {
                    if (!empty($attribute)) {
                        if ($attribute['module_type'] === 'posts') {
                            $ids[] = !empty($attribute['values']) ? $attribute['values'] : null;
                        }
                    }
                }
                return implode(', ', $ids);
            } else {
                return 0;
            }
        }
    }
}

if (!function_exists('category_ids')) {
    /**
     * @param $post
     * @return string
     */
    function category_ids($post)
    {
        if (!empty($post->product_attributes)) {
            $ids = [];
            foreach ($post->product_attributes as $attribute) {
                if (!empty($attribute) && $attribute->attribute === 'category_id') {
                    if ($attribute['module_type'] === 'products') {
                        $ids[] = !empty($attribute['values']) ? $attribute['values'] : null;
                    }
                }
            }
            return $ids;
        } else {
            return 0;
        }
    }
}

if (!function_exists('product_attributes')) {
    function product_attributes($post, $p = false)
    {
        if ($p == true) {
            if (!empty($post->product_attributes)) {
                $html['photos'] = null;
                $html['id'] = null;

                $html['photos'] = '<div class="timeline-item" style="border: 1px solid #EEE; padding: 0 5px;"><h4 class="timeline-header">Uploaded photos</h4><div class="timeline-body">';

                foreach ($post->product_attributes as $image) {
                    if ($image->module_type === 'products' && $image->attribute !== 'product_information' && $image->attribute === 'image') {
                        $img = App\Image::find($image->values);
                        //dump($img);
                        $html['photos'] .= '<img src="' . url($img->icon_size_directory) . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                        //$html .= '<span>' . $img->id . '</span>';
                        $html['photos'] .= '<a href="' . url('delete_attribute', ['id' => $image->id]) . '">x</a>';
                    }
                    $html['id'] .= $image->id;
                }
                $html['photos'] .= '</div></div>';
                return $html;
            }
        } else {
            if (!empty($post->product_attributes)) {
                $html['values'] = null;
                $html['id'] = null;

                foreach ($post->product_attributes as $variation) {
                    if ($variation->module_type === 'products' && $variation->attribute === 'product_information') {
                        $html['values'] = $variation->values;
                        $html['id'] = $variation->id;
                    }
                }

                return $html;
            }
        }
    }
}

if (!function_exists('images_by_ids')) {
    function images_by_ids($values, array $options = [])
    {
        $default = [
            'link' => false,
            'size' => 'icons'
        ];
        $options = array_merge($default, $options);

        if (!empty($values)) {
            if (strpos($values, ',') !== false) {
                $image_ids = explode(',', $values);

                foreach ($image_ids as $img_id) {
                    $img = App\Image::find($img_id);

                    if ($options['link'] == false) {
                        $del_link = '<a href="' . url('delete_attribute', ['id' => $img->id]) . '">x</a>';
                    } else {
                        $del_link = false;
                    }

                    if ($options['size'] === 'full') {
                        $imgdir = url($img->full_size_directory);
                    } elseif ($options['size'] === 'icons') {
                        $imgdir = url($img->icon_size_directory);
                    } else {
                        $imgdir = url($img->icon_size_directory);
                    }

                    $html[] = '<img src="' . $imgdir . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                    //$html .= '<span>' . $img->id . '</span>';
                    $html[] .= $del_link;
                }
                return $html;
            } else {
                $img = App\Image::find($values);

                if ($options['link'] == false) {
                    $del_link = '<a href="' . url('delete_attribute', ['id' => $img->id]) . '">x</a>';
                } else {
                    $del_link = false;
                }

                if ($options['size'] === 'full') {
                    $imgdir = url($img->full_size_directory);
                } elseif ($options['size'] === 'icons') {
                    $imgdir = url($img->icon_size_directory);
                } else {
                    $imgdir = url($img->icon_size_directory);
                }

                $html = '<img src="' . $imgdir . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                //$html .= '<span>' . $img->id . '</span>';
                $html .= $del_link;

                return $html;
            }
        }
    }
}

if (!function_exists('uploaded_photos')) {
    function uploaded_photos($post, $p = false)
    {
        if ($p == true) {
            if (!empty($post->product_attributes)) {
                $html = '<div class="timeline-item" style="border: 1px solid #EEE; padding: 0 5px;"><h4 class="timeline-header">Uploaded photos</h4><div class="timeline-body">';

                foreach ($post->product_attributes as $image) {
                    if ($image->module_type === 'products' && $image->attribute !== 'product_information') {
                        $img = App\Image::find($image->values);
                        $html .= '<img src="' . url($img->icon_size_directory) . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                        //$html .= '<span>' . $img->id . '</span>';
                        $html .= '<a href="' . url('delete_attribute', ['id' => $image->id]) . '">x</a>';
                    }
                }
                $html .= '</div></div>';

                return $html;
            }
        } else {
            if (!empty($post->attributes)) {
                $html = '<div class="timeline-item" style="border: 1px solid #EEE; padding: 0 5px;"><h4 class="timeline-header">Uploaded photos</h4><div class="timeline-body">';
                foreach ($post->attributes as $image) {
                    if ($image->module_type === 'posts' && $image->attribute !== 'product_information') {
                        $img = App\Image::find($image->values);
                        $html .= '<img src="' . url($img->icon_size_directory) . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                        //$html .= '<span>' . $img->id . '</span>';
                        $html .= '<a href="' . url('delete_attribute', ['id' => $image->id]) . '">x</a>';
                    } elseif ($image->module_type === 'pages' && $image->attribute !== 'product_information') {
                        $img = App\Image::find($image->values);
                        $html .= '<img src="' . url($img->icon_size_directory) . '" alt="' . $img->original_name . '" class="margin" style="max-width: 80px; max-height: 80px; border: 1px dotted #ddd;">';
                        //$html .= '<span>' . $img->id . '</span>';
                        $html .= '<a href="' . url('delete_attribute', ['id' => $image->id]) . '">x</a>';
                    }
                }
                $html .= '</div></div>';

                return $html;
            }
        }
    }
}
if (!function_exists('get_product_categories')) {
    function get_product_categories()
    {
        $default = [
            'type' => 'category',
            'limit' => 500,
            'offset' => 0
        ];
        $cats = App\Term::where('type', ($default['type'] === 'category') ? 'category' : 'others')->take(!empty($default['limit']) ? $default['limit'] : 20)->get();
        $categories = $cats->toArray();

        return $categories;
    }
}

/**
 *
 */
if (!function_exists('get_userrole_by_user_id')) {
    function get_userrole_by_user_id($userid)
    {
        $role_id = \App\Role_user::where('user_id', $userid)->get()->first();
        return $role_id;
    }
}
/**
 *
 */
if (!function_exists('message_handler')) {
    function message_handler()
    {
        $html = null;
    }
}

if (!function_exists('sendSMS')) {
    function sendSMS($receiver, $message)
    {
        
         $receivers = explode(',',$receiver);

        $rand = random_code();
        $smscontent = urlencode($message);
        $user = 'rflus';
        $pass ='18d=5P45';
        $sid = 'RfLusEng';
        $url="http://sms.sslwireless.com/pushapi/dynamic/server.php";

        $param = "user=$user&pass=$pass";
        $i = 0;
        foreach ($receivers as $receiver){

            $param .=    "&sms[".$i."][0]= ".$receiver." &sms[".$i."][1]=". $smscontent."&sms[".$i."][2]=".$rand;
            ++$i;
        }
        $param .=   "&sid=$sid";
        //dd($param);

        $crl = curl_init();
        curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($crl,CURLOPT_URL,$url);
        curl_setopt($crl,CURLOPT_HEADER,0);
        curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($crl,CURLOPT_POST,1);
        curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
        $response = curl_exec($crl);
        curl_close($crl);
        
        // $receiver = explode(',',$receiver);
        // $receiver = $receiver[0];

        // $rand = random_code();
        // $smscontent = urlencode($message);
        // $user = 'rflus';
        // $pass = '18d=5P45';
        // $sid = 'RfLusEng';
        // $curl = curl_init();
        // curl_setopt_array(
        //     $curl,
        //     [
        //         CURLOPT_RETURNTRANSFER => 1,
        //         //http://sms.sslwireless.com/pushapi/dynamic/server.php?user=rflus&pass=18d=5P45&msisdn=8801680139540&csmsid=0239482309&sms=test&sid=RfLusEng
        //         CURLOPT_URL => 'http://sms.sslwireless.com/pushapi/dynamic/server.php?user=' . $user . '&pass=' . $pass . '&sid=' . $sid . '&sms=' . $smscontent . '&msisdn=88' . $receiver . '&csmsid=' . $rand,
        //         CURLOPT_USERAGENT => 'Sample cURL Request'
        //     ]
        // );
        // $resp = curl_exec($curl);
        // curl_close($curl);

    }
}

if (!function_exists('adminSMSConfig')) {
    function adminSMSConfig($paymentsetting)
    {
        $admin = [];

        if (!empty($paymentsetting->admin_cell_one)) {
            $admin[] = $paymentsetting->admin_cell_one;
        }

        if (!empty($paymentsetting->admin_cell_two)) {
            $admin[] = $paymentsetting->admin_cell_two;
        }

        if (!empty($paymentsetting->admin_cell_three)) {
            $admin[] = $paymentsetting->admin_cell_three;
        }

        if (!empty($paymentsetting->admin_cell_four)) {
            $admin[] = $paymentsetting->admin_cell_four;
        }

        if (!empty($paymentsetting->admin_cell_five)) {
            $admin[] = $paymentsetting->admin_cell_five;
        }

        return $admin;
    }
}
if (!function_exists('get_delivery_fee')) {
    function get_delivery_fee($type = null)
    {
        $data = App\PaymentSetting::find(1)->first();
        if ($type == true) {
            return $data->inside_dhaka_fee;
        } else {
            return $data->outside_dhaka_fee;
        }
    }
}

if (!function_exists('convert_youtube')) {
    function convert_youtube($string)
    {
        return preg_replace(
            "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            '<iframe src="//www.youtube.com/embed/$2" allowfullscreen></iframe>',
            $string
        );
    }
}

if (!function_exists('get_thana')) {
    function get_thana()
    {
        $thanas = DB::table('districts')->distinct()->select('thana')->groupBy('thana')->get();
        return $thanas;
    }
}
if (!function_exists('get_shipment')) {
    function get_shipment()
    {
        $shipment = DB::table('shipments')->get();
        return $shipment;
    }
}
if (!function_exists('get_dis_or_div_by_thana')) {
    function get_dis_or_div_by_thana($thana)
    {
        $thana = DB::table('districts')->distinct()->select('*')->where('thana', $thana)->get()->first();
        return $thana;
    }
}
if (!function_exists('get_districts')) {
    function get_districts()
    {
        $districts = DB::table('districts')->distinct()->groupBy('district')->select('*')->where('is_active', 1)->get();
        return $districts;
    }
}

if (!function_exists('get_thanas_by_district')) {
    function get_thanas_by_district($district)
    {
        $thanas = DB::table('districts')->distinct()->groupBy('thana')->select('*')->where('district', $district)->get();
        return $thanas;
    }
}

if (!function_exists('arr_delete')) {
    function arr_delete($main, $del)
    {
        foreach ($del as $key => $value) {
            unset($main[$key]);
        }
        return $main;
    }
}

if (!function_exists('get_dynamic_category')) {
    function get_dynamic_category($cat_id, $cols)
    {
        $thml = '';
        $diy_cat_parent = App\Term::Where(['parent' => 1])->orderBy('position', 'ASC')->get();
        //dd($diy_cat_parent);

        $diy_cat_sub = App\Term::Where(['parent' => $cat_id])->orderBy('position', 'ASC')->get();
        //dump($diy_cat_sub->count());

        if ($diy_cat_sub->count() != 0) {
            //dump($diy_cat_parent);
            if (!empty($cols)) {
                $cc = 'style="column-count: ' . $cols . '"';
            } else {
                $cc = 'style="column-count: 3;"';
            }
            $thml .= '<ul class="cd_sub_cat menu-generator-space" ' . $cc . '>';
            foreach ($diy_cat_sub as $dc_sub) {
                $thml .= '<li><h6><a href="/' . $dc_sub->seo_url. '"> ' . $dc_sub->name . '</a></h6>';

                $diy_cat_chil = App\Term::Where(['parent' => $dc_sub->id])->orderBy('position', 'ASC')->get();
                if ($diy_cat_chil->count() != 0) {
                    $thml .= '<ul class="menu-generator-space">';

                    foreach ($diy_cat_chil as $dc_chil) {
                        $thml .= '<li><a href="/' .$dc_chil->seo_url . '"> ' . $dc_chil->name . '</a></li>';
                    }

                    $thml .= '</ul>';
                }

                $thml .= '</li>';
            }
            $thml .= '</ul>';
        }

        echo $thml;
    }
}

if (!function_exists('random_code')) {
    function random_code()
    {
        $a = '';
        for ($i = 0; $i < 8; $i++) {
            $a .= mt_rand(0, 9);
        }
        return $a;
    }
}

if (!function_exists('get_user_role')) {
    function get_user_role($id)
    {
        $role = DB::table('role_user as ru')
            ->join('roles as r', 'r.id', '=', 'ru.role_id')
            ->select('*', 'r.id as r_id', 'ru.id as ru_id')->where(['ru.user_id' => $id])->first();
        return $role;
    }
}

if (!function_exists('role_receiver')) {
    function role_employee()
    {
        // 1 = Admin , 2 = manager, 3 = editor, 4 = product_manag, 9 = employee
        $data = [1, 9];
        return $data;
    }
}

if (!function_exists('is_employee')) {
    function is_employee()
    {
        $is_login = auth()->user();
        if (auth()->check()) {
            $role = get_user_role($is_login->id);
            if ($role) {
                if (in_array($role->r_id, role_employee())) {
                    $data = true;
                } else {
                    $data = false;
                }
            } else {
                $data = false;
            }
        } else {
            $data = false;
        }
        return $data;
    }
}


if (!function_exists('writeLog')) {
    function writeLog($logName, $logData)
    {
        file_put_contents('./log-' . $logName . date("j.n.Y") . '.log', $logData, FILE_APPEND);
    }
}

if (!function_exists('get_content')) {
    function get_content($URL)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $URL);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
if (!function_exists('getStringToSign')) {
    function getStringToSign($message)
    {
        $signableKeys = [
            'Message',
            'MessageId',
            'Subject',
            'SubscribeURL',
            'Timestamp',
            'Token',
            'TopicArn',
            'Type'
        ];

        $stringToSign = '';

        if ($message['SignatureVersion'] !== '1') {
            $errorLog = "The SignatureVersion \"{$message['SignatureVersion']}\" is not supported.";
            writeLog('SignatureVersion-Error', $errorLog);
        } else {
            foreach ($signableKeys as $key) {
                if (isset($message[$key])) {
                    $stringToSign .= "{$key}\n{$message[$key]}\n";
                }
            }
            writeLog('StringToSign', $stringToSign . "\n");
        }
        return $stringToSign;
    }
}
if (!function_exists('validateUrl')) {
    function validateUrl($url)
    {
        $defaultHostPattern = '/^sns\.[a-zA-Z0-9\-]{3,}\.amazonaws\.com(\.cn)?$/';
        $parsed = parse_url($url);

        if (empty($parsed['scheme']) || empty($parsed['host']) || $parsed['scheme'] !== 'https' || substr($url, -4) !== '.pem' || !preg_match($defaultHostPattern, $parsed['host'])) {
            return false;
        } else {
            return true;
        }
    }
}

if (!function_exists('bkash_Get_Token')) {
    function bkash_Get_Token(){
        
        // Test Bkash API
        // $post_token=array(
	    // 'app_key'=>'5tunt4masn6pv2hnvte1sb5n3j',
		// 'app_secret'=>'1vggbqd4hqk9g96o9rrrp2jftvek578v7d2bnerim12a87dbrrka'
	    // );
	    
	   // $url=curl_init('https://checkout.sandbox.bka.sh/v1.2.0-beta/checkout/token/grant');
    //     $posttoken=json_encode($post_token);
    //     $header=array(
		  //      'Content-Type:application/json',
				// 'password:hWD@8vtzw0',
				// 'username:sandboxTestUser');
    //     curl_setopt($url,CURLOPT_HTTPHEADER, $header);
    //     curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
    //     curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($url,CURLOPT_POSTFIELDS, $posttoken);
    //     curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);

    //     $resultdata=curl_exec($url);
    //     curl_close($url);

    //     return json_decode($resultdata, true);
	    
	    // Formal Bkash API
	    
	    $post_token=array(
	       'app_key'=>'5nej5keguopj928ekcj3dne8p',
		   'app_secret'=>'1honf6u1c56mqcivtc9ffl960slp4v2756jle5925nbooa46ch62'
	    );
	    
	    
       $url=curl_init('https://checkout.sandbox.bka.sh/v1.2.0-beta/checkout/token/grant');
        $posttoken=json_encode($post_token);
        $header=array(
		        'Content-Type:application/json',
				'password:test%#de23@msdao',
				'username:testdemo');
        curl_setopt($url,CURLOPT_HTTPHEADER, $header);
        curl_setopt($url,CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($url,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($url,CURLOPT_POSTFIELDS, $posttoken);
        curl_setopt($url,CURLOPT_FOLLOWLOCATION, 1);

        $resultdata=curl_exec($url);
        curl_close($url);

        return json_decode($resultdata, true);
    }
}

if (!function_exists('vendor_shop_id')) {
    function vendor_shop_id(){

        $data = [
            '1' => 'RFLBestbuy',
            '436' => 'RFLBestbuy',
            '281' => 'VisionEmporium',
            '437' => 'RegalEmporium',
            '438' => 'DurantaBikeGallery',
            '439' => 'EasyBuild',
        ];

       return $data;
    }
}




if(!function_exists('sendSmsFormatting')){
    function sendSmsFormatting($phone,$order,$customer,$message){

        //$message = $message.$order_id.$customer;

        $replace_array  = [
            '#customer_name#' => $customer??'',
            '#order_id#' => $order??''
        ];

        $message = strtr(strip_tags($message),$replace_array);
        if(is_array($phone)){
            foreach($phone as $number){
                sendSMSBySSL($number,$message);
            }
        }else{
            return sendSMSBySSL($phone,$message);
        }

    }
}


if (!function_exists('sendSMSBySSL')) {
    function sendSMSBySSL($receiver, $message)
    {
        if(!$receiver || !$message){
            return;
        }

        $rand = random_code();
        $smscontent = urlencode($message);
        $user = 'rflus';
        $pass = '97<8W63y';
        $sid = 'RflusEng';

        //Old
        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://sms.sslwireless.com/pushapi/dynamic/server.php?user=' . $user . '&pass=' . $pass . '&sid=' . $sid . '&sms=' . $smscontent . '&msisdn=88' . $receiver . '&csmsid=' . $rand,
                CURLOPT_USERAGENT => 'Sample cURL Request'
            )
        );
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
        //echo $resp;

        /** SSL */
    }


    if (!function_exists('exclude_unwanter_permission_link')) {
        function exclude_unwanter_permission_link($value)
        {
            $exclude_url = [
                '_debugbar',
                'oauth',
                'harimayco',
                'broadcasting',
                'route_list',
                'blank',
                'logout',
                'signout',
                'loginEntry'
            ];

            foreach($exclude_url as $eu) {
                if(strpos($value, $eu) !== false) {                    
                    return true;
                }
            }
     
            return false;
        }
    }
}


if (!function_exists('get_vendor_url_set')) {
    function get_vendor_url_set($userid)
    {
        $vendor_set = [
            'useraccesspermission_' . $userid . '_d9a3e27592d7567b91ad890fc7b3c9b2' => 'admin/modify_password_view/{id}',
            'useraccesspermission_' . $userid . '_462b0c66d483ec5251fe733f0118e41b' => 'admin/modify_password/{id}',
            'useraccesspermission_' . $userid . '_a1052e22191baf7b89e9dbb3374e6ab1' => 'admin/vendor_url_existing',
            'useraccesspermission_' . $userid . '_69515b93b61ff8471540323025cfc2b1' => 'admin/modify_role_view/{id}',
            'useraccesspermission_' . $userid . '_64164b42c7c41b124bb649057487e397' => 'admin/modify_role',
            'useraccesspermission_' . $userid . '_ccd3b60a27756c461aec5ab0665cd4dc' => 'products_express_delivery',
            'useraccesspermission_' . $userid . '_700b117ece72198bf9a2f59875fb43f3' => 'products_enable_comment',
            'useraccesspermission_' . $userid . '_0fec2b6a5cf3bc73c1c99e58782a2e09' => 'vendor.dashboard.index',
            'useraccesspermission_' . $userid . '_7a4a5b3a189122d1983689c190ed088c' => 'vendor.setting.index',
            'useraccesspermission_' . $userid . '_f80c9ed288235cd066ab3beed3a53443' => 'vendor.setting.vendor',
            'useraccesspermission_' . $userid . '_6ff1173aabc20ef4e1b4d6f495d571d8' => 'vendor.site.content',
            'useraccesspermission_' . $userid . '_93809c33af7f3f829288004208841e7d' => 'vendor.site.saveSlider',
            'useraccesspermission_' . $userid . '_6fba9ace2123e90fec4169b94d613a82' => 'vendor.site.deleteSlider',
            'useraccesspermission_' . $userid . '_cbab40a986f5a9371e6f0f7169c3e765' => 'vendor.media.index',
            'useraccesspermission_' . $userid . '_4733362629c9378ced1375726fe44e0e' => 'vendor.site.saveChoicesProducts',
            'useraccesspermission_' . $userid . '_9b3b5a115de7b5be48b9385617258740' => 'vendor.order.index',
            'useraccesspermission_' . $userid . '_5ecce48347a056b17dae28dcb9cbabef' => 'vendor.product.index',
            'useraccesspermission_' . $userid . '_c408d9288c1ff7968e9b77cd1c63b084' => 'vendor.product.create',
            'useraccesspermission_' . $userid . '_97f0e63411a6becc868ac90c0ce27cb7' => 'vendor.product.edit',
            'useraccesspermission_' . $userid . '_f1d3a617efc231aa068df0816bc73bb1' => 'vendor.product.store',
            'useraccesspermission_' . $userid . '_77faa3c885c8b0485716f745d20ecef6' => 'vendor.product.update.basic',
            'useraccesspermission_' . $userid . '_a0a6996be88537b69c59f2fe038321d5' => 'admin.medias.index',
            'useraccesspermission_' . $userid . '_2dddd499efae5db416c5c5267ba7c6c0' => 'admin.medias.create',
            'useraccesspermission_' . $userid . '_7280858d998e585002c24b1a5155a77c' => 'admin.medias.store',
            'useraccesspermission_' . $userid . '_cba89a90e046ec3dc6994391cc14a99a' => 'admin.medias.show',
            'useraccesspermission_' . $userid . '_ad7e82b913b843290a68d5ad96f7271f' => 'admin.medias.edit',
            'useraccesspermission_' . $userid . '_693e655ad5a892a78094eff40b08045c' => 'admin.medias.update',
            'useraccesspermission_' . $userid . '_b0b4a18346418ca9aed58c6c7347e4ad' => 'admin.medias.destroy',
            'useraccesspermission_' . $userid . '_d92fa6da3bb9f510a2281519766ab814' => 'admin.upload.index',
            'useraccesspermission_' . $userid . '_12998503f132e7ef58562f6e203db986' => 'admin.upload.create',
            'useraccesspermission_' . $userid . '_3f33abc4a528034341e82f6251491f53' => 'admin.upload.store',
            'useraccesspermission_' . $userid . '_97cabf1c445d68541eea86dd3dff9a53' => 'admin.upload.show',
            'useraccesspermission_' . $userid . '_636f4d958bafb36fe0bd2c8c737871cc' => 'admin.upload.edit',
            'useraccesspermission_' . $userid . '_5acb8f97fb60dcfa44c7ba68b68ac005' => 'admin.upload.update',
            'useraccesspermission_' . $userid . '_23b4831ebb47cd2d807e6b369235cea9' => 'admin.upload.destroy',
            'useraccesspermission_' . $userid . '_cf53d19a8da9cb986a5cac73d1edde2f' => 'admin.flash_management.flash_schedule_status',
            'useraccesspermission_' . $userid . '_d9fd38ffb0fe0b9c527203bf66634940' => 'admin.flash_management.add_schedule_products',
            'useraccesspermission_' . $userid . '_c6258cd921989ebbb0fb2dfc53ce4a83' => 'admin.flash_management.flash_item_save',
            'useraccesspermission_' . $userid . '_d18c2d317ca4850751c97c6cd78c886f' => 'admin.flash_management.store_flash_items',
            'useraccesspermission_' . $userid . '_7a4ccf7e175472c739968241abb2efd6' => 'admin.flash_management.delete_flash_item',
            'useraccesspermission_' . $userid . '_3c2071bab3abe3d20d5b2e30888c3f2f' => 'admin.flash_management.index',
            'useraccesspermission_' . $userid . '_50ffb6a34a50e37ce5bca28078825199' => 'admin.flash_management.store',
            'useraccesspermission_' . $userid . '_d1e96fa4b2440fdf5797057e3a0e3d8d' => 'admin.flash_management.show',
            'useraccesspermission_' . $userid . '_8f844d07b8137b509cd5323fe1986736' => 'admin.flash_management.edit',
            'useraccesspermission_' . $userid . '_a88fbe761cb9ad3b3c138de7c9664eec' => 'admin.flash_management.update'
        ];

        return $vendor_set;
    }
}


if (!function_exists('get_vendor_url_set')) {
    function get_vendor_url_set($userid)
    {
        $vendor_set = [
            'useraccesspermission_' . $userid . '_d9a3e27592d7567b91ad890fc7b3c9b2' => 'admin/modify_password_view/{id}',
            'useraccesspermission_' . $userid . '_462b0c66d483ec5251fe733f0118e41b' => 'admin/modify_password/{id}',
            'useraccesspermission_' . $userid . '_a1052e22191baf7b89e9dbb3374e6ab1' => 'admin/vendor_url_existing',
            'useraccesspermission_' . $userid . '_69515b93b61ff8471540323025cfc2b1' => 'admin/modify_role_view/{id}',
            'useraccesspermission_' . $userid . '_64164b42c7c41b124bb649057487e397' => 'admin/modify_role',
            'useraccesspermission_' . $userid . '_ccd3b60a27756c461aec5ab0665cd4dc' => 'products_express_delivery',
            'useraccesspermission_' . $userid . '_700b117ece72198bf9a2f59875fb43f3' => 'products_enable_comment',
            'useraccesspermission_' . $userid . '_0fec2b6a5cf3bc73c1c99e58782a2e09' => 'vendor.dashboard.index',
            'useraccesspermission_' . $userid . '_7a4a5b3a189122d1983689c190ed088c' => 'vendor.setting.index',
            'useraccesspermission_' . $userid . '_f80c9ed288235cd066ab3beed3a53443' => 'vendor.setting.vendor',
            'useraccesspermission_' . $userid . '_6ff1173aabc20ef4e1b4d6f495d571d8' => 'vendor.site.content',
            'useraccesspermission_' . $userid . '_93809c33af7f3f829288004208841e7d' => 'vendor.site.saveSlider',
            'useraccesspermission_' . $userid . '_6fba9ace2123e90fec4169b94d613a82' => 'vendor.site.deleteSlider',
            'useraccesspermission_' . $userid . '_cbab40a986f5a9371e6f0f7169c3e765' => 'vendor.media.index',
            'useraccesspermission_' . $userid . '_4733362629c9378ced1375726fe44e0e' => 'vendor.site.saveChoicesProducts',
            'useraccesspermission_' . $userid . '_9b3b5a115de7b5be48b9385617258740' => 'vendor.order.index',
            'useraccesspermission_' . $userid . '_5ecce48347a056b17dae28dcb9cbabef' => 'vendor.product.index',
            'useraccesspermission_' . $userid . '_c408d9288c1ff7968e9b77cd1c63b084' => 'vendor.product.create',
            'useraccesspermission_' . $userid . '_97f0e63411a6becc868ac90c0ce27cb7' => 'vendor.product.edit',
            'useraccesspermission_' . $userid . '_f1d3a617efc231aa068df0816bc73bb1' => 'vendor.product.store',
            'useraccesspermission_' . $userid . '_77faa3c885c8b0485716f745d20ecef6' => 'vendor.product.update.basic',
            'useraccesspermission_' . $userid . '_a0a6996be88537b69c59f2fe038321d5' => 'admin.medias.index',
            'useraccesspermission_' . $userid . '_2dddd499efae5db416c5c5267ba7c6c0' => 'admin.medias.create',
            'useraccesspermission_' . $userid . '_7280858d998e585002c24b1a5155a77c' => 'admin.medias.store',
            'useraccesspermission_' . $userid . '_cba89a90e046ec3dc6994391cc14a99a' => 'admin.medias.show',
            'useraccesspermission_' . $userid . '_ad7e82b913b843290a68d5ad96f7271f' => 'admin.medias.edit',
            'useraccesspermission_' . $userid . '_693e655ad5a892a78094eff40b08045c' => 'admin.medias.update',
            'useraccesspermission_' . $userid . '_b0b4a18346418ca9aed58c6c7347e4ad' => 'admin.medias.destroy',
            'useraccesspermission_' . $userid . '_d92fa6da3bb9f510a2281519766ab814' => 'admin.upload.index',
            'useraccesspermission_' . $userid . '_12998503f132e7ef58562f6e203db986' => 'admin.upload.create',
            'useraccesspermission_' . $userid . '_3f33abc4a528034341e82f6251491f53' => 'admin.upload.store',
            'useraccesspermission_' . $userid . '_97cabf1c445d68541eea86dd3dff9a53' => 'admin.upload.show',
            'useraccesspermission_' . $userid . '_636f4d958bafb36fe0bd2c8c737871cc' => 'admin.upload.edit',
            'useraccesspermission_' . $userid . '_5acb8f97fb60dcfa44c7ba68b68ac005' => 'admin.upload.update',
            'useraccesspermission_' . $userid . '_23b4831ebb47cd2d807e6b369235cea9' => 'admin.upload.destroy',
            'useraccesspermission_' . $userid . '_cf53d19a8da9cb986a5cac73d1edde2f' => 'admin.flash_management.flash_schedule_status',
            'useraccesspermission_' . $userid . '_d9fd38ffb0fe0b9c527203bf66634940' => 'admin.flash_management.add_schedule_products',
            'useraccesspermission_' . $userid . '_c6258cd921989ebbb0fb2dfc53ce4a83' => 'admin.flash_management.flash_item_save',
            'useraccesspermission_' . $userid . '_d18c2d317ca4850751c97c6cd78c886f' => 'admin.flash_management.store_flash_items',
            'useraccesspermission_' . $userid . '_7a4ccf7e175472c739968241abb2efd6' => 'admin.flash_management.delete_flash_item',
            'useraccesspermission_' . $userid . '_3c2071bab3abe3d20d5b2e30888c3f2f' => 'admin.flash_management.index',
            'useraccesspermission_' . $userid . '_50ffb6a34a50e37ce5bca28078825199' => 'admin.flash_management.store',
            'useraccesspermission_' . $userid . '_d1e96fa4b2440fdf5797057e3a0e3d8d' => 'admin.flash_management.show',
            'useraccesspermission_' . $userid . '_8f844d07b8137b509cd5323fe1986736' => 'admin.flash_management.edit',
            'useraccesspermission_' . $userid . '_a88fbe761cb9ad3b3c138de7c9664eec' => 'admin.flash_management.update'
        ];

        return $vendor_set;
    }
}



