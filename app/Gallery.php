<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['media_id','category_id','serial','caption','active'];


    public function image()
    {
        return $this->hasOne(Image::class,'id','media_id');
    }
}
