<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingSessionRegistration extends Model
{
    protected $table = 'tsession_registration';
    protected $fillable = ['district' ,'upozilla' ,'institute_category' ,'institute_name' ,'teacher_name' ,'designation_name' ,'gender' ,'index_number' ,'email' ,'mobile'];
}
