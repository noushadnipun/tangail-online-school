<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{

    protected $table = 'widgets';
    protected $fillable = [
        'name', 'type', 'position', 'cssid', 'cssclass', 'description', 'special', 'is_active'
    ];

}
