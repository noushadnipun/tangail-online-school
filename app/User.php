<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'employee_no', 'username', 'thumb', 'seo_url', 'birthday', 'gender', 'marital_status', 'join_date', 'father', 'mother', 'company', 'school', 'address', 'thana', 'phone', 'teacher_type', 'subject_id', 'employee_title',
        'district', 'deliveryfee', 'vendor_banner', 'vendor_web', 'vendor_logo', 'vendor_title', 'vendor_description', 'vendor_dis_img', 'vendor_dis_img_title', 'postcode',
        'vendor_mark_as_authorized', 'trade_license_no', 'vendor_vat_certificate', 'vendor_etin_certificate', 'vendor_bank_solvency_certificate',
        'vendor_incorporation_certificate', 'vendor_article_of_memorandum', 'vendor_bank_account_details', 'vendor_nid', 'vendor_md_photo', 'vendor_md_signature',
        'is_active', 'vendor_status', 'provider', 'provider_id', 'login_token', 'mark_verifed', 'vendor_shop_type'
    ];

    protected $appends = ['role_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function role()
    {
        return $this->hasOne('App\Role');
    }

    public function role_user()
    {
        return $this->hasOne('App\Role_user');
    }


    public function isAdmin()
    {
        return $this->roles()->where('role_id', 1)->first();
    }

    public function isEditor()
    {
        return $this->roles()->where('role_id', 2)->first();
    }

    public function isStaff()
    {
        return $this->roles()->where('role_id', 3)->first();
    }

    public function isTaecher()
    {
        return $this->roles()->where('role_id', 4)->first();
    }

    public function isUser()
    {
        return $this->roles()->where('role_id', 5)->first();
    }

    public function wishlists()
    {
        return $this->hasMany(Wishlist::class, 'user_id')->orderByDesc('created_at')->with('product.image');
    }


    public function orderMaster()
    {
        return $this->hasMany(OrdersMaster::class, 'user_id', 'id')
            ->with('orderShipments.orderDetails')
            ->orderBy('created_at', 'desc');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrdersDetail::class, 'user_id', 'id');
    }

    public function showroomProfile()
    {
        return $this->hasOne(Showroom::class, 'user_profile_id', 'id');
    }


    public function getRoleIdAttribute()
    {
        $role = $this->roles()->first();
        return $role->id ?? 0;
    }


    public function vendorOrders()
    {

        return $this->hasManyThrough(
            OrdersDetail::class,
            Product::class,
            'vendor_id',
            'product_id',
            'id',
            'id'
        );
    }

    public function vendorProducts()
    {
        return $this->hasMany(Product::class, 'vendor_id', 'id');
    }

    public function vendorRatings()
    {
        return $this->hasManyThrough(
            Review::class,
            Product::class,
            'vendor_id',
            'product_id',
            'id',
            'id'
        );
    }

    public function uploadImages()
    {
        return $this->hasMany(Image::class, 'user_id', 'id');
    }

}
