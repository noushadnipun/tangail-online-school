<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSetting extends Model
{
    protected $table = 'homesettings';
    protected $fillable = [
        'cat_first', 'cat_second', 'cat_third', 'cat_fourth', 'cat_fifth', 'cat_sixth', 'cat_seventh', 'cat_eighth', 'home_category'
    ];
}
