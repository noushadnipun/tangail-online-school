<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ictlab extends Model
{
    protected $table = 'ictlab';
    protected $fillable = ['upozila', 'lab_type', 'institute_name', 'incharge_teacher', 'mobile', 'email', 'laptop', 'desktop', 'projector', 'smart_televison', 'internet'];
}
