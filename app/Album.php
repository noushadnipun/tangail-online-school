<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'albums';
    protected $fillable = [
        'name', 'position', 'cssid', 'cssclass', 'description', 'special', 'is_active'
    ];
}
