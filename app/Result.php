<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'results';
    protected $fillable = ['upozila', 'exam_name', 'year', 'total_candidates', 'total_pass', 'percentage_pass', 'a_plus', 'a', 'a_min', 'b', 'c', 'd', 'f'];
}
