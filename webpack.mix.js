const mix = require('laravel-mix');
require('./resources/js/app/mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.vueAutoRouting();


if (process.env.SSR === '1') {
    mix.js('resources/js/server.js', 'public/frontend/js')
        .webpackConfig(() => ({
            target: 'node',
            resolve: {
                extensions: ['.js', '.vue', '.json'],
                alias: {
                    '@': __dirname + '/resources/js'
                },
            },
        }))
}else {

    mix.js('resources/js/app.js', 'public/frontend/js')
        .sass('resources/css/sass/app.scss', 'public/frontend/css')
        .sass('resources/css/sass/responsivemobilemenu.scss', 'public/frontend/css')
        .stylus('resources/css/stylus/main.styl','public/frontend/css')
        .options({
            vue: {
                cssModules: {
                    localIdentName: '[name]-[local]-[hash:base64]',
                    camelCase: true
                }
            }
        }).version().extract();
}

//mix.browserSync('http://localhost/Laravel/samvue/');
